/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "reflex/data_types/BCD.h"

//template<>
void reflex::data_types::BCD<2>::encode(const uint8 &val)
{
	this->high = (val / 10);
	this->low = (val%10);
//	uint8 data = (val / 10)<<4;
//	data |= (val%10);
//	this->value = data;
}

uint8 reflex::data_types::BCD<2>::decode() const
{
	uint8 value=(10*this->high);
	value+= this->low;
//	uint8 value=(10*(this->value>>4)) + (this->value&0xF);
	return value;
}

void reflex::data_types::BCD<4>::encode(const uint16 &val)
{
	uint16 tmp = val;
	this->high.high = ((tmp/1000));
	tmp = tmp % 1000;
	this->high.low = ((tmp/100) );
	tmp= tmp% 100;
	this->low.high = ((tmp/10) );
	tmp=tmp%10;
	this->low.low = (tmp);
}

uint8 reflex::data_types::BCD<4>::decode() const
{
	uint16 value= (1000*this->high.high);
	value+= this->high.low*100;
	value+= this->low.high*10;
	value+= this->low.low;
	return value;
}
