/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 
 *	author = Soeren Hoeckner
*
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */

#include "reflex/data_types/FifoQueue.h"
#include "reflex/debug/Assert.h"

using namespace reflex::data_types;
using namespace reflex;

void FiFoQueue<ChainLink*>::enqueue(ChainLink* value) {
	//value may not be part of any list
	Assert(!value->isLinked());
	if(first){ //if queue is not empty
		last->next = value;
	}else{		//if queue is empty
		first = value;
	}
	last = value;
	value->next = 0;
}
