#include "reflex/componentUpdate/ComponentManager.h"

using namespace reflex;

extern ComponentManager::Header __beginof_componentheaders__[];
extern ComponentManager::Header __endof_componentheaders__[];

ComponentManager::Header* const ComponentManager::BEGINOF_COMPONENTHEADERS =
  (ComponentManager::Header* const ) __beginof_componentheaders__;
ComponentManager::Header* const ComponentManager::ENDOF_COMPONENTHEADERS =
  (ComponentManager::Header* const ) __endof_componentheaders__;

ComponentManager::ComponentManager(MemoryManager* memoryManager, Pool* pool) :
  command_handler(*this)
  , packet_handler(*this)
  , command_input(&command_handler)
  , packet_input(&packet_handler)
{
  this->memoryManager = memoryManager;
  this->pool = pool;
  component = 0;
  state = IDLE;
  command_input.assign(STARTUP_ALL, 0);
}

void ComponentManager::handleCommand()
{
  Command command;
  uint16 argument;
  command_input.get(command, argument);
  switch (command)
    {
    case RECEIVE:
      state = RECEIVING_HEADER;
      acknowledgement_request = (uint8) argument;
      break;
    case INSTALL:
      state = installComponent();
      break;
    case UNINSTALL:
      state = uninstallComponent(getComponentById(argument));
      break;
    case STARTUP:
      state = startupComponent(getComponentById(argument));
      break;
    case STARTUP_ALL:
      state = startupAllComponents();
      break;
    case SHUTDOWN:
      state = shutdownComponent(getComponentById(argument));
      break;
    case LIST:
      state = listComponents();
      break;
    default:
      respond(COMMAND_INVALID);
      break;
    }

}

void ComponentManager::handlePacket()
{
  buffer = packet_input.get();
  Command command;
  switch (state)
    {
    case IDLE:
      buffer->read(command);
      uint16 argument;
      argument = 0;
      buffer->read(argument);
      command_input.assign(command, argument);
      break;
    case RECEIVING_HEADER:
      buffer->read(command);
      if (command == RECEIVE_HEADER) state = receiveHeader();
      else if (command == ABORT_INSTALL) state = abortInstallation();
      //not go to ERROR state since maybe the other componnet pakets will received later and this paket is only a disturbance
      else respond(COMMAND_INVALID_RECEIVE); 
      break;
    case RECEIVING_COMPONENT:
      buffer->read(command);
      if (command == RECEIVE_DATA) state = receiveComponent();
      else if (command == ABORT_INSTALL) state = abortInstallation();
      //not go to ERROR state since maybe the other componnet pakets will received later and this paket is only a disturbance
      else respond(COMMAND_INVALID_RECEIVE); 
      break;
    case ERROR:
      buffer->read(command);
      if (command == ABORT_INSTALL) state = abortInstallation();
      else respond(ERROR_STATE);
      break;
    default:
      respond(PACKET_INVALID);
      break;
    }
  buffer->downRef(); // if we notified somebody else about the buffer, then we performed an additional upref() during execution
}

ComponentManager::State ComponentManager::listComponents()
{
  // count installed components
  uint8 count = getROMcomponentNumber() + getRAMcomponentNumber();

  // send component count
  Buffer* buffer = new (pool) Buffer(pool);
  buffer->write(count);
  buffer->write(BEGINOF_COMPONENTHEADERS);
  buffer->write(ENDOF_COMPONENTHEADERS);
  respond(COMPONENT_LIST, buffer);


  // send rom componnet headers...
  for (Header* slot = BEGINOF_COMPONENTHEADERS; slot < ENDOF_COMPONENTHEADERS; slot++)
    {
      if (slot->id != 0xFFFF)
	{
	  buffer = new (pool) Buffer(pool);
	  buffer->write(slot, sizeof(Header));

	  // ...and if they running
	  uint8 runningFlag = 0;
	  for (RuntimeInfo* info = runtimeInfo_list.first(); info != 0; info = info->next)
	    {
	      if (info->header->id == slot->id)
		{
		  runningFlag = 1;
		}
	    }
	  buffer->write(runningFlag);

	  this->outgoingPacket_receiver->assign(buffer);

	}
    }
	
  // do the same thing for ram components...
  for (RamComponent* ramC = RamComponent_list.first(); ramC != 0; ramC = ramC->next)
    {
      buffer = new (pool) Buffer(pool);
      buffer->write(&(ramC->header), sizeof(Header));
		
      // ...and if they running
      uint8 runningFlag = 0;
      for (RuntimeInfo* info = runtimeInfo_list.first(); info != 0; info = info->next)
	{
	  if (info->header->id == ramC->header.id)
	    {
	      runningFlag = 1;
	    }
	}
      buffer->write(runningFlag);
      this->outgoingPacket_receiver->assign(buffer);
    }
  return IDLE;
}

ComponentManager::State ComponentManager::receiveHeader()
{

  component = new (*memoryManager) Header();
  if (component == 0)
    {
      respond(NO_SPACE_FOR_HEADER);
      return ERROR;
    }


  buffer->read(component, sizeof(Header)); // buffer has a better read-function, but it did not work for some fuckin' reason

  //check if component is already installed
  Header* temp = new (*memoryManager) Header(); 
  temp = getComponentById(component->id);
  if (temp != 0)
    {
      respond(ALREADY_INSTALLED);
      return IDLE;
    }

  // alloc space for component
  caddr_t addr = MemoryManager::alloc(component->size, component->file);
  if (addr == component->file)
    {
      receive_pos = component->file;
      return RECEIVING_COMPONENT;
    }
  else
    {
      respond(NO_SPACE_FOR_COMPONENT);
      return ERROR;
    }
}

/**
 * Delivers a component arriving in packets directly into memory .
 * When finished receiving, component is installed
 */
ComponentManager::State ComponentManager::receiveComponent()
{
  caddr_t currentAdress;
  uint16 temp;
  buffer->read(temp);
  currentAdress = (caddr_t) temp;
  
  //check if component part was allready received
   if (MemoryManager::isEqual(buffer->getStart(), currentAdress, buffer->getLength()))
     {
       if (acknowledgement_request == 1) respond(DATA_ALREADY_RECEIVED);
       return RECEIVING_COMPONENT;
     }

  // write component part do destination
  MemoryManager::memcopy(buffer->getStart(), currentAdress, buffer->getLength());
  bytes_received += buffer->getLength();

  // schedule installation if recieved completely 
  if (bytes_received >= component->size)
    {
      bytes_received = 0;
      command_input.assign(INSTALL, 0);
    }
  if (acknowledgement_request == 1) respond(DATA_RECEIVED);
  return RECEIVING_COMPONENT;
}


/**
 * stops component installation, cleans up and return to IDLE-state
 * can only be reached from RECEIVING_COMPONENT-state
 */
ComponentManager::State ComponentManager::abortInstallation()
{
  
  if ((component->info & PERSISTENT) == PERSISTENT)
    {
      MemoryManager::freeRom(component->file, component->size);
    } 
  else
    {
      MemoryManager::freeRam(component->file);
      MemoryManager::fill(component->file, component->size,0x00);
    }
  bytes_received = 0;
  MemoryManager::freeRam(component);
  component = 0;
  if (acknowledgement_request == 1) respond(ABORT_SUCCESSFUL);
  return IDLE;
}




/**
 * Copy the the temporary component information into
 * the component table and start the component.
 */
ComponentManager::State ComponentManager::installComponent()
{
  // ROM: search a place for the component header and copy header
  if ((component->info & PERSISTENT) == PERSISTENT)
    {
      Header* slot = getComponentById(component->id);
      if (slot == 0)
	{
	  for (slot = BEGINOF_COMPONENTHEADERS; slot < ENDOF_COMPONENTHEADERS; slot++)
	    {
	      if (slot->id == 0xFFFF)
		{
		  break;
		}
	    }
	  // no more space for component header
	  if (slot >= ENDOF_COMPONENTHEADERS)
	    {
	      MemoryManager::freeRom(component->file,component->size);
	      //MemoryManager::freeRam(temp_file);
	      MemoryManager::freeRam(component);
	      respond(NO_SLOT_FOR_COMPONENTHEADER);
	      return ERROR;
	    }
	}
      MemoryManager::memcopy(component, slot, sizeof(Header), true);
      MemoryManager::freeRam(component);
      component = 0;

      // start component if nessessay
      if ((slot->info & AUTOSTART) == AUTOSTART)
	{
	  command_input.assign(STARTUP, (uint16) slot->id);
	  return STARTING_UP;
	}
      else
	{
	  respond(INSTALLATION_SUCCESSFUL);
	  return IDLE;
	}
    }
  // RAM: create a element containing componenent header
  else
    {
      RamComponent* ramC = new (*memoryManager) RamComponent();
      ramC->header = *(component);
      ramC->next = 0;
      RamComponent_list.enque(ramC);
      MemoryManager::freeRam(component);
      component = 0;

      // start component if nessessay
      if ((ramC->header.info & AUTOSTART) == AUTOSTART)
	{
	  command_input.assign(STARTUP, (uint16) ramC->header.id);
	  return STARTING_UP;
	}
      else
	{
	  respond(INSTALLATION_SUCCESSFUL);
	  return IDLE;
	}
    }

  respond(INSTALLATION_FAILED);
  return ERROR;
}


ComponentManager::State ComponentManager::uninstallComponent(Header* header)
{
  if (header == 0)
    {
      respond(UNKNOWN_COMPONENT);
      return IDLE;
    }
  // stop running component
  for (RuntimeInfo* current = runtimeInfo_list.first(); current != 0; current =  current->next)
    {
      if (current->header == header)
	{
	  // the component is running, so stop it
	  shutdownComponent(header);
	  // since it is posible that shutdown does not worked because one or
	  // more activites of the component where already scheduled, 
	  // rescedule the uninstallation and return
	  command_input.assign(UNINSTALL, (uint16) header->id);
	  return IDLE;
	}
    }

  // remove it from memory
  if ((header->info & PERSISTENT) == PERSISTENT)
    {
      MemoryManager::freeRom(header->file, header->size);
    }
  else
    {
      for (RamComponent* ramC = RamComponent_list.first(); ramC != 0; ramC = ramC->next)
	{
	  if (ramC->header.id == header->id)
	    {
	      MemoryManager::freeRam(ramC->header.file);
	      MemoryManager::fill(ramC->header.file, ramC->header.size,0x00);
	      // remove it from ram component list
	      RamComponent_list.remove(ramC);
	      MemoryManager::freeRam(ramC);
	    }
	}
	
    }
  // remove it from segment with component informations
  if ((header >= BEGINOF_COMPONENTHEADERS) && (header < ENDOF_COMPONENTHEADERS))
    {
      uint8 installedComponents = getROMcomponentNumber();
      // last and only Component is uninstalled
      if (installedComponents == 1) 
	{
	  MemoryManager::erase((caddr_t) (BEGINOF_COMPONENTHEADERS));
	}
      // more components installed in ROM so save their headers
      else
	{
	  Header headers[installedComponents*sizeof(Header)]; //leads to warning but better than using 512 bytes all the time
	  uint8 i = 0;
	  Header* slot;
		    
	  //save headers of all other components
	  for (slot = BEGINOF_COMPONENTHEADERS; slot < ENDOF_COMPONENTHEADERS; slot++)
	    {
	      if ((slot->id != 0xFFFF) && (slot->id != header->id))
		{
		  headers[i] = *slot;
		  i++;
		}
	    }
	  //clear  segment with component informations
	  MemoryManager::erase((caddr_t) (BEGINOF_COMPONENTHEADERS));
	  //rewrite informations
	  Header* source = (Header*) headers;
	  MemoryManager::memcopy(source, (caddr_t) (BEGINOF_COMPONENTHEADERS), sizeof(Header)*(installedComponents-1));
	}
    }

  respond(UNINSTALL_SUCCESSFUL);
  return IDLE;
}

ComponentManager::State ComponentManager::startupComponent(Header* comp)
{
  if (comp == 0)
    {
      respond(UNKNOWN_COMPONENT);
      return IDLE;
    }

  // check if component is already running
  for (RuntimeInfo* info = runtimeInfo_list.first(); info != 0; info
	 = info->next)
    {
      if (info->header == comp)
	{
	  respond(ALREADY_RUNNING);
	  return IDLE;
	}
    }

  // call component startup function
  void* (*startup)() = (void*(*)()) (comp->startup);
  void* addr = 0;

  addr = startup();
  
  // if successfull started create runtime information
  if (addr > 0)
    {
      RuntimeInfo* info = new (*memoryManager) RuntimeInfo();
      info->addr = addr;
      info->header = comp;
      info->next = 0;
      runtimeInfo_list.enque(info); // hahaha, funny naming
      respond(STARTUP_SUCCESSFUL);
    }
  else
    {
      respond(STARTUP_FAILED);
    }

  return IDLE;
}

ComponentManager::State ComponentManager::startupAllComponents()
{
  for (Header* slot = BEGINOF_COMPONENTHEADERS; slot < ENDOF_COMPONENTHEADERS; slot++)
    {
      if ((slot->id != 0xFFFF) && ((slot->info & AUTOSTART) == AUTOSTART))
	{
	  startupComponent(slot);
	}
    }
  return IDLE;
}

ComponentManager::State ComponentManager::shutdownComponent(Header* header)
{

  if (header == 0)
    {
      respond(UNKNOWN_COMPONENT);
      return IDLE;
    }

  // find runtime informations for component;
  for (RuntimeInfo* info = runtimeInfo_list.first(); info != 0; info
	 = info->next)
    {
      if (info->header == header)
	{
	  bool(*shutdown)(void*) = (bool(*)(void*)) (header->shutdown);
	  if (shutdown(info->addr))
	    {
	      runtimeInfo_list.remove(info);
	      MemoryManager::freeRam(info);
	      respond(SHUTDOWN_SUCCESSFUL);
	    }
	  else
	    {
	      // shutdown failed because one or more activites of the component 
	      // where already scheduled, rescedule the shutdown and return ... 
	      // amd hope that all possibilities for schedule the activities
	      // where locked or this may be a good point of a deadlock if
	      // the activities never reach rescheduling_count = 0
	      command_input.assign(SHUTDOWN, (uint16) header->id);
	      respond(SHUTDOWN_FAILED);
	    }
	}
    }

  return IDLE;
}

ComponentManager::Header* ComponentManager::getComponentById(uint16 id)
{
  // search for matching component in ROM...
  for (Header* comp = BEGINOF_COMPONENTHEADERS; comp < ENDOF_COMPONENTHEADERS; comp++)
    {
      if (comp->id == id)
	{
	  return comp;
	}
    }
  // ... and RAM
  for (RamComponent* ramC = RamComponent_list.first(); ramC != 0; ramC = ramC->next)
    {
      if (ramC->header.id == id)
	{
	  return &(ramC->header);
	}
    }
	
  return 0;
}


uint8 ComponentManager::getROMcomponentNumber()
{
  uint8 cnt = 0;
  for (Header* slot = BEGINOF_COMPONENTHEADERS; slot < ENDOF_COMPONENTHEADERS; slot++)
    {
      if (slot->id != 0xFFFF)
	{
	  cnt++;
	}
    }
  return cnt;
}

uint8 ComponentManager::getRAMcomponentNumber()
{
  uint8 cnt = 0;
  for (RamComponent* ram = RamComponent_list.first(); ram != 0; ram = ram->next)
    {
      cnt++;
    }
  return cnt;
}

void ComponentManager::respond(Respond msg, Buffer* buffer)
{
  if (buffer == 0)
    {
      buffer = new (pool) Buffer(pool);
      if (buffer == 0)
	{
	  return;
	}
    }

  buffer->write((uint8) msg);
  outgoingPacket_receiver->assign(buffer);
  buffer = 0;
}

