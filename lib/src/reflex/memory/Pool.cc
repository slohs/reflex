/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/memory/Pool.h"

#include "reflex/memory/PoolManager.h"

#include "NodeConfiguration.h"

using namespace reflex;

Pool::Pool()
{
	id = getApplication().poolManager.registerPool(this);
}

