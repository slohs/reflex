/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/memory/PoolManager.h"

using namespace reflex;

uint8 PoolManager::registerPool(Pool* pool)
{
	Assert(poolCount < MaxPoolCount);

	pools[poolCount] = pool;
	return poolCount++;
}


