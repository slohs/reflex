/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Stefan Nuernberger
 */

#include "reflex/misc/Prescaler.h"

namespace reflex {

/** constructor
 *  @param opmode : periodic or oneshot operation
 */
Prescaler::Prescaler(OperationMode opmode)
{
	this->opmode = opmode;
	prescaleValue = 0;
	counter = 0;
}

/** connect_output
 *  set the subsequent sink
 *  @param output event that will be triggered
 */
void Prescaler::connect_output(Sink0 *output)
{
	this->output = output;
}

/** notify counts the ticks an notifies the subsequent sink if
 *  prescale count is reached.
 */
void Prescaler::notify()
{
	if(prescaleValue)
	{
		if (opmode == PERIODIC) {
			++counter;
		} else { // ONESHOT
			--prescaleValue;
		}

		if(counter == prescaleValue)
		{
			counter = 0;
			output->notify();
		}
	}
}

/** set the prescale value.
 *  @param ticks: the prescale value, a 0 stops prescaling.
 */
void Prescaler::set(unsigned int ticks)
{
	prescaleValue = ticks;
	counter = 0;
}

} // ns reflex
