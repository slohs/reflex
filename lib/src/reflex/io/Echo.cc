/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses): Echo
  
 *	Author:		Karsten Walther
 */
#include "Echo.h"

using namespace reflex;

void Echo::assign(char value){
	if(value == 10){
		//linebreak and flush
		out->writeln();
	}else if(value == 13){
		//do nothing on carriage return
	}else{
		//all other characters
		out->write(value);
	}
}

