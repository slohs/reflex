/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/io/OutputElement.h"
#include "NodeConfiguration.h"

using namespace reflex;

OutputElement::OutputElement(const char* name, const char* type)
{
	this->name = name;
	this->type = type;
	id = system.ioManager.registerOutputElement(this);
}

