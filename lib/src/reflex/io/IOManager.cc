/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/io/IOManager.h"
#include "reflex/io/InteractiveElement.h"
#include "reflex/io/OutputElement.h"
#include "NodeConfiguration.h"

using namespace reflex;

IOManager::IOManager(OutputChannel* out)
{
	//this->out = out;
	idCount = 0;
	pos = 0;
}

char IOManager::registerOutputElement(OutputElement* obj)
{
	elements[idCount] = obj;

	return idCount++;
}

void _reset()
{
	void (*_start)() = (void (*)())0x8000;
	(*_start)();
}

void IOManager::assign(char data)
{
	if(data == '\r'){
		char nr = 0;
			for(int i = 1; i<pos; i++){
			nr *= 10;
			nr = (buffer[i]-'0');
		}
		switch (buffer[0]) {
			case 'R' :  //Reset
					_reset();
			case 'C' :  //print element Count
					printCount();
					break;
			case 'P' :  //print one element
					if(nr < idCount){
						printElement(getOutput(nr));
					}
					break;
			case 'F' :  //forcing an element
					//tmp->force(&(buffer[3]));
					break;
			case 'U' :  //unforcing an element
					//tmp->unforce();
					break;
			case 'W' :  //write current value of an element
					if(nr < idCount){
						getOutput(nr)->writeValue();
					}
					break;
			default:
					break;

		};
		buffer[0]=0;
		pos = 0;
	}else{
		if(pos<20){
			buffer[pos++] = data;
		}
	}
}

OutputElement* IOManager::getOutput(unsigned char id){
	for(unsigned char i=0; i<idCount; i++){
		if(elements[i]->id == id){
			return elements[i];
		}
	}
	return 0;
}

void IOManager::printElement(OutputElement* elem)
{
	if(elem){
		system.out.write("id ");
		system.out.write((unsigned)(elem->id));
		system.out.write(' ');
		system.out.write(elem->name);
		system.out.write(' ');
		system.out.write(elem->type);
		system.out.writeln();
	}
}

void IOManager::printCount()
{
	system.out.write("idcount ");
	system.out.write(idCount);
	system.out.writeln();
}

