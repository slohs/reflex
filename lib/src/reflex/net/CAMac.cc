/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */

#include "reflex/net/CAMac.h"
#include "reflex/interrupts/InterruptLock.h"
#include "NodeConfiguration.h"
using namespace reflex;

void CAMac::run()
{
	this->lock();
	fired=false;
	timer.set(timerValue);
}

void CAMac::notify()
{
	getApplication().led.turnOff(Led::RED);

	if(toNotify){
		toNotify->notify();
	}
	this->unlock();
}

void CAMac::assign(char value)
{
	//if any packet is for transmission and already in transmission
	if(this->islocked() == true && !fired){
		timer.set(timerValue);
	}
}

void CAMac::handleTick()
{

	//only transmit one packet at a time
	if(!fired){
		fired = true;
		device->assign(queue.get());
	}
}
