/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/net/ManchesterCoder.h"

using namespace reflex;

const char ManchesterCoder::lookupTable[16] = {
			(char)0x55,(char)0x56,(char)0x59,(char)0x5a, 
			(char)0x65,(char)0x66,(char)0x69,(char)0x6a,
			(char)0x95,(char)0x96,(char)0x99,(char)0x9a, 
			(char)0xa5,(char)0xa6,(char)0xa9,(char)0xaa};

ManchesterCoder::ManchesterCoder(Pool* pool) : input(this)
{
	this->pool = pool;
}

void ManchesterCoder::assign(char byte)
{
/*	volatile char* ser1 = (volatile char*)0x7f;
	*ser1 = byte;
	return;
*/	Assert(upperOutput != 0);

	//if bits in byte are pairwise different (bit 0 and 1, 2 and 3 ...) a
	//valid Manchester encoded byte was received
	if( 0x55 == ( ((byte & 0xaa) >> 1) xor (byte & 0x55) ) ){
		if(firstNibble){
			decodedByte = (byte & 0x2) * 8;
			decodedByte |= (byte & 0x8) * 4;
			decodedByte |= (byte & 0x20) * 2;
			decodedByte |= (byte & 0x80);
			firstNibble = false;

		}else{
			decodedByte |= (byte & 0x2) / 2;
			decodedByte |= (byte & 0x8) / 4;
			decodedByte |= (byte & 0x20) / 8;
			decodedByte |= (byte & 0x80) / 16;

			firstNibble = true;

			upperOutput->assign(decodedByte);

		}
	}else{	
		firstNibble = true;
	}
}

#include "NodeConfiguration.h"

void ManchesterCoder::run()
{
	Assert(lowerOutput != 0);

	Buffer* buffer = new (pool) Buffer(pool);
	Buffer* current = input.get();

	if(buffer){	
		caddr_t pos = current->getStart();
		uint8 length = current->getLength();
		char tmp;
		for(uint8 i = 0; i < length; i++){
			tmp = *pos++;
			buffer->write(lookupTable[(tmp >> 4) & 0xf]);//encode high nibble
			buffer->write(lookupTable[tmp & 0xf]); //low nibble
		}
		buffer->push((char)0x44);
		buffer->push((char)0x44);
		buffer->push((char)0x44);
		buffer->push((char)0x44);
//		buffer->push(0x3333);
//		buffer->push(0x3333);
		lowerOutput->assign(buffer);
	}else{
		getApplication().led.turnOff(Led::ORANGE);
	}
	current->downRef();

}
