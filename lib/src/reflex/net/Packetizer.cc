/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Sören Höckner, Karsten Walther, Olaf Krause
 */
#include "reflex/net/Packetizer.h"
#include "reflex/debug/Assert.h"
#include "conf.h"
//#include "reflex/io/Led.h"

using namespace reflex;

Packetizer::Packetizer(Pool *pool)
{
	this->pool = pool;
	state = IDLE;
	receiveBuffer = 0;
	preamble = 0;
}

void Packetizer::init(Sink1<Buffer*> *lowerOut, Sink1<Buffer*> *upperOut )
{
	Assert(upperOut !=0);
	this->upperOut = upperOut;

	Assert(lowerOut !=0);
	this->lowerOut = lowerOut;
}

bool Packetizer::isBusy()
{
	return state != IDLE;
}

#include "NodeConfiguration.h"

void Packetizer::assign(char byte)
{

/*	volatile char* ser1 = (volatile char*)0x7f;
	*ser1 = byte;
	return;
*/
	//get data
	if (state == DATA){
		if (!receiveBuffer->write(byte)) // buffer full?, write was unsuccessfull
		{
			receiveBuffer->downRef(); //clear buffer, msg is droped
			state = IDLE;
			return;
		}

		crc -= byte;

		// did we receive everything
		if (--receiveCount <= 0) {

			if(!crc){
				upperOut->assign(receiveBuffer); //all successfull, buffer is passed to upperOut
			}else{
				receiveBuffer->downRef(); //clear buffer, msg is droped
			}
			state = IDLE;
		}

		return;
	}
	// get packet size : Data and Header
	if (state == SIZE)
	{
		receiveBuffer = new (pool) Buffer(pool);
		if (receiveBuffer)
		{
			receiveCount = byte;
			state = OFFSET;
		}
		else
		{
			state = IDLE;
		}
		return;
	}
	//get checksum
	if(state == CRC){
		crc = byte;
		state = DATA;
		return;
	}
	/** get the Offset of the tos Pointer and initialize the new Buffer */
	if(state == OFFSET)
	{
		receiveOffset = byte;
		state = CRC;
		receiveBuffer->initOffsets(receiveOffset);
		return;
	}
	//get the tos of the sent buffer
	if((state == IDLE) && (byte == '@'))
	{
		state = SIZE;
	}

	// we have received a packet separator
/*	preamble = (preamble * 256) | byte;
	if (preamble == 0x3333) {
		state = SIZE;
		preamble = 0;
	}
*/
}

void Packetizer::assign(Buffer *buffer)
{
	Assert(lowerOut!=0);
	uint8 size = buffer->getLength();	//should return size of data
	uint8* pos = buffer->getStart();
	uint8 offset = (uint8)( pos -  reinterpret_cast<uint8*>( buffer + 1 )  ); // get the offset of the tos pointer

	Assert(offset>3);// if there is room for the out header informations!

	char sendcrc = 0;
	for(int i=0; i<size; i++)
	{
		sendcrc += *pos++;
	}
	//push Packetizer header information
	buffer->push(sendcrc);
	buffer->push(offset);
	buffer->push(size);
	buffer->push('@');
	// throw buffer;
	this->lowerOut->assign(buffer);
}
