/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	 Stefan Nuernberger
 */

#include "reflex/timer/VirtualTimer.h"

using namespace reflex;

/**
 * constructor
 * @param opmode operation mode
 * @param prioritized mark this timer as high priority (system timer)
 */
VirtualTimer::VirtualTimer(OperationMode opmode, bool prioritized)
	: output(0)
{
	isOneshot = opmode;
	this->priority = prioritized;
	/* register this VTimerBase */
	support->vTimer.registerTimer(this);
}

/**
 \brief Unloads the virtual timer instance.

 The timer is removed from the global virtual timer list.
 A running timer will be stopped automatically.
 */
VirtualTimer::~VirtualTimer() {
	InterruptLock lock;
	stop();
	support->vTimer.removeTimer(this);
}

/** Triggers connected event.
 *  Implements Sink0 interface.
 */
void VirtualTimer::notify() {
	if (output) output->notify();
}

/** set
 *  starts (and stops) the timer
 *  @param ticks sets the value for countdown, 0 stops the timer
 */
void VirtualTimer::set(Time ticks) {
	delta = ticks;
	support->vTimer.set(this); // update timer
}

/**
 * setPriority
 * set the priority for this timer (true or false)
 * @param priority true for high priority timer
 */
void VirtualTimer::setPriority(bool priority) {
	this->priority = priority;
}

/** getSystemTime
 * @return Time current local time value
 */
Time VirtualTimer::getSystemTime() {
    return support->hwTimer.getNow();
}

/** getTimestamp
 * @return Time current local time value
 */
uint64 VirtualTimer::getTimestamp() {
    return support->hwTimer.getTimestamp();
}
