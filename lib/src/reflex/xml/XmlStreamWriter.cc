#include "reflex/memory/Buffer.h"
#include "reflex/xml/XmlStreamWriter.h"

using namespace reflex;
using namespace xml;

XmlStreamWriter::XmlStreamWriter(Buffer* buffer) :
	buffer(buffer)
{
	Assert(buffer != 0);
	this->state = Idle;
}

void XmlStreamWriter::writeStartDocument()
{
	char string[] = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
	this->buffer->write(&string[0], sizeof(string));
	this->state = StartDocument;
}

void XmlStreamWriter::writeEndDocument()
{
	uint8* pos = buffer->getStart();
	const uint8* old = pos;

	if (this->state == StartElement)
	{
		*pos++ = '/';
		*pos++ = '>';
	}

	*pos++ = '\n';

	this->state = Idle;
	this->buffer->allocData(pos - old);
}

void XmlStreamWriter::writeStartElement(const char* name)
{
	uint8* pos = buffer->getStart();
	const uint8* old = pos;

	if (this->state == StartElement)
	{
		*pos++ = '>';
	}

	*pos++ = '<';

	for (; *name != '\0'; name++)
	{
		*pos++ = *name;
	}
	this->state = StartElement;
	this->buffer->allocData(pos - old);
}

void XmlStreamWriter::writeAttributeName(const char* name)
{
	uint8* pos = buffer->getStart();
	const uint8* old = pos;

	*pos++ = ' ';

	for (; *name != '\0'; name++)
	{
		*pos++ = *name;
	}

	*pos++ = '=';
	*pos++ = '\"';

	this->state = StartAttribute;
	this->buffer->allocData(pos - old);
}

void XmlStreamWriter::writeAttributeData(const char* data)
{
	uint8* pos = buffer->getStart();
	const uint8* old = pos;

	for (; *data != '\0'; data++)
	{
		*pos++ = *data;
	}

	*pos++ = '\"';

	this->buffer->allocData(pos - old);
}

void XmlStreamWriter::writeAttribute(const char* name, const char* data)
{
	uint8* pos = buffer->getStart();
	const uint8* old = pos;

	if (this->state == StartAttribute)
	{
		*pos++ = '\"';
	}

	*pos++ = ' ';

	for (; *name != '\0'; name++)
	{
		*pos++ = *name;
	}

	*pos++ = '=';
	*pos++ = '\"';

	for (; *data != '\0'; data++)
	{
		*pos++ = *data;
	}

	*pos++ = '\"';
	this->buffer->allocData(pos - old);
}

void XmlStreamWriter::writeCharacters(const char* characters)
{
	uint8* pos = buffer->getStart();
	const uint8* old = pos;

	switch (this->state)
	{
	case StartAttribute:
		*pos++ = '\"';
	case StartElement:
		*pos++ = '>';
		break;
	}

	for (; *characters != '\0'; characters++)
	{
		*pos++ = *characters;
	}
	this->buffer->allocData(pos - old);
	this->state = Characters;
}

void XmlStreamWriter::writeEndElement(const char* name)
{
	uint8* pos = buffer->getStart();
	const uint8* old = pos;
	switch (this->state)
	{
	case StartAttribute:
		*pos++ = '\"';
	case StartElement:
		*pos++ = ' ';
		*pos++ = '/';
		*pos++ = '>';
		break;
	default:
		*pos++ = '<';
		*pos++ = '/';
		for (; *name != '\0'; name++)
		{
			*pos++ = *name;
		}
		*pos++ = '>';
	}

	this->buffer->allocData(pos - old);
	this->state = EndElement;
}
