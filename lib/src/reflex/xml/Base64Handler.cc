#include "reflex/memory/Buffer.h"
#include "reflex/memory/Pool.h"
#include "reflex/xml/Base64Handler.h"

using namespace reflex;
using namespace xml;

Base64Handler::Base64Handler()
{
	this->setConfig(WriteEnabled);
	unfinished[0] = 0;
	unfinished[1] = 0;
	unfinished[2] = 0;
}

void Base64Handler::init(Pool* pool)
{
	this->pool = pool;
}

void Base64Handler::handleReceiveEvent(XmlHandler::XmlEvent event, Buffer* content)
{
	if ((!pool) || (!output_binaryData))
	{
		return;
	}

	if (event & XmlAfterStartElement)
	{
		/* Do cleanup */
		unfinished[0] = 0;
		unfinished[1] = 0;
		unfinished[2] = 0;
		if (this->binary)
		{
			this->binary->downRef();
			this->binary = 0;
		}
	}
	else if ((event & XmlContent) && (config & WriteEnabled))
	{
		/* Create a new buffer when necessary */
		if (!this->binary)
		{
			this->binary = new (this->pool) Buffer(pool);
			Assert(this->binary);
			this->bytesLeft = this->binary->getLength();
		}
		/* Do conversion */
		// NOT IMPLEMENTED, yet
		/* Forward buffer when full */
		if (this->bytesLeft < 3)
		{
			this->output_binaryData->assign(this->binary);
			this->binary = 0;
		}
	}
}

uint8 Base64Handler::handleSendEvent(uint8 event, Buffer* content)
{
	return XmlNoAction;
}
