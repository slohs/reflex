#include "reflex/memory/Buffer.h"
#include "reflex/memory/Pool.h"
#include "reflex/xml/XmlHandler.h"
#include "reflex/xml/XmlReader.h"

using namespace reflex;
using namespace xml;

XmlReader::XmlReader() :
	input_char(&act_parseChar), act_handleBuffer(*this), act_parseChar(*this)
{
	input_buffer.init(&this->act_handleBuffer);
	state = ScanForElement;
}

void XmlReader::init(XmlTagTable* table, Pool* pool)
{
	this->table = table;
	this->tag = new (pool) Buffer(pool);
	Assert(this->tag);
	this->data = new (pool) Buffer(pool);
	Assert(this->data);
}

void XmlReader::parse(char c)
{
	switch (state)
	{
	case ScanForElement:
		if (c == '<')
		{
			state = ElementOpened;
		}
		break;
	case ElementOpened:
		if (((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')))
		{
			tag->reset();
			tag->write(c);
			state = StartElement;
		}
		else if (c == '/')
		{
			tag->reset();
			state = EndElement;
		}
		else
		{
			state = ScanForElement;
		}
		break;
	case StartElement:
		if (((c >= '0') && (c <= '9')) || ((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')) || (c == '-') || (c == '_'))
		{
			tag->write(c);
		}
		else if (c == '>')
		{
			/* handleStartElement must be delayed until we can decide between poll request and normal tag */
			state = StartElementClosed;
		}
		else if (c == '/')
		{
			/* Empty element (<tag/>) is not treated as poll request */
			state = EmptyElement;
		}
		else if (c == ' ')
		{
			state = Attributes;
		}
		else
		{
			state = ScanForElement;
		}
		break;
		/* Attributes are ignored */
	case Attributes:
		if (c == '>')
		{
			this->handleStartElement();
			state = StartElementClosedNoPoll;
		}
		else if (c == '/')
		{
			state = EmptyElement;
		}
		break;
	case StartElementClosed:
		if (c == '<')
		{
			state = ElementOpenedAndMaybePoll;
		}
		else if ((c >= '!') && (c <= '~'))
		{
			/* It's not a poll request, handle the delayed tagOpenend-event from case StartElement */
			this->handleStartElement();
			data->write(c);
			state = Content;
		}
		else
		{
			state = ScanForElement;
		}
		break;
	case StartElementClosedNoPoll:
		if ((c >= '!') && (c <= '~'))
		{
			data->write(c);
			state = Content;
		}
		else
		{
			state = ScanForElement;
		}
		break;
	case EmptyElement:
		if (c == '>')
		{
			this->handleEmptyElement();
		}
		state = ScanForElement;
		break;
	case Content:
		if (c == '<')
		{
			this->handleContent();
			data->reset();
			state = ElementOpened;
		}
		else
		{
			data->write(c);
		}
		break;
	case EndElement:
		if (((c >= '0') && (c <= '9')) || ((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')) || (c == '-') || (c == '_'))
		{
			tag->write(c);
		}
		else if (c == '>')
		{
			this->handleEndElement();
			state = ScanForElement;
		}
		else
		{
			state = ScanForElement;
		}
		break;
	case ElementOpenedAndMaybePoll:
		if (c == '/')
		{
			/* Treat the combination <tag></... as a poll request */
			this->handlePoll();
			state = ScanForElement;
		}
		else if (((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z')))
		{
			/* It's not a poll request, handle the delayed event from case StartElement */
			tag->reset();
			tag->write(c);
			state = StartElement;
		}
		else
		{
			state = ScanForElement;
		}
		break;
	case IgnoreUntilElementEnd:
		if (c == '>')
		{
			state = ScanForElement;
		}
		break;
	}
}

void XmlReader::handleStartElement()
{
	/* Terminate the tag */
	tag->write('\0');
	handler = table->handlerByName(reinterpret_cast<char*> (tag->getStart()));
	if (handler)
	{
		handler->handleReceiveEvent(XmlHandler::XmlAfterStartElement, 0);
	}
}

void XmlReader::handleContent()
{
	if (handler)
	{
		handler->handleReceiveEvent(XmlHandler::XmlContent, data);
	}
}

void XmlReader::handlePoll()
{
	tag->write('\0');
	if (output_pollRequest)
	{
		const LookupEntry* entry = table->entryByName(reinterpret_cast<char*> (tag->getStart()));
		this->output_pollRequest->assign(entry);
	}
}

void XmlReader::handleEndElement()
{
	tag->write('\0');
	handler = table->handlerByName(reinterpret_cast<char*> (tag->getStart()));
	if (handler)
	{
		handler->handleReceiveEvent(XmlHandler::XmlAfterEndElement, 0);
	}
}

void XmlReader::handleEmptyElement()
{
	tag->write('\0');
	handler = table->handlerByName(reinterpret_cast<char*> (tag->getStart()));
	if (handler)
	{
		handler->handleReceiveEvent(XmlHandler::XmlAfterEmptyElement, 0);
	}
}

void XmlReader::run_handleChar()
{
	this->parse(this->input_char.get());
}

void XmlReader::run_handleBuffer()
{
	Buffer* buffer = input_buffer.get();
	char* c = reinterpret_cast<char*> (buffer->getStart());
	for (Buffer::SizeT length = buffer->getLength(); length > 0; length--)
	{
		this->parse(*c);
		c++;
	}

	buffer->downRef();
}
