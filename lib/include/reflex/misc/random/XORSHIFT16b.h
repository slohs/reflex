/*
 * XORSHIFT16B.h
 *
 *  Created on: 09.08.2011
 *      Author: sbuechne
 */

#ifndef XORSHIFT16b_H_
#define XORSHIFT16b_H_

#include "reflex/misc/random/Random.h"

namespace reflex
{
namespace misc
{
/**
 * Random number generator.
 * @tparam SeedGenerator this can be any type, which provides a getRandomSeed() method!
 *
 * For the meaning of A,B and C see the paper: George Marsaglia "Xorshift RNGs" and http://www.arklyffe.com/main/2010/08/29/xorshift-pseudorandom-number-generator/
 * The folloing combinations seeming tob possible
 *
 * (A,B,C)
 *
 * (1, 1, 14)	(1, 1, 15)	(1, 5, 2 )	(1, 7, 4 )	(1, 7, 11)	(1, 11, 3 )
 * (1, 15, 6 )	(1, 15, 7 )	(2, 5, 1 )	(2, 5, 13)	(2, 5, 15)	(2, 7, 13)
 * (2, 7, 15)	(3, 1, 12)	(3, 1, 15)	(3, 5, 11)	(3, 11, 1 )	(3, 11, 11)
 * (3, 13, 9 )	(4, 3, 7 )	(4, 7, 1 )	(4, 11, 11)	(5, 7, 14)	(5, 9, 8 )
 * (5, 11, 6 )	(5, 11, 11)	(6, 7, 13)	(6, 11, 5 )	(6, 15, 1 )	(7, 1, 11)
 * (7, 3, 4 )	(7, 9, 8 )	(7, 9, 13)	(7, 15, 1 )	(8, 9, 5 )	(8, 9, 7 )
 * (9, 7, 13)	(9, 13, 3 )	(11, 1, 7 )	(11, 3, 13)	(11, 5, 3 )	(11, 7, 1 )
 * (11, 11, 3 )	(11, 11, 4 )	(11, 11, 5 )	(12, 1, 3 )	(12, 3, 13)	(13, 3, 11)
 * (13, 3, 12)	(13, 5, 2 )	(13, 7, 2 )	(13, 7, 6 )	(13, 7, 9 )	(13, 9, 7 )
 * (14, 1, 1 )	(14, 7, 5 )	(15, 1, 1 )	(15, 1, 3 )	(15, 5, 2 )	(15, 7, 2 )
 */
template<class SeedGenerator, uint8 A = 13, uint8 B = 9, uint8 C = 7>
class XORSHIFT16b : public Random
{
public:
	/**
	 * Constructor
	 * @param seedGenerator	The seedGenerator is used when the last random number was 0.
	 */
	XORSHIFT16b(SeedGenerator* seedGenerator)
		: RANDMAX(0xFFFF), seedGenerator(seedGenerator)
	{
		newSeed( seedGenerator->getRandomSeed() );
	}

	void newSeed(uint16 seed)
	{
		nextSeed = seed;
	}

	void setSeedGenerator(SeedGenerator* seedGenerator)
	{
		this->seedGenerator = seedGenerator;
	}

	XORSHIFT16b()
		: RANDMAX(0xFFFF), seedGenerator(NULL)
	{

	}

	uint16 next();

	const uint16 RANDMAX;
private:
	uint16 lastRandom;
	uint16 nextSeed;
	SeedGenerator *seedGenerator;
};

} /* namespace misc */
} /* namespace reflex */
#endif /* XORSHIFT16b_H_ */

#ifndef __XORSHIFT16b_CC__
#define __XORSHIFT16b_CC__


namespace reflex
{
namespace misc
{

template<class SeedGenerator, uint8 A, uint8 B, uint8 C>
	uint16 XORSHIFT16b<SeedGenerator,A,B,C>::next()
	{
		if(lastRandom == 0)
		{
			lastRandom = nextSeed;

			if(seedGenerator != NULL)
				nextSeed = seedGenerator->getRandomSeed();
		}

		lastRandom ^= (lastRandom << A);
		lastRandom ^= (lastRandom >> B);
	    return lastRandom ^= (lastRandom << C);
	}
}
}

#endif
