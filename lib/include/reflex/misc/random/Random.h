/*
 * Random.h
 *
 *  Created on: 09.08.2011
 *      Author: sbuechne
 */

#ifndef RANDOM_H_
#define RANDOM_H_

namespace reflex{
namespace misc{

class Random
{
public:
	virtual uint16 next() = 0;

	template<typename T>
	T next(T max)
	{
		return next() % max;
	}
};

}} /*reflex::misc*/
#endif
