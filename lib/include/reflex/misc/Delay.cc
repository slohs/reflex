
#include "reflex/misc/Delay.h"

namespace reflex {

///** configure as no delay, use ONESHOT timer mode
// */
//Delay::Delay() : timer(VirtualTimer::ONESHOT)
//{
//    delay = 0;
//}

///** incoming event that will be delayed
// */
//void Delay::notify()
//{
//    if (delay) {
//        timer.set(delay);
//    } else {
//        // immediate notify
//        timer.notify();
//    }
//}

///** set new delay value
// *  takes effect on next incoming event
// */
//void Delay::set_delay(Time newDelay)
//{
//    delay = newDelay;
//}

///** get pointer to input
// */
//Sink0* Delay::get_in_input()
//{
//    return this;
//}

///** set delayed output
// */
//void Delay::set_out_delayed(Sink0 *output)
//{
//    timer.connect_output(output);
//}


/** assign value to output
 */
template<typename T>
void Delay1<T>::Output::notify()
{
    if (out) {
        out->assign(value);
    }
}

/** configure as no delay, use ONESHOT timer mode
 */
template<typename T>
Delay1<T>::Delay1() : timer(VirtualTimer::ONESHOT)
{
    delay = 0;
    output.out = 0;
    timer.connect_output(&output);
}

/** incoming data that will be delayed
 */
template<typename T>
void Delay1<T>::assign(T value)
{
    output.value = value;
    if (delay) {
        timer.set(delay);
    } else {
        // immediate notify
        output.notify();
    }
}

/** set new delay value
 *  takes effect on next incoming event
 */
template<typename T>
void Delay1<T>::set_delay(Time newDelay) {
    delay = newDelay;
}

/** get pointer to input
 */
template<typename T>
Sink1<T>* Delay1<T>::get_in_input()
{
    return this;
}

/** set delayed output
 */
template<typename T>
void Delay1<T>::set_out_delayed(Sink1<T> *output)
{
    this->output.out = output;
}

} // reflex

