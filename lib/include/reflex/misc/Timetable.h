#ifndef Timetable_h
#define Timetable_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	Timetable
 *
 *	Author:		Maik Kr�ger, Karsten Walther
 *
 *	Description:	A time based activation plan for activities
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/System.h"
#include "reflex/sinks/Sink.h"
#include "reflex/scheduling/Activity.h"

namespace reflex {

/** Holds pointers to several activities and for each a tickcount, if
 *  the tickcount is reached.
 */
class Timetable: public Sink0 {
public:
	/** Sets the counter for registers activities to 0.
	 * Constructor
	 */
	Timetable()
	{
		counter = 0;
	}

	/**
	 * if the time has come trigger an activity
	 */
	virtual void notify();

	/** Add an activity to the timetable
	 *
	 *  @param act the activity
	 *  @param millisec the time after which the act is rescheduled
	 */
	void registerActivity(Activity* act, unsigned int millisec);

	/** Remove an activity from timetable.
	 *
	 *  @param activity the activity for removal
	 */
	void removeActivity(Activity* activity);

private:
	//specify the size of the timetable
	enum {maxElems = 5};

	//pointer to activities which should be scheduled
	Activity* act[maxElems];
	//the remaining ticks befor the activity is scheduled
	unsigned int countdown[maxElems];
	//the currently number of timetable entries
	volatile unsigned char counter;

};//end class

}// end namespace

#endif

