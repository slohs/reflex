#ifndef REFLEX_MISC_DELAY_H
#define REFLEX_MISC_DELAY_H
/*
 *  REFLEX - Real-time Event FLow EXecutive
 *
 *  A lightweight operating system for deeply embedded systems.
 *
 *  Class(es):  Delay, Delay1
 *
 *  Author:     Stefan Nuernberger
 *
 *  Description: A component to delay the propagation in the event channel
 *
 *
 *  This file is part of REFLEX.
 *
 *  Copyright 2012 BTU Cottbus, Distributed Systems / Operating Systems
 *  Group. All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    1. Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *
 *    2. Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in
 *       the documentation and/or other materials provided with the
 *       distribution.
 *
 *  THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DISTRIBUTED SYSTEMS /
 *  OPERATING SYSTEMS GROUP ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL BTU COTTBUS, DISTRIBUTED SYSTEMS / OPERATING SYSTEMS
 *  GROUP OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 *  TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 *  LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *  The views and conclusions contained in the software and documentation
 *  are those of the authors and should not be interpreted as representing
 *  official policies, either expressed or implied, of BTU Cottbus,
 *  Distributed Systems / Operating Systems Group.
 *
 **/
#include "reflex/sinks/Sink.h"
#include "reflex/timer/VirtualTimer.h"

namespace reflex {

/** Delay the notify of a Sink0
 */
class Delay : public Sink0 {
private:
    Time delay; ///< the current delay in milliseconds
    VirtualTimer timer; ///< delay timer producing output

public:

    /** configure as no delay, use ONESHOT timer mode
     */
    Delay() : timer(VirtualTimer::ONESHOT)
    {
        delay = 0;
    }

    /** incoming event that will be delayed
     */
    virtual void notify()
    {
        if (delay) {
            timer.set(delay);
        } else {
            // immediate notify
            timer.notify();
        }
    }

    /** set new delay value
     *  takes effect on next incoming event
     */
    void set_delay(Time newDelay)
    {
        delay = newDelay;
    }

    /** get pointer to input
     */
    Sink0* get_in_input()
    {
        return this;
    }


    /** set delayed output
     */
    void set_out_delayed(Sink0 *output)
    {
        timer.connect_output(output);
    }
};


/** Delay propagation of a Sink1
 */
template <typename T>
class Delay1 : public Sink1<T> {
private:
    Time delay; ///< the current delay in milliseconds
    VirtualTimer timer; ///< delay timer producing output
    class Output : public Sink0 {
    public:
        T value; ///< the delayed value
        Sink1<T> *out; ///< subsequent sink

        /** assign value to output
         */
        void notify();
    } output; ///< output

public:

    /** configure as no delay, use ONESHOT timer mode
     */
    Delay1();

    /** incoming data that will be delayed
     */
    virtual void assign(T value);

    /** set new delay value
     *  takes effect on next incoming event
     */
    void set_delay(Time newDelay);

    /** get pointer to input
     */
    Sink1<T>* get_in_input();

    /** set delayed output
     */
    void set_out_delayed(Sink1<T> *output);
};

} //namespace reflex

#include "reflex/misc/Delay.cc"

#endif // REFLEX_MISC_DELAY_H
