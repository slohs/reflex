#ifndef XMLCONTAINER_H_
#define XMLCONTAINER_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlContainer
 *
 *	Description: Simple data storage for components that can be accessed through XML
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/sinks/Sink.h"
#include "reflex/xml/XmlDefaultHandler.h"
namespace reflex
{

namespace xml
{

/**
 * Simple (synchronized) data storage for components that can be accessed through XML.
 *
 * Unlike XmlChannel, this class is a plain storage for data and can replace
 * integer and enum members in components, that are read and written by activities
 * but do not cause events. Configuration parameters are an example for this.
 *
 */
/**
 * Plain data storage that can be accessed from system and XML.
 *
 * @tparam DataT The data type of the container.
 * @tparam ConversionPolicy a class that converts raw data from and into XML data.
 *
 * Unlike XmlChannel, this class is a plain storage for data and can replace
 * integer and enum members in components, that are read and written by activities
 * but do not cause events. Therefore, the Sink1 overhead is not needed.
 *
 * Unlike XmlChannel, the methods get() and set() are not interrupt safe, because
 * XmlContainer is to be used only inside a single component.
 *
 * @see XmlHandler for configuring read and write access.
 * @see XmlConverterInteger and XmlConverterEnum for @a ConversionPolicy examples.
 *
 */
template<class DataT, class ConversionPolicy>
class XmlContainer: public XmlDefaultHandler<DataT, XmlContainer<DataT, ConversionPolicy> > , public ConversionPolicy
{
public:

	/**
	 * Caches the data internally.
	 * This method is needed by XmlDefaultHandler to forward raw data
	 * after conversion.
	 */
	void set(DataT data);

	/**
	 * Loads the data from cache.
	 */
	DataT get() const;

	/**
	 * @see get()
	 */
	void get(DataT& data) const;

private:
	DataT data; /**< Data cache */
};

template<class DataT, class ConversionPolicy>
void XmlContainer<DataT, ConversionPolicy>::get(DataT& data) const
{
	data = this->get();
}

template<class DataT, class ConversionPolicy>
DataT XmlContainer<DataT, ConversionPolicy>::get() const
{
	return this->data;
}

template<class DataT, class ConversionPolicy>
void XmlContainer<DataT, ConversionPolicy>::set(DataT data)
{
	this->data = data;
}

}
}

#endif /* XMLCONTAINER_H_ */
