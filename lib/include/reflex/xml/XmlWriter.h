#ifndef XMLMWRITER_H_
#define XMLWRITER_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlWriter
 *
 *	Description: Parses outgoing XML streams using using XmlHandler.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "conf.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Event.h"
#include "reflex/sinks/Fifo.h"
#include "reflex/sinks/SingleValue.h"

namespace reflex
{
class Buffer;
class Pool;

namespace xml
{
class XmlHandler;
class XmlTagTable;

/**
 * Fills an empty XML message skeleton with data from different handlers.
 *
 * A message skeleton can look like:
 * \code
 * <?xml version="1.0" encoding="ISO-8859-1"?>
 * <statusMessage>
 * 		<sensors>
 * 			<temperature unit="°C"></humidity>
 * 			<humidity unit="%"></humidity>
 * 		</sensors>
 * 		<actors>
 * 			<relais0></relais0>
 * 			<relais1></relais1>
 * 		</actors>
 * </statusMessage>
 * \endcode
 *
 * XmlWriter parses every XML element, asks XmlTagTable for an appropriate
 * XmlHandler and calls XmlHandler::handleSendEvent on success for every event.
 * Events can be a start-element, and end-element or an empty-element. XmlWriter
 * parses plain element-names only and therefore ignores attributes, namespaces
 * and directives.
 *
 * To start a message, just write the start-address of the message to XmlWriter::input_message.
 * The message itself should reside in flash memory. To read from there,  abstraction class
 * MemoryPolicy is used. This way, XmlWriter will also work on AVRs, where access to flash
 * memory needs separate instructions.
 *
 */
class XmlWriter
{
	typedef XmlTagTable::LookupEntry LookupEntry;

#ifndef XML_MESSAGE_QUEUE_SIZE
#define XML_MESSAGE_QUEUE_SIZE 5
#endif

	enum
	{
		MessageQueueSize = XML_MESSAGE_QUEUE_SIZE
	};

public:

	/** Connects XmlWriter to a lower layer buffer input. */
	void connectTo(Sink1<Buffer*>* input_txBuffer);

	/** Input for outgoing messages. */
	Fifo2<const char*, Sink0*, MessageQueueSize> input_message;

	/**
	 * Pointer to the outgoing com component that
	 * sends the buffer (or encapsulates it).
	 */
	Sink1<Buffer*>* output_chunk;

	/**
	 * Output to signal that a poll event has
	 * been finished.
	 */
	Sink0* output_pollFinished;

	/**
	 * Acknowledge input for the com component to
	 * signal a finished transmission.
	 */
	Event input_chunkHandled;

	/** Input for poll requests of a XmlStreamReader. */
	SingleValue1<const XmlTagTable::LookupEntry*> input_pollRequest;

	XmlWriter();

	/**
	 * Sets all necessary resources. Call this method right after
	 * construction. A minimum of 2 buffers is needed by XmlWriter
	 * to work properly.
	 *
	 * @param table the table where to search for XmlHandlers.
	 * @param pool to construct new message fragments..
	 */
	void init(XmlTagTable* table, Pool* pool);

private:
	/**
	 * Initiates XML parsing and locks the parser until the message
	 * has been sent completely.
	 */
	void run_startMessage();

	/**
	 * The parser always works with a single buffer. run_continueParsing() can not
	 * proceed before the buffer has been freed.
	 */
	void run_bufferSent();

	/**
	 * Starts to handle a poll request from XmlWriter.
	 * It will lock the parser until the poll has been handled completely.
	 */
	void run_pollRequest();

	/**
	 * Parses the message and sends it buffer by buffer.
	 */
	void parse();

	inline void handleScanForElement();
	inline void handleElementOpened();
	inline void handleIgnoreElement();
	inline void handleStartElement();
	inline void handleAttributesOrEmptyElement();
	inline void handleEmptyElement();
	inline void handleEndElement();
	inline void handleEndDocument();
	inline void handleAfterStartElement();
	inline void handleAfterEndElement();
	inline void handleAfterEmptyElement();
	inline void handlePollStartElement();
	inline void handlePollContent();
	inline void handlePollEndElement();

	/**
	 * Internal state machine states.
	 */
	enum State : uint8
	{
		/* States where the message skeleton is read from Flash */
		Idle, //!< Idle
		ScanForElement, //!< ScanForElement
		ElementOpened, //!< ElementOpened
		IgnoreElement, //!< IgnoreElement
		StartElement, //!< StartElement
		AttributesOrEmptyElement,//!< AttributesOrEmptyElement
		EmptyElement, //!< EmptyElement
		EndElement, //!< EndElement
		EndDocument, //!< EndDocument
		/* States where XmlHandlers fill content into the message */
		AfterStartElement, //!< AfterStartElement
		AfterEndElement, //!< AfterEndElement
		AfterEmptyElement, //!< AfterEmptyElement
		/* States for poll requests */
		PollStartElement, //!< PollStartElement
		PollContent, //!< PollContent
		PollEndElement,	//!< PollEndElement
		AfterPollEndElement
	};

	Sink0* output_endDocument;

	ActivityFunctor<XmlWriter, &XmlWriter::run_startMessage> act_startMessage;
	ActivityFunctor<XmlWriter, &XmlWriter::run_bufferSent> act_bufferSent;
	ActivityFunctor<XmlWriter, &XmlWriter::run_pollRequest> act_pollRequest;

	State state;
	const char* filePos;
	char c;
	Pool* pool;
	Buffer* tag;
	Buffer* txBuffer;
	Buffer::SizeT bytesLeft; ///< bytes left in txBuffer
	XmlHandler* handler;
	XmlTagTable* table;
};

}
}

#endif /* XMLSTREAMPARSER_H_ */
