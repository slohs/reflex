/*
 * memcmp.h
 *
 *  Created on: 02.09.2011
 *      Author: sbuechne
 */

#ifndef MEMCMP_H_
#define MEMCMP_H_

namespace reflex
{

inline bool memcmp(uint8* first, uint8* second, size_t sz)
{
	for(size_t b = 0; b<sz; b++)
		if(first[b] != second[b])
			return false;
	return true;
}
}


#endif /* MEMCMP_H_ */
