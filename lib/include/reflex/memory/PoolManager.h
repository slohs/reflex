#ifndef PoolManager_h
#define PoolManager_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	PoolManager
 *
 *	Author:		Karsten Walther
 *
 *	Description: The pool manager keeps track of the pools in the system,
 *               it is mainly used to allow buffers to store only a poolID
 *				 rather than a pool pointer, so overhead per buffer is reduced.
 *
 *				 Uses a global constant definition (use an enum) with the
 *				 name MaxPoolCount. This sizes the array in that the pool
 *				 pointers are stored.
 *
 *				 Note: The PoolManager object must be initialized before any
 *                     pool.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
#include "reflex/memory/Buffer.h"
#include "reflex/memory/Pool.h"
#include "conf.h"
#include "reflex/debug/Assert.h"

namespace reflex {

class PoolManager {
public:
	PoolManager()
	{
		STATIC_ASSERT((uint16)MaxPoolCount <= (uint16)MAX_UINT8, too_many_pools_to_handle);

		//initially there are no pools registered.
		poolCount = 0;
	}

private:
	//Since PoolManager is only an internal helper to Pool and Buffer these
	//are made to friends
	friend class Pool;
	friend class Buffer;

	/**	returns the pointer to a pool
	 *
	 *	@param 	id the id of the pool
	 *	@return	the pointer to the identified pool
	 **/
	Pool* getPool(uint8 id)
	{
		Assert(id < poolCount);

		return pools[id];
	}

	/** registers a pool, is called during construction of the pools
	 *
	 *  @param pointer to the pool to register
	 *  @return the assigned poolID
	 */
	uint8 registerPool(Pool* pool);

	Pool* pools[MaxPoolCount]; ///< array which stores the pointers to the
							   ///< registered pools
	uint8 poolCount;		///< number of registered pools
};

} //end namespace reflex
#endif

