/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	PacketMuxDemux
 *
 *	Author:		Karsten Walther
 *
 *	Description:
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#ifndef PacketMuxDemux_h
#define PacketMuxDemux_h

#include "reflex/types.h"
#include "reflex/sinks/Sink.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/memory/Buffer.h"
#include "reflex/debug/Assert.h"

namespace reflex
{
template<uint8 PORTS>
class PacketMuxDemux : public Activity, public Sink1<Buffer*>, public Sink0 {
public:
	PacketMuxDemux()
	{
		queue.init(this);
	}

	void init(Sink1<Buffer*>* lowerOut);

	void init(uint8 nr, Sink1<Buffer*>* receiver, Sink0* toNotify);

	virtual void run();

	virtual void notify();

	virtual void assign(Buffer* buffer);

private:
	///only NodeConfiguration is friend since this is the place for connecting
	///things
	friend class NodeConfiguration;

	class Port : public Sink1<Buffer*> {
	public:
		virtual void assign(Buffer* buffer)
		{
			buffer->push(nr);
			queue->assign(buffer);
		}

		Queue<Buffer*>* queue;
		Sink1<Buffer*>* receiver;
		Sink0* toNotify;
		uint8 nr;
	};

	Port ports[PORTS];
	Queue<Buffer*> queue;
	uint8 currentPort;
	Sink1<Buffer*>* lowerOut;
};

template<uint8 PORTS>
void PacketMuxDemux<PORTS>::init(Sink1<Buffer*>* lowerOut)
{
	this->lowerOut = lowerOut;
}

template<uint8 PORTS>
void PacketMuxDemux<PORTS>::init(uint8 nr, Sink1<Buffer*>* receiver, Sink0* toNotify)
{
	ASSERT(nr < PORTS);
	ports[nr].queue = &queue;
	ports[nr].receiver = receiver;
	ports[nr].toNotify = toNotify;
	ports[nr].nr = nr;
}

template<uint8 PORTS>
void PacketMuxDemux<PORTS>::run()
{
	this->lock();
	Buffer* buffer = queue.get();
	if (!buffer) return;
	buffer->peek(currentPort);
	lowerOut->assign(buffer);
}

template<uint8 PORTS>
void PacketMuxDemux<PORTS>::notify()
{
	if(ports[currentPort].toNotify){
		ports[currentPort].toNotify->notify();
	}
	this->unlock();
}

template<uint8 PORTS>
void PacketMuxDemux<PORTS>::assign(Buffer* buffer)
{
	uint8 nr;
	buffer->pop(nr);

	ports[nr].receiver->assign(buffer);
}

} //end namespace reflex

#endif

