#ifndef REFLEX_LIB_VIRTUAL_TIMER_H
#define REFLEX_LIB_VIRTUAL_TIMER_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	VirtualTimer
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	A virtual timer for timeout-notification, may
 *			operate in oneshot or periodic mode
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/
#include "reflex/sinks/Sink.h"
#include "reflex/timer/VTimerBase.h"
#include "reflex/timer/VirtualTimerSupport.h"
#include "reflex/data_types/Singleton.h"

namespace reflex {

/**
 * The virtual timer
 */
class VirtualTimer : public VTimerBase {
public:
	/** operation modes for timer */
	enum OperationMode {
		PERIODIC
		,ONESHOT
	};

	/**
	 * constructor
	 * @param opmode operation mode
	 * @param prioritized mark this timer as high priority (system timer)
	 */
	VirtualTimer(OperationMode opmode = PERIODIC, bool prioritized = false);

	~VirtualTimer();

	/** connect_output
	 *  set the subsequent sink. The connected output (Sink0) may not perform
	 *  any complex tasks. Best practices says to only connect Events. However,
	 *  we also allow at least the Prescaler class.
	 *  WARNING: Under no circumstances may the connected sink directly set
	 *  a new timer value (besides 0) for ANY virtual timer! Use an event
	 *  triggered Activity for that!
	 */
	inline void connect_output(Sink0 *output)
	{
		this->output = output;
	}

	/** Triggers connected event.
	 *  Implements Sink0 interface.
	 */
	virtual void notify();

	/** set
	 *  starts (and stops) the timer
	 *  @param ticks sets the value for countdown, 0 stops the timer
	 */
	void set(Time ticks);

	/**
	 * setPriority
	 * set the priority for this timer (true or false)
	 * @param priority true for high priority timer
	 */
	void setPriority(bool priority);

	/** getSystemTime
	 * @return Time current local time value
	 */
	Time getSystemTime();


    /** getTimestamp
     * @return uint64 timestamp in hardware precision
     */
    uint64 getTimestamp();

private:
	Sink0 *output; // sink that is notified when timer fires
	data_types::Singleton<VirtualTimerSupport> support; // provides the virtualized timer
};

} //namespace reflex

#endif // REFLEX_LIB_VIRTUAL_TIMER_H
