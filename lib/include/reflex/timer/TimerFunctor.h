/*
 * TimerFunctor.h
 *
 *  Created on: 15.12.2011
 *      Author: sbuechne
 */

#ifndef TIMERFUNCTOR_H_
#define TIMERFUNCTOR_H_

#include "reflex/sinks/Event.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/scheduling/ActivityFunctor.h"

namespace reflex
{
	template<class Klasse, void (Klasse::* MemFn)(), Time ticks = 0, reflex::VirtualTimer::OperationModes opmode = reflex::VirtualTimer::PERIODIC>
	class TimerFunctor
	{
	public:
		inline TimerFunctor(Klasse& th)
		 : executer(th),
		   timer(opmode)

		{
			timer.connect_output(&tEvent);
			tEvent.init(&executer);
			timer.set(ticks);
		}

		inline void set(Time t)
		{
			timer.set(t);
		}

		inline void stop()
		{
			timer.stop();
		}

		reflex::ActivityFunctor<Klasse, MemFn> executer;
		reflex::VirtualTimer timer;
		reflex::Event tEvent;
	};
}

#endif /* TIMERFUNCTOR_H_ */
