/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#ifndef REFLEX_LIB_DATA_TYPES_FIFOQUEUE_H
#define REFLEX_LIB_DATA_TYPES_FIFOQUEUE_H

#include "reflex/data_types/ChainLink.h"
#include "reflex/data_types/Queue.h"
#include "reflex/debug/Assert.h"

namespace reflex{ namespace data_types{

template<typename T>
class FifoQueue;

template<>
class FifoQueue<ChainLink*>
	: public Queue
{
public:
	FifoQueue(){}
	void enqueue(ChainLink& obj) {this->enqueue(&obj);}
	void enqueue(ChainLink*);
	inline ChainLink* dequeue();
};


template<typename T>
class FifoQueue<T*>
	: public FifoQueue<ChainLink*>
{
public:
	FifoQueue(){}
	inline T* front() {return static_cast<T*>(FifoQueue<ChainLink*>::front());}
	inline const T* front() const{return static_cast<T*>(FifoQueue<ChainLink*>::front());}
	inline T* back() {return static_cast<T*>(FifoQueue<ChainLink*>::back());}
	inline const T* back() const{return static_cast<T*>(FifoQueue<ChainLink*>::back());}
	inline T* dequeue() {return static_cast<T*>(FifoQueue<ChainLink*>::dequeue());}
};

template<typename T>
class FifoQueue
	: public FifoQueue<ChainLink*>
{
public:
	FifoQueue(){}
	inline T& dequeue();
};



/* inlined cause it consumes less code inlined */
inline
ChainLink* FifoQueue<ChainLink*>::dequeue() {
	Assert(first);
	ChainLink* elem = first;
	if (first) {
		first = first->next;
		if(!first) last=first;
	}
	elem->unlink();
	return elem;
}

/*FIXME: make it non-inlined but currently it produces less code*/
inline
void FifoQueue<ChainLink*>::enqueue(ChainLink* value) {
	//value may not be part of any list
	Assert(value);
	Assert(!value->isLinked());
	if(first){ //if queue is not empty
		last->next = value;
	}else{		//if queue is empty
		first = value;
	}
	last = value;
	//should not be necessary cause
	//value->next = 0;
}


template<typename T>
inline T& FifoQueue<T>::dequeue()
{
	T* val=static_cast<T*>(FifoQueue<ChainLink*>::dequeue());
	Assert(val);
	return *val;
}



}} //ns data_types,reflex
#endif // FIFOQUEUE_H
