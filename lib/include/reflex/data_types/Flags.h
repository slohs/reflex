/*
 * REFLEX - Real-time Event FLow EXecutive
 *
 * A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef FLAGS_H_
#define FLAGS_H_

#include "reflex/debug/StaticAssert.h"
#include "reflex/types.h"

namespace reflex {
namespace data_types {

//! \cond
template<uint8 Size> struct SizeTrait;
template<> struct SizeTrait<1> { typedef uint8 Type; };
template<> struct SizeTrait<2> { typedef uint16 Type; };
template<> struct SizeTrait<4> { typedef uint32 Type; };
//template<> struct SizeTrait<8> { typedef uint64 Type; };
//! \endcond


/*!
 \ingroup data_types
 \brief Provides a type-safe way of storing OR-combinations of enum values.


 The \c Flags<Enum, Base> class is a template class, where \a Enum is an
 enumeration type and \a Base the underlying integral data type. It is used
 for storing combinations of enum values. If the template parameter \a Base is
 omitted, the smallest possible integral type, where \a Enum fits in, will be
 selected.

 The traditional C++ approach for storing OR-combinations of enum values is to
 use an int or uint variable. The inconvenience with this approach is that
 there's no type checking at all; any enum value can be OR'd with any other
 enum value and passed on to a function that takes an int or uint.

 REFLEX uses Flags to provide type safety.

 Example:
 \snippet DataTypesSnippets.cc FlagsDeclaration

 MyClass::OutputFlags type is simply a typedef for Flags<MyClass::OutputFlag>.
 MyClass::setOutputsEnabled() takes a MyClass::OutputFlags parameter, which
 means that any combination of MyClass::OutputFlags values, or 0, is legal:

 \snippet DataTypesSnippets.cc FlagsUsage

 If you try to pass a value from another enum or just a plain integer other
 than 0, the compiler will report an error. If you need to cast integer values
 to flags in a untyped fashion, you can use the Flags::toFlag() function.
 If you want to use Flags for your own enum types, use the DECLARE_FLAGS()
 and DECLARE_OPERATORS_FOR_FLAGS() macros.

 \note The idea for this solution, parts of the code and documentation are
 copied from the QFlags class of the Qt framework.

 \todo Add the \c constexpr keyword which is supported as from gcc4.7.

 */
template<typename Enum, typename Base = typename SizeTrait<sizeof(Enum)>::Type>
class Flags {

private:
	typedef void** Zero;
	Base i;

public:
	//! Typedef for the underlying integral \a base template type.
	typedef Base base_type;

	//! Typedef for the \a Enum template type.
	typedef Enum enum_type;

	//! Constructs a copy of \a other.
	inline Flags(const Flags &other) : i(other.i) {}

	//! Constructs a Flags object storing the given flag.
	inline Flags(Enum flag) : i(flag) {}

	/*!
	 Constructs a Flags object with no flags set. \a zero must be a literal
	 0 value.
	 */
	inline Flags(Zero zero = 0) : i(0) { (void)zero; }

	//! Assigns \a other to this object and returns a reference to this object.
	inline Flags &operator=(const Flags &other) { i = other.i; return *this; }

	//! \overload
	inline Flags &operator&=(Base mask) { i &= mask; return *this; }

	/*!
	 Performs a bitwise OR operation with \a other and stores the result in this
	 Flags object. Returns a reference to this object.
	 */
	inline Flags &operator|=(Flags other) { i |= other.i; return *this;	}

	//! \overload
	inline Flags &operator|=(Enum other) { i |= other; return *this; }

	/*!
	 Performs a bitwise XOR operation with \a other and stores the result in this
	 Flags object. Returns a reference to this object.
	 */
	inline Flags &operator^=(Flags other) { i ^= other.i; return *this; }

	//! \overload
	inline Flags &operator^=(Enum other) { i ^= other; return *this; }

	//! Returns the value stored in the Flags object as an integer.
	inline operator Base() const { return i; }

	/*!
	 Returns a Flags object containing the result of the bitwise OR operation
	 on this object and \a other.
	 */
	inline Flags operator|(Flags other) const { return Flags(Enum(i | other.i)); }

	//! \overload
	inline Flags operator|(Enum other) const { return Flags(Enum(i | other)); }

	/*!
	Returns a Flags object containing the result of the bitwise XOR operation
	on this object and \a other.
	 */
	inline Flags operator^(Flags other) const { return Flags(Enum(i ^ other.i)); }

	//! \overload
	inline Flags operator^(Enum other) const { return Flags(Enum(i ^ other)); }

	/*!
	 Returns a Flags object containing the result of the bitwise AND operation
	 on this object and \a mask.
	 */
	inline Flags operator&(Base mask) const { return Flags(Enum(i & mask)); }

	//! \overload
	inline Flags operator&(Enum other) const { return Flags(Enum(i & other)); }

	/*!
	 Returns a Flags object that contains the bitwise negation of this object.
	 */
	inline Flags operator~() const { return Flags(Enum(~i)); }

	/*!
	 Returns true if no flag is set (i.e., if the value stored by the Flags
	 object is 0); otherwise returns false.
	 */
	inline bool operator!() const { return !i; }

	inline bool testFlag(Enum flag) const;
	inline static Flags fromInt(Base value);
};

//! Returns true if the \a flag is set, otherwise false.
template<typename Enum, typename Base>
bool Flags<Enum, Base>::testFlag(Enum flag) const {
	return (i & flag) == flag && (flag != 0 || i == Base(flag));
}


/*!
 Converts \a value of an integral base type into a Flag in a not-typesafe
 manner. Using this function is not reommended, but sometimes necessary.
 */
template<typename Enum, typename Base>
Flags<Enum, Base> Flags<Enum, Base>::fromInt(Base value) {
	Flags f;
	f.i = value;
	return f;
}

}
}

/*!
 \relates reflex::data_types::Flags

 The DECLARE_FLAGS() macro expands to
 \code typedef Flags<Enum> FlagsType; \endcode

 \a Enum is the name of an existing enum type, whereas \a FlagsType is the name
 of the Flags<Enum> typedef. See the Flags documentation for details.

 \see reflex::data_types::Flags, DECLARE_OPERATORS_FOR_FLAGS().

 */
#define DECLARE_FLAGS(FlagsType, Enum)\
typedef reflex::data_types::Flags<Enum> FlagsType;

/*!
 \relates reflex::data_types::Flags

 The DECLARE_OPERATORS_FOR_FLAGS() macro declares global \c operator|() functions
 for \a FlagsType, which is of type Flags<T>. See the Flags documentation for details.

 \see reflex::data_types::Flags, DECLARE_FLAGS()
 */
#define DECLARE_OPERATORS_FOR_FLAGS(FlagsType) \
	inline reflex::data_types::Flags<FlagsType::enum_type> operator|(FlagsType::enum_type f1, FlagsType::enum_type f2) \
		{ return reflex::data_types::Flags<FlagsType::enum_type>(f1) | f2; } \
	inline reflex::data_types::Flags<FlagsType::enum_type> operator|(FlagsType::enum_type f1, reflex::data_types::Flags<FlagsType::enum_type> f2) \
		{ return f2 | f1; }

#endif /* FLAGS_H_ */
