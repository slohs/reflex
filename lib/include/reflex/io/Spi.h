#ifndef SPI_H_
#define SPI_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
namespace reflex
{

/**
 \brief Generic SPI driver interface.

 The Serial Peripheral Interface (SPI) is a widely used peripheral interface to
 connect different IO devices to a controller. All devices share signals for
 clock (SCK), Master Out Slave In (MOSI) and Master In Slave Out (MISO).
 An additional signal is needed to address a single chip and activate its
 communication interface (CS).

 As the most comon configuration, the microcontroller acts as bus master, which
 initiates any data transfer. It selects a device via CS and sends a datagram
 out on the MOSI pin. The slave answers synchronously to the clock it sees on
 SCK. In the case, that the master only wants to read from the slave, it has to
 send dummy data which has the size of the expected datagram.

 Spi can be used to create platform-independent device drivers. Data is transfered
 via readWrite() which accesses the hardware synchronously. It is very likely,
 that one reads a few bytes from a device and - depending on that result -
 immediatly transfers another block.
 For most applications this is the fastest and most convenient way to utilize the
 SPI interface.

 Any SPI driver implementation should consider:
 - Initialize the SPI hardware during construction.
 - Offer different configuration options e.g. for clock speed.
 - Configure and set the appropriate IO pins.

 \attention Ongoing transfers may be interrupted by other activities or interrupt handlers,
 but never by another instance of an SPI driver. The application developer is responsible
 to ensure that.
 */
class Spi
{
public:
	/**
	 \brief Sends and receives data synchronously.

	 This method transfers data from a given \a address with a defined
	 \a length to the slave device. The incoming data immediately replaces all
	 data at \a address.

	 The pin for chip select must not be enabled in this method. This allows
	 multiple method calls during one data transfer.
	 */
	virtual void readWrite(void* address, size_t length) = 0;

	/**
	 \brief Sends and receives data synchronously.

	 \overload
	 */
	template<typename T>
	void readWrite(T& data)
	{
		this->readWrite(&data, sizeOf(data));
	}

	/*!
	 \brief Enables and disables the chip select pin (CS).
	 */
	virtual void setCsEnabled(bool enabled) = 0;

};
}

#endif /* SPI_H_ */
