#ifndef InteractiveElement_h
#define InteractiveElement_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses): InteractiveElement
 *
 *	Author:		Karsten Walther
 *
 *	Description: Base class for remote controllable objects.
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "NodeConfiguration.h"
#include "reflex/io/OutputElement.h"

namespace reflex {

/** Is the interface for remote controllable object, provides methods for
 *  setting and unsetting the object and methods for reading object information.
 *  Class requires an outputchannel object as member of NodeConfiguration named
 *  out.
 */
class InteractiveElement : public OutputElement {
public:
	/**	Initializes the identifier string of the object, which can be
	 *  used for object identification on remote controlling side.
	 *
	 *  @param name pointer to the human readable identifier string
	 */
	InteractiveElement(const char* name) : OutputElement(name)
	{
	}

	/** Method for dynamically change of the identifier string.
	 *
	 *  @param name pointer to the identifier string
	 */
	void init(const char* name);

	/** Allows remote setting of the object to a wanted state. The parameter is
	 *  a pointer to the data, which must be interpreted by the specific
	 *  implementation. The object state is not changed locally until unforce
	 *  is called.
	 *
	 *  @param data pointer to the datafield
	 */
	virtual	void force(void* data)=0;

	/** Releases the object from remote control.
	 */
	virtual void unforce()=0;

	/** Forces the object to publish its state.
	 */
	virtual void writeValue()=0;

};

} //end namespace reflex


#endif
