#ifndef IOManager_h
#define IOManager_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses): IOManager
 *
 *	Author:		Karsten Walther
 *
 *	Description: Manager for remote controllable elements in the system,
 * 			     interpretes some commands and propagates controlling data
 *				 to the elements.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/sinks/Sink.h"
#include "reflex/io/OutputChannel.h"
#include "conf.h"

namespace reflex {

//Forward declaration for solving circular dependencies
class InteractiveElement;
class OutputElement;

/** The manager keeps a list of interactive and output elements. Each element is given
 *  an ID, which is used for communication. Manager allows to print list of
 *  registered element, values of elements an (un-)forcing of them. IOManager
 *  requires a standard outputchannel called out in the NodeConfiguration object.
 *  Further the number of output elements must be specified in file conf.h
 *  by the application.
 */
class IOManager : public Sink1<char>{
public:
	/** Initializes receive state.
	 */
	IOManager(OutputChannel* out);

	/** Inserts an element into the list of output objects
	 *
	 *  @param obj pointer to the OutputElement object
	 */
	char registerOutputElement(OutputElement* obj);

	/** Input for the bytes received by the underlying network.
	 *
	 *  @param data last received byte
	 */
	virtual void assign(char data);

private:
	OutputElement* getOutput(unsigned char id);

	/** Prints id, name and type information of the element.
	 *
	 *  @param elem pointer to the element of interest
	 */
	void printElement(OutputElement* elem);

	/** Prints the number of registered elements.
	 */
	void printCount();

	/** Array of pointers to the registered elements.
	 */
	OutputElement* elements[NrOfOutputElements];

	//count of registered elements
	unsigned char idCount;

	//receive buffer for incoming commands
	char buffer[20];
	//writeposition for receive buffer
	char pos;
};

} //end namespace reflex

#endif
