/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_PMC_H
#define REFLEX_MSP430X_PMC_H


#include "reflex/mcu/msp430x.h"

#include "reflex/flash/Registers.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/types.h"

namespace reflex {
namespace msp430x {

/**
 * Flash driver of the cc430 series.
 * @TODO: convert the flash driver into a event driven one??
 */
class FLASH
{
public:
	enum FlashConstants {
		BLOCKSIZE = 128,
		SEGMENTSIZE = 512
	};


	FLASH();


	/**
	 * erases from start size bytes. This is segment based, so all segments
	 * touched are erased.
	 *
	 * @param start address within segment to start erase
	 * @param size to erase
	 * @return success
	 */
	static bool erase(caddr_t start, size_t size);

	/** uses byte and long write since block write can only be initiated from
	 *  RAM
	 *
 	 * @param src source address to take data from
 	 * @param dest destination address to write too
 	 * @param size number of bytes to write
 	 * @return success
	 */
 	static bool write(caddr_t src, caddr_t dest, size_t size);


 	/**
 	 * basically the same like writeBytes but will ERASE the flash to write
 	 *
 	 * @param src source address to take data from
 	 * @param dest destination address to write too
 	 * @param size number of bytes to write
 	 * @return success
 	 */
 	static bool copy(caddr_t src, caddr_t dest, size_t size);


	/**
	 * dummy method for code update, simple memcopy
	 *
 	 * @param src source address to take data from
 	 * @param dest destination address to write too
 	 * @param size number of bytes to read
	 */
	static void read(caddr_t dest, caddr_t src, size_t size);




//private:

	/**
	 *  writes a long into flash memory
	 *
	 * @param src piiter to value to write
	 * @param dest target address
	 * @return success
	 */
	static bool writeLong(caddr_t src, caddr_t dest);

	/**
	 *  writes a word into flash memory
	 *
	 * @param pos target address
	 * @param 16bit value to write
	 * @return success
	 */
	static bool writeWord(uint16* pos, uint16 value);

	/**
	 *  writes a byte into flash memory
	 *
	 * @param pos target address
	 * @param 8bit value to write
	 * @return success
	 */
	static bool writeByte(char* pos,char value);



};

}} //ns msp430x


#endif // FLASH
