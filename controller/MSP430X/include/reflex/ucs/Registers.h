/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_UCS_REGISTERS_H
#define REFLEX_UCS_REGISTERS_H

#include "reflex/mcu/msp430x.h"
#include "reflex/types.h"
#include "reflex/data_types.h"

namespace reflex {
namespace msp430x {
namespace ucs {


 enum RegisterBITS {
   //UCSCTL0
   MODx = 0x00F80
   ,DCOx = 0x1F00
   //UCSCTL1
   ,DISMOD = 0x0001
   ,DCORSELx = 0x0070
   ,DCORSEL0 = 0x0000
   ,DCORSEL1 = 0x0010
   ,DCORSEL2 = 0x0020
   ,DCORSEL3 = 0x0030
   ,DCORSEL4 = 0x0040
   ,DCORSEL5 = 0x0050
   ,DCORSEL6 = 0x0060
   ,DCORSEL7 = 0x0070
   //UCSCTL2
   ,FLLNx = 0x03FF
   ,FLLDx = 0x7000
   ,FLLD1 = 0x0000
   ,FLLD2 = 0x1000
   ,FLLD4 = 0x2000
   ,FLLD8 = 0x3000
   ,FLLD16 = 0x4000
   ,FLLD32 = 0x5000
   //UCSCTL3
   ,FLLREFDIVx = 0x0007
   ,FLLREFDIV1 = 0x0000
   ,FLLREFDIV2 = 0x0001
   ,FLLREFDIV4 = 0x0002
   ,FLLREFDIV8 = 0x0003
   ,FLLREFDIV12 = 0x0004
   ,FLLREFDIV16 = 0x0005
   ,SELREFx = 0x0070
   ,SELREF0 = 0x0000
   ,SELREF1 = 0x0020
   ,SELREF2 = 0x0030
   //UCSCTL4
   ,SELMx = 0x0007
   ,SELMxt1 = 0x0000
   ,SELMvlo = 0x0001
   ,SELMrefo = 0x0002
   ,SELMdco = 0x0003
   ,SELMdcodiv = 0x0004
   ,SELMxt2 = 0x0005
   ,SELSx = 0x0070
   ,SELSxt1 = 0x0000
   ,SELSvlo = 0x0010
   ,SELSrefo = 0x0020
   ,SELSdco = 0x0030
   ,SELSdcodiv = 0x0040
   ,SELSxt2 = 0x0050
   ,SELAx = 0x0007
   ,SELAxt1 = 0x0000
   ,SELAvlo = 0x0100
   ,SELArefo = 0x0200
   ,SELAdco = 0x0300
   ,SELAdcodiv = 0x0400
   ,SELAxt2 = 0x0500
   //UCSCTL5
   ,DIVMx = 0x0007
   ,DIVM1 = 0x0000
   ,DIVM2 = 0x0001
   ,DIVM4 = 0x0002
   ,DIVM8 = 0x0003
   ,DIVM16 = 0x0004
   ,DIVM32 = 0x0005
   ,DIVSx = 0x0070
   ,DIVS1 = 0x0000
   ,DIVS2 = 0x0010
   ,DIVS4 = 0x0020
   ,DIVS8 = 0x0030
   ,DIVS16 = 0x0040
   ,DIVS32 = 0x0050
   ,DIVAx = 0x0700
   ,DIVA1 = 0x0000
   ,DIVA2 = 0x0100
   ,DIVA4 = 0x0200
   ,DIVA8 = 0x0300
   ,DIVA16 = 0x0400
   ,DIVA32 = 0x0500
   ,DIVPAx = 0x7000
   ,DIVPA1 = 0x0000
   ,DIVPA2 = 0x1000
   ,DIVPA4 = 0x2000
   ,DIVPA8 = 0x3000
   ,DIVPA16 = 0x4000
   ,DIVPA32 = 0x5000
   //UCSCTL6
   ,XT1OFF = 0x0001
   ,SMCLKOFF = 0x0002
   ,XCAPx = 0x000C
   ,XCAP0 = 0x0000
   ,XCAP1 = 0x0004
   ,XCAP2 = 0x0008
   ,XCAP3 = 0x000C
   ,XT1BYPASS = 0x0010
   ,XTS = 0x0020
   ,XT1DRIVEx = 0x00C0
   ,XT1DRIVE0 = 0x0000
   ,XT1DRIVE1 = 0x0040
   ,XT1DRIVE2 = 0x0080
   ,XT1DRIVE3 = 0x00C0
   ,XT2OFF = 0x0100
   //UCSCTL7
   ,DCOFFG = 0x0001
   ,XT1LFOFFG = 0x0002
   ,XT1HFOFFG = 0x0004
   ,XT2OFFG = 0x0008
   //UCSCTL78
   ,UCSCTL7 = 0x0001
   ,MCLKREQEN = 0x0002
   ,SMCLKREQEN = 0x0004
   ,MODOSCREQEN = 0x0008
 };

	enum ClockSource {
	   XT1CLK = 0x0
	   ,VLOCLK = 0x1
	   ,REFOCLK = 0x2
	   ,DCOCLK = 0x3
	   ,DCOCLKDIV = 0x4
	   ,XT2CLK = 0x5
	   ,ClkSrcMASK = XT1CLK|VLOCLK|REFOCLK|DCOCLK|DCOCLKDIV|XT2CLK
	};
	enum Clock {
	   MCLK = 0
	  ,SMCLK = 4
	  ,ACLK = 8
	};

	enum frequencyMultiplicator
		{		  
		  Mhz1 = 32
		  ,Mhz2_45 = 79
		  ,Mhz8 = 249
		  ,Mhz12 = 379
		  ,Mhz16 = 489 // 16,05632 ab hier unsicher
		  ,Mhz20 = 609 // 19.968480
		  ,Mhz25 = 759 // 24,90368
		};


 namespace aclk {
	enum ClockSource {
	   XT1CLK = SELAxt1
	   ,VLOCLK = SELAvlo
	   ,REFOCLK = SELArefo
	   ,DCOCLK = SELAdco
	   ,DCOCLKDIV = SELAdcodiv
	   ,XT2CLK = SELAxt2
	   ,ClkSrcMASK = XT1CLK|VLOCLK|REFOCLK|DCOCLK|DCOCLKDIV|XT2CLK
	};
 } //ns aclk
 namespace mclk {
	enum ClockSource {
	   XT1CLK = SELMxt1
	   ,VLOCLK = SELMvlo
	   ,REFOCLK = SELMrefo
	   ,DCOCLK = SELMdco
	   ,DCOCLKDIV = SELMdcodiv
	   ,XT2CLK = SELMxt2
	   ,ClkSrcMASK = XT1CLK|VLOCLK|REFOCLK|DCOCLK|DCOCLKDIV|XT2CLK
	};
 } //ns mclk
 namespace smclk {
	enum ClockSource {
	   XT1CLK = SELSxt1
	   ,VLOCLK = SELSvlo
	   ,REFOCLK = SELSrefo
	   ,DCOCLK = SELSdco
	   ,DCOCLKDIV = SELSdcodiv
	   ,XT2CLK = SELSxt2
	   ,ClkSrcMASK = XT1CLK|VLOCLK|REFOCLK|DCOCLK|DCOCLKDIV|XT2CLK
	};
 } //ns smclk

	/** Registerfile for the Unified Clock System module
	 */
	struct Registers
	{
	   volatile unsigned int  UCSCTL0;   ///UCSCTL0 at address 0x0160,
	   volatile unsigned int  UCSCTL1;   ///UCSCTL1 at address 0x0162,
	   volatile unsigned int  UCSCTL2;   ///UCSCTL2 at address 0x0164,
	   volatile unsigned int  UCSCTL3;   ///UCSCTL3 at address 0x0166,
	   volatile unsigned int  UCSCTL4;   ///UCSCTL4 at address 0x0168,
	   volatile unsigned int  UCSCTL5;   ///UCSCTL5 at address 0x016A,
	   volatile unsigned int  UCSCTL6;   ///UCSCTL6 at address 0x016C,
	   volatile unsigned int  UCSCTL7;   ///UCSCTL7 at address 0x016E,
	   volatile unsigned int  UCSCTL8;   ///UCSCTL8 at address 0x0170,
	public:
		Registers(){}
		operator Registers*() {return operator->();}
		Registers* operator-> () {return reinterpret_cast<Registers*>(bases::UCS+offsets::UCS);}
	 };

}//ns ucs

}}//ns msp430,reflex


#endif // REGISTERS_H
