/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_PMM_REGISTERS_H
#define REFLEX_MSP430X_PMM_REGISTERS_H

#include "reflex/data_types.h"
#include "reflex/types.h"
#include "reflex/mcu/msp430x.h"

namespace reflex {
namespace msp430x {
	namespace pmm {

		enum BITS {
			//PMMCTL0
			PMMCOREVx = 0x0003
			,PMMCOREV0 = 0x0000
			,PMMCOREV1 = 0x0001
			,PMMCOREV2 = 0x0002
			,PMMCOREV3 = 0x0003
			,PMMSWBOR = 0x0004
			,PMMSWPOR = 0x0008
			,PMMREGOFF = 0x0010
			,PMMHPMRE = 0x0080
			,PMMPW = 0xA500
			//PMMCTL1
			,PMMREFMD = 0x0001
			,PMMCMD00 = 0x0030
			,PMMCMDx = 0x0030
			,PMMCMD01 = 0x0000
			,PMMCMD10 = 0x0010
			,PMMCMD11 = 0x0020
			//SVSMHCTL
			,SVSMHRRLx = 0x0007 //???????
			,SVSMHRRL0 = 0x0001 //???????
			,SVSMHDLYST = 0x0008
			,SVSHMD = 0x0010
			,SVSMHEVM = 0x0040
			,SVSMHACE = 0x0080
			,SVSHRVLx = 0x0300 //???
			,SVSHRVL0 = 0x0100
			,SVSHE = 0x0400
			,SVSHFP = 0x0800
			,SVMHOVPE = 0x1000
			,SVMHE = 0x4000
			,SVMHFP = 0x8000
			//SVSMLCTL
			,SVSMLRRLx = 0x0007 //???????
			,SVSMLRRL0 = 0x0001 //???????
			,SVSMLDLYST = 0x0008
			,SVSLMD = 0x0010
			,SVSMLEVM = 0x0040
			,SVSMLACE = 0x0080
			,SVSLRVLx = 0x0300 //???
			,SVSLRVL0 = 0x0100
			,SVSLE = 0x0400
			,SVSLFP = 0x0800
			,SVMLOVPE = 0x1000
			,SVMLE = 0x4000
			,SVMLFP = 0x8000
			//PMMIFG
			,SVSMLDLYIFG = 0x0001
			,SVMLIFG = 0x0002
			,SVMLVLRIFG = 0x0004
			,SVSMHDLYIFG = 0x0010
			,SVMHIFG = 0x0020
			,SVMHVLRIFG = 0x0040
			,PMMBORIFG = 0x0100
			,PMMRSTIFG = 0x0200
			,PMMPORIFG = 0x0400
			,SVSHIFG = 0x1000
			,SVSLIFG = 0x2000
			,PMMLPM5IFG = 0x8000
			//PMMRIE
			,SVSMLDLYIE = 0x0001
			,SVMLIE = 0x0002
			,SVMLVLRIE = 0x0004
			,SVSMHDLYIE = 0x0010
			,SVMHIE = 0x0020
			,SVMHVLRIE = 0x0040
			,SVSLPE = 0x0100
			,SVMLVLRPE = 0x0200
			,SVSHPE = 0x1000
			,SVMHVLRPE = 0x2000
			//PM5CTL0
			,LOCKIO = 0x0001
		};

		enum CoreVlevel {
			LEVEL0 = PMMCOREV0
			,LEVEL1 = PMMCOREV1
			,LEVEL2 = PMMCOREV2
			,LEVEL3 = PMMCOREV3
		};


		class Registers {
			typedef data_types::Register<uint16> Register;
		public:
			Register  PMMCTL0;   ///FCTL1 at address 0x0120,
			//volatile unsigned int  PMMCTL0;   ///FCTL1 at address 0x0120,
			Register  PMMCTL1;   ///FCTL1 at address 0x0122,
			Register  SVSMHCTL;   ///FCTL1 at address 0x0124,
			Register  SVSMLCTL;   ///FCTL1 at address 0x0126,
			uint16 : 16;
			uint16 : 16;
			uint16	PMMIFG;  ///FCTL3 at address 0x012C,
			Register  PMMIE;  ///FCTL4 at address 0x012E,
			Register  PM5CTL0;  ///FCTL4 at address 0x0130,

		public:
			Registers(){}
			operator Registers*() {return operator->();}
			Registers* operator-> () {return reinterpret_cast<Registers*>(bases::PMM+offsets::PMM);}

		};

	}
}} //ns msp430x, reflex

#endif // REGISTERS_H
