/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Stefan Nuernberger
 */

/* we include the .cc in the .h file (template class workaround)
* This is also the reason why the .cc is under the "include" and
* not the "src" directory (so it is found in the include path).
* However we include the .h file here, too so the IDEs aren't confused.
*/
#include "reflex/usci/USCI_UART.h"
#include "reflex/pmc/Registers.h"
#include "reflex/interrupts/InterruptLock.h"

namespace reflex {
namespace msp430x {
namespace usci {

template<typename Registers>
USCI_UART<Registers>::TX::TX(PowerManageAble *rx)
	: PowerManageAble(SECONDARY)
{
	this->setSleepMode(LPM4); // for transmit
	this->rx = rx;
}

template<typename Registers>
USCI_UART<Registers>::RX::RX()
{
}

/** Configure USCI module for UART
  */
template<typename Registers>
USCI_UART<Registers>::USCI_UART() :
	PowerManageAble(PRIMARY),
	usci_iv( Registers::Interrupt_traits::globalVector() ),
	tx(this)
{
	this->setSleepMode(LPM4); // for receive

	// register ISRs
	usci_iv[Registers::Interrupt_traits::RX_BR] = &rx;
	usci_iv[Registers::Interrupt_traits::TX_BE] = &tx;

	/* set up USCI module in UART mode, 2400 baud aclk */
//	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
//	Registers()->UCCTL1 |= UCSSELaclk;	// CLK = ACLK
//	Registers()->UCCTL0 = 0x00;		// UART mode
//	Registers()->UCBR0 = 0x0d;		// 32kHz/2400=13.xx (see User's Guide)
//	Registers()->UCBR1 = 0x00;		//
//	Registers()->UCMCTL = UCBRSx & 0x0c;	// modulation for 2400 baud with ACLK (see User's Guide)

	/* set up USCI module in UART mode, 9600 baud aclk */
	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
	Registers()->UCCTL1 |= UCSSELaclk;	// CLK = ACLK
	Registers()->UCCTL0 = 0x00;		// UART mode
	Registers()->UCBR0 = 0x03;		// 32kHz/9600=3.xx (see User's Guide)
	Registers()->UCBR1 = 0x00;		//
	Registers()->UCMCTL = UCBRSx & 0x06;	// modulation for 9600 baud with ACLK (see User's Guide)

	/* set up USCI module in UART mode, 9600 baud smclk */
//	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
//	Registers()->UCCTL1 |= UCSSELsmclk;	// CLK = SMCLK (8 MHz)
//	Registers()->UCCTL0 = 0x00;		// UART mode
//	Registers()->UCBR0 = 0x41;		// 8 MHz/9600=833.xx (see User's Guide)
//	Registers()->UCBR1 = 0x03;		// ditto
//	Registers()->UCMCTL = UCBRSx & 0x04;	// modulation for 9600 baud with SMCLK (see User's Guide)

	/* set up USCI module in UART mode, 19200 baud smclk */
//	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
//	Registers()->UCCTL1 |= UCSSELsmclk;	// CLK = SMCLK (8 MHz)
//	Registers()->UCCTL0 = 0x00;		// UART mode
//	Registers()->UCBR0 = 0xa0;		// 8 MHz/19200=416.xx (see User's Guide)
//	Registers()->UCBR1 = 0x01;		// ditto
//	Registers()->UCMCTL = UCBRSx & 0x0c;	// modulation for 19200 baud with SMCLK (see User's Guide)

	/* set up USCI module in UART mode, 115200 baud smclk */
//	Registers()->UCCTL1 |= UCSWRST;		// **Put state machine in reset**
//	Registers()->UCCTL1 |= UCSSELsmclk;	// CLK = SMCLK (8 MHz)
//	Registers()->UCCTL0 = 0x00;		// UART mode
//	Registers()->UCBR0 = 0x45;		// 8 MHz/115200=69.xx (see User's Guide)
//	Registers()->UCBR1 = 0x00;		//
//	Registers()->UCMCTL = UCBRSx & 0x08;	// modulation for 115200 baud with SMCLK (see User's Guide)

	/* FIXME: This should use some sort of port mapping configuration object
	 * It is currently aimed at TX = Button::M1, RX = Button::M2 on EZChronos */
	/* set up port mapping here... */
	Port2()->map(1, pmc::UCA0TXD);
	Port2()->map(2, pmc::UCA0RXD);
	// set pins to mapped (USCI-UART) functionality
	Port2()->DIR |= 0x04; /* Button::M1 as output pin */
	Port2()->SEL |= (0x04 /* Button::M1 */ + 0x02 /* Button::M2 */);
}

/** ISR for received characters
 */
template<typename Registers>
void USCI_UART<Registers>::RX::notify()
{
	// send received character through event channel
	if (output) output->assign(Registers()->UCRXBUF);
	// RXIFG cleared by IV access in InterruptDispatcher
}

/** ISR for successful transmit
 */
template<typename Registers>
void USCI_UART<Registers>::TX::notify()
{
	// TXIFG cleared by IV access in InterruptDispatcher
	this->switchOff();
	if (ready) ready->notify();
}

/** asynchronous transmit of a single byte
 */
template<typename Registers>
void USCI_UART<Registers>::TX::assign(uint8 ch)
{
	// wait till previous transfer finished
	while (this->isEnabled());
	this->switchOn(); // enable USCI for transmit
	// set next character to transmit
	Registers()->UCTXBUF = ch;
}

//! PowerManager enable TX interrupt
template<typename Registers>
void USCI_UART<Registers>::TX::enable() {
	if (!rx->isEnabled()) {
		// **Initialize USCI state machine**
		Registers()->UCCTL1 &= (uint8) ~UCSWRST;
	}
	// enable TX interrupt
	Registers()->UCIE |= UCTXIE;
}

//! PowerManager disable TX interrupt
template<typename Registers>
void USCI_UART<Registers>::TX::disable() {
	if (!rx->isEnabled()) {
		// **Put state machine in reset**
		Registers()->UCCTL1 |= UCSWRST;
	}
	// disable TX interrupt
	Registers()->UCIE &= (uint8) ~UCTXIE;
}

//! PowerManager enable RX interrupt
template<typename Registers>
void USCI_UART<Registers>::enable() {
	if (!tx.isEnabled()) {
		// **Initialize USCI state machine**
		Registers()->UCCTL1 &= (uint8) ~UCSWRST;
	}
	// enable RX interrupt
	Registers()->UCIE |= UCRXIE;
}

//! PowerManager disable RX interrupt
template<typename Registers>
void USCI_UART<Registers>::disable() {
	if (!tx.isEnabled()) {
		// **Put state machine in reset**
		Registers()->UCCTL1 |= UCSWRST;
	}
	// disable RX interrupt
	Registers()->UCIE &= (uint8) ~UCRXIE;
}

} // usci
} // msp430x
} // reflex
