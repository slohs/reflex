/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 USCI_SPI
 *
 *	Author:	     Andre Sieber, Stefan Nuernberger
 *
 *	Description: SPI driver.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_USCI_SPI_H
#define REFLEX_MSP430X_USCI_SPI_H

#include "reflex/usci/Registers.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/sinks/Queue.h"
#include "reflex/sinks/Sink.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/memory/Buffer.h"

namespace reflex{
namespace msp430x {
namespace usci {

template<typename Registers>
class USCI_SPI : public Activity, public PowerManageAble
{
public:

    USCI_SPI ();

    //must be configured from outside.....
    enum PINS {
        PIN_CLK = 0x80,
        PIN_MISO = 0x40,
        PIN_MOSI = 0x20,
        PIN_CSN = 0x02
    };

    void enable();
    void disable();

    /**
    * Initialize the sender and the receiver, which use the serial
    *
    * @param receiver the object, which gets all receiving characters
    * @param sender gets notified if something was successfully sent
    */
    void init(Sink1<Buffer*>* receiver, Sink0* sender);

    /**
     * This method is called by the scheduler.
     * It starts sending the next buffer from the queue.
     */
    virtual void run();

    /**
     * All data waiting for transmission is stored in this queue
     * to get scheduled
     */
    Queue<Buffer*> input;


    uint8 send(uint8);

protected:
    /**
     * When the SCI receives data, it will be sent to the receiver
     */
    Sink1<Buffer*>* receiver;

    /**
     * The sender is notified, if last send request is finished
     */
    Sink0* sender;

    /**
     * holds the buffer, which is currently sending
     */
    Buffer* current;


    /**
     * the length of the data, which has to be sent
     */
    uint8 length;

    /**
     * the position of the byte, which is sent next
     */
    char* pos;
};

} // ns usci
} // ns msp430x
} // ns reflex

// include source file (template class workaround)
#include "USCI_SPI.cc"

#endif // REFLEX_MSP430X_USCI_SPI_H
