/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	USCI_UART
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	UART driver. NOTE: UART only available on USCI_Ax
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_USCI_UART_H
#define REFLEX_MSP430X_USCI_UART_H

#include "reflex/usci/Registers.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/interrupts/InterruptFunctor.h"

namespace reflex{
namespace msp430x {
namespace usci {

/** UART using USCI module,
 *  9600 baud full duplex through ACLK
 *  separate rx and tx interrupts
 *  single byte event interface
 *  Enable class in PowerManager to activate receive mode.
 */
template<typename Registers>
class USCI_UART : public PowerManageAble
{
private:
	typename Registers::IVDispatcher usci_iv; ///< Interrupt vector dispatcher for TX and RX

public:
	/** Configure USCI module for UART
	  */
	USCI_UART();

	//! set output for received characters
	inline void set_out_rx(Sink1<uint8> *rx) {
		this->rx.output = rx;
	}

	//! set output for successful asynchronous transmit notification
	inline void set_out_tx_ready(Sink0 *tx_ready) {
		this->tx.ready = tx_ready;
	}

	//! acquire input for asynchronous character transmit
	inline Sink1<uint8> *get_in_tx() {
		return &tx;
	}

	//! PowerManager enable (Receive mode)
	void enable();

	//! PowerManager disable (Receive mode)
	void disable();

private:
	/** Transmitter class for asynchronous transmit
	 */
	class TX : public Sink0, public Sink1<uint8>, public PowerManageAble
	{
	private:
		PowerManageAble *rx; ///< to check whether RX is active on PM change
	public:
		Sink0 *ready; ///< notified when next character may follow

		TX(PowerManageAble *rx);

		/** notify after successful transmit (interrupt routine)
		 */
		void notify();

		/** assign character to send
		 * This method loads the character into the
		 * transmit buffer and returns immediately.
		 * Successful transmit will be signaled through
		 * the tx_ready event, after which more characters
		 * may follow.
		 * @param ch, character to send
		 */
		void assign(uint8 ch);

		//! PowerManager enable
		void enable();

		//! PowerManager disable
		void disable();
	} tx; ///< transmit channel

	/** Receiver class for receiving characters
	 */
	class RX : public Sink0
	{
	public:
		Sink1<uint8> *output; ///< received characters go here

		RX();

		/** read received character from UART
		 */
		void notify();
	} rx; ///< receive channel
};

} // ns usci
} // ns msp430x
} // ns reflex

/* workaround for template class inclusion */
#include "USCI_UART.cc"

#endif // USCI_UART_H
