#ifndef REFLEX_MSP430X_IO_SD_CARD_H
#define REFLEX_MSP430X_IO_SD_CARD_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	SD_Card
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description: SD/MMC Card access through SPI. This wraps the
 *          extern "C" methods required by Petit FatFs (FileOutput.h)
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/types.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/io/GenericDiskIO.h"


namespace reflex {
namespace msp430x {

template <typename TPort, uint8 POWERPIN, uint8 CSPIN, uint8 CLKPIN, uint8 DIPIN, uint8 DOPIN>
class SD_Card : public PowerManageAble, public GenericDiskIO
{
public:
    enum Pins {
         CS = 0x1 << CSPIN
        ,CLK = 0x1 << CLKPIN
        ,DI = 0x1 << DIPIN
        ,DOUT = 0x1 << DOPIN // 'DO' already used as a macro in PFF
        ,POWER = 0x1 << POWERPIN
    };

    SD_Card();

    /* init disk */
    virtual DSTATUS disk_initialize();
    /* write partial sector */
    virtual DRESULT disk_writep(const BYTE*, DWORD);
    /* read partial sector */
    virtual DRESULT disk_readp(BYTE*, DWORD, WORD, WORD);

    /* PM functionality, switch SD_Card On/Off */
    void enable();
    void disable();

private:
    /* Definitions for MMC/SDC command */
    enum Commands {
         CMD0	= (0x40+0)	/* GO_IDLE_STATE */
        ,CMD1	= (0x40+1)	/* SEND_OP_COND (MMC) */
        ,ACMD41	= (0xC0+41)	/* SEND_OP_COND (SDC) */
        ,CMD8	= (0x40+8)	/* SEND_IF_COND */
        ,CMD16	= (0x40+16)	/* SET_BLOCKLEN */
        ,CMD17	= (0x40+17)	/* READ_SINGLE_BLOCK */
        ,CMD24	= (0x40+24)	/* WRITE_BLOCK */
        ,CMD55	= (0x40+55)	/* APP_CMD */
        ,CMD58	= (0x40+58)	/* READ_OCR */
    };
    /* Card type flags (CardType) */
    enum CardTypes {
         CT_MMC		= 0x01	/* MMC ver 3 */
        ,CT_SD1		= 0x02	/* SD ver 1 */
        ,CT_SD2		= 0x04	/* SD ver 2 */
        ,CT_SDC		= (CT_SD1|CT_SD2)	/* SD */
        ,CT_BLOCK	= 0x08	/* Block addressing */
    };

    BYTE CardType;			/* b0:MMC, b1:SDv1, b2:SDv2, b3:Block addressing */

    /* Initialize MMC control port (CS/CLK/DI:output, DO:input) */
    inline void init_port() {
        // initialize Port
        TPort()->DIR |= (CS | CLK | DI); // make CS, CLK, DI output pins
        TPort()->IE &= ~(CS | CLK | DI | DOUT); // disable interrupts on all pins
        TPort()->DIR &= ~(DOUT); // make DO input pin
    }
    /* Delay n microseconds */
    inline void dly_us(unsigned int us) {
        us <<= 3; // approx. 8 "nop"s per us
        while (us--) {
            asm(" nop");
        }
    }
    inline void bset(uint8 pins) {
        TPort()->OUT |= pins;
        dly_us(20);
    }
    inline void bclr(uint8 pins) {
        TPort()->OUT &= ~pins;
        dly_us(20);
    }
    inline uint8 btest(uint8 pins) {
        return (TPort()->IN & pins);
        dly_us(20);
    }

    /* bit banging operations */
    void release_spi();
    BYTE send_cmd(BYTE, DWORD);
    void skip_mmc(WORD);
    BYTE rcvr_mmc();
    void xmit_mmc(BYTE);
    void forward(BYTE) {}; // not using in-time processing...
};

} // ns msp430x
} // ns reflex

// template class workaround ...
#include "SD_Card.cc"

#endif // REFLEX_MSP430X_IO_SD_CARD_H
