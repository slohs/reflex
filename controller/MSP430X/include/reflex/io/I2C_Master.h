/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	 I2C_Master
 *
 *	Author:	     Stefan Nuernberger
 *
 *	Description: Bus master driver for I2C. Pure software implementation
 *               drives clock and data lines directly without hardware
 *               support. NOTE: Use a hardware assisted master if possible.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_I2C_MASTER_H
#define REFLEX_I2C_MASTER_H

#include "reflex/io/I2C.h"
#include "reflex/io/Ports.h"
#include "reflex/types.h"

/** I2C_Master
  *
  * This is an implementation for the master bus driver of an i2c bus.
  * Drivers for i2c devices (connected to the bus) hook up to this
  * class to communicate with the devices.
  *
  * This is a pure software implementation that drives the SCL (clock)
  * and SDA (data) lines directly, so timing is not very accurate.
  *
  * Currently only 7-bit addressing is supported.
  * Multi-Master mode is currently NOT supported.
  */
namespace reflex{
namespace msp430x {

template<typename TPort, uint8 CLKPIN, uint8 DATAPIN>
class I2C_Master : public devices::I2C_Master
{
public:
    /* constructor */
    I2C_Master();

    /* i2c_command
     * issue bus control command
     * @param addr, slave address
     * @param cmd, bus command
     * @return bool, true if ack received (only CHECK_ACK), false else
     */
    virtual bool i2c_command(uint8 addr, devices::i2c_control_t cmd);

    /* i2c_send
     * send a byte to the device
     * @param byte, the byte to send
     */
    virtual void i2c_send(uint8 byte);

    /* i2c_recv
     * receive a byte from the device
     * @param addr, slave address for RESTART
     * @param finish, send ACK, (NACK+)STOP, (NACK+RE)[START_WRITE|START_READ]
     * @return uint8, received byte
     */
    virtual uint8 i2c_recv(uint8 addr, devices::i2c_control_t finish);

    /* features of PowerManageAble */
    void enable();
    void disable();

private:
    enum BitmasksAndPositions {
            // bitmasks for pin operation
             SCL = 0x1 << CLKPIN // serial clock line
            ,SDA = 0x1 << DATAPIN // serial data line
    };

    bool busy; // busy flag, marks whether bus is "in use"

    /* I2C bus commands */

    /* generate_start
     * put START condition on wire
     */
    void generate_start();

    /* generate_restart
     * put REPEATED RESTART condition on wire
     */
    void generate_restart();

    /* generate_stop
     * put a STOP signal on wire
     */
    void generate_stop();

    /* send_ack
     * put an ACK on wire
     */
    void send_ack();

    /* send_nack
     * put a NACK on wire
     */
    void send_nack();

    /* recv_ack
     * try to read an ACK from wire
     * @return true if successful, false otherwise (NACK received)
     */
    bool recv_ack();
};

} // ns msp430x
} // ns reflex

// include .cc file (template class workaround)
#include "reflex/io/I2C_Master.cc"

#endif // REFLEX_I2C_MASTER_H
