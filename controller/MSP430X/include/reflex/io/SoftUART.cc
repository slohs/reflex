/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Stefan Nuernberger
 */
#include "SoftUART.h"
#include "reflex/pmc/Registers.h"
#include "reflex/timer/Registers.h"

namespace reflex {
namespace msp430x {

using namespace timer_a;

template<typename Timer, typename TPort, uint8 TXPIN, uint8 RXPIN>
SoftUART<Timer,TPort,TXPIN,RXPIN>::SoftUART()
    : PowerManageAble(PowerManageAble::PRIMARY),
      tx_bit_timing(Timer::Interrupt_traits::globalVectorCCR0(),
          *this, PowerManageAble::SECONDARY)
{
    /* select SMCLK/8, stopped */
    Timer()->TACTL = TASSEL10 | ID11 | MC00;
    Timer()->TAR = 0; // reset count register
    Timer()->TACCTL0 = OUT | SCS; // force TX output high

    /* configure port mapping
     * FIXME: this is still fixed to Timer1_A3... */
    TPort()->map(TXPIN, pmc::TA1CCR0A); // Timer1_A3 Compare Output 0
    TPort()->map(RXPIN, pmc::TA1CCR1A); // Timer1_A3 Capture Input 1

    /* configure port pins */
    TPort()->SEL |= (RX | TX); // set pins to mapped functionality
    TPort()->IE &= ~(RX | TX); // disable interrupts on all pins (not used)
    TPort()->DIR |= TX; // make TX an output pin (TA1CCR0 Compare Output)
    TPort()->DIR &= ~RX; // make RX an input pin (TA1CCR0 Capture Input)

    /* set sane defaults for members */
    tx_buf = 0;
    rx_buf = 0;

    /* deepest sleep mode for CCR0 interrupt is LPM2 */
    this->setSleepMode(LPM2);
    /* deepest sleep mode for Timer is LPM2 */
    tx_bit_timing.setSleepMode(LPM2);
}

/** assign character to send
 * This method puts the character on the wire.
 * Finished transmit will also be signaled through
 * the tx_ready event, after which more characters
 * may follow.
 * @param ch, character to send
 */
template<typename Timer, typename TPort, uint8 TXPIN, uint8 RXPIN>
void SoftUART<Timer,TPort,TXPIN,RXPIN>::assign(uint8 ch)
{
    // check for currently active transmit
    while (Timer()->TACCTL0 & CCIE);

    // transmit next character
    tx_buf = ch;
    tx_buf |= 0x100; // STOP bit marker '1'
    tx_buf <<= 0x1; // START bit marker '0'

    tx_bit_timing.switchOn(); ///< resume clock, compare mode
}

//! PowerManager enable RX interrupt
template<typename Timer, typename TPort, uint8 TXPIN, uint8 RXPIN>
void SoftUART<Timer,TPort,TXPIN,RXPIN>::enable()
{
//	Timer()->TACCTL1 = CAP | CM10 | SCS | CCIE; ///< enable capture mode
//	Timer()->TACTL |= MC10; ///< resume timer
}

//! PowerManager disable RX interrupt
template<typename Timer, typename TPort, uint8 TXPIN, uint8 RXPIN>
void SoftUART<Timer,TPort,TXPIN,RXPIN>::disable()
{
//	Timer()->TACCTL1 &= ~CCIE; ///< disable interrupt
//	if (!tx_bit_timing.isEnabled()) {
//		Timer()->TACTL &= ~MCx; ///< pause timer
//	}
}

//! handle Timer_A CCR0 interrupt for bit timing
template<typename Timer, typename TPort, uint8 TXPIN, uint8 RXPIN>
void SoftUART<Timer,TPort,TXPIN,RXPIN>::handle_tx_timer()
{
    static unsigned int tx_bit_cnt = 10; // start + 8 data + stop

    Timer()->TACCR0 = Timer()->TACCR0 + BIT_TIMING;
    if (tx_bit_cnt == 0)
    {
        tx_bit_cnt = 10; // reset count
        tx_bit_timing.switchOff(); // disable interrupt

        /* asynchronous transmit notification, can't be done directly */
        if (tx_ready) this->trigger();
        return;
    }

    if (tx_buf & 0x01)
    {
        // set logic 1 on next compare
        Timer()->TACCTL0 = (Timer()->TACCTL0 & ~OUTMODx) | OUTMOD001;
    } else {
        // set logic 0 on next compare
        Timer()->TACCTL0 = (Timer()->TACCTL0 & ~OUTMODx) | OUTMOD101;
    }
    tx_buf >>= 0x1;
    --tx_bit_cnt;
}

//! PowerManager enable TX bit timing compare interrupt
template<typename Timer, typename TPort, uint8 TXPIN, uint8 RXPIN>
void SoftUART<Timer,TPort,TXPIN,RXPIN>::enable_tx_timer()
{
    Timer()->TACCR0 = Timer()->TAR + BIT_TIMING; ///< adjust for next bit time
    Timer()->TACCTL0 |= CCIE; ///< enable interrupt
    Timer()->TACTL |= MC10; ///< configure continuous mode
}

//! PowerManager disable TX bit timing compare interrupt
template<typename Timer, typename TPort, uint8 TXPIN, uint8 RXPIN>
void SoftUART<Timer,TPort,TXPIN,RXPIN>::disable_tx_timer()
{
    Timer()->TACCTL0 = OUT | SCS; // force TX output high
    if (!this->isEnabled()) {
        Timer()->TACTL &= ~MCx; ///< pause timer
    }
}

//! asynchronous transmit notification
template<typename Timer, typename TPort, uint8 TXPIN, uint8 RXPIN>
void SoftUART<Timer,TPort,TXPIN,RXPIN>::run()
{
    // FIXME: not sure why we should wait here or for how long...
    //	__delay_cycles(25000);
    tx_ready->notify(); // notify output
}

} // msp430x
} // reflex
