#ifndef REFLEX_MSP430X_HARDWARETIMERMILLI_H
#define REFLEX_MSP430X_HARDWARETIMERMILLI_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es): HardwareTimerMilli
 *
 *	Author: Stefan Nuernberger
 *
 *	Description: HardwareTimer in millisecond precision
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/timer/HardwareTimer.h"
#include "reflex/timer/Timer0_A5.h"

namespace reflex {
namespace msp430x {

/** base for configurable hardware timer precision
 */
template<enum Timer0_A5::Precision>
class HardwareTimerPrecision;

/** base for hardware timer precision of 1kHz (ACLK/32)
 */
template<>
struct HardwareTimerPrecision<Timer0_A5::ACLK1kHz>
{
    enum Precision {
        PRECISION_SHIFT = 0, // shift from ACLK/8/4 to milliseconds
        OVERFLOW_SHIFT = 16, // shift for the overflow counter to local time
        MAX_TIMER_VALUE = MAX_UINT16 >> PRECISION_SHIFT, // largest interval
        SAFE_MINIMUM = 2 // minimum interval where it is safe to set timer directly
    };
};

/** base for hardware timer precision of 32kHz (ACLK/1)
 */
template<>
struct HardwareTimerPrecision<Timer0_A5::ACLK32kHz>
{
    enum Precision {
        PRECISION_SHIFT = 5, // shift from ACLK/1 to milliseconds
        OVERFLOW_SHIFT = 11, // shift for the overflow counter to local time
        MAX_TIMER_VALUE = MAX_UINT16 >> PRECISION_SHIFT, // largest interval
        SAFE_MINIMUM = 2 // minimum interval where it is safe to set timer directly
    };
};

/**
 * This class is an abstraction of Timer0_A5 to be used as an input
 * to a VirtualizedTimer. It configures a Timer0_A5 as its input
 * source in continuous mode. It downscales the 32khz ACLK to
 * millisecond precision, either directly by configuring the hardware divider,
 * or indirectly by shifting the 32kHz value in software. Additionally it counts
 * the Timer overflows to offer a local time value of sizeof(Time) bytes. This
 * should be used as efficient local system time. If this overflows too recently,
 * the 64bit timestamp (getTimestamp()) in hardware precision (32kHz or 1kHz)
 * may be used instead.
 */
template<Timer0_A5::Precision prec>
class HardwareTimerMilli : public HardwareTimer, public Sink0
{
private:
    enum Precision {
        PRECISION_SHIFT = HardwareTimerPrecision<prec>::PRECISION_SHIFT,
        OVERFLOW_SHIFT = HardwareTimerPrecision<prec>::OVERFLOW_SHIFT,
        MAX_TIMER_VALUE = HardwareTimerPrecision<prec>::MAX_TIMER_VALUE,
        SAFE_MINIMUM = HardwareTimerPrecision<prec>::SAFE_MINIMUM
    };

	/* Counter for overflows */
	class Counter : public Sink0 {
	public:
		/* constructor */
		Counter() : value(0) {}

		/* notify
		 * implements Sink0 interface
		 */
		void notify();

		/* get
         * @return Time the current value in Time width
		 */
        Time get() {
            return (Time) value;
		}

        /* get64
         * @return uint64 current value in full width
         */
        uint64 get64() {
            return value;
        }

	private:
        uint64 value;
	};

    Timer0_A5 *timer; ///< the real hardware timer
    Time delta; ///< time until notify
    Time time; ///< time when the timer was set
    Counter overflows; ///< overflow counter

public:
	/** constructor */
	HardwareTimerMilli();

    /** configureTimer
     * configure the underlying Timer0_A5 instance
     * @param timer the Timer0_A5 instance used by the HardwareTimer
     */
    void configureTimer(Timer0_A5 *timer);

    /** notify
	 * implements Sink0
	 * notifies output
	 */
	void notify();

	/** getNow
	 * @return current counter value (local time)
	 */
	Time getNow();

    /** getTimestamp
     * @return 64 bit counter value in hardware precision
     */
    uint64 getTimestamp();

	/** start
	 * set a timeout until next interrupt and start the timer
	 * @param ticks number of ticks until timer event
	 */
	void start(Time ticks);

	/**
	 * startAt
	 * set a new time interrupt to occur ticks after t0
	 * @param t0 time from where to set the timer
	 * @param ticks number of ticks after t0 when the timer should fire
	 */
	void startAt(Time t0, Time ticks);

	/** stop
	 * stop the hardware timer. The local time counter will still work,
	 * but the alarm counter will be turned off.
	 */
	void stop();

private:

	/** setAlarm
	 * set the alarm counter
	 */
	void setAlarm();
};

}} // msp430x,reflex

/* workaround for template class inclusion */
#include "reflex/timer/HardwareTimerMilli.cc"

#endif

