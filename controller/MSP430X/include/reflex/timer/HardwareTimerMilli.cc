/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	HardwareTimerMilli
 *
 *	Author:		Stefan Nuernberger
 **/

/* include header for IDEs / template class includes source from header */
#include "reflex/timer/HardwareTimerMilli.h"
#include "reflex/interrupts/InterruptLock.h"

namespace reflex {
namespace msp430x {

/** Counter notify - count overflow
 */
template <Timer0_A5::Precision prec>
void HardwareTimerMilli<prec>::Counter::notify() {
    ++value;
}

/** constructor
 */
template <Timer0_A5::Precision prec>
HardwareTimerMilli<prec>::HardwareTimerMilli() {
}

/** configureTimer
 * configure the underlying Timer0_A5 instance
 * @param timer the Timer0_A5 instance used by the HardwareTimer
 */
template<Timer0_A5::Precision prec>
void HardwareTimerMilli<prec>::configureTimer(Timer0_A5 *timer) {
        this->timer = timer;
        this->timer->init(*this, overflows); // register handlers for alarm and overflows
        this->timer->setPrecision(prec); // set correct precision from base
        this->timer->enableOverflow(); // enable overflow notifications
}

/** notify
 * implements Sink0
 * adjusts alarm timer and notifies output
 */
template <Timer0_A5::Precision prec>
void HardwareTimerMilli<prec>::notify() {
    timer->stopAlarm();
    setAlarm(); // set new alarm or fire if alarm time reached.
}

/** getNow
 * @return current counter value (local time)
 */
template <Timer0_A5::Precision prec>
Time HardwareTimerMilli<prec>::getNow() {
    // atomic operation
    InterruptLock lock;

    if (timer->isOverflowPending()) {
        timer->clearOverflow();
        overflows.notify();
    }

    // assemble local time
    return ((overflows.get() << OVERFLOW_SHIFT)
        | (timer->getTime() >> PRECISION_SHIFT));
}

/** getTimestamp
 * @return 64 bit counter value in hardware precision
 */
template <Timer0_A5::Precision prec>
uint64 HardwareTimerMilli<prec>::getTimestamp()
{
    // atomic operation
    InterruptLock lock;

    if (timer->isOverflowPending()) {
        timer->clearOverflow();
        overflows.notify();
    }

    // assemble hw precision timestamp
    return ((overflows.get64() << (OVERFLOW_SHIFT + PRECISION_SHIFT))
            | (timer->getTime()));
}

/** start
 * set a timeout until next interrupt and start the timer
 * @param ticks number of ticks until timer event
 */
template <Timer0_A5::Precision prec>
void HardwareTimerMilli<prec>::start(Time ticks) {
    startAt(getNow(), ticks);
}

/**
 * startAt
 * set a new time interrupt to occur ticks after t0
 * @param t0 time from where to set the timer
 * @param ticks number of ticks after t0 when the timer should fire
 */
template <Timer0_A5::Precision prec>
void HardwareTimerMilli<prec>::startAt(Time t0, Time ticks) {
    this->time = t0;
    this->delta = ticks;

    setAlarm();
}

/** stop
 * stop the hardware timer alarm
 * NOTE: THIS DOES NOT STOP THE LOCAL TIME COUNTER!
 */
template <Timer0_A5::Precision prec>
void HardwareTimerMilli<prec>::stop() {
    timer->stopAlarm();
}

/** setAlarm
 * set the alarm counter
 */
template <Timer0_A5::Precision prec>
void HardwareTimerMilli<prec>::setAlarm() {
    Time now = getNow();
    Time elapsed = now - time;

    if (elapsed >= delta) {
        // Alarm fires! Call output
        // NOTE: Possiblity of reentrance of this method
        //	 if output sets new alarm timeout in its notify().
        //	 If new time is chosen too small, we may end
        //	 up eating the stack! Be careful!
        if (output) output->notify();
        return;
    }

    Time remaining = delta - elapsed;

    /* update alarm timer */
    uint16 nextAlarm;

    /* check maximum */
    if (remaining > MAX_TIMER_VALUE) {
        remaining = MAX_TIMER_VALUE;
    }

    /* check minimum */
    if (remaining > SAFE_MINIMUM) {
        nextAlarm = (uint16)((now + remaining) << PRECISION_SHIFT);
    } else {
        nextAlarm = (uint16)((remaining << PRECISION_SHIFT) + timer->getTime());
    }

    timer->startAlarm(nextAlarm);
}

}} // msp430x, reflex
