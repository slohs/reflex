/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_RF1A_CONFIG_H
#define REFLEX_RF1A_CONFIG_H

#include "reflex/rf/Registers.h"

namespace reflex {
namespace msp430x {
namespace rf1A {


#ifndef BV
#define BV(n)      (1 << (n))
#endif


/***************************************************************
 *  SmartRF Studio(tm) Export
 *
 *  Radio register settings specifed with C-code
 *  compatible #define statements.
 *
 ***************************************************************/
//some defines taken from the example code
#define __mrfi_LENGTH_FIELD_SIZE__      1
#define __mrfi_ADDR_SIZE__              4
#define __mrfi_MAX_PAYLOAD_SIZE__       20

#define __mrfi_RX_METRICS_SIZE__        2
#define __mrfi_RX_METRICS_RSSI_OFS__    0
#define __mrfi_RX_METRICS_CRC_LQI_OFS__ 1
#define __mrfi_RX_METRICS_CRC_OK_MASK__ 0x80
#define __mrfi_RX_METRICS_LQI_MASK__    0x7F

#define __mrfi_NUM_LOGICAL_CHANS__      4
#define __mrfi_NUM_POWER_SETTINGS__     3

#define __mrfi_BACKOFF_PERIOD_USECS__   250

#define __mrfi_LENGTH_FIELD_OFS__       0
#define __mrfi_DST_ADDR_OFS__           (__mrfi_LENGTH_FIELD_OFS__ + __mrfi_LENGTH_FIELD_SIZE__)
#define __mrfi_SRC_ADDR_OFS__           (__mrfi_DST_ADDR_OFS__ + __mrfi_ADDR_SIZE__)
#define __mrfi_PAYLOAD_OFS__            (__mrfi_SRC_ADDR_OFS__ + __mrfi_ADDR_SIZE__)

#define __mrfi_HEADER_SIZE__            (2 * __mrfi_ADDR_SIZE__)
#define __mrfi_FRAME_OVERHEAD_SIZE__    (__mrfi_LENGTH_FIELD_SIZE__ + __mrfi_HEADER_SIZE__)

//unsure, I think it means without hardware security
#define  NWK_HDR_SIZE   3
#define  NWK_PAYLOAD    MAX_NWK_PAYLOAD


#define MRFI_RSSI_OFFSET    74   /* no units */
#define MRFI_LENGTH_FIELD_OFS               __mrfi_LENGTH_FIELD_OFS__
#define MRFI_LENGTH_FIELD_SIZE              __mrfi_LENGTH_FIELD_SIZE__
#define MRFI_HEADER_SIZE                    __mrfi_HEADER_SIZE__
#define MRFI_FRAME_BODY_OFS                 __mrfi_DST_ADDR_OFS__
#define MRFI_BACKOFF_PERIOD_USECS           __mrfi_BACKOFF_PERIOD_USECS__
#define MRFI_RX_METRICS_SIZE        __mrfi_RX_METRICS_SIZE__
#define MRFI_MAX_PAYLOAD_SIZE  	__mrfi_MAX_PAYLOAD_SIZE__
#define MRFI_MAX_FRAME_SIZE         (MRFI_MAX_PAYLOAD_SIZE + __mrfi_FRAME_OVERHEAD_SIZE__)

#define MRFI_RANDOM_OFFSET                   67
#define MRFI_RANDOM_MULTIPLIER              109
#define MRFI_MIN_SMPL_FRAME_SIZE            (MRFI_HEADER_SIZE + NWK_HDR_SIZE)

/* rx metrics definitions, known as appended "packet status bytes" in datasheet parlance */
#define MRFI_RX_METRICS_CRC_OK_MASK         __mrfi_RX_METRICS_CRC_OK_MASK__
#define MRFI_RX_METRICS_LQI_MASK            __mrfi_RX_METRICS_LQI_MASK__

#define MRFI_RADIO_PARTNUM          0x00
#define MRFI_RADIO_VERSION          0x06

//menzehan
#define TXFIFO_SIZE 64
#define RXFIFO_SIZE	64

/* Max time we can be in a critical section within the delay function.
 * This could be fine-tuned by observing the overhead is calling the bsp delay
 * function. The overhead should be very small compared to this value.
 */
#define MRFI_MAX_DELAY_US 16 /* usec */

/* Packet automation control - base value is power up value whick has APPEND_STATUS enabled; no CRC autoflush */
#define PKTCTRL1_BASE_VALUE         BV(2)
#define PKTCTRL1_ADDR_FILTER_OFF    PKTCTRL1_BASE_VALUE
#define PKTCTRL1_ADDR_FILTER_ON     (PKTCTRL1_BASE_VALUE | (BV(0)|BV(1)))

#ifdef MRFI_ASSERTS_ARE_ON
#define RX_FILTER_ADDR_INITIAL_VALUE  0xFF
#endif


#define MRFI_NUM_LOGICAL_CHANS 4


  /* The SW timer is calibrated by adjusting the call to the microsecond delay
   * routine. This allows maximum calibration control with repects to the longer
   * times requested by applicationsd and decouples internal from external calls
   * to the microsecond routine which can be calibrated independently.
   */
#if defined(SW_TIMER)
#define APP_USEC_VALUE    1000
#else
#define APP_USEC_VALUE    1000
#endif

#define MAX_HOPS  3
#define MRFI_CCA_RETRIES        4
#define   PLATFORM_FACTOR_CONSTANT   (2 + 2*(MAX_HOPS*(MRFI_CCA_RETRIES*(8*MRFI_BACKOFF_PERIOD_USECS)/1000)))



//#define MRFI_SETTING_IOCFG0     27

/* GDO1 == RSSI_VALID signal */
//#define MRFI_SETTING_IOCFG1     30

/* Main Radio Control State Machine control configuration:
 * - Remain RX state after RX
 * - Go to IDLE after TX
 * - RSSI below threshold and NOT receiving.
 */
//#define MRFI_SETTING_MCSM1      0x3C

/* Main Radio Control State Machine control configuration:
 * Auto Calibrate - when going from IDLE to RX/TX
 * XOSC is OFF in Sleep state.
 */
//#define MRFI_SETTING_MCSM0      (0x10)



/*
 *  Packet Length - Setting for maximum allowed packet length.
 *  The PKTLEN setting does not include the length field but maximum frame size does.
 *  Subtract length field size from maximum frame size to get value for PKTLEN.
 */
//#define MRFI_SETTING_PKTLEN     (MRFI_MAX_FRAME_SIZE - MRFI_LENGTH_FIELD_SIZE)

/* Packet automation control - Original value except WHITE_DATA is extracted from SmartRF setting. */
//#define MRFI_SETTING_PKTCTRL0   (0x05 | (SMARTRF_SETTING_PKTCTRL0 & BV(6)))

/* FIFO threshold - this register has fields that need to be configured for the CC1101 */
//#define MRFI_SETTING_FIFOTHR    (0x07 | (SMARTRF_SETTING_FIFOTHR & (BV(4)|BV(5)|BV(6)|BV(7))))





#ifndef SMARTRF_CC430_H
#define SMARTRF_CC430_H

//reengeniert settings
//#define SMARTRF_RADIO_CC430
//#define SMARTRF_SETTING_FSCTRL1    0x08
//#define SMARTRF_SETTING_FSCTRL0    0x00
//#define SMARTRF_SETTING_FREQ2      0x21
//#define SMARTRF_SETTING_FREQ1      0x71
//#define SMARTRF_SETTING_FREQ0      0x7A
//#define SMARTRF_SETTING_MDMCFG4    0x7B
//#define SMARTRF_SETTING_MDMCFG3    0x83
//#define SMARTRF_SETTING_MDMCFG2    0x13
//#define SMARTRF_SETTING_MDMCFG1    0x22
//#define SMARTRF_SETTING_MDMCFG0    0xF8
//#define SMARTRF_SETTING_CHANNR     0x00
//#define SMARTRF_SETTING_DEVIATN    0x42
//#define SMARTRF_SETTING_FREND1     0xB6
//#define SMARTRF_SETTING_FREND0     0x1D
//#define SMARTRF_SETTING_MCSM0      0x10
//#define SMARTRF_SETTING_MCSM1      0x3F//0x3C
//#define SMARTRF_SETTING_MCSM2      0x07
//#define SMARTRF_SETTING_WOREVT1    0x87
//#define SMARTRF_SETTING_WOREVT0    0x6B
//#define SMARTRF_SETTING_WORCTL     0xF8
//#define SMARTRF_SETTING_FOCCFG     0x1D
//#define SMARTRF_SETTING_BSCFG      0x1C
//#define SMARTRF_SETTING_AGCCTRL2   0xC7
//#define SMARTRF_SETTING_AGCCTRL1   0x00
//#define SMARTRF_SETTING_AGCCTRL0   0xB2
//#define SMARTRF_SETTING_FSCAL3     0xEA
//#define SMARTRF_SETTING_FSCAL2     0x2A
//#define SMARTRF_SETTING_FSCAL1     0x00
//#define SMARTRF_SETTING_FSCAL0     0x1F
//#define SMARTRF_SETTING_FSTEST     0x59
//#define SMARTRF_SETTING_TEST2      0x81
//#define SMARTRF_SETTING_TEST1      0x35
//#define SMARTRF_SETTING_TEST0      0x09
//#define SMARTRF_SETTING_PTEST      0x7F
//#define SMARTRF_SETTING_AGCTEST    0x88
//#define SMARTRF_SETTING_FIFOTHR    0x07//0x47
//#define SMARTRF_SETTING_IOCFG2     0x29
//#define SMARTRF_SETTING_IOCFG1     0x1E
//#define SMARTRF_SETTING_IOCFG0     0x06//0x1B
//#define SMARTRF_SETTING_PKTCTRL1   0x00
//#define SMARTRF_SETTING_PKTCTRL0   0x05
//#define SMARTRF_SETTING_ADDR       0x00
//#define SMARTRF_SETTING_PKTLEN     0x1E

//works, but often errors
//#define SMARTRF_SETTING_FSCTRL1    0x08
//#define SMARTRF_SETTING_FSCTRL0    0x00
//#define SMARTRF_SETTING_FREQ2      0x21
//#define SMARTRF_SETTING_FREQ1      0x74
//#define SMARTRF_SETTING_FREQ0      0xAC
//#define SMARTRF_SETTING_MDMCFG4    0xCA
//#define SMARTRF_SETTING_MDMCFG3    0x83
//#define SMARTRF_SETTING_MDMCFG2    0x13
//#define SMARTRF_SETTING_MDMCFG1    0x22
//#define SMARTRF_SETTING_MDMCFG0    0xF8
//#define SMARTRF_SETTING_CHANNR     0x00
//#define SMARTRF_SETTING_DEVIATN    0x34
//#define SMARTRF_SETTING_FREND1     0x56
//#define SMARTRF_SETTING_FREND0     0x10
//#define SMARTRF_SETTING_MCSM0      0x18
//#define SMARTRF_SETTING_MCSM1      0x3C
//#define SMARTRF_SETTING_MCSM2      0x07
//#define SMARTRF_SETTING_WOREVT1    0x87
//#define SMARTRF_SETTING_WOREVT0    0x6B
//#define SMARTRF_SETTING_WORCTL     0xF8
//#define SMARTRF_SETTING_FOCCFG     0x16
//#define SMARTRF_SETTING_BSCFG      0xC1
//#define SMARTRF_SETTING_AGCCTRL2   0x43
//#define SMARTRF_SETTING_AGCCTRL1   0x40
//#define SMARTRF_SETTING_AGCCTRL0   0x91
//#define SMARTRF_SETTING_FSCAL3     0xE9
//#define SMARTRF_SETTING_FSCAL2     0x2A
//#define SMARTRF_SETTING_FSCAL1     0x00
//#define SMARTRF_SETTING_FSCAL0     0x1F
//#define SMARTRF_SETTING_FSTEST     0x59
//#define SMARTRF_SETTING_TEST2      0x81
//#define SMARTRF_SETTING_TEST1      0x35
//#define SMARTRF_SETTING_TEST0      0x09
//#define SMARTRF_SETTING_PTEST      0x7F
//#define SMARTRF_SETTING_AGCTEST    0x88
//#define SMARTRF_SETTING_FIFOTHR    0x47
//#define SMARTRF_SETTING_IOCFG2     0x29
//#define SMARTRF_SETTING_IOCFG1     0x1E
//#define SMARTRF_SETTING_IOCFG0     0x1B
//#define SMARTRF_SETTING_PKTCTRL1   0x00
//#define SMARTRF_SETTING_PKTCTRL0   0x01
//#define SMARTRF_SETTING_ADDR       0x00
//#define SMARTRF_SETTING_PKTLEN     0x0A

////works
//#define SMARTRF_SETTING_FSCTRL1    0x0C
//#define SMARTRF_SETTING_FSCTRL0    0x00
//#define SMARTRF_SETTING_FREQ2      0x22
//#define SMARTRF_SETTING_FREQ1      0xB1
//#define SMARTRF_SETTING_FREQ0      0x3B
//#define SMARTRF_SETTING_MDMCFG4    0x2D
//#define SMARTRF_SETTING_MDMCFG3    0x3B
//#define SMARTRF_SETTING_MDMCFG2    0x13
//#define SMARTRF_SETTING_MDMCFG1    0x22
//#define SMARTRF_SETTING_MDMCFG0    0xF8
//#define SMARTRF_SETTING_CHANNR     0x14
//#define SMARTRF_SETTING_DEVIATN    0x62
//#define SMARTRF_SETTING_FREND1     0xB6
//#define SMARTRF_SETTING_FREND0     0x10
//#define SMARTRF_SETTING_MCSM0      0x18
//#define SMARTRF_SETTING_FOCCFG     0x1D
//#define SMARTRF_SETTING_BSCFG      0x1C
//#define SMARTRF_SETTING_AGCCTRL2   0xC7
//#define SMARTRF_SETTING_AGCCTRL1   0x00
//#define SMARTRF_SETTING_AGCCTRL0   0xB0
//#define SMARTRF_SETTING_FSCAL3     0xEA
//#define SMARTRF_SETTING_FSCAL2     0x2A
//#define SMARTRF_SETTING_FSCAL1     0x00
//#define SMARTRF_SETTING_FSCAL0     0x1F
//#define SMARTRF_SETTING_FSTEST     0x59
//#define SMARTRF_SETTING_TEST2      0x88
//#define SMARTRF_SETTING_TEST1      0x31
//#define SMARTRF_SETTING_TEST0      0x09
//#define SMARTRF_SETTING_FIFOTHR    0x07
//#define SMARTRF_SETTING_IOCFG2     0x29
//#define SMARTRF_SETTING_IOCFG0     0x06
//#define SMARTRF_SETTING_PKTCTRL1   0x0c //0x04
//#define SMARTRF_SETTING_PKTCTRL0   0x05
//#define SMARTRF_SETTING_ADDR       0x00
//#define SMARTRF_SETTING_PKTLEN     0xFF




//reengineered values! working, reception from original is possible
#define SMARTRF_SETTING_CHANNR     0x00

#define SMARTRF_SETTING_MCSM0      0x18
#define SMARTRF_SETTING_PKTLEN     0xFF
#define SMARTRF_SETTING_PKTCTRL0   0x05
#define SMARTRF_SETTING_FIFOTHR    0x07
#define SMARTRF_SETTING_FSCTRL1    0x08
#define SMARTRF_SETTING_FSCTRL0    0x00
#define SMARTRF_SETTING_FREQ2      0x21
#define SMARTRF_SETTING_FREQ1      0x71
#define SMARTRF_SETTING_FREQ0      0x7A
#define SMARTRF_SETTING_MDMCFG4    0x7B
#define SMARTRF_SETTING_MDMCFG3    0x83
#define SMARTRF_SETTING_MDMCFG2    0x13
#define SMARTRF_SETTING_MDMCFG1    0x22
#define SMARTRF_SETTING_MDMCFG0    0xF8
#define SMARTRF_SETTING_DEVIATN    0x42
#define SMARTRF_SETTING_FOCCFG     0x1D
#define SMARTRF_SETTING_BSCFG      0x1C
#define SMARTRF_SETTING_AGCCTRL2   0xC7
#define SMARTRF_SETTING_AGCCTRL1   0x00
#define SMARTRF_SETTING_AGCCTRL0   0xB2
#define SMARTRF_SETTING_FREND1     0xB6
#define SMARTRF_SETTING_FREND0     0x10
#define SMARTRF_SETTING_FSCAL3     0xEA
#define SMARTRF_SETTING_FSCAL2     0x2A
#define SMARTRF_SETTING_FSCAL1     0x00
#define SMARTRF_SETTING_FSCAL0     0x1F
#define SMARTRF_SETTING_TEST2      0x81
#define SMARTRF_SETTING_TEST1      0x35
#define SMARTRF_SETTING_TEST0      0x09
/**
 * new test values:
 */
//#define SMARTRF_SETTING_CHANNR     0x00
//
//#define SMARTRF_SETTING_MCSM0      0x07
//#define SMARTRF_SETTING_PKTLEN     0xFF
//#define SMARTRF_SETTING_PKTCTRL0   0x05
//#define SMARTRF_SETTING_FIFOTHR    0x07
//#define SMARTRF_SETTING_FSCTRL1    0x06
//#define SMARTRF_SETTING_FSCTRL0    0x00
//#define SMARTRF_SETTING_FREQ2      0x21
//#define SMARTRF_SETTING_FREQ1      0x62
//#define SMARTRF_SETTING_FREQ0      0x76
//#define SMARTRF_SETTING_MDMCFG4    0xf5
//#define SMARTRF_SETTING_MDMCFG3    0x83
//#define SMARTRF_SETTING_MDMCFG2    0x13
//#define SMARTRF_SETTING_MDMCFG1    0x22
//#define SMARTRF_SETTING_MDMCFG0    0xF8
//#define SMARTRF_SETTING_DEVIATN    0x15
//#define SMARTRF_SETTING_FOCCFG     0x16
//#define SMARTRF_SETTING_BSCFG      0x6C
//#define SMARTRF_SETTING_AGCCTRL2   0x03
//#define SMARTRF_SETTING_AGCCTRL1   0x40
//#define SMARTRF_SETTING_AGCCTRL0   0x91
//#define SMARTRF_SETTING_FREND1     0x56
//#define SMARTRF_SETTING_FREND0     0x10
//#define SMARTRF_SETTING_FSCAL3     0xEF
//#define SMARTRF_SETTING_FSCAL2     0x2C
//#define SMARTRF_SETTING_FSCAL1     0x18
//#define SMARTRF_SETTING_FSCAL0     0x1F
//#define SMARTRF_SETTING_TEST2      0x81
//#define SMARTRF_SETTING_TEST1      0x35
//#define SMARTRF_SETTING_TEST0      0x09

#endif

//reengineered values! working, reception from original is possible
#define MRFI_SETTING_IOCFG0     0x1B
#define MRFI_SETTING_IOCFG1     0x1E
#define MRFI_SETTING_MCSM1      0x3C
#define MRFI_SETTING_MCSM0      0x10
//#define MRFI_SETTING_PKTLEN     0x1E
#define MRFI_SETTING_PKTCTRL1   0x04 /* append mode enabled*/
#define MRFI_SETTING_PKTCTRL0   0x05 /* CRC enable + variable length*/
#define MRFI_SETTING_FIFOTHR    0x47

/**
 * new test values:
 */
//#define MRFI_SETTING_IOCFG0     0x06
//#define MRFI_SETTING_IOCFG1     0x2E
//#define MRFI_SETTING_MCSM1      0x30
//#define MRFI_SETTING_MCSM0      0x10
#define MRFI_SETTING_PKTLEN     0x3D//61 Bytes (+2 Byte Status + 1Byte Length = 64 Byte)
//#define MRFI_SETTING_PKTCTRL0   0x05
//#define MRFI_SETTING_FIFOTHR    0x7


const uint8 mrfiRadioCfg[][2] =
{
  /* internal radio configuration */
  {  IOCFG0,    MRFI_SETTING_IOCFG0       }, /* Configure GDO_0 to output PA_PD signal (low during TX, high otherwise). */
  {  IOCFG1,    MRFI_SETTING_IOCFG1       }, /* Configure GDO_1 to output RSSI_VALID signal (high when RSSI is valid, low otherwise). */
  {  MCSM1,     MRFI_SETTING_MCSM1        }, /* CCA mode, RX_OFF_MODE and TX_OFF_MODE */
  {  MCSM0,     MRFI_SETTING_MCSM0        }, /* AUTO_CAL and XOSC state in sleep */
  {  PKTLEN,    MRFI_SETTING_PKTLEN       },
  {  PKTCTRL0,  MRFI_SETTING_PKTCTRL0     },
  {  PKTCTRL1,  MRFI_SETTING_PKTCTRL1	  },/* added by menzehan*/
  {  FIFOTHR,   MRFI_SETTING_FIFOTHR      },
  {  SYNC1,     0xD3                      },/* added by menzehan*/
  {  SYNC0,     0x91                      },/* added by menzehan*/
/* imported SmartRF radio configuration */
  {  FSCTRL1,   SMARTRF_SETTING_FSCTRL1   },
  {  FSCTRL0,   SMARTRF_SETTING_FSCTRL0   },
  {  FREQ2,     SMARTRF_SETTING_FREQ2     },
  {  FREQ1,     SMARTRF_SETTING_FREQ1     },
  {  FREQ0,     SMARTRF_SETTING_FREQ0     },
  {  MDMCFG4,   SMARTRF_SETTING_MDMCFG4   },
  {  MDMCFG3,   SMARTRF_SETTING_MDMCFG3   },
  {  MDMCFG2,   SMARTRF_SETTING_MDMCFG2   },
  {  MDMCFG1,   SMARTRF_SETTING_MDMCFG1   },
  {  MDMCFG0,   SMARTRF_SETTING_MDMCFG0   },
  {  DEVIATN,   SMARTRF_SETTING_DEVIATN   },
  {  FOCCFG,    SMARTRF_SETTING_FOCCFG    },
  {  BSCFG,     SMARTRF_SETTING_BSCFG     },
  {  AGCCTRL2,  SMARTRF_SETTING_AGCCTRL2  },
  {  AGCCTRL1,  SMARTRF_SETTING_AGCCTRL1  },
  {  AGCCTRL0,  SMARTRF_SETTING_AGCCTRL0  },
  {  FREND1,    SMARTRF_SETTING_FREND1    },
  {  FREND0,    SMARTRF_SETTING_FREND0    },
  {  FSCAL3,    SMARTRF_SETTING_FSCAL3    },
  {  FSCAL2,    SMARTRF_SETTING_FSCAL2    },
  {  FSCAL1,    SMARTRF_SETTING_FSCAL1    },
  {  FSCAL0,    SMARTRF_SETTING_FSCAL0    },
  {  TEST2,     SMARTRF_SETTING_TEST2     },
  {  TEST1,     SMARTRF_SETTING_TEST1     },
  {  TEST0,     SMARTRF_SETTING_TEST0     },
};







/*
 *  Logical channel table - this table translates logical channel into
 *  actual radio channel number.  Channel 0, the default channel, is
 *  determined by the channel exported from SmartRF Studio.  The other
 *  table entries are derived from that default.  Each derived channel is
 *  masked with 0xFF to prevent generation of an illegal channel number.
 *
 *  This table is easily customized.  Just replace or add entries as needed.
 *  If the number of entries changes, the corresponding #define must also
 *  be adjusted.  It is located in mrfi_defs.h and is called __mrfi_NUM_LOGICAL_CHANS__.
 *  The static assert below ensures that there is no mismatch.
 */
static const uint8 LogicalChanTable[] =
{
  SMARTRF_SETTING_CHANNR,
  50,
  80,
  110
};

#define NUM_OF_LOGICAL_CHANS (sizeof(LogicalChanTable))
/*
 *  RF Power setting table - this table translates logical power value
 *  to radio register setting.  The logical power value is used directly
 *  as an index into the power setting table. The values in the table are
 *  from low to high. The default settings set 3 values: -20 dBm, -10 dBm,
 *  and 0 dBm. The default at startup is the highest value. Note that these
 *  are approximate depending on the radio. Information is taken from the
 *  data sheet.
 *
 *  This table is easily customized.  Just replace or add entries as needed.
 *  If the number of entries changes, the corresponding #define must also
 *  be adjusted.  It is located in mrfi_defs.h and is called __mrfi_NUM_POWER_SETTINGS__.
 *  The static assert below ensures that there is no mismatch.
 *
 * For the CC430 use the CC1100 values.
 */
static const uint8 RFPowerTable[] =
{
	 0x03	//-30dBm -> 11,9mA
	,0x0D	//-20dBm -> 12,4mA
	,0x1C	//-15dBm -> 13,0mA
	,0x34	//-10dBm -> 14,5mA
	,0x57	//-5dBm -> 15,1mA
	,0x8E	//0dBm -> 16,9mA
	,0x85	//5dBm -> 20,0mA
	,0xCC	//7dBm -> 25,8mA
	,0xC3	//10dBm -> 31,1mA
};

#define NUM_OF_POWER_SETTINGS (sizeof(RFPowerTable))



}//ns ucs

}}//ns msp430,reflex


#endif // REGISTERS_H
