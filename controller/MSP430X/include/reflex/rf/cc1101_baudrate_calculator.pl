#!/usr/bin/perl

#computes the DRATE_E and DRATE_M needed for the baudrate setting of the cc1101
#assumes fxosc is 26MHz

$fxosc = 26000000;
print "enter baudrate: ";
$baudrate = int(<STDIN>);
chomp $baudrate;


$DRATE_E = int( log2(($baudrate * 2**20)/$fxosc) );
$DRATE_M = (($baudrate * 2**28) / ($fxosc * 2**$DRATE_E)) -256;

$DRATE_E_HEX = sprintf("%x", $DRATE_E);
$DRATE_M_HEX = sprintf("%x", round($DRATE_M));

print "DRATE_E: " . $DRATE_E .  " hex: " . $DRATE_E_HEX . "\n";
print "DRATE_M: " . round($DRATE_M) .  " hex: " . $DRATE_M_HEX. "\n";


sub log2 { 
  my $n = shift; 
  return (log($n)/log(2)); 
}

sub round {
    my($number) = shift;
    return int($number + .5 * ($number <=> 0));
  }
