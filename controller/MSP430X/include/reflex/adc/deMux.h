#ifndef deMux_h
#define deMux_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	deMux
 *
 *	Author:		Andre Sieber
 *
 *	Description: Famous Commander Application.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/sinks/Sink.h"
#include "reflex/types.h"

#include "reflex/sinks/Fifo.h"
#include "reflex/sinks/Queue.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/adc/ADC12_A.h"
/** This class delivers adc data to registered sinks, definined by the adc channel
 */
class deMux : public reflex::Sink2<uint8,uint16>, public reflex::Activity
{
public:
	deMux();

	virtual void run();

	void init(reflex::Sink1<reflex::mcu::ADC12_A::Request>* adcOut) {this->adcOut = adcOut;}

	virtual void assign(uint8 channel,uint16 value);
	void registerSink(reflex::Sink1<uint16>* sink, uint8 channel);
	void removeSink(uint8 channel);
	
	reflex::Fifo1<reflex::mcu::ADC12_A::Request, (int)3> input;
	//reflex::Queue<reflex::mcu::ADC12_A::Request> input;
	//reflex::SingleValue1<reflex::mcu::ADC12_A::Request> input;
protected:
	reflex::Sink1<uint16>* sinks[15];
	reflex::Sink1<reflex::mcu::ADC12_A::Request>* adcOut; //! output to adc which engage a measure.

};

#endif
