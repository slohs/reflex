/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_MSP430X_REF_REGISTERS_H
#define REFLEX_MSP430X_REF_REGISTERS_H

#include "reflex/types.h"
#include "reflex/data_types.h"
#include "reflex/MachineDefinitions.h"

namespace reflex {
namespace msp430x{
namespace ref{
	enum Bits {
		 ON=                  (0x0001)       /* REF Reference On */
		,OUT=                 (0x0002)       /* REF Reference output Buffer On */
		//,RESERVED            (0x0004)  /* Reserved */
		,TCOFF=               (0x0008)       /* REF Temp.Sensor off */
		,VSEL0=               (0x0010)       /* REF Reference Voltage Level Select Bit:0 */
		,VSEL1=               (0x0020)       /* REF Reference Voltage Level Select Bit:1 */
		,VSEL_2=			VSEL0
		//,RESERVED            (0x0040)  /* Reserved */
		,MSTR=                (0x0080)       /* REF Master Control */
		,GENACT=              (0x0100)       /* REF Reference generator active */
		,BGACT =              (0x0200)       /* REF Reference bandgap active */
		,GENBUSY=             (0x0400)       /* REF Reference generator busy */
		,BGMODE=                 (0x0800)       /* REF Bandgap mode */

	};
	//! Registerfile for the real time clock module
	class Registers
	{
		enum { BASE_ADDR = bases::REF+offsets::REF };
		typedef Registers RegisterFile;

	public:
		typedef data_types::Register<uint16> Register;
		typedef data_types::ReadOnly<Register> RORegister;

	public:
		Register CTL0;
	public:
		Registers(){}
		operator RegisterFile*() {return operator->();}
		RegisterFile* operator-> () {return begin();}
		const RegisterFile* operator-> () const {return begin();}

	private:
		RegisterFile* begin() { return reinterpret_cast<RegisterFile*>(BASE_ADDR); }
		const RegisterFile* begin() const { return reinterpret_cast<const RegisterFile*>(BASE_ADDR); }
	};



}
}
}

#endif // REGISTERS_H
