#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# *	Author:		 Carsten Schulze
# */

ifdef DEVICE
DEFINED_REFLEX_VARS += DEVICE $(DEVICE)
endif
ifndef DEVICE
MISSING_REFLEX_VARS += DEVICE the currently used microcontroller type e.g. 149
endif

ifdef LIBPATH
DEFINED_REFLEX_VARS += LIBPATH $(LIBPATH)
endif
ifndef LIBPATH
MISSING_REFLEX_VARS += LIBPATH the path to gcc libraries
endif

SREC = $(APPLICATION).s19
HEX = $(APPLICATION).hex
EXECUTABLE = $(APPLICATION).elf
MAIN_TARGET = $(HEX)

TOOLPREFIX ?= $(TOOLPATH)msp430-

ASFLAGS +=
CFLAGS +=	-O2 -finline-small-functions \
			-fomit-frame-pointer -Wall -mmcu=$(DEVICE) \
			-pedantic -ansi -Wno-long-long \

CXXFLAGS += $(CFLAGS)\
			-fno-exceptions -fno-rtti\
			-Wno-non-virtual-dtor -fcheck-new\
			-fno-stack-check -frepo

#pronounce device, needed for the linkprocess
LDFLAGS += -mmcu=$(DEVICE) -nostdlib -nostartfiles
LDLIBS = -lgcc

#using garbage collection against unused code
CFLAGS += -ffunction-sections -fdata-sections
ifeq ($(DEBUG), 0)
	LDFLAGS += -Wl,--gc-sections
endif

#OBJCOPY_FLAGS = -O srec --srec-len 52
OBJDUMP_FLAGS = -SDx


$(SREC): $(EXECUTABLE)
	$(OBJCOPY) -O srec --srec-len 52 $(EXECUTABLE) $(SREC)

$(HEX): $(EXECUTABLE)
	$(OBJCOPY) -O ihex $^ $@

########## includes and base sources for the controller ########

INCLUDES += -I$(REFLEXPATH)/controller/$(CONTROLLER)/include
include $(REFLEXPATH)/controller/$(CONTROLLER)/Sources.mk

