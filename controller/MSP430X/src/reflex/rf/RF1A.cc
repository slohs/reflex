/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *

 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "reflex/rf/RF1A.h"

#include "reflex/debug/Assert.h"
#include "reflex/interrupts/InterruptLock.h"

using namespace reflex;
using namespace msp430x;
using namespace rf1A;

RF1A::RF1A()
{
	radioState = reflex::msp430x::rf1A::sleep;
}


//FIXME: specify interrupt sources
void RF1A::startRX(void)
{
	//Registers()->RF1AIFG &= ~0x0010;                         // Clear a pending interrupt (bit 4)
	//Registers()->RF1AIE  |= 0x0010;                          // Enable the interrupt

	Registers()->RF1AIFG &= ~0x0200;                         // Clear a pending interrupt (bit 4)
	Registers()->RF1AIE  |= 0x0200;                          // Enable the interrupt

	strobe( RF_SIDLE );
	strobe( RF_SRX );
	radioState = rx;
}

void RF1A::sleep(void)
{
	stopRX();
	strobe( RF_SXOFF );
	//@author: menzehan
	radioState = reflex::msp430x::rf1A::sleep;
}


void RF1A::stopRX(void)
{
	//Registers()->RF1AIE &= ~0x0010;                          // Disable RX interrupts
	//Registers()->RF1AIFG &= ~0x0010;                         // Clear pending IFG

	Registers()->RF1AIFG &= ~0x0200;                         // Clear a pending interrupt (bit 4)
	Registers()->RF1AIE  &= ~0x0200;                          // Enable the interrupt

	strobe( RF_SIDLE );
	strobe( RF_SFRX);      // Flush the receive FIFO of any residual data
	radioState = idle;
}

//should be the same error like readreg but does not appear
void RF1A::writeReg(uint8 addr, uint8 value)
{
	//Assert((addr <= 0x2E) || (addr == PATABLE)); //Check for valid range,0x3E is for PATABLE access
	InterruptLock lock;
//@author menzehan:

//	while (!(Registers()->RF1AIFCTL1 & RFINSTRIFG)); // wait for radio accept instruction
//	Registers()->RF1AINSTRW.high = (RF_SNGLREGWR | addr); //write cmd to RF1AINSTRB
//
//	while( !(Registers()->RF1AIFCTL1 & RFDINIFG)  ); //wait for radio accept data
//	Registers()->RF1AINSTRW.low = value; // write value to RF1ADINB

    while (!(Registers()->RF1AIFCTL1 & (RFINSTRIFG | RFDINIFG))); // wait for radio accept instruction and data !!!!
    Registers()->RF1AINSTRW = (RF_SNGLREGWR | addr) << 8 | value; //write cmd and data
//
//    while( !(Registers()->RF1AIFCTL1 & RFDINIFG)  ); //wait for radio accept data
//    Registers()->RF1AINSTRW.low = value; // write value to RF1ADINB
//end menzehan
}


void RF1A::writeTxFifo(uint8 *buffer, uint8 count)
{
    Assert(count > 0 && count < 65);
    InterruptLock lock;

    while (!(Registers()->RF1AIFCTL1 & (RFINSTRIFG | RFDINIFG))); // wait for radio accept instruction and data !!!!
    Registers()->RF1AINSTRW = (RF_TXFIFOWR << 8) | (0xFF & (*buffer)); //write data to txfifo cmd

    // slow version of implementation!!!!
    buffer++;
    count--;// first byte was already written while transmitting commando

    while(count)
    {
        while( !(Registers()->RF1AIFCTL1 & RFDINIFG)); //wait for radio accept data
        if(count == 1)//last byte must written into FIFO
        {
            Registers()->RF1ADINW = *buffer << 8;
            return;
        } else {
            Registers()->RF1ADINW = (*buffer << 8)  | buffer[1] ;
            buffer += 2;
            count -= 2;
        }
    }
    //end menzehan
}


void RF1A::WriteSmartRFReg(const unsigned char SmartRFSetting[][2], uint8 size)
{
  uint8 i;
  for (i=0; i < (size); i++)
  {
	  writeReg(SmartRFSetting[i][0], SmartRFSetting[i][1]);

  }
}

//since single reg read does not work (see errata) regred must be used
//remember the after read the data, the content of the next shows up immediately
//so don't get confused while debugging!
uint8 RF1A::readReg(uint8 addr)
{
	  uint8 regValue;
	  uint8 instruction;

	  //Assert( (addr <= 0x3B) || (addr == PATABLE) );//Check for valid range,0x3E is for PATABLE access
	  InterruptLock lock;
	  while (!(Registers()->RF1AIFCTL1 & RFINSTRIFG)); // wait for radio accept instruction

	  if( (addr <= 0x2E) || (addr == 0x3E))
	  {
		 // instruction = (RF_SNGLREGRD | addr); //read single register cmd to RF1AINSTR1B
		  instruction = (RF_REGRD | addr); //read single register cmd to RF1AINSTR1B
	  }
	  else
	  {
		  instruction = (RF_STATREGRD | addr); //read from one of the status registers cmd to RF1AINSTR1B
	  }

	  Registers()->RF1AINSTR1W.high = instruction;
	  while (!(Registers()->RF1AIFCTL1 & RFDOUTIFG));
	  regValue = Registers()->RF1ASTAT1W.low; //auto read from RF1ADOUT1B
	  return(regValue);
}

//there is a much more complex version within the mrfi_radio_interface.h in the simpliTI example
uint8 RF1A::strobe(uint8 strobe)
{
	uint8 statusByte;

	 // Check for valid strobe command
	  if((strobe == 0xBD) || ((strobe >= 0x30) && (strobe <= 0x3D)))
	  {
	    Registers()->RF1AIFCTL1 &= ~(RFSTATIFG);             // Clear the status read flag

	    while( !(Registers()->RF1AIFCTL1 & RFINSTRIFG)  );   // Wait for INSTRIFG
	    Registers()->RF1AINSTRW.high = strobe;                    // Write the strobe command RF1AINSTRB

	    if(strobe != 0x30) while( !(Registers()->RF1AIFCTL1 & RFSTATIFG) );
	    statusByte = Registers()->RF1ASTAT0W.high; //read status from RF1ASTATB
	  }
	  else
	    return 0;                               // Invalid strobe was sent

	  return statusByte;
}

void RF1A::writeBurstReg(uint8 addr, uint8 *buffer, uint8 count)
{
	  // Write Burst works wordwise not bytewise - known errata
	  unsigned int i;

	  while (!(Registers()->RF1AIFCTL1 & RFINSTRIFG)); // wait for radio accept instruction
	  Registers()->RF1AINSTRW = ((addr | RF_REGWR)<<8 ) + buffer[0]; // Send address + Instruction into RF1AINSTRW

	  for (i = 1; i < count; i++)
	  {
		  Registers()->RF1AINSTRW.low = buffer[i];                   // Send data
		  while( !(Registers()->RF1AIFCTL1 & RFDINIFG)  );        // Wait for TX to finish
	  }
	  i =  Registers()->RF1ASTAT0W.high;                            // Reset RFDOUTIFG flag which contains status byte

}

void RF1A::readBurstReg(uint8 addr, uint8 *buffer, uint8 count)
{
	uint8 i;

	  while (!(Registers()->RF1AIFCTL1 & RFINSTRIFG));       // Wait for INSTRIFG
	  Registers()->RF1AINSTR1W.high = (addr | RF_REGRD);          // Send addr of first conf. reg. to be read  RF1AINSTR1B
	                                            // ... and the burst-register read instruction
	  for (i = 0; i < (count-1); i++)
	  {
	    while (!(Registers()->RF1AIFCTL1 & RFDOUTIFG)); // Wait for the Radio Core to update the RF1ADOUTB reg
	    buffer[i] = Registers()->RF1ASTAT1W.low;                 // Read DOUT from Radio Core F1ADOUT0B register + clears RFDOUTIFG
	                                            // Also initiates auo-read for next DOUT byte
	  }
	  buffer[count-1] = Registers()->RF1ASTAT1W.low;             // Store the last DOUT from Radio Core RF1ADOUT0B register

}


void RF1A::readRxFifo(uint8 *buffer, uint8 count)
{

	  do
	  {
		  InterruptLock lock;
		  while (!(Registers()->RF1AIFCTL1 & RFINSTRIFG)); // wait for radio accept instruction

		  Registers()->RF1AINSTR1W.high = RF_SNGLRXRD;
		  while (!(Registers()->RF1AIFCTL1 & RFDOUTIFG));
		  *buffer = Registers()->RF1ASTAT1W.low; //auto read from RF1ADOUT1B
		  buffer++;
		  count--;
	  } while(count);
}

void RF1A::resetCore()
{
	 strobe(RF_SRES);                          // Reset the Radio Core
	 strobe(RF_SNOP);                          // Reset Radio Pointer
	 //@author menzehan:
	 radioState = reflex::msp430x::rf1A::sleep;
}

void RF1A::WritePATable()
{
  unsigned char valueRead = 0;
  while(valueRead != 0x51)
  {
    /* Write the power output to the PA_TABLE and verify the write operation.  */
    unsigned char i = 0;

    /* wait for radio to be ready for next instruction */
    while( !(Registers()->RF1AIFCTL1 & RFINSTRIFG));
    Registers()->RF1AINSTRW = 0x7E51; // PA Table write (burst) //and write first value see errata

    /* wait for radio to be ready for next instruction */
    while( !(Registers()->RF1AIFCTL1 & RFINSTRIFG));
    Registers()->RF1AINSTR1W.high = RF_PATABRD;                 // -miguel read & write RF1AINSTR1B

    // Traverse PATABLE pointers to read
    for (i = 0; i < 7; i++)
    {
      while( !(Registers()->RF1AIFCTL1 & RFDOUTIFG));
      valueRead  = Registers()->RF1ASTAT1W.low; //RF1ADOUT1B;
    }
    while( !(Registers()->RF1AIFCTL1 & RFDOUTIFG));
    valueRead  = Registers()->RF1ASTAT0W.low; //RF1ADOUTB; ????????
  }
}


void RF1A::off()
{
    strobe(RF_SXOFF);
    radioState = reflex::msp430x::rf1A::sleep;
}
