/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *

 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "reflex/lcd/LCD_B.h"
#include "reflex/io/Ports.h"

using namespace reflex;
using namespace mcu;

/** sets the pins to lcd functinallity or to
 */
void reflex::msp430x::LCD_B::setSegments(const uint16 (&pins)[4], const uint16 &value)
{
	volatile uint16* ctlRegs = &(Registers()->PCTL0);
	for (unsigned i=0;i<4;i++) {
		*(ctlRegs++) = pins[i]&value;
	}
}

void reflex::msp430x::LCD_B::setMux(const lcd::LCDMX& muxMode)
{
	uint8 oldVal = Registers()->CTL0.low & (~lcd::MUX_MASK);
	Registers()->CTL0.low = oldVal|muxMode;
}

void reflex::msp430x::LCD_B::setLCDFreq(const lcd::FrequDiv &div, const lcd::FrequPre &pre)
{
	Registers()->CTL0 = Registers()->CTL0.low | div | pre;
}

void reflex::msp430x::LCD_B::setBLKFreq(const lcd::BLKDiv &div, const lcd::BLKPre &pre)
{
	uint16 oldReg = Registers()->BLKCTL &(~(lcd::BLKDiv_MASK | lcd::BLKPre_MASK));
	Registers()->BLKCTL = oldReg| div | pre;
}
