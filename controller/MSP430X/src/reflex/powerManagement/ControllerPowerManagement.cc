#include "reflex/debug/StaticAssert.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/powerManagement/PowerManager.h"

extern "C" {
void _lpm0();
void _lpm1();
void _lpm2();
void _lpm3();
void _lpm4();
void _lpm5();
}

using namespace reflex;

void (* const PowerManager::modes[mcu::NrOfPowerModes])() = {PowerManager::active,_lpm0, _lpm1, _lpm2, _lpm3, _lpm4, _lpm5};

