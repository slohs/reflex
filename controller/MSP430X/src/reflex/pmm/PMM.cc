/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *

 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/

#include "reflex/pmm/PMM.h"
#include "reflex/mcu/msp430x.h"
#include "reflex/sys/SYS.h"


using namespace reflex;
using namespace msp430x;
using namespace pmm;

PMM::PMM() {
	current = LEVEL0; //at Power Up its at the lowest level
}

void PMM::SetVCore(pmm::CoreVlevel level) // Note: change level by one step only
{
	unsigned char actLevel;
	do {
		actLevel = Registers()->PMMCTL0.low & PMMCOREVx;
		if (actLevel < level)
			SetVCoreUp(++actLevel); // Set VCore (step by step)
		if (actLevel > level)
			SetVCoreDown(--actLevel); // Set VCore (step by step)
	} while (actLevel != (unsigned char) level);

	current = level;

}

void PMM::SetVCoreUp(unsigned char level) // Note: change level by one step only
{
		Registers()->PMMCTL0.high = 0xA5; // Open PMM module registers for write access

		Registers()->SVSMHCTL = SVSHE + SVSHRVL0 * level + SVMHE + SVSMHRRL0 * level; // Set SVS/M high side to new level

		Registers()->SVSMLCTL = SVSLE + SVMLE + SVSMLRRL0 * level; // Set SVM new Level
		while ((Registers()->PMMIFG & SVSMLDLYIFG) == 0); // Wait till SVM is settled (Delay)
		Registers()->PMMCTL0.low = level; // Set VCore to level
		Registers()->PMMIFG &= ~(SVMLVLRIFG + SVMLIFG); // Clear already set flags

		if ((Registers()->PMMIFG & SVMLIFG))
			while ((Registers()->PMMIFG & SVMLVLRIFG) == 0); // Wait till level is reached

		Registers()->SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0* level; // Set SVS/M Low side to new level
		Registers()->PMMCTL0.high = 0x00; // Lock PMM module registers for write access
}

void PMM::SetVCoreDown(unsigned char level) {
	Registers()->PMMCTL0.high = 0xA5; // Open PMM module registers for write access
	Registers()->SVSMLCTL = SVSLE + SVSLRVL0 * level + SVMLE + SVSMLRRL0* level; // Set SVS/M Low side to new level
	while ((Registers()->PMMIFG & SVSMLDLYIFG) == 0); // Wait till SVM is settled (Delay)
	Registers()->PMMCTL0.low = level; // Set VCore to level
	Registers()->PMMCTL0.high = 0x00; // Lock PMM module registers for write access
}

/*
 * these functions not used because setting frequency to highest
 * possible which certain power level
 */
//array of supported MCLK frequencies, indexed by coreVlevel
//const uint16 frequencyStepsKHz[] = { 12000, 16000, 20000, 25000 };
//uint8 PMM::supportedMCLK()
//{
//	return frequencyStepsKHz[current];
//}
//
//uint8 PMM::getOptimalVoltage(uint16 kHz)
//{
//
//	for(uint8 Vlevel = LEVEL0; Vlevel<=LEVEL3; Vlevel++)
//	{
//		if (frequencyStepsKHz[Vlevel] >= kHz)
//		{
//			return Vlevel;
//		}
//	}
//	return 0xFF; //error value
//
//}
//
//void PMM::setOptimalVoltage(uint16 kHz)
//{
//	uint8 Vlevel = getOptimalVoltage(kHz);
//	if (Vlevel != 0xFF)
//			SetVCore((CoreVlevel) Vlevel);
//
//}
