/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		Reinhard Hemmerling, S�ren H�ckner, Karsten Walther
 */

#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/MachineDefinitions.h"

using namespace reflex;

void InterruptGuardian::init()
{
	//all handler are initialized to invalidInterruptHandler
	for(unsigned int i = 0; i < mcu::interrupts::MAX_HANDLERS; i++){
		handlers[i] = &InterruptHandler::invalidInterruptHandler;
	}
}

void InterruptGuardian::registerInterruptHandler( InterruptHandler* handler, mcu::InterruptVector vector)
{
	Assert( (vector > mcu::interrupts::INVALID_INTERRUPT) && (vector < mcu::interrupts::MAX_HANDLERS) );
	// register handler in logical interrupt table
	handlers[vector] = handler;
}


