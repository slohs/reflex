#include "reflex/debug/StaticAssert.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/powerManagement/PowerManager.h"
#include <unistd.h>

extern "C" void _interruptsEnable();
extern "C" void _wait();
extern "C" void _yield();


void (* const reflex::PowerManager::modes[reflex::mcu::NrOfPowerModes])() = {reflex::PowerManager::active,_wait,_yield};
