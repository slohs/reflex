#ifndef InputWrapper_h
#define InputWrapper_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	InputWrapper
 *
 *	Author:		Karsten Walther, Reinhard Hemmerling
 *
 *	Description:	Allows sensor emulation through a stimuli file
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/scheduling/Activity.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

namespace reflex {
/**
 * The InputWrapper class provides support for timed input data.
 * This is accomplished by reading (timecode, data) pairs from a
 * file and checking the timecode against the system time.
 * Whenever the clock reaches the timecode, the data is written
 * to a trigger variable (specified on construction) and a new
 * (timecode, data) pair is read.
 *
 * The records should be sorted by timecode.
 */
template <typename T>
class InputWrapper : public Activity
{
	Sink1<T>* output;	//!< the output variable reference
	int file;

	// Event for input trigger
	Event event;

	// virtual timer for timeout notification
	VirtualTimer timer;
	// this is the (timecode, data) pair
	Time time;
	T data;

protected:

	/** Helper function, reads a timecode from the data file.
	 *
	 *  @param time reference to the Time variable in which the timestamp is
	 *  stored
	 *  @return number of read bytes
	 */
	int readTime(Time& time)
	{
		return read(file, &time, sizeof(Time));
	}

	/** Helper funtion, reads a data value from the file.
	 *
	 *  @param data reference to the data field in which data is stored.
	 *  @return number of read bytes
	 */
	int readData(T& data)
	{
		return read(file, &data, sizeof(T));
	}

public:

	/** Create a new InputWrapper with inputFile naming the data file
	 *  and trigger being a trigger that receives data values when
	 *  the timecode was reached.
	 *
	 *  @param inputFile name of the stimuli file
	 *  @param output reference to the subsequent component
	 */
	InputWrapper(char* inputFile) : timer(VirtualTimer::ONESHOT)
	{
		// open the data file
		file = open(inputFile, O_RDONLY);
		timer.init(event); // timer triggers the event
		// event triggers this input activity
		event.init(this);

		// read initial (timecode, data) pair
		readTime(time);
		readData(data);

		// set timer for next event
		Time currentTime = timer.getSystemTime();
		if (currentTime < time)
			timer.set(time - currentTime);
		else
			timer.set(1);
	}

	/**
	 * init
	 * initialize the subsequent output
	 */
	void init(Sink1<T>* output) {
		this->output = output;
	}

	/**
	 * Close the InputWrapper.
	 */
	virtual ~InputWrapper()
	{
		timer.stop();

		// close the data file
		if (file >= 0)
			close(file);
	}

	/** notify
	 * implements Activity
	 */
	void run()
	{
		// get current time
		Time currentTime = timer.getSystemTime();

		// check if timecode was reached
		while (currentTime >= time)
		{
			if (output)
				output->assign(data);

			// read next (timecode, data) pair
			if (!readTime(time))
				return; // no more entries
			readData(data);
		}

		// set timeout notification
		timer.set(time - currentTime);
	}

};

} //namespace reflex

#endif
