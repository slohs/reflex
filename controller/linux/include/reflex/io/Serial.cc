/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 */

/* UNFINISHED - style */
#include "errno.h"

#include "reflex/io/Serial.h"
#include "reflex/interrupts/InterruptLock.h"

namespace reflex {

using namespace std;

template <char BufferSize>
void Serial<BufferSize>::read_loop(void* arg) {
	char c;

	while(true) {
		if(read(tty, &c, 1) == -1) {
			if(errno != EINTR) {
				perror("Serial.cc: readerror");
				_exit(1);
			}
		}
		receiver->assign(c);
	}
	return 0;
}

 template <char BufferSize>
Serial<BufferSize>::Serial(speed_t baud) :
	fifo(this)
{
	sender = 0;
	receiver = 0;

	tty = -1;
}

 template <char BufferSize>
void Serial<BufferSize>::init(Sink1<char>* rx, Sink0* tx, char* tty_name)
{
	receiver = rx;
	sender = tx;

	open_tty(tty_name);

	if(receiver){
		if(pthread_create(&read_thread, NULL, read_loop, this) != 0) {
			close(tty);
			fprintf(stderr, "Serial.cc: failed to create read thread");
			_exit(1);
		}
	}else{
		// test necessary?
		pthread_cancel(read_thread);
	}
}

 template <char BufferSize>
void Serial<BufferSize>::run()
{
	// avoid race-conditions  not needed probably
	InterruptLock lock;

	fifo.get(sendPos, sendLen);

	write(tty, sendPos, sendLen);

	if(sender) {
		sender->notify();
	}
}

 template <char BufferSize>
void Serial<BufferSize>::open_tty(char *tty_name)
{
	struct termios ios;

	if ((tty = open(tty_name, O_RDWR)) < 0) {
		perror(tty_name);
		_exit(1);
	}

	if (!isatty(tty)) {
		close(tty);
		fprintf(stderr, "Serial.cc: %s: not a tty\n", tty_name);
		_exit(1);
	}

	memset(&ios, 0, sizeof(ios));

	ios.c_cflag = CREAD | CLOCAL | CS8;
	ios.c_cc[VMIN] = 1;
	ios.c_cc[VTIME] = 0;
	cfsetispeed(&ios, B19200);
	cfsetospeed(&ios, B19200);

	if (tcsetattr(tty, TCSANOW, &ios) == -1) {
		perror("Serial.cc: tcsetattr");
		_exit(1);
	}
}

}//end namespace
