#ifndef SYSTEM_TIMER_H
#define SYSTEM_TIMER_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	SystemTimer
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description: Emulated system timer in linux guest environment
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/timer/HardwareTimer.h"
#include "reflex/types.h"

#include <sys/time.h>

namespace reflex {

/**
 * The SystemTimer in the guest system emulates a hardware clock by
 * registering as signal handler and letting the system signal when
 * the next alarm occurs.
 *
 * The SystemTimer uses a POSIX clock for LocalTime and a POSIX itimer
 * (REAL) for alarm notification.
 */
class SystemTimer : public HardwareTimer, public InterruptHandler
{
public:
	/** constructor
	 * initializes base class and variables
	 */
	SystemTimer();

	/** getNow
	 * @return current counter value (local time)
	 */
	virtual Time getNow();

    /** getTimestamp
     * @return 64 bit counter value in hardware precision
     */
    virtual uint64 getTimestamp();

	/** start
	 * set a timeout until next interrupt and start the timer
	 * @param ticks number of ticks until timer event
	 */
	void start(Time ticks);

	/**
	 * startAt
	 * set a new time interrupt to occur ticks after t0
	 * @param t0 time from where to set the timer
	 * @param ticks number of ticks after t0 when the timer should fire
	 */
	void startAt(Time t0, Time ticks);

	/** stop
	 * stop the hardware timer (alarm)
	 * NOTE: this does not stop the local time counter!
	 */
	void stop();

	/** handle
	 * Timer handler called by the interrupt wrapper. It is responsible
	 * for resetting the hardware to allow further interrupts.
	 */
	virtual void handle();

	/* sigHandler
	 * the signal handler
	 * @param sig signal number to handle
	 */
    static void sigHandler(int sig);

    /** enable
     * enable the clock and timer
     */
	virtual void enable();

	/** disable
	 * disable the clock and timer
	 */
	virtual void disable();

private:
	/* system clock variables */
	Time systemStartTime; // time of system start (t0)
	Time stoppedAt; // when the timer was last stopped

	/* alarm timer variables */
	static struct itimerval itv; // alarm value for itimer

	// generate (simulated) timer interrupts?
	static bool active;

	enum Precision {
		DEFER_SIGNAL_USEC = 10000, // time to defer a pending interrupt
	};
};

} //namespace


#endif
