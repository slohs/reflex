#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		Soeren Hoeckner
# *
# */


#ASM_SOURCES_CONTROLLER += \
#	interrupts/interruptFunctions.s

C_SOURCES_CONTROLLER += \
	interrupts/interruptFunctions.c

CC_SOURCES_CONTROLLER += \
	interrupts/MachineInterruptGuardian.cc \
	powerManagement/ControllerPowerManagement.cc \
	reset.cc
