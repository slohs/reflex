ASM_SOURCES_CONTROLLER = \
	power.s \
	interruptFunctions.s

C_SOURCES_CONTROLLER =

CC_SOURCES_CONTROLLER += \
	compiler/gcc_virtual.cc \
	interrupts/MachineInterruptGuardian.cc \
	powerManagement/ControllerPowerManagement.cc \
	memory/memcpy.cc \
	timer/HardwareTimerMilli.cc \

CC_SOURCES_LIB += \
	timer/VirtualTimer.cc \
	timer/VirtualizedTimer.cc