#ifndef MACHINEDEFINITIONS_ATMEGA169_H_
#define MACHINEDEFINITIONS_ATMEGA169_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "MachineDefinitions_default.h"

namespace reflex
{
namespace atmega
{

namespace internal {

template<int INT_OVF, int INT_COMPA, int INT_COMPB, int INT_COMPC, int INT_CAPT>
class TimerRegisters<Atmega169, Timer0, INT_OVF, INT_COMPA, INT_COMPB, INT_COMPC, INT_CAPT>
{
public:
	enum
	{
		OVF = INT_OVF, COMPA = INT_COMPA, COMPB = INT_COMPB, COMPC = INT_COMPC, CAPT = INT_CAPT
	};

	uint8 dummy0[0x35 - 0x00];
	volatile uint8 TIFRn; // 0x35
	uint8 dummy1[0x44 - 0x36];
	union
	{
		volatile uint8 TCCRnA; // 0x44
		volatile uint8 TCCRnB;
	};
	uint8 dummy2; // 0x45
	union
	{
		volatile uint8 TCNTnH;
		volatile uint8 TCNTnL; // 0x46
	};

	// OCRnA is 8 Bit only
	union
	{
		volatile uint8 OCRnAH;
		volatile uint8 OCRnAL; // 0x47
	};
	// OCRnC does not exist
	union
	{
		volatile uint8 OCRnBH;
		volatile uint8 OCRnBL; // 0x48
		volatile uint8 OCRnCH;
		volatile uint8 OCRnCL;
	};
	uint8 dummy3[0x6E - 0x49];
	volatile uint8 TIMSKn;

	TimerRegisters* const operator->()
	{
		return reinterpret_cast<TimerRegisters* const > (0x0);
	}

};


template<int INT_OVF, int INT_COMPA, int INT_COMPB, int INT_COMPC, int INT_CAPT>
class TimerRegisters<Atmega169, Timer2, INT_OVF, INT_COMPA, INT_COMPB, INT_COMPC, INT_CAPT>
{
public:
	enum
	{
		OVF = INT_OVF, COMPA = INT_COMPA, COMPB = INT_COMPB, COMPC = INT_COMPC, CAPT = INT_CAPT
	};

	uint8 dummy0[0x37 - 0x00];
	volatile uint8 TIFRn; // 0x37
	uint8 dummy1[0x70 - 0x38];
	volatile uint8 TIMSKn; // 0x70
	uint8 dummy2[0xB0 - 0x71];
	union
	{
		volatile uint8 TCCRnA; // 0xB0
		volatile uint8 TCCRnB;
	};
	uint8 dummy3[0xB2 - 0xB1];
	volatile uint8 TCNTn; // 0xB2
	union
	{
		volatile uint8 OCRnAL; // 0xB3
		volatile uint8 OCRnAH;
		volatile uint8 OCRnBL;
		volatile uint8 OCRnBH;
		volatile uint8 OCRnCL;
		volatile uint8 OCRnCH;
	};

	TimerRegisters* const operator->()
	{
		return reinterpret_cast<TimerRegisters* const > (0x00);
	}

};

}


template<> struct McuCore<Atmega169> : public internal::DefaultController
{
	enum RegisterBits {
		//TCCR2
		WGM21 = 1 << 3,
		WGM20 = 1 << 6,
		//TCCR0A / TCCR0B
		COM0A1 = 1 << 5,
		COM0A0 = 1 << 4,
		COM0B1 = 1 << 1, // dummy, has to be different from A
		COM0B0 = 1 << 0,// dummy, has to be different from A
		WGM01 = 1 << 3,
		WGM00 = 1 << 6,
		WGMn1 = 1 << 1,
		WGMn0 = 1 << 0,
		// PCINT
		PCIE0 = 1 << 6,
		PCIE1 = 1 << 7
	};

	enum InterruptVector
	{
		INVALID_INTERRUPT = -1,
		INTVEC_INT0 = 0,
		INTVEC_PCINT0 = 1,
		INTVEC_PCINT1 = 2,
		INTVEC_TIMER2COMPA = 3,
		INTVEC_TIMER2OVF = 4,
		INTVEC_TIMER1CAPT = 5,
		INTVEC_TIMER1COMPA = 6,
		INTVEC_TIMER1COMPB = 7,
		INTVEC_TIMER1OVF = 8,
		INTVEC_TIMER0COMPA = 9,
		INTVEC_TIMER0COMPB = 9, // dummy for Timer.h, no COMPB available in HW
		INTVEC_TIMER0OVF = 10,
		INTVEC_SPI_STC,
		INTVEC_USART0_RX,
		INTVEC_USART0_UDRE,
		INTVEC_USART0_TX,
		INTVEC_USI_START,
		INTVEC_USI_OVERFLOW,
		INTVEC_ANALOG_COMP,
		INTVEC_ADC,
		INTVEC_EE_READY,
		INTVEC_SPM_READY,
		INTVEC_LCD,
		MAX_HANDLERS
	};

	enum PortPins
	{
		SS = 1 << 4, SCK = 1 << 7, MOSI = 1 << 5, MISO = 1 << 6
	};

	template<TimerNr nr> struct TimerRegisters: public internal::TimerRegisters<Atmega169, nr,
		(nr == Timer0) ? INTVEC_TIMER0OVF :
		(nr == Timer1) ? INTVEC_TIMER1OVF :
		(nr == Timer2) ? INTVEC_TIMER2OVF :
		INVALID_INTERRUPT,
		(nr == Timer0) ? INTVEC_TIMER0COMPA :
		(nr	== Timer1) ? INTVEC_TIMER1COMPA :
		(nr == Timer2) ? INTVEC_TIMER2COMPA :
		INVALID_INTERRUPT,
		(nr == Timer0) ? INTVEC_TIMER0COMPB :
		(nr == Timer1) ? INTVEC_TIMER1COMPB :
		INVALID_INTERRUPT,
		INVALID_INTERRUPT,
		(nr == Timer1) ? INTVEC_TIMER1CAPT :
		INVALID_INTERRUPT>
	{

	};

	template<UsartNr nr> struct UsartRegisters : public internal::UsartRegisters<Atmega169, nr,
		(nr == Usart0) ? INTVEC_USART0_RX :
		INVALID_INTERRUPT,
		(nr == Usart0) ? INTVEC_USART0_UDRE :
		INVALID_INTERRUPT,
		(nr == Usart0) ? INTVEC_USART0_TX :
		INVALID_INTERRUPT>
	{

	};

};
}
}

#endif /* MACHINEDEFINITIONS_ATMEGA169_H_ */
