#ifndef MACHINEDEFINITIONS_DEFAULT_H_
#define MACHINEDEFINITIONS_DEFAULT_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/debug/StaticAssert.h"
#include "reflex/types.h"

namespace reflex
{
namespace atmega
{

/*!
 * \internal
 * \brief Internal register definitions.
 */
namespace internal
{
/**
 \brief Default implementation for USART registers
 Each serial port follows the same register layout, so that all
 serial interfaces can be addressed in one struct.
 */
template<McuType mcuType, UsartNr nr, int INT_RX, int INT_UDRE, int INT_TX>
struct UsartRegisters
{
	enum InterruptVectors
	{
		RxVector = INT_RX, UdreVector = INT_UDRE, TxcVector = INT_TX
	};

	volatile uint8 UCSRA;
	volatile uint8 UCSRB;
	volatile uint8 UCSRC;
	uint8 dummy;
	volatile uint16 UBRR;
	volatile uint8 UDR;

	/* Not the most beautiful way, but gcc will optimize it away */
	UsartRegisters* operator->()
	{
		switch (nr)
		{
		case 0:
			return reinterpret_cast<UsartRegisters*> (0xC0);
			break;
		case 1:
			return reinterpret_cast<UsartRegisters*> (0xC8);
			break;
		case 2:
			return reinterpret_cast<UsartRegisters*> (0xD0);
			break;
		case 3:
			return reinterpret_cast<UsartRegisters*> (0x130);
			break;
		default:
			STATIC_ASSERT(nr <= Usart3, max_4_UARTS_available)
			;
			break;
		}
	}
};

/**
 * Timer registers are scattered around in the register file, so that
 * different structs are necessary.
 */
template<McuType mcuType, TimerNr timerNr, int INT_OVF, int INT_COMPA, int INT_COMPB,
		int INT_COMPC, int INT_CAPT>
class TimerRegisters;

template<McuType mcuType, int INT_OVF, int INT_COMPA, int INT_COMPB, int INT_COMPC, int INT_CAPT>
class TimerRegisters<mcuType, Timer0, INT_OVF, INT_COMPA, INT_COMPB, INT_COMPC, INT_CAPT>
{
public:
	enum
	{
		OVF = INT_OVF, COMPA = INT_COMPA, COMPB = INT_COMPB, COMPC = INT_COMPC, CAPT = INT_CAPT
	};

	uint8 dummy0[0x35 - 0x00];
	volatile uint8 TIFRn; // 0x35
	uint8 dummy1[0x44 - 0x36];
	volatile uint8 TCCRnA; // 0x44
	volatile uint8 TCCRnB; // 0x45
	union
	{
		volatile uint8 TCNTnH;
		volatile uint8 TCNTnL; // 0x46
	};

	// OCRnA is 8 Bit only
	union
	{
		volatile uint8 OCRnAH;
		volatile uint8 OCRnAL; // 0x47
	};
	// OCRnC does not exist
	union
	{
		volatile uint8 OCRnBH;
		volatile uint8 OCRnBL; // 0x48
		volatile uint8 OCRnCH;
		volatile uint8 OCRnCL;
	};
	uint8 dummy2[0x6E - 0x49];
	volatile uint8 TIMSKn;

	TimerRegisters* const operator->()
	{
		return reinterpret_cast<TimerRegisters* const > (0x0);
	}

};

template<McuType mcuType, int INT_OVF, int INT_COMPA, int INT_COMPB, int INT_COMPC, int INT_CAPT>
class TimerRegisters<mcuType, Timer1, INT_OVF, INT_COMPA, INT_COMPB, INT_COMPC, INT_CAPT>
{
public:
	enum
	{
		OVF = INT_OVF, COMPA = INT_COMPA, COMPB = INT_COMPB, COMPC = INT_COMPC, CAPT = INT_CAPT
	};

	uint8 dummy0[0x36 - 0x00]; // 0x00..0x35
	volatile uint8 TIFRn; // 0x36
	uint8 dummy1[0x6F - 0x37]; // 0x37..0x6E
	volatile uint8 TIMSKn; // 0x6F
	uint8 dummy2[0x80 - 0x70]; // 0x70..0x7F
	volatile uint8 TCCRnA; // 0x80
	volatile uint8 TCCRnB;
	volatile uint8 TCCRnC;
	uint8 dummy3[1];
	volatile uint8 TCNTnL;
	volatile uint8 TCNTnH;
	volatile uint8 ICRnL;
	volatile uint8 ICRnH;
	volatile uint8 OCRnAL;
	volatile uint8 OCRnAH;
	volatile uint8 OCRnBL;
	volatile uint8 OCRnBH;
	volatile uint8 OCRnCL;
	volatile uint8 OCRnCH; // 0x8D

	TimerRegisters* const operator->()
	{
		return reinterpret_cast<TimerRegisters* const > (0x00);
	}
};

template<McuType mcuType, int INT_OVF, int INT_COMPA, int INT_COMPB, int INT_COMPC, int INT_CAPT>
class TimerRegisters<mcuType, Timer2, INT_OVF, INT_COMPA, INT_COMPB, INT_COMPC, INT_CAPT>
{
public:
	enum
	{
		OVF = INT_OVF, COMPA = INT_COMPA, COMPB = INT_COMPB, COMPC = INT_COMPC, CAPT = INT_CAPT
	};

	uint8 dummy0[0x37 - 0x00];
	volatile uint8 TIFRn; // 0x37
	uint8 dummy1[0x70 - 0x38];
	volatile uint8 TIMSKn; // 0x70
	uint8 dummy2[0xB0 - 0x71];
	volatile uint8 TCCRnA; // 0xB0
	volatile uint8 TCCRnB;
	volatile uint8 TCNTn; // 0xB2
	union
	{
		volatile uint8 OCRnAL; // 0xB3
		volatile uint8 OCRnAH;
	};
	union
	{
		volatile uint8 OCRnBL; // 0xB4
		volatile uint8 OCRnBH;
		volatile uint8 OCRnCL;
		volatile uint8 OCRnCH;
	};

	TimerRegisters* const operator->()
	{
		return reinterpret_cast<TimerRegisters* const > (0x00);
	}

};

template<McuType mcuType, int INT_OVF, int INT_COMPA, int INT_COMPB, int INT_COMPC, int INT_CAPT>
class TimerRegisters<mcuType, Timer3, INT_OVF, INT_COMPA, INT_COMPB, INT_COMPC, INT_CAPT>
{
public:
	enum
	{
		OVF = INT_OVF, COMPA = INT_COMPA, COMPB = INT_COMPB, COMPC = INT_COMPC, CAPT = INT_CAPT
	};

	uint8 dummy0[0x38]; // 0x00..0x37
	volatile uint8 TIFRn; // 0x38
	uint8 dummy1[0x38]; // 0x39..0x70
	volatile uint8 TIMSKn; // 0x71
	uint8 dummy2[0x1E]; // 0x72..0x8F
	volatile uint8 TCCRnA; // 0x90
	volatile uint8 TCCRnB;
	volatile uint8 TCCRnC;
	uint8 dummy3[1];
	volatile uint8 TCNTnL;
	volatile uint8 TCNTnH;
	volatile uint8 ICRnL;
	volatile uint8 ICRnH;
	volatile uint8 OCRnAL;
	volatile uint8 OCRnAH;
	volatile uint8 OCRnBL;
	volatile uint8 OCRnBH;
	volatile uint8 OCRnCL;
	volatile uint8 OCRnCH; // 0x9D

	TimerRegisters* const operator->()
	{
		return reinterpret_cast<TimerRegisters* const > (0x00);
	}
};

template<McuType mcuType, int INT_OVF, int INT_COMPA, int INT_COMPB, int INT_COMPC, int INT_CAPT>
class TimerRegisters<mcuType, Timer4, INT_OVF, INT_COMPA, INT_COMPB, INT_COMPC, INT_CAPT>
{
public:
	enum
	{
		OVF = INT_OVF, COMPA = INT_COMPA, COMPB = INT_COMPB, COMPC = INT_COMPC, CAPT = INT_CAPT
	};

	uint8 dummy0[0x39]; // 0x00..0x38
	volatile uint8 TIFRn; // 0x39
	uint8 dummy1[0x38]; // 0x40..0x71
	volatile uint8 TIMSKn; // 0x72
	uint8 dummy2[0x2D]; // 0x73..0x9F
	volatile uint8 TCCRnA; // 0xA0
	volatile uint8 TCCRnB;
	volatile uint8 TCCRnC;
	uint8 dummy3[1];
	volatile uint8 TCNTnL;
	volatile uint8 TCNTnH;
	volatile uint8 ICRnL;
	volatile uint8 ICRnH;
	volatile uint8 OCRnAL;
	volatile uint8 OCRnAH;
	volatile uint8 OCRnBL;
	volatile uint8 OCRnBH;
	volatile uint8 OCRnCL;
	volatile uint8 OCRnCH; // 0xAD

	TimerRegisters* const operator->()
	{
		return reinterpret_cast<TimerRegisters* const > (0x00);
	}
};

template<McuType mcuType, int INT_OVF, int INT_COMPA, int INT_COMPB, int INT_COMPC, int INT_CAPT>
class TimerRegisters<mcuType, Timer5, INT_OVF, INT_COMPA, INT_COMPB, INT_COMPC, INT_CAPT>
{
public:
	enum
	{
		OVF = INT_OVF, COMPA = INT_COMPA, COMPB = INT_COMPB, COMPC = INT_COMPC, CAPT = INT_CAPT
	};

	uint8 dummy0[0x3A]; // 0x00..0x39
	volatile uint8 TIFRn; // 0x3A
	uint8 dummy1[0x38]; // 0x3B..0x72
	volatile uint8 TIMSKn; // 0x73
	uint8 dummy2[0xAC]; // 0x74..0x11F
	volatile uint8 TCCRnA; // 0x120
	volatile uint8 TCCRnB;
	volatile uint8 TCCRnC;
	uint8 dummy3[1];
	volatile uint8 TCNTnL;
	volatile uint8 TCNTnH;
	volatile uint8 ICRnL;
	volatile uint8 ICRnH;
	volatile uint8 OCRnAL;
	volatile uint8 OCRnAH;
	volatile uint8 OCRnBL;
	volatile uint8 OCRnBH;
	volatile uint8 OCRnCL;
	volatile uint8 OCRnCH; // 0x12D

	TimerRegisters* const operator->()
	{
		return reinterpret_cast<TimerRegisters* const > (0x00);
	}
};

/*!
 \brief Abstract controller definition for all atmega family members.

 This struct serves as base class for nearly all atmega controllers and includes.
 Most modern atmega controllers share a similar register layout.

 */
struct DefaultController
{
	/*!
	 \ingroup atmega
	 \brief Enumerator to address the different IO ports of a controller.

	 Have a look into the datasheet, which controller does offer which
	 port.

	 \see IOPort
	 */
	enum IOPorts
	{
		PortA = 0x20, //!< PortA
		PortB = 0x23, //!< PortB
		PortC = 0x26, //!< PortC
		PortD = 0x29, //!< PortD
		PortE = 0x2c, //!< PortE
		PortF = 0x2F, //!< PortF
		PortG = 0x32, //!< PortG
		PortH = 0x100,//!< PortH
		PortJ = 0x103,//!< PortJ
		PortK = 0x106,//!< PortK
		PortL = 0x109
	//!< PortL
	};

	/*!
	 \ingroup atmega
	 \brief Bit offsets for the internal controller registers of the atmega.
	 */
	enum RegisterBits
	{
		// Watchdog
		PORF = 0x01, //!< PORF
		EXTRF = 0x02, //!< EXTRF
		BORF = 0x04, //!< BORF
		WDRF = 0x08, //!< WDRF
		WDP0 = 0x01, //!< WDP0
		WDP1 = 0x02, //!< WDP1
		WDP2 = 0x04, //!< WDP2
		WDE = 0x08, //!< WDE
		WDCE = 0x10, //!< WDCE
		WDP3 = 0x20, //!< WDP3
		WDIE = 0x40, //!< WDIE
		WDIF = 0x80, //!< WDIF
		//ADCSRA
		ADPS0 = 0x01, //!< ADPS0
		ADPS1 = 0x02, //!< ADPS1
		ADPS2 = 0x04, //!< ADPS2
		ADIE = 0x08, //!< ADIE
		ADIF = 0x10, //!< ADIF
		ADFR = 0x20, //!< ADFR
		ADSC = 0x40, //!< ADSC
		ADEN = 0x80, //!< ADEN
		//ADCSRB
		ADTS0 = 0x01, //!< ADTS0
		ADTS1 = 0x02, //!< ADTS1
		ADTS2 = 0x04, //!< ADTS2
		MUX5 = 0x08, //!< MUX5
		ACME = 0x40, //!< ACME
		//ADMUX
		MUX0 = 0x01, //!< MUX0
		MUX1 = 0x02, //!< MUX1
		MUX2 = 0x04, //!< MUX2
		MUX3 = 0x08, //!< MUX3
		MUX4 = 0x10, //!< MUX4
		ADLAR = 0x20, //!< ADLAR
		REFS0 = 0x40, //!< REFS0
		REFS1 = 0x80, //!< REFS1
		//ACSR
		ACIS0 = 0x01, //!< ACIS0
		ACIS1 = 0x02, //!< ACIS1
		ACIC = 0x04, //!< ACIC
		ACIE = 0x08, //!< ACIE
		ACI = 0x10, //!< ACI
		ACO = 0x20, //!< ACO
		ACBG = 0x40, //!< ACBG
		ACD = 0x80, //!< ACD
		//TCCR0A / TCCR0B
		COM0A1 = 1 << 7,//!< COM0A1
		COM0A0 = 1 << 6,//!< COM0A0
		COM0B1 = 1 << 5,//!< COM0B1
		COM0B0 = 1 << 4,//!< COM0B0
		WGM01 = 1 << 1, //!< WGM01
		WGM00 = 1 << 0, //!< WGM00
		//TCCR2
		WGM21 = 1 << 1,
		WGM20 = 1 << 0,
		// TCCRnC
		FOCnA = 1 << 7, //!< FOCnA
		FOCnB = 1 << 6, //!< FOCnB
		FOCnC = 1 << 5, //!< FOCnC
		//TCCRnB
		ICNCn = 0x80, //!< ICNCn
		ICESn = 0x40, //!< ICESn
		//
		WGMn3 = 0x10, //!< WGMn3
		WGMn2 = 0x08, //!< WGMn2
		CSn2 = 0x04, //!< CSn2
		CSn1 = 0x02, //!< CSn1
		CSn0 = 0x01, //!< CSn0
		//TCCRnA
		COMnA1 = 0x80, //!< COMnA1
		COMnA0 = 0x40, //!< COMnA0
		COMnB1 = 0x20, //!< COMnB1
		COMnB0 = 0x10, //!< COMnB0
		COMnC1 = 0x08, //!< COMnC1
		COMnC0 = 0x04, //!< COMnC0
		WGMn1 = 0x02, //!< WGMn1
		WGMn0 = 0x01, //!< WGMn0
		//TIFRn
		OCFnC = 0x08, //!< OCFnC
		OCFnB = 0x04, //!< OCFnB
		OCFnA = 0x02, //!< OCFnA
		TOVn = 0x01, //!< TOVn
		// TIMSKn
		ICIEn = 0x20, //!< ICIEn
		OCIEnC = 0x08, //!< OCIEnC
		OCIEnB = 0x04, //!< OCIEnB
		OCIEnA = 0x02, //!< OCIEnA
		TOIEn = 0x01, //!< TOIEn
		// ASSR
		EXCLK = 1 << 4, //!< EXCLK
		AS2 = 1 << 3, //!< AS2
		TCN2UB = 1 << 2,//!< TCN2UB
		OCR2UB = 1 << 1,//!< OCR2UB
		TCR2UB = 1 << 0,//!< TCR2UB
		//GTCCR
		TSM = 0x80, //!< TSM
		PSRASY = 0x02, //!< PSRASY
		PSRSYNC = 0x01, //!< PSRSYNC
		//UCSRA
		MPCM = 0x01, //!< MPCM
		U2X = 0x02, //!< U2X
		UPE = 0x04, //!< UPE
		DOR = 0x08, //!< DOR
		FE = 0x10, //!< FE
		UDRE = 0x20, //!< UDRE
		TXC = 0x40, //!< TXC
		RXC = 0x80, //!< RXC
		//UCSRB
		TXB8 = 0x01, //!< TXB8
		RXB8 = 0x02, //!< RXB8
		UCSZ2 = 0x04, //!< UCSZ2
		TXEN = 0x08, //!< TXEN
		RXEN = 0x10, //!< RXEN
		UDRIE = 0x20, //!< UDRIE
		TXCIE = 0x40, //!< TXCIE
		RXCIE = 0x80, //!< RXCIE
		//UCSRC
		UCPOL = 0x01, //!< UCPOL
		UCSZ0 = 0x02, //!< UCSZ0
		UCSZ1 = 0x04, //!< UCSZ1
		USBS = 0x08, //!< USBS
		UPM0 = 0x10, //!< UPM0
		UPM1 = 0x20, //!< UPM1
		UMSEL0 = 0x40, //!< UMSEL0
		UMSEL1 = 0x80, //!< UMSEL1
		PCIE0 = 1 << 6, //!< PCIE0
		PCIE1 = 1 << 7, //!< PCIE1
		/* EECR */
		EEPM1 = 0x20, //!< EEPM1
		EEPM0 = 0x10, //!< EEPM0
		EERIE = 0x08, //!< EERIE
		EEMPE = 0x04, //!< EEMPE
		EEPE = 0x02, //!< EEPE
		EERE = 0x01, //!< EERE
		//SPCR control register
		SPIE = 0x80, ///< SPI Interrupt enable
		SPE = 0x40, ///< SPI enable
		DORD = 0x20, ///< DataOrder LSB(1)/ MSB(0) written first
		MSTR = 0x10, ///< Master (1)/Slave(0) Mode
		CPOL = 0x08, ///< Clock Polarity
		CPHA = 0x04, ///< Clock Phase
		SPR1 = 0x02, ///< Clock Rate Select1
		SPR0 = 0x01, ///< Clock Rate Select0
		//SPSR status register
		SPIF = 0x80, ///< SPI Interrupt Flag
		WCOL = 0x40, ///< Write collision
		SPI2x = 0x01
	///< double speed flag
	};

	/*!
	 \ingroup atmega
	 \brief Default register layout for the third controller generation.

	 Any register can be accessed from within user code:
	 \code
	 mcu::Core::Registers()->MCUCR = 0x00;
	 uint8 status = mcu::Core::Registers()->MCUSR;
	 \endcode



	 */
	struct Registers
	{
		volatile uint8 reserved0[0x3B - 0x00]; // 0x00..0x35
		volatile uint8 PCIFR; // 0x3B
		volatile uint8 EIFR; // 0x3C
		volatile uint8 EIMSK; // 0x3D
		volatile uint8 GPIOR0; // 0x3E
		volatile uint8 EECR; // 0x3F
		volatile uint8 EEDR; // 0x40
		volatile uint8 EEARL; // 0x41
		volatile uint8 EEARH; // 0x42
		volatile uint8 GTCCR; // 0x43
		uint8 reserved1[0x4A - 0x44];
		volatile uint8 GPIOR1; // 0x4A
		volatile uint8 GPIOR2; // 0x4B
		volatile uint8 SPCR; // 0x4C
		volatile uint8 SPSR; // 0x4D
		volatile uint8 SPDR; // 0x4E
		volatile uint8 reserved20; // 0x4F
		volatile uint8 ACSR; // 0x50
		volatile uint8 OCDR; // 0x51
		volatile uint8 reserved21; // 0x52
		volatile uint8 SMCR; // 0x53
		volatile uint8 MCUSR; // 0x54
		volatile uint8 MCUCR; // 0x55
		volatile uint8 reserved2[0x5D - 0x56];
		volatile uint8 SPL; // 0x5D
		volatile uint8 SPH; // 0x5E
		volatile uint8 SREG; // 0x5F
		volatile uint8 WDTCSR; // 0x60
		volatile uint8 CLKPR; // 0x61
		volatile uint8 reserved3[0x64 - 0x62];
		volatile uint8 PRR0; // 0x64
		volatile uint8 PRR1; // 0x65
		volatile uint8 OSCCAL; // 0x66
		volatile uint8 reserved4; // 0x67
		volatile uint8 PCICR; // 0x68
		volatile uint8 EICRA; // 0x69
		volatile uint8 EICRB; // 0x6A
		volatile uint8 PCMSK0; // 0x6B
		volatile uint8 PCMSK1; // 0x6C
		volatile uint8 PCMSK2; // 0x6D
		uint8 reserved9[0x73 - 0x6E];
		volatile uint8 PCMSK3; // 0x73
		volatile uint8 XMCRA; // 0x74
		volatile uint8 XMCRB; // 0x75
		volatile uint8 reserved5[0x78 - 0x76];
		volatile uint8 ADCL; // 0x78
		volatile uint8 ADCH; // 0x79
		volatile uint8 ADCSRA; // 0x7A
		volatile uint8 ADCSRB; // 0x7B
		volatile uint8 ADMUX; // 0x7C
		volatile uint8 DIDR2; // 0x7D
		volatile uint8 DIDR0; // 0x7E
		volatile uint8 DIDR1; // 0x7F
		volatile uint8 reserved6[0xB6 - 0x80]; // timer registers
		volatile uint8 ASSR;
		volatile uint8 reserved7;
		volatile uint8 TWBR;
		volatile uint8 TWSR;
		volatile uint8 TWAR;
		volatile uint8 TWDR;
		volatile uint8 TWCR;
		volatile uint8 TWAMR;
		volatile uint8 reserved8[2];

		Registers* operator->()
		{
			return reinterpret_cast<Registers*> (0);
		}
	};

};

}

}

}

#endif /* MACHINEDEFINITIONS_DEFAULT_H_ */
