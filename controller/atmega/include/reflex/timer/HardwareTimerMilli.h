#ifndef HardwareTimerMilli_h
#define HardwareTimerMilli_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 HardwareTimerMilli
 *
 *	Author:	 Stefan Nuernberger
 *
 *	Description: HardwareTimer in millisecond precision
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/timer/CompareMatch.h"
#include "reflex/timer/HardwareTimer.h"
#include "reflex/timer/Overflow.h"
#include "reflex/timer/Timer.h"

namespace reflex
{

/*!
 \ingroup atmega
 \brief Hardware timer implementation for virtual timers

 \todo Add some relevant documentation.

 */
class HardwareTimerMilli: public HardwareTimer, public Sink0
{
private:
	enum Precision
	{
		PrecisionShift = 5, //!< shift from clock to milliseconds
		OverflowShift = 5, //!< shift for the overflow counter to local time
		MaxTimerValue = MAX_UINT8 >> PrecisionShift, //!< largest interval
		SafeMinimum = 2	//!< minimum interval where it is safe to set timer directly
	};

    class Counter: public Sink0
	{
	public:
		Counter() :
			value(0)
		{
		}

		void notify();

		Time get()
		{
            return (Time) value;
		}

        uint64 get64()
        {
            return value;
        }

    private:
        uint64 value;
	};

	//Counter0* timer; // the real hardware timer
	Time delta; // time until notify
	Time time; // time when the timer was set
	Counter overflows; // overflow counter

public:

	/** constructor */
	HardwareTimerMilli();

	/** notify
	 * implements Sink0
	 * notifies output
	 */
	void notify();

	/** getNow
	 * @return current counter value (local time)
	 */
	Time getNow();

    /** getTimestamp
     * @return 64 bit counter value in hardware precision
     */
    uint64 getTimestamp();

	/** start
	 * set a timeout until next interrupt and start the timer
	 * @param ticks number of ticks until timer event
	 */
	void start(Time ticks);

	/**
	 * startAt
	 * set a new time interrupt to occur ticks after t0
	 * @param t0 time from where to set the timer
	 * @param ticks number of ticks after t0 when the timer should fire
	 */
	void startAt(Time t0, Time ticks);

	/** stop
	 * stop the hardware timer. The local time counter will still work,
	 * but the alarm counter will be turned off.
	 */
	void stop();

private:

	/** setAlarm
	 * set the alarm counter
	 */
	void setAlarm();

	mcu::Timer<mcu::Timer0> timer;
	mcu::Overflow<mcu::Timer0> overflow;
	mcu::CompareMatch<mcu::Timer0, mcu::ChannelA> compareMatch;
};

} // reflex

#endif
