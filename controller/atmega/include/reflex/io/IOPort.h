#ifndef PORT_H_
#define PORT_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2012 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/data_types/Flags.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/types.h"

namespace reflex {
namespace atmega {

/*!
 \addtogroup atmega
 @{
 */

/**
 \brief Bit masks for IO port-pins of the ATMEGA controller family

 \see IOPort
 */
enum PortPin {
	Pin7 = 1 << 7, //
	Pin6 = 1 << 6,
	Pin5 = 1 << 5,
	Pin4 = 1 << 4,
	Pin3 = 1 << 3,
	Pin2 = 1 << 2,
	Pin1 = 1 << 1,
	Pin0 = 1 << 0,
	AllPins = Pin7 | Pin6 | Pin5 | Pin4 | Pin3 | Pin2 | Pin1 | Pin0
};

/*!
 \brief A flag type to combine a set of port pins.
 */
typedef reflex::data_types::Flags<PortPin> PortPins;
DECLARE_OPERATORS_FOR_FLAGS(PortPins)

/*!
 \enum IOPortEnumerator
 \brief IO port enumerator on ATMEGA controllers
 \see IOPort
 */

/*!
 \brief Driver for IO Ports on the ATMEGA controller family

 IO ports on the ATMEGA are accessed by reading/writing bit masks from/to
 pseudo register files PortA .. PortK. The number of ports depends on
 the controller being used.

 To configure ::Pin0 and ::Pin1 as output
 and leave all other pins as input, use
 \snippet IOPortSnippets.cc Init Port
 To read ::Pin6 and ::Pin7, use
 \snippet IOPortSnippets.cc Read Port
 To write a logical 0 to ::Pin0 and 1 to ::Pin1 while leaving all other pins
 unchanged, use
 \snippet IOPortSnippets.cc Write Port

 Writing a '1' to the PIN register while the pin is configured as input
 activates the pin's pullup resistor.

 \see PortPins
 */
template<Core::IOPorts port>
struct IOPort {
	volatile uint8 PIN; ///< Input / Pull-up register
	volatile uint8 DDR; ///< Data direction register
	volatile uint8 PORT; ///< Output register

	//! Dereference operator to access an IO register directly
	IOPort* operator->() {
		return reinterpret_cast<IOPort*> (port);
	}
};

//! @}
}
}

#endif /* PORT_H_ */
