#ifndef SPIHARDWARE_H
#define SPIHARDWARE_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/MachineDefinitions.h"
#include "reflex/io/Spi.h"
#include "reflex/io/IOPort.h"
#include "reflex/types.h"

namespace reflex
{

namespace atmega
{

/**
 \ingroup atmega
 \brief Synchronous SPI hardware driver in bus master mode for the ATMEGA controller family.

 This driver offers several configuration options. The \a ChipSelectPort and \a ChipSelectPin
 are bound to a driver instance during compile time. When calling readWrite(), this pin is
 automatically set to low during data transfer. The link speed is dependend on the clock and
 can be set via setDivider() and clock phase and polarity via setDataMode().

 The port configuration is done immediatly after construction and will reset all used pins to
 behave as inputs when being destroyed.

 \see Spi for a more generous view on this driver
 \see McuImplementation and Mcu

 */
template<typename ChipSelectPort = IOPort<Core::PortB> , uint8 ChipSelectPin = Core::SS>
class SpiHardware: public Spi
{
public:
	/*!
	 \brief System clock divider.

	 SPI double speed mode is already taken into account.
	 */
	enum Divider
	{
		OSCdiv2 = Core::SPI2x << 8, //!< OSCdiv2
		OSCdiv4 = 0, //!< OSCdiv4
		OSCdiv8 = Core::SPI2x << 8 | Core::SPR0, //!< OSCdiv8
		OSCdiv16 = Core::SPR0, //!< OSCdiv16
		OSCdiv32 = Core::SPI2x << 8 | Core::SPR1,//!< OSCdiv32
		OSCdiv64 = Core::SPR1, //!< OSCdiv64
		OSCdiv128 = Core::SPR1 | Core::SPR0
	//!< OSCdiv128
	};

	/*!
	 \brief Clock/data phase and polarity.

	 See the datasheet for the meaning of the different modes.
	 */
	enum DataMode
	{
		Mode0 = 0x0, Mode1 = Core::CPHA, Mode2 = Core::CPOL, Mode3 = Core::CPOL + Core::CPHA
	};

	SpiHardware();
	~SpiHardware();
	DataMode dataMode() const;
	Divider divider() const;
	void setCsEnabled(bool enabled);
	void setDataMode(DataMode mode);
	void setDivider(Divider divider);
	void readWrite(void* addr, size_t length);

private:

	enum
	{
		PolarityMask = Core::CPOL | Core::CPHA,
		DividerMaskLo = Core::SPR1 | Core::SPR0,
		DividerMaskHi = Core::SPI2x
	};

	inline uint8 readWrite(uint8 data);

	typedef Core::Registers Registers;
	typedef IOPort<Core::PortB> SpiPort;

};

template<typename ChipSelectPort, uint8 ChipSelectPin>
SpiHardware<ChipSelectPort, ChipSelectPin>::SpiHardware()
{
	SpiPort()->DDR |= (Core::MOSI | Core::SCK);
	SpiPort()->DDR &= ~Core::MISO;
	ChipSelectPort()->DDR |= ChipSelectPin;
	ChipSelectPort()->PORT |= ChipSelectPin;
	Registers()->SPCR = Core::SPE | Core::MSTR;
}

template<typename ChipSelectPort, uint8 ChipSelectPin>
SpiHardware<ChipSelectPort, ChipSelectPin>::~SpiHardware()
{
	Registers()->SPCR = 0;
	ChipSelectPort()->PORT &= ~ChipSelectPin;
	ChipSelectPort()->DDR &= ~ChipSelectPin;

	SpiPort()->DDR &= ~(Core::MOSI | Core::SCK | Core::MISO);
}

template<typename ChipSelectPort, uint8 ChipSelectPin>
typename SpiHardware<ChipSelectPort, ChipSelectPin>::Divider SpiHardware<ChipSelectPort, ChipSelectPin>::divider() const
{
	return static_cast<Divider> ((Registers()->SPCR & DividerMaskLo) | (Registers()->SPSR & DividerMaskHi)
			<< 8);
}

template<typename ChipSelectPort, uint8 ChipSelectPin>
typename SpiHardware<ChipSelectPort, ChipSelectPin>::DataMode SpiHardware<ChipSelectPort, ChipSelectPin>::dataMode() const
{
	return static_cast<DataMode> (Registers()->SPCR & PolarityMask);
}

template<typename ChipSelectPort, uint8 ChipSelectPin>
void SpiHardware<ChipSelectPort, ChipSelectPin>::setDivider(Divider divider)
{
	/* Set clock speed */
	Registers()->SPCR &= ~DividerMaskLo;
	Registers()->SPCR |= divider;
	Registers()->SPSR &= ~(DividerMaskHi);
	Registers()->SPSR |= divider >> 8;
}

template<typename ChipSelectPort, uint8 ChipSelectPin>
void SpiHardware<ChipSelectPort, ChipSelectPin>::setDataMode(DataMode mode)
{
	/* Set spi mode */
	Registers()->SPCR &= ~PolarityMask; //clear the two bits
	Registers()->SPCR |= mode; //set only bits for DataMode
}

/*!
 \brief Synchronous read/write operation for a given memory region.

 Performs the data transfer operation and sets the configured pin for chip select
 \a ChipSelectPin on \a ChipSelectPort.
 */
template<typename ChipSelectPort, uint8 ChipSelectPin>
void SpiHardware<ChipSelectPort, ChipSelectPin>::readWrite(void* addr, size_t size)
{
	uint8* pos = reinterpret_cast<uint8*> (addr);
	while (size > 0)
	{
		*pos = this->readWrite(*pos);
		pos++;
		size--;
	}
}

/**
 \brief Executes a single byte read/write operation.
 */
template<typename ChipSelectPort, uint8 ChipSelectPin>
uint8 SpiHardware<ChipSelectPort, ChipSelectPin>::readWrite(uint8 data)
{
	Registers()->SPDR = data;
	while ((Registers()->SPSR & Core::SPIF) != Core::SPIF)
	{
	}
	return Registers()->SPDR;
}

template<typename ChipSelectPort, uint8 ChipSelectPin>
void SpiHardware<ChipSelectPort, ChipSelectPin>::setCsEnabled(bool enabled)
{
	if (enabled)
	{
		ChipSelectPort()->PORT &= ~ChipSelectPin;
	}
	else
	{
		ChipSelectPort()->PORT |= ChipSelectPin;
	}
}

}

} //namespace reflex


#endif /* SPIHARDWARE_H */
