/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 InvalidInterruptHandler

 *	Author:	Karsten Walther, Carsten Schulze
 *
 *	Description: implementation of InterruptGuardian's init and register method
 *	             definition of InvalidInterruptHandler
 */
#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/types.h"
#include "reflex/debug/Assert.h"

using namespace reflex;

void InterruptGuardian::init()
{
	//all handler are initialized to invalidInterruptHandler
	for (unsigned int i = 0; i < mcu::interrupts::MAX_HANDLERS; i++)
	{
		handlers[i] = &InterruptHandler::invalidInterruptHandler;
	}
}

void InterruptGuardian::registerInterruptHandler(InterruptHandler* handler,
		mcu::InterruptVector vector)
{
	Assert(vector < mcu::interrupts::MAX_HANDLERS)
	if (vector != mcu::interrupts::INVALID_INTERRUPT)
	{
		handlers[vector] = handler;
	}
}

void InterruptGuardian::unregisterInterruptHandler(InterruptHandler* handler,
		mcu::InterruptVector vector)
{
	Assert(vector > mcu::interrupts::INVALID_INTERRUPT && vector < mcu::interrupts::MAX_HANDLERS)
	Assert(handlers[vector] == handler)
	handlers[vector] = &InterruptHandler::invalidInterruptHandler;
}
