#include "reflex/memory/Eeprom.h"
#include "reflex/interrupts/InterruptLock.h"

using namespace reflex;
using namespace atmega;

bool Eeprom::isWriteOperationPending()
{
	return Registers()->EECR & Core::EEPE;
}

uint8 Eeprom::readByte(const void* addr)
{
	while (Registers()->EECR & Core::EEPE)
	{
		; //wait for previous write to complete
	}

	InterruptLock lock;

	Registers()->EEARH = ((reinterpret_cast<uint16> (addr) >> 8) & 0x0F); //write only bytes 11-8
	Registers()->EEARL = reinterpret_cast<uint16> (addr);
	Registers()->EECR |= Core::EERE; // enable read, data is available immediately

	return (Registers()->EEDR);
}

/*
 *  NOTE that the SPMEN in SPMCSR should be checked for zero, but
 *  since we do not write to other memories it seems to be save
 */
void Eeprom::writeByte(uint8* addr, uint8 data)
{
	//wait for previous write to complete
	while (Registers()->EECR & Core::EEPE)
	{
		;
	}

	if (readByte(addr) == data)
	{
		return;
	}

	uint8 addrMsb = reinterpret_cast<uint16> (addr) >> 8;
	uint8 addrLsb = reinterpret_cast<uint16> (addr);

	InterruptLock lock;

	Registers()->EEARH = addrMsb;
	Registers()->EEARL = addrLsb;
	Registers()->EEDR = data;

	/* The following access sequence does not correlate with the data sheet,
	 * but there is no other way to meet the timing criteria from within C++.
	 */
	Registers()->EECR = Core::EEMPE; // set EEPROM master programming enable
	Registers()->EECR = Core::EEMPE | Core::EEPE; // set EEPROM programming enable
}
