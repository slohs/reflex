#include "reflex/io/PinChangeInterrupt.h"
#include "reflex/System.h"

using namespace reflex;
using namespace atmega;

PinChangeInterrupt::PinChangeInterrupt() :
			InterruptHandler(static_cast<InterruptVector> (Core::INVALID_INTERRUPT),
					PowerManageAble::SECONDARY)
{
	_pinGroup = InvalidPinGroup;
	out_event = 0;
	this->setSleepMode(mcu::StandBy);
}

PinChangeInterrupt::~PinChangeInterrupt()
{
	if (pinGroup() != InvalidPinGroup)
	{
		setPinsDisabled(AllPins);
	}
}

/*!
 The driver is enabled on the first enabled pin of the current group.
 This method will fail if no pin-group has been set.
 */
void PinChangeInterrupt::setPinsEnabled(PortPins pins)
{
	Assert(pinGroup() != InvalidPinGroup)

	switch (_pinGroup)
	{
	case PinGroup3:
		Register()->PCMSK3 |= pins;
		break;
	default:
		*(&Register()->PCMSK0 + _pinGroup) = pins;
	}

	if (!this->isEnabled())
	{
		this->switchOn();
	}
}

/*!
 The driver is disabled if the last pin of the current group was disabled.
 This method will fail if no pin-group has been set.
 */
void PinChangeInterrupt::setPinsDisabled(PortPins pins)
{
	Assert(pinGroup() != InvalidPinGroup)

	*(&Register()->PCMSK0 + _pinGroup) &= ~pins;

	if (*(&Register()->PCMSK0 + _pinGroup) == 0)
	{
		this->switchOff();
	}
}

void PinChangeInterrupt::handle()
{
	if (this->out_event)
	{
		switchOff();
		this->out_event->notify();
	}
}

/*!
 This method fails if no pin-group has been set.
 */
void PinChangeInterrupt::enable()
{
	Assert(pinGroup() != InvalidPinGroup)

#ifdef atmega169
	Register()->EIMSK |= (1 << (_pinGroup+6));
#else
	Register()->PCICR |= (1 << _pinGroup);
#endif
}

/*!
 This method will fail if no pin-group has been set.
 */
void PinChangeInterrupt::disable()
{
	Assert(pinGroup() != InvalidPinGroup)

#ifdef atmega169
	Register()->EIMSK &= ~(1 << (_pinGroup+6));
#else
	Register()->PCICR &= ~(1 << _pinGroup);
#endif
}

PinChangeInterrupt::PinGroup PinChangeInterrupt::pinGroup() const
{
	return static_cast<PinGroup> (_pinGroup);
}

/*!
 This method fails if not all pins of the old group has been disabled
 before.
 */
void PinChangeInterrupt::setPinGroup(PinGroup group)
{
	Assert(!this->isEnabled())

	if (_pinGroup != InvalidPinGroup)
	{
		getSystem().guardian.unregisterInterruptHandler(this, Core::INTVEC_PCINT0 + _pinGroup);
	}
	_pinGroup = group;
	if (_pinGroup != InvalidPinGroup)
	{
		getSystem().guardian.registerInterruptHandler(this, Core::INTVEC_PCINT0 + _pinGroup);
	}
}

/*!
 This method fails if no pin-group has been set.
 */
uint8 PinChangeInterrupt::enabledPins() const
{
	Assert(pinGroup() != InvalidPinGroup)

	uint8 pins = 0;
	switch (_pinGroup)
	{
	case PinGroup3:
		pins = Register()->PCMSK0;
		break;
	default:
		pins = *(&Register()->PCMSK0 + _pinGroup);
	}
	return pins;
}

