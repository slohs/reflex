# Compiler and Linker Configuration for atmega controllers.

TOOLPREFIX ?= $(TOOLPATH)avr-

# AS is overwritten in System.mk with gcc
# Set it back to as here
ifeq ($(origin AS), default)
	AS = $(TOOLPREFIX)as
endif

LIBPATH =

ifdef DEVICE
DEFINED_REFLEX_VARS += "DEVICE ="$(DEVICE)
DEFINES +=-D'$(DEVICE)' # DEVICE is needed in header files
endif
ifndef DEVICE
MISSING_REFLEX_VARS += "DEVICE is the microcontroller variant"
endif

ifdef LIBPATH
DEFINED_REFLEX_VARS += "LIBPATH = " $(LIBPATH)
else
MISSING_REFLEX_VARS += "LIBPATH - library path of the avrgcc gcc-lib"
endif

#LIBPATH		+= -L$(GCC_INSTALL_PATH)/

#######  Compiler and Linker Flags ########
ASFLAGS		+= -mmcu=$(DEVICE)

CFLAGS		= -mmcu=$(DEVICE) -mrelax
CFLAGS		+= -ansi -pedantic
CFLAGS		+= -Wall -Wno-unused-local-typedefs -fno-builtin -Wno-variadic-macros
CFLAGS		+= -Os -flto
CFLAGS		+= -fdata-sections -ffunction-sections # Link time garbage collection

CXXFLAGS	=	$(CFLAGS) -std=c++0x
CXXFLAGS	+=  -Wno-non-virtual-dtor
CXXFLAGS	+=	-fno-rtti -fno-exceptions # Link time garbage collection

LDFLAGS		+=	$(LIBPATH) $(CXXFLAGS)
LDFLAGS 	+= -Wl,--gc-sections

#######  Includes and Base Sources for the Controller ########

INCLUDES += -I$(REFLEXPATH)/controller/$(CONTROLLER)/include
include $(REFLEXPATH)/controller/$(CONTROLLER)/Sources.mk
