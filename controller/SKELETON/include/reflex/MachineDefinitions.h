#ifndef MachineDefinitions_h
#define MachineDefinitions_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	none
 *
 *	Author:		Sören Höckner
 *
 *	Description:	Platform specific definitions like base addresses of
 *					device register blocks.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */


namespace reflex {
namespace skeleton {
enum RegisterBase
{
	ADC_BASE = 0x24,
	PORTA_BASE = 0x39,
	PORTB_BASE = 0x36,
	PORTC_BASE = 0x33,
	PORTD_BASE = 0x30,
	PORTE_BASE = 0x21,
	PORTF_BASE = 0x60,
	PORTG_BASE = 0x63,
};

enum ClockFrequency {
	TICKS_PER_MS = 16000 //in ms since Frequency (8MHz) cannot represented with 16bits
};

#WARINING "Define the controller specific power modifications"
enum PowerModes {
	ACTIVE=0,
	PM_IDLE,
	PM_WAIT, 
	PM_SUSPEND,
	PM_POWER_DOWN,
	DEEPESTSLEEPMODE=PM_POWER_DOWN,
	NrOfPowerModes = 5
};
}
namespace mcu=skeleton; //alias for the core reflex sources
} //namespace reflex

#endif

