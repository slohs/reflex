#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:	  Karsten Walther
# *
#*
 #*    This file is part of REFLEX.
 #*
 #*    REFLEX is free software: you can redistribute it and/or modify
 #*    it under the terms of the GNU Lesser General Public License as 
 #*    published by the Free Software Foundation, either version 3 of the 
 #*    License, or (at your option) any later version.
 #*
 #*    REFLEX is distributed in the hope that it will be useful,
 #*    but WITHOUT ANY WARRANTY; without even the implied warranty of
 #*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #*    GNU Lesser General Public License for more details.
 #*
 #*    You should have received a copy of the GNU General Public License
 #*    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 #* */



TOOLPREFIX ?= $(TOOLPATH)skeleton-

#change the utils if needed
#
#ifeq ($(origin AS), default)
#	AS = $(TOOLPREFIX)gcc
#endif
#ifeq ($(origin CC), default)
#	CC = $(TOOLPREFIX)gcc
#endif
#ifeq ($(origin CXX), default)
#	CXX = $(TOOLPREFIX)g++
#endif
#ifeq ($(origin LD), default)
#	LD = $(TOOLPREFIX)g++
#endif
#ifeq ($(origin LD), default)
#	AR = $(TOOLPREFIX)ar
#endif
#ifeq ($(origin OBJCOPY), default)
#	OBJCOPY = $(TOOLPREFIX)objcopy
#endif
#ifeq ($(origin OBJDUMP), default)
#	OBJDUMP = $(TOOLPREFIX)objdump
#endif

LDLIBS += -lgcc

ifdef DEVICE
DEFINED_REFLEX_VARS += "DEVICE = " $(DEVICE)
endif
ifndef DEVICE
MISSING_REFLEX_VARS += "DEVICE is the microcontroller variant"
endif

ifdef LIBPATH
DEFINED_REFLEX_VARS += "LIBPATH = " $(LIBPATH)
else
MISSING_REFLEX_VARS += "LIBPATH - library path of the avrgcc gcc-lib"
endif


ASFLAGS		+=	-mmcu=$(DEVICE)
CFLAGS		?=	-O1 -Wall -fno-builtin
CFLAGS		+=	-mmcu=$(DEVICE) 
CXXFLAGS	?=	-O2 -Wall -fno-rtti -fno-builtin -fno-exceptions \
    			-fomit-frame-pointer -Wno-non-virtual-dtor
CXXFLAGS	+= -mmcu=$(DEVICE) 

###################### includes and base sources for the controller ########

INCLUDES += -I$(REFLEXPATH)/controller/$(CONTROLLER)/include
include $(REFLEXPATH)/controller/$(CONTROLLER)/Sources.mk


