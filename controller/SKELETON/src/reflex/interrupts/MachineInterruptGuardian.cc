/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 InvalidInterruptHandler
  
 *	Author:	Karsten Walther, Carsten Schulze, Sören Höckner
 *
 *	Description: implementation of InterruptGuardian's init and register method
 *	             definition of InvalidInterruptHandler
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as 
 *    published by the Free Software Foundation, either version 3 of the 
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */

#include "reflex/interrupts/InterruptGuardian.h"
#include "reflex/interrupts/InterruptVector.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/types.h"
#include "reflex/debug/Assert.h"

using namespace reflex;


namespace reflex {

/** InterruptHandler implementation which can be used to detect occurence
 *  of non-registered interrupts.
 */
class InvalidInterruptHandler : public InterruptHandler
{
public:
	InvalidInterruptHandler() : InterruptHandler(INVALID_INTERRUPT) {}

    virtual void handle() { ASSERT(false); };
	virtual void enable() {};
	virtual void disable() {};
};

} //reflex

/** only one instance of default interrupt handler is needed */
InvalidInterruptHandler invalidInterruptHandler;

void InterruptGuardian::init()
{
	//all handler are initialized to invalidInterruptHandler
	for(unsigned int i = 0; i < MAX_HANDLERS; i++){
		handlers[i] = &invalidInterruptHandler;
	}
}

void InterruptGuardian::registerInterruptHandler(
	InterruptHandler* handler,
	InterruptVector vector)
{
	if((vector >= INVALID_INTERRUPT) && (vector < MAX_HANDLERS) ){
		// register handler in logical interrupt table
		handlers[vector] = handler;
	}
}

