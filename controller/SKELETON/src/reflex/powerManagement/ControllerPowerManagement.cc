/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 InvalidInterruptHandler
  
 *	Author:	Karsten Walther, Sören Höckner
 *
 *	Description: implementation of InterruptGuardian's init and register method
 *	             definition of InvalidInterruptHandler
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as 
 *    published by the Free Software Foundation, either version 3 of the 
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */



#include "reflex/debug/StaticAssert.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/powerManagement/PowerManager.h"

extern "C" {
	void _idle();
	void _wait();
	void _suspend();
	void _powerdown();
}

using namespace reflex;

void (* const reflex::PowerManager::modes[NrOfPowerModes])() = {PowerManager::active,_idle,_wait,_suspend,_powerdown};
