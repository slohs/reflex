/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze, Karsten Walther
 */
#include "reflex/io/Serial.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/io/Port.h"
#include "reflex/types.h"
#include "reflex/System.h"
#include "reflex/debug/Assert.h"

using namespace reflex;

Serial::Serial(uint8 id, SerialRegisters::Baud baud)
	: rxHandler((mcu::InterruptVector)(mcu::interrupts::INTVEC_USART0_RXI - (6*id)) ,*this,PowerManageAble::PRIMARY)
	, txHandler((mcu::InterruptVector)(mcu::interrupts::INTVEC_USART0_TXI - (6*id)) ,*this,PowerManageAble::SECONDARY)//set also secondary bit for txHandler
	, txDisableAct(*this)
{
	input.init(this);
	this->id = id;
	sender = 0;
	receiver = 0;
	current = 0;

	//set deepest allowed sleep mode
	mcu::PowerModes sleepMode;


	regs = ((volatile struct SerialRegisters*)(mcu::UART0_BASE)) + id;
	sfrRegs = (volatile struct SFRRegisters*)(uint16)id;

	regs->UCTL |= SerialRegisters::SWRST;


	//  uart calculator: http://mspgcc.sourceforge.net/baudrate.html
	//  @baudrates below 19600 the aclock is used, since it is more stable on broken tmodeskys
	switch(baud)
	  {
	  case SerialRegisters::B300:
	    regs->UTCTL = SerialRegisters::SSEL01; //use ACLOCK
	    regs->UMCTL=0x44;
		sleepMode=mcu::LPM3;
	    break;
	  case SerialRegisters::B600:
	    regs->UTCTL = SerialRegisters::SSEL01; //use ACLOCK
	    regs->UMCTL=0xB5;
		sleepMode=mcu::LPM3;
		break;
	  case SerialRegisters::B1200:
	    regs->UTCTL = SerialRegisters::SSEL01; //use ACLOCK
	    regs->UMCTL=0x03;
		sleepMode=mcu::LPM3;
		break;
	  case SerialRegisters::B2400:
	    regs->UTCTL = SerialRegisters::SSEL01; //use ACLOCK
	    regs->UMCTL=0x6B;
		sleepMode=mcu::LPM3;
		break;
	  case SerialRegisters::B4800:
	    regs->UTCTL = SerialRegisters::SSEL01; //use ACLOCK
	    regs->UMCTL=0x6F;
		sleepMode=mcu::LPM3;
		break;
	  case SerialRegisters::B9600:
	    regs->UTCTL = SerialRegisters::SSEL01; //use ACLOCK
		sleepMode=mcu::LPM3;
		regs->UMCTL=0x29;
	    break;
	  case SerialRegisters::B19200:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0x5b;
		sleepMode=mcu::LPM1;
		break;
	  case SerialRegisters::B31250:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0x00;
		sleepMode=mcu::LPM1;
		break;
	  case SerialRegisters::B38400:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0x11;
		sleepMode=mcu::LPM1;
		break;
	  case SerialRegisters::B56000:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0xf7;
		sleepMode=mcu::LPM1;
		break;
	  case SerialRegisters::B57600:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0xef;
		sleepMode=mcu::LPM1;
		break;
	  case SerialRegisters::B115200:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0xaa;
		sleepMode=mcu::LPM1;
		break;
	  case SerialRegisters::B128000:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0x55;
		sleepMode=mcu::LPM1;
		break;
	  case SerialRegisters::B230400:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0xdd;
		sleepMode=mcu::LPM1;
		break;
	  case SerialRegisters::B460800:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0x52;
		sleepMode=mcu::LPM1;
		break;
	  case SerialRegisters::B921600:
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL=0x5b;
		sleepMode=mcu::LPM1;
		break;
	  default:
	    // just default used, because selected Clock is very fast
	    regs->UTCTL = SerialRegisters::SSEL10; //use smclk
	    regs->UMCTL = SerialRegisters::MCTLDEFAULT;
		sleepMode=mcu::LPM1;
	  }
	//set Baudrate
	regs->UBR0 = (char)baud;
	regs->UBR1 = (char)(baud>>8);

	regs->URCTL = 0;

	txHandler.setSleepMode(sleepMode);
	rxHandler.setSleepMode(sleepMode);


	// set Port 3 pins for USART0
	Port3()->SEL |= Port::UART0_BITS << (2*id);
	Port3()->DIR |= Port::TX0_BIT << (2*id);
	Port3()->DIR &= ~Port::RX0_BIT << (2*id);
	Port3()->OUT &= ~Port::TX0_BIT << (2*id);

	//volatile Port p1(Port::PORT1);
	Port1()->SEL &= ~(Port::CTS_IN | Port::CTS_OUT);
	Port1()->OUT &= ~Port::CTS_OUT;
	Port1()->DIR |= Port::CTS_OUT;

	regs->UCTL = SerialRegisters::USART_CHAR;
}

void Serial::init(Sink1<char>* receiver, Sink0* sender)
{
	this->receiver = receiver;
	this->sender = sender;
}

/*	Note: to wait explicitly for the clear interrupt before sending the next Bufer
	leads to a lower throughput. But this happens only, if a lot of small Buffers
	are used. If you wanne high throughput via Serial, use larger Buffers.
  */
void Serial::handleTX()
{
	Assert(current);

	if(length==0){
		// everything has been written so
		if(sender)	{sender->notify();}
		current->downRef();
		if (!this->triggered()) {
			//txHandler.switchOff();
			//initiate switchoff
			txDisableAct.trigger();
		}
		this->unlock();
		return;
	}
	// write next byte to buffer
	length--;
	regs->UTXBUF = *pos++;
}

void Serial::handleRX()
{
	if (regs->URCTL & SerialRegisters::RXERR) { // check for errors
		regs->URCTL &= (char) ~(
						SerialRegisters::FE |
						SerialRegisters::PE |
						SerialRegisters::OE |
						SerialRegisters::BRK |
						SerialRegisters::RXERR); // clear error flags
		return;
	}
	char tmp = regs->URXBUF;
	if (receiver) receiver->assign(tmp);
}

void Serial::run()
{
	current = input.get();
	Assert(current);
	length = current->getLength();
	if (length > 0)
	{
		this->lock();
		txHandler.switchOn();
		pos = reinterpret_cast<char*> (current->getStart());

		// write first byte to buffer
		length--;
		regs->UTXBUF = *pos++;
	}
	else
	{
		current->downRef();
		if (sender)
		{
			sender->notify();
		}
	}
}

void Serial::enableRX()
{
	sfrRegs->ME |= SerialRegisters::URXE0  >> (id * 2);
	sfrRegs->IE |=  SerialRegisters::URXIE0 >> (id * 2);
}

void Serial::disableRX()
{
	sfrRegs->ME &= ~(SerialRegisters::URXE0  >> (id * 2));
	sfrRegs->IE &= ~(SerialRegisters::URXIE0 >> (id * 2));
}

void Serial::enableTX()
{
	sfrRegs->ME |= SerialRegisters::UTXE0  >> (id * 2);
	sfrRegs->IE |= SerialRegisters::UTXIE0 >> (id * 2);
}

void Serial::disableTX()
{
	sfrRegs->ME &= ~(SerialRegisters::UTXE0  >> (id * 2));
	sfrRegs->IE &= ~(SerialRegisters::UTXIE0 >> (id * 2));
}

void Serial::transmitterFinished()
{
	/*check, whether the transmission has finished before
	we can disable the transmitter*/
	if ( (regs->UTCTL & (SerialRegisters::TXEPT)) == 0 ) {
		InterruptLock lock;
		// prevent this activity to be scheduled after the Serial::run activity
		if ( !this->triggered() ) {
			txDisableAct.trigger();
		}
		return;
	}
	txHandler.switchOff();
}
