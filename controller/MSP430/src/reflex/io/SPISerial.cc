/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze, Karsten Walther
 */
#include "reflex/io/Serial.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/System.h"
#include "reflex/io/Port.h"
#include "reflex/io/SerialRegisters.h"
#include "reflex/io/SPISerial.h"

using namespace reflex;

SPISerial::SPISerial(unsigned char nr) :
	txiHandler(mcu::interrupts::INTVEC_USART1_TXI ,*this,PowerManageAble::PRIMARY),
	rxiHandler(mcu::interrupts::INTVEC_USART1_RXI ,*this,PowerManageAble::PRIMARY),
	input(this)
{
	sender = 0;
	receiver = 0;
	current = 0;

	txiHandler.setSleepMode(mcu::LPM0);
	txiHandler.isSecondary=true;
	rxiHandler.setSleepMode(mcu::LPM0);

	regs = (volatile SerialRegisters*)(mcu::USART_BASE);
	regs += 1; //correction of offset for uart1
	
	regsSFR = (volatile SFRRegisters*)(mcu::USART_SFR_BASE);
	(char*)regsSFR += nr; //correction of offset for uart1
	
	// configure USART0 for SPI mode, 8 bit characters, master mode
	regs->UCTL = (SerialRegisters::USART_CHAR | SerialRegisters::MM |
	SerialRegisters::SYNC | SerialRegisters::SWRST);

	// delay UCLK by half clock cycle, falling edge output, use SMCLK, 3 wire SPI
	regs->UTCTL = (SerialRegisters::CKPH | /* SerialRegisters::CKPL | */
	SerialRegisters::SSEL10 | SerialRegisters::STC);
	
	// set highest clock speed
	regs->UBR0 = 0x02;
	regs->UBR1 = 0;
	regs->UMCTL = 0;
	
	
	
	// clear interrupt flags
	regsSFR->IFG &= ~(SerialRegisters::UTXIFG0 | SerialRegisters::URXIFG0);
	
	// release software reset
	regs->UCTL &= ~SerialRegisters::SWRST;
	
	enable();
	
	//led.turnOff(Led::ALL);
}

void SPISerial::init(Sink1<char>* receiver, Sink0* sender)
{
	this->receiver = receiver;
	if(receiver){
		regsSFR->IE |= SerialRegisters::URXIE1;
	}else{
		regsSFR->IE &= ~SerialRegisters::URXIE1;
	}
	this->sender = sender;
}

void SPISerial::handleTXI()
{
	if (! current) {
		return;
	}
	
	if(length--){
		regs->UTXBUF = *pos++;
	}else{
		if(sender){
			sender->notify();
		}
		current->downRef();
//		getSystem().powerManager.disableObject(&txHandler);
		txHandler.switchOff();
		unlock();
		//led.blink(Led::RED);
	}
}

void SPISerial::handleRXI()
{
	if (regs->URCTL & SerialRegisters::RXERR) { // check for errors
		regs->URCTL &= (char) ~(
						SerialRegisters::FE |
						SerialRegisters::PE |
						SerialRegisters::OE |
						SerialRegisters::BRK |
						SerialRegisters::RXERR); // clear error flags
		return;
	}
	//led.blink(Led::BLUE);
	if ( receiver) 
		receiver->assign(regs->URXBUF);
	
	
}

void SPISerial::run()
{
	this->lock();
	
	current = input.get();
	length = current->getLength();
	pos = (char*)current->getStart();

	if ( !length ) {
		current->downRef();
		return;
	}
	
	length--;
	
	regs->UTXBUF = *pos++;
	
}

void SPISerial::enable()
{
	//enable USART interrupts
	
	volatile Port p3(Port::PORT3);
	
	// SPI pins are multiplexed with Port 3 general-purpose IO
	// Select module funtionality
	*p3.SEL |= Port::SPI0_BITS;
	p3.reg->DIR |= (Port::SIMO0 | Port::UCLK0);
	p3.reg->DIR &= ~Port::SOMI0;
	
	regsSFR->ME |= SerialRegisters::USPIE0;
	
	regsSFR->IE = SerialRegisters::URXIE0 | SerialRegisters::UTXIE0;
	
}

void SPISerial::disable()
{
	volatile Port p3(Port::PORT3);
	
	// SPI pins are multiplexed with Port 3 general-purpose IO
	// Select IO funtionality
	*p3.SEL &= ~Port::SPI0_BITS;
	p3.reg->DIR &= ~Port::SPI0_BITS;
	
	volatile SerialSFRRegisters* regsSFR = (volatile SerialSFRRegisters*)(USART_SFR_BASE);
	
	regsSFR->ME &= ~SerialRegisters::USPIE0;

	//disable USART interrupts
	regsSFR->IE &= ~SerialRegisters::URXIE0 & ~SerialRegisters::UTXIE0;
}

