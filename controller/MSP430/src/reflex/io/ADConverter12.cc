/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:	Carsten Schulze
 */
#include "reflex/MachineDefinitions.h"
#include "reflex/io/ADCRegisters.h"
#include "reflex/interrupts/InterruptVector.h"
#include "reflex/io/ADConverter12.h"


using namespace reflex;

ADConverter12::ADConverter12() 
	: InterruptHandler(mcu::interrupts::ADC12,PowerManageAble::SECONDARY)
	, input(this)
{
	this->setSleepMode(mcu::LPM0); //FIXME: verify that sleepmode for ADConverter
	registers = (ADC12Registers*)mcu::ADC12_BASE;
	keep = false;
}

void ADConverter12::init(Sink2<char, uint16>* output)
{
	this->output = output;
}

void ADConverter12::run()
{
	//if no conversion is in progress
	if(!(registers->ADC12CTL1 & ADC12BUSY)) {
		currentChannel = input.get() & 0xf; //store channel number

		/*
			use addr 0, we only make single conversation
			SHP = 1 -> SAMPCON from sampling timer
			Clock divider = 1
			ADC12SSEL11 = use SMCLK
			CONSEQ0 = single conversion single channel
		*/
		
		//stop converter
		if (keep == false) {
			//enable();
			//getSystem().powerManager.enableObject(this);
			this->switchOn();
		}
	
		registers->ADC12CTL1 = CSTARTADD_0 | SHP | ADC12SSEL11 | CONSEQ_00 | SHS_00;

		registers->ADC12CTL0 &= ~ENC; // clear ENC to access ADC12MCTL0

		/* use only channel 0, but with different input
		   SREF_0 -> reference ... see manual
		*/
		registers->ADC12MCTL[0] = currentChannel | SREF_000 ; //set channel

		// registers->ADC12MEM[0] = 0; // clearing ?
		registers->ADC12CTL0 |= ADC12ON | ENC | ADC12SC | REFON | ADC12TOVIE | SHT0_0 ; //starting conversion
	}
}

void ADConverter12::handle()
{
	//interrupt flag is cleared when accessing the memory register

	unsigned short result = registers->ADC12MEM[0]; //use allway 0

	//stop converter
	if (keep == false) {
		//disable();
		//getSystem().powerManager.disableObject(this);
		this->switchOff();
	}
	//propagate sampled value
	//	getApplication().out.write("done");
	//getApplication().out.writeln();
	if(output)	output->assign(currentChannel, result);
}

void ADConverter12::enable()
{
	registers->ADC12CTL0 &= ~ENC; // clear ENC to access ADC12MCTL0
	registers->ADC12CTL0 |= ADC12ON | REFON;
	registers->ADC12IE = ADC12IE_0; // turn on the only used interrupt
}

void ADConverter12::disable()
{
	registers->ADC12CTL0 &= ~ENC; // clear ENC to access ADC12MCTL0
	registers->ADC12CTL0 &= ~( ADC12ON | REFON );
	registers->ADC12IE &= ~ADC12IE_0; // disable used interrupt
}

void ADConverter12::keepOn(bool keep )
{
	this->keep = keep;
	//start converter, if allways on
	if (keep == true) {
		//enable();
		//getSystem().powerManager.enableObject(this);
		this->switchOn();
		
	}
}
