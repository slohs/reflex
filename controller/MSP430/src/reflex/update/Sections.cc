/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze
 *
 *	(c) Carsten Schulze, BTU Cottbus 2006
 */

#include "reflex/update/Sections.h"

using namespace reflex;

extern "C" char __codeEnd__;
extern "C" char __codeStart__;
extern "C" char __codeStartALL__;
extern "C" char __endUpdateAble;
extern "C" char __startSpaceforUpdate;
extern "C" char __endSpaceforUpdate;


Sections::Sections()
{
	UpdateAble[0] = (caddr_t)&__codeStart__;
	UpdateAble[1] = (caddr_t)&__codeEnd__;
	
	UpdateAbleALL[0] = (caddr_t)&__codeStartALL__;
	UpdateAbleALL[1] = (caddr_t)&__codeEnd__;
	
	SpaceforUpdateImg[0] = (caddr_t)&__startSpaceforUpdate;
	SpaceforUpdateImg[1] = (caddr_t)&__endSpaceforUpdate;	
}
