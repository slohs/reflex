/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze
 */

#include "reflex/timer/BasicClockModule.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/timer/TimerB.h"

using namespace reflex;
using namespace timerB;

TimerB::TimerB(Sink0& output) 
	: InterruptHandler(mcu::interrupts::TIMER_B1_TBR,PowerManageAble::PRIMARY)
	, output(output)
{
	timerBReg = (TimerBReg*)mcu::TIMERB_REGISTERS_BASE;

	//SPEED : SMCLK / 4 (should be near CPU cycles
//	timerBReg->TBCTL = TBSSEL10 | MC10 | TBID10; //also disables interrupt, continue mode
	//SPEED : ACLK / 1, up mode
	timerBReg->TBCTL = TBSSEL01 | MC01 | TBID00;

	timerBReg->TBIV &= ~TBIVx; //reset pending interrupt

	timerBReg->TBR = 0;
	timerBReg->TBCCR0 = 0;

	//counts to maximum
	timerBReg->TBCTL &= ~TBCNTLx;

	timerBReg->TBCCTL0 = CM10 | OUTMOD101;

	//setTime(TIME); //make Interrupts by myself

	//start();
	/* set deepest sleep mode (LPM1 with SMCLK, LPM3 with ACLK) */
	this->setSleepMode(mcu::LPM3);
}

void TimerB::start()
{
	this->switchOn(); //enable through powermanager
}
void TimerB::stop()
{
	this->switchOff(); //disable through powermanager
}

void TimerB::handle()
{
	timerBReg->TBIV &= ~TBIVx; //reset pending interrupt
	output.notify();
}

void TimerB::enable()
{
	timerBReg->TBIV &= ~TBIVx; //reset pending interrupt
	timerBReg->TBCTL |= TBIE;
}

void TimerB::disable()
{
	timerBReg->TBCTL &= ~TBIE;
	timerBReg->TBCTL &= ~TBIFG;
}

void TimerB::enableCapture(TimerID timer, bool int_enable, Device device)
{
	volatile uint16 *tbcctl_reg = (volatile uint16 *) &this->timerBReg->TBCCTL0;
	tbcctl_reg += timer;

	//enable capturing for TimerID 
	(*tbcctl_reg) = 0x0000; 
	(*tbcctl_reg) |= CM01 | SCS | CAP | device;
	if (int_enable) (*tbcctl_reg) |= CCIE;
}

Time TimerB::getTBCCR(TimerID timer)
{
	volatile uint16 *tbccr_reg = (volatile uint16 *) &this->timerBReg->TBCCR0;
	tbccr_reg += timer;	

	return *tbccr_reg;
}

void TimerB::setTime(const Time& time)
{
	timerBReg->TBCCR0 = time;
	timerBReg->TBR = 0;
}

