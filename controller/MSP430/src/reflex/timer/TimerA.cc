/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze
 */

#include "reflex/timer/TimerARegisters.h"
#include "reflex/timer/TimerA.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/System.h"

using namespace reflex;
using namespace timerA;

/** constructor
 */
TimerA::TimerA() : InterruptHandler(mcu::interrupts::TIMER_A1_A2_TAR,PowerManageAble::PRIMARY)
{
	/* set register pointer */
	registers = (timerA::Registers*)mcu::TIMERA_REGISTERS_BASE;
	/* select ACLK/1, stopped */
	registers->TACTL = TASSEL01 | ID00 | MC00;
	registers->TAR = 0; // reset count register
	registers->TACCTL0 = 0; // reset CCR0 alarm (unused)
	registers->TACCTL1 = 0; // reset alarm control
	registers->TACCTL2 = 0; // reset CCR2 alarm (unused)

	/* deepest sleep mode for ACLK is LPM3 */
	this->setSleepMode(mcu::LPM3);
}

/** init
 * @param alarm output to notify on alarm
 * @param overflow output to notify on timer overflow
 */
void TimerA::init(Sink0& alarm, Sink0& overflow) {
	this->alarm = &alarm;
	this->overflow = &overflow;
}

/** start
 * start the counter. Alarm and overflow interrupts may
 * occur after that.
 */
void TimerA::start()
{
	registers->TACTL |= MC10;
}

/** stop
 * stop the counter. No interrupts will happen after this
 * was called.
 */
void TimerA::stop() {
	registers->TACTL &= ~MCx; // pause timer
}

/** getTime
 * get the current counter value
 * @return uint16 current counter register value
 */
uint16 TimerA::getTime() {
	/* NOTE: The timer source is not in sync with system clock,
	 * so we have to take a majority vote (or stop the timer).
	 */
	uint16 time; // read count register
	do {
		time = registers->TAR;
	} while (time != registers->TAR);

	return time;
}

/** isOverflowPending
 * @return bool true if an overflow interrupt is pending
 */
bool TimerA::isOverflowPending() {
	return ((registers->TACTL & TAIE) && (registers->TACTL & TAIFG));
}

/** clearOverflow
 * clear the overflow interrupt flag
 */
void TimerA::clearOverflow() {
	registers->TACTL &= ~TAIFG; // clear pending interrupt
}

/** enableOverflow
 * enable overflow interrupt notification
 */
void TimerA::enableOverflow() {
	clearOverflow();
	registers->TACTL |= TAIE; // enable overflow interrupt
}

/** disableOverflow
 * disable overflow interrupt notification
 */
void TimerA::disableOverflow() {
	registers->TACTL &= ~TAIE; // disable overflow interrupt
}

/** startAlarm
 * start a oneshot alarm.
 * @param time when the alarm should occur.
 */
void TimerA::startAlarm(uint16 time) {

	registers->TACCTL1 &= ~CCIFG; // reset pending interrupt
	registers->TACCR1 = time; // set compare register
	registers->TACCTL1 |= CCIE; // enable alarm interrupt

	registers->TACTL |= MC10; // start timer
}

/** stopAlarm
 * disable alarm notification.
 */
void TimerA::stopAlarm() {
	/* deactivate interrupt */
	registers->TACCTL1 &= ~CCIE;
//	registers->TACCTL1 &= ~(CCIE | CCIFG);
}

/**
 * Timer handler called by the interrupt wrapper.
 * It is responsible for resetting the hardware to allow
 * further interrupts.
 */
void TimerA::handle()
{
	InterruptLock lock;

	switch (registers->TAIV) {
	case TAIFG_TACTL: // overflow
		clearOverflow(); // clear interrupt flag
		if (overflow) overflow->notify(); // call handler
		break;
	case TACCR1_CCIFG: // compare alarm
		stopAlarm(); // deactivate oneshot alarm
		if (alarm) alarm->notify(); // call handler
		break;
	default: /* unhandled interrupt */ break;
	}
}

/** enable
 * PowerManager function
 * enables the TimerA
 */
void TimerA::enable()
{
	start();
}

/** disable
 * PowerManager function
 * disables the TimerA
 */
void TimerA::disable()
{
	stop();
}
