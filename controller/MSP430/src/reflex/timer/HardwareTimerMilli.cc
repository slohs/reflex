/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	HardwareTimerMilli
  
 *	Author:		Stefan Nuernberger
 * */

#include "reflex/timer/HardwareTimerMilli.h"
#include "reflex/interrupts/InterruptLock.h"

namespace reflex {
namespace msp430 {


void HardwareTimerMilli::Counter::notify() {
	++value;
}

/** constructor
 */
HardwareTimerMilli::HardwareTimerMilli() {
}

/** configureTimer
 * configure the underlying TimerA instance
 * @param timer the TimerA instance used by the HardwareTimer
 */
void HardwareTimerMilli::configureTimer(TimerA& timer) {
		this->timer = &timer;
		timer.init(*this, overflows); // register handlers for alarm and overflows
		timer.enableOverflow(); // enable overflow notifications
}

/** notify
 * implements Sink0
 * adjusts alarm timer and notifies output
 */
void HardwareTimerMilli::notify() {
	setAlarm();
}

/** getNow
 * @return current counter value (local time)
 */
Time HardwareTimerMilli::getNow() {
	// atomic operation
	InterruptLock lock;

	if (timer->isOverflowPending()) {
		timer->clearOverflow();
		overflows.notify();
	}

	// assemble local time
	return ((overflows.get() << OVERFLOW_SHIFT)
		| (timer->getTime() >> PRECISION_SHIFT));
}

/** getTimestamp
 * @return 64 bit counter value in hardware precision
 */
uint64 HardwareTimerMilli::getTimestamp()
{
    // atomic operation
    InterruptLock lock;

    if (timer->isOverflowPending()) {
        timer->clearOverflow();
        overflows.notify();
    }

    // assemble hw precision timestamp
    return ((overflows.get64() << (OVERFLOW_SHIFT + PRECISION_SHIFT))
            | timer->getTime());
}

/** start
 * set a timeout until next interrupt and start the timer
 * @param ticks number of ticks until timer event
 */
void HardwareTimerMilli::start(Time ticks) {
	startAt(getNow(), ticks);
}

/**
 * startAt
 * set a new time interrupt to occur ticks after t0
 * @param t0 time from where to set the timer
 * @param ticks number of ticks after t0 when the timer should fire
 */
void HardwareTimerMilli::startAt(Time t0, Time ticks) {
	this->time = t0;
	this->delta = ticks;

	setAlarm();
}

/** stop
 * stop the hardware timer alarm
 * NOTE: THIS DOES NOT STOP THE LOCAL TIME COUNTER!
 */
void HardwareTimerMilli::stop() {
	timer->stopAlarm();
}

/** setAlarm
 * set the alarm counter
 */
void HardwareTimerMilli::setAlarm() {
	Time now = getNow();
	Time elapsed = now - time;

	if (elapsed >= delta) {
		if (output) output->notify(); // notify output
		return;
	}

	Time remaining = delta - elapsed;

	/* update alarm timer */
	uint16 nextAlarm;

	/* check maximum */
	if (remaining > MAX_TIMER_VALUE) {
		remaining = MAX_TIMER_VALUE;
	}

	/* check minimum */
	if (remaining > SAFE_MINIMUM) {
		nextAlarm = (uint16)((now + remaining) << PRECISION_SHIFT);
	} else {
		nextAlarm = (uint16)((remaining << PRECISION_SHIFT) + timer->getTime());
	}

	timer->startAlarm(nextAlarm);
}

} // msp430
} // reflex
