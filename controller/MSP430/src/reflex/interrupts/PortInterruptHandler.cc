/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Stefan Nuernberger
 *
 *	Description: implementation of PortInterruptHandler
 */

#include "reflex/interrupts/PortInterruptHandler.h"
#include "reflex/System.h"

using namespace reflex;

/**
 * constructor
 * @param port number of port to register
 * @param pin bitmask of interrupt pin on port
 * @param isSecondary mark handler as secondary
 */
PortInterruptHandler::PortInterruptHandler
		(InterruptCapablePorts port, InterruptCapablePins pin, const PowerManageAble::Priority priority) :
		PowerManageAble(priority)
{
	if (port == PORT1) {
		getSystem().port1.registerInterruptHandler(this, pin);
	} else if (port == PORT2) {
		getSystem().port2.registerInterruptHandler(this, pin);
	}
}
