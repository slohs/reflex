#ifndef UpdateManager_h
#define UpdateManager_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	UpdateDaemon
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Masterblock for all update components, which also
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 */

#include "reflex/memory/Buffer.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Queue.h"
#include "reflex/sinks/Event.h"
#include "reflex/memory/Pool.h"
#include "reflex/memory/Flash.h"
#include "reflex/update/Command.h"
#include "reflex/update/UpdateDaemon.h"

/**
 *  This is a plattform specific wrapper class for the code update
 */
class UpdateManager : public reflex::Activity {
public:



    UpdateManager();

	void init(reflex::Sink1<reflex::Buffer*>* ioDevice,
			  reflex::Sink0* macTimerInput,
			  reflex::Sink1<reflex::Buffer*>* outputUpdateManagerApplicationData,
			  reflex::Pool* pool);

	virtual void run();

	reflex::Event forwardingFinished;
	reflex::Event waitEvent;


	friend class NodeConfiguration;
	
	reflex::Queue<reflex::Buffer*> input;
	reflex::Queue<reflex::Buffer*> applicationInput;
	reflex::Sink1<reflex::Buffer*>* outputToUpdateDaemon;
	reflex::Sink1<reflex::Buffer*>* outputUpdateManagerApplicationData;
	reflex::Sink0* outputToUpdateDaemonForward;

private:



	reflex::Buffer* packet;
	reflex::Buffer* applicationPacket;



	reflex::Sink0* macTimerInput;

	reflex::Pool* pool;


	void forwardApplicationData();
	void handleForwardingFinished();

	reflex::ActivityFunctor<UpdateManager,&UpdateManager::handleForwardingFinished> forwardingFinishedFunctor;

	reflex::UpdateDaemon<reflex::Flash> ud;

};


#endif
