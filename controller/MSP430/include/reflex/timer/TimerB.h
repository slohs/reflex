#ifndef TimerB_h
#define TimerB_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 TimerB
 *
 *	Author:	     Carsten Schulze, Olaf Krause
 *
 *	Description: This is a implementation for the timer B
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/sinks/Sink.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/types.h"
#include "reflex/timer/BasicClockRegisters.h"
#include "reflex/timer/TimerBRegisters.h"
#include "reflex/io/Port.h"

namespace reflex {

/*FIXME: 	TimerB has the ability to chose among different clock sources. this 
			feature isn't used yet and also infects the powermanagement ability
*/

/**
 * This Object generates a continuous timerinterrupt and sends it to output
 * @author Carsten Schulze, Olaf Krause
  
 */
class TimerB 
	: public InterruptHandler
{
public:
	/**
	 * constructor
	 * @param output sink which is notified, if an timer interrut occurs
	 */
	TimerB(Sink0& output);


/**
 *	Address of the control register from the specific TimerB.
 */
enum TimerID{
	TIMER0 = 0x0,
	TIMER1 = 0x1,
	TIMER2 = 0x2,
	TIMER3 = 0x3,
	TIMER4 = 0x4,
	TIMER5 = 0x5,
	TIMER6 = 0x6
};

/**
 *	The device which captures the TimerB.
 */
enum Device {
	CCIxA 	= 0x0000,
	CCIxB 	= 0x1000, //note: TimerB1 for SFD capture use this, maybe failure in datasheet
	VCC			= 0x2000,
	GND			= 0x3000
};


	/**
	 * sets the timer-register and enables the Timerinterrupt,
	 * so the timer starts
	 */
	void start();

	/**
	 * stop the timer
	 */
	void stop();
	
	/**
	 * Timer handler called by the interrupt wrapper. It is responsible
	 * for resetting the hardware to allow further interrupts.
	 */
	virtual void handle();

	/** Enables the TimerB-Interrupt
	 *  Implements InterruptHandler interface
	 */
	virtual void enable();

	/** 
	 *	Enables the TimerB capture mode
	 *  @param timer: specifies the Timer(register), for which the capture mode shall be enabled
	 *	@param int_enable: enables the interrupt
	 *	@param device: specifies the device which triggers the capture (see datasheet)
	 */		
	virtual void enableCapture(TimerID timer, bool int_enable, Device device);		

	/**
	 * Disables the timer B-Inerrupt (and clears pendling)
	 */
	virtual void disable();

	/**
	 * returns the content of the timer B counter register
	 * @return the content of the timer B counter register
	 */
	Time getTime() {return timerBReg->TBR;}
	
	/**
	 * returns the content of the timer B CCR register from CCR(TimerID)
	 * e.g. TBRCCR1 contains the timestamp produced by CC2420 at SFD if capture is enabled
	 * @return the content of the timer B CCR register
	 */
	virtual Time getTBCCR(TimerID timer); 


	/**
	 * Sets the time at which the interrupt shall occur
	 * it also resets the counter of the timer B
	 * @param time value when next interrupt should occur
	 */
	void setTime(const Time& time);
	
private :

	/**
	 * necessary to calculate the interrupt-interval.
	 * Now it's possible to use milliseconds instead of CPU-Ticks
	 */
	enum TimerBSpeed {
		/** speed of the timer B clock */
		TIMERHZ = basicClockModule::ACLKHZ
	};
	
	/**
	 * this is the Object, witch is notified if the time is over.
	 *  output should do the real operation
	 */
	Sink0& output;

	/**
	 * pointer to the registers of timer B
	 */
	volatile timerB::TimerBReg* timerBReg;

};

} //reflex

#endif
