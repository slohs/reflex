#ifndef TimerARegisters_h
#define TimerARegisters_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 TimerARegisters
 *
 *	Author:	 Carsten Schulze, Karsten Walther
 *
 *	Description: This are the registers used to access timer A
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

namespace reflex {

namespace timerA {


/**
 * Used Bitmasks and memory mapped registers for timer A
 *
 */
struct Registers {
	volatile unsigned int  TAIV;        ///TAIV at address 0x012e
	volatile char dummy1[0x30];
	volatile unsigned int  TACTL;       ///TACTL at address 0x0160,
	volatile unsigned int  TACCTL0;     ///TACCTL0 at address 0x0162,
	volatile unsigned int  TACCTL1;     ///TACCTL1 at address 0x0164,
	volatile unsigned int  TACCTL2;     ///TACCTL2 at address 0x0166,
	volatile char dummy2[0x8];
	volatile unsigned int  TAR;         ///TAR at address 0x0170,
	volatile unsigned int  TACCR0;      ///TACCR0 at address 0x0172,
	volatile unsigned int  TACCR1;      ///TACCR1 at address 0x0174,
	volatile unsigned int  TACCR2;      ///TACCR2 at address 0x0176
};

enum TIMER_A{
	//TACTL
	TASSELx     = 0x0300, ///clock source select
	TASSEL00	= 0x0000, ///TACLK as source
	TASSEL01    = 0x0100, ///ACLK as source
	TASSEL10    = 0x0200, ///SMCLK as source
	TASSEL11	= 0x0300, ///INCLK as source
	IDx         = 0x00c0, ///Divider of clock
	ID00		= 0x0000, ///Factor 1
	ID01        = 0x0040, ///Factor 2
	ID10        = 0x0080, ///Factor 4
	ID11        = 0x00c0, ///Factor 8
	MCx         = 0x0030, ///Count mode
	MC00		= 0x0000, ///stop
	MC01        = 0x0010, ///Up mode: the timer counts up to TACCR0
	MC10        = 0x0020, ///continuous mode
	MC11        = 0x0030, ///Down mode
	TACLR       = 0x0004, ///resets divider and TAR
	TAIE        = 0x0002, ///Timer A interrupt enable (overflow)
	TAIFG       = 0x0001, ///Timer A interrupt flag (overflow)
	//TAIV
	TAIVx       = 0x000e,
	TACCR1_CCIFG	= 0x0002,
	TACCR2_CCIFG	= 0x0004,
	TAIFG_TACTL		= 0x000a,
	//TACCTLx
	CMx		=	0xc000,
	CM01	=	0x4000,
	CM10	=	0x8000,
	CM11	=	0xc000,
	CCISx	=	0x3000,
	CCIS01	=	0x1000,
	CCIS10	=	0x2000,
	CCIS11	=	0x3000,
	SCS		=	0x0800,
	SCCI	=	0x0400,
	CAP		=	0x0100, /// capture mode for TACCTLx
	OUTMODx	=	0x00e0,
	OUTMOD001 	=	0x0020,
	OUTMOD010 	=	0x0040,
	OUTMOD011 	=	0x0060,
	OUTMOD100 	=	0x0080,
	OUTMOD101	=	0x00a0,
	OUTMOD110	=	0x00c0,
	OUTMOD111	=	0x00e0,
	CCIE		=	0x0010,
	CCI			=	0x0008,
	OUT			=	0x0004,
	COV			=	0x0002,
	CCIFG		=	0x0001
};

} //timerA

} //reflex

#endif
