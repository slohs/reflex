#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Carsten Schulze
# */

ifdef DEVICE
DEFINED_REFLEX_VARS += DEVICE $(DEVICE)
endif
ifndef DEVICE
MISSING_REFLEX_VARS += DEVICE the currently used microcontroller type e.g. 149
endif

ifdef LIBPATH
DEFINED_REFLEX_VARS += LIBPATH $(LIBPATH)
endif
ifndef LIBPATH
MISSING_REFLEX_VARS += LIBPATH the path to gcc libraries
endif

SREC = $(APPLICATION).s19
SREC_ALIGN = $(APPLICATION)_a.s19
EXECUTABLE = $(APPLICATION).elf
MAIN_TARGET = $(EXECUTABLE)

TOOLPREFIX ?= $(TOOLPATH)msp430-

ASFLAGS +=
CFLAGS +=	-O2 -fomit-frame-pointer -Wall -mmcu=$(DEVICE) \
			-pedantic -ansi -Wno-long-long

CXXFLAGS += $(CFLAGS)\
			-fno-exceptions -fno-rtti\
			-Wno-non-virtual-dtor -fcheck-new\
			-fno-stack-check -fimplicit-inline-templates -fpermissive

#pronounce device, needed for the linkprocess
LDFLAGS += -mmcu=$(DEVICE)

#using garbage collection against unused code
CFLAGS += -ffunction-sections -fdata-sections
ifeq ($(DEBUG), 0)
	LDFLAGS += -Wl,--gc-sections
endif

LDLIBS =

OBJCOPY_FLAGS = -O srec --srec-len 52
OBJDUMP_FLAGS = -SDx


$(SREC): $(EXECUTABLE)
	$(OBJCOPY) $(OBJCOPY_FLAGS) $(EXECUTABLE) $(SREC)


########## includes and base sources for the controller ########

INCLUDES += -I$(REFLEXPATH)/controller/$(CONTROLLER)/include
include $(REFLEXPATH)/controller/$(CONTROLLER)/Sources.mk

