############################################################################
#
# REFLEX - Real-time Event FLow EXecutive
#
# A lightweight operating system for deeply embedded systems.
#
# This file contains the common definition of variables, targets and rules
# for building a Reflex-application with make.
#

# Authors:	Karsten Walther, Sören Höckner
#
############################################################################

############################################################################
# Include source definitions
############################################################################
include $(APPLICATIONPATH)/Sources.mk
include $(REFLEXPATH)/system/Sources.mk

############################################################################
# include specific make configurations
############################################################################
include $(REFLEXPATH)/platform/$(PLATFORM)/Platform.mk
include $(REFLEXPATH)/controller/$(CONTROLLER)/Controller.mk

# change those variables, that are defined by default.
ifeq ($(origin AS), default)
	AS = $(TOOLPREFIX)gcc
endif
ifeq ($(origin CC), default)
	CC = $(TOOLPREFIX)gcc
endif
ifeq ($(origin CXX), default)
	CXX = $(TOOLPREFIX)g++
endif
ifeq ($(origin LD), default)
	LD = $(TOOLPREFIX)g++
endif
ifeq ($(origin AR), default)
	AR = $(TOOLPREFIX)ar
endif

OBJCOPY ?= $(TOOLPREFIX)objcopy
OBJDUMP ?= $(TOOLPREFIX)objdump


############################################################################
# gather include and src directories
# prefix paths for sources in controller, platform, system or lib directory
############################################################################
INCLUDES += \
	-I$(REFLEXPATH)/system/include \
	-I$(REFLEXPATH)/lib/include \
	-I$(REFLEXPATH)/devices/include

STARTUP_SOURCE += \
	$(addprefix $(REFLEXPATH)/controller/$(CONTROLLER)/src/reflex/,$(STARTUP_SOURCE_CONTROLLER))

CXX_SOURCES += \
	$(addprefix $(REFLEXPATH)/controller/$(CONTROLLER)/src/reflex/,$(CC_SOURCES_CONTROLLER))\
	$(addprefix $(REFLEXPATH)/devices/src/reflex/,$(CC_SOURCES_DEVICES))\
	$(addprefix $(REFLEXPATH)/platform/$(PLATFORM)/src/reflex/,$(CC_SOURCES_PLATFORM))\
	$(addprefix $(REFLEXPATH)/system/src/reflex/,$(CC_SOURCES_SYSTEM)) \
	$(addprefix $(REFLEXPATH)/lib/src/reflex/,$(CC_SOURCES_LIB))\
	$(EXTERNAL_CC_SOURCES_APPLICATION)\
	$(addprefix $(APPLICATIONPATH)/,$(CC_SOURCES_APPLICATION))

C_SOURCES += \
	$(addprefix $(APPLICATIONPATH)/,$(C_SOURCES_APPLICATION))\
	$(addprefix $(REFLEXPATH)/platform/$(PLATFORM)/src/reflex/,$(C_SOURCES_PLATFORM))\
	$(addprefix $(REFLEXPATH)/controller/$(CONTROLLER)/src/reflex/,$(C_SOURCES_CONTROLLER))\
	$(addprefix $(REFLEXPATH)/system/src/reflex/,$(C_SOURCES_SYSTEM))

ASM_SOURCES += \
	$(addprefix $(REFLEXPATH)/platform/$(PLATFORM)/src/reflex/,$(ASM_SOURCES_PLATFORM))\
	$(addprefix $(REFLEXPATH)/controller/$(CONTROLLER)/src/reflex/,$(ASM_SOURCES_CONTROLLER)) \
	$(addprefix $(REFLEXPATH)/controller/$(CONTROLLER)/src/reflex/,$(ASM_SOURCES_SYSTEM))

CHECK_SOURCES += \
	$(addprefix $(REFLEXPATH)/controller/$(CONTROLLER)/src/reflex/,$(CC_SOURCES_CHECK_CONTROLLER))\
	$(addprefix $(REFLEXPATH)/system/src/reflex/,$(CC_SOURCES_CHECK_SYSTEM))\
	$(addprefix $(REFLEXPATH)/lib/src/reflex/,$(CC_SOURCES_CHECK_LIB))


SOURCES = $(STARTUP_SOURCE) $(C_SOURCES) $(CXX_SOURCES) $(ASM_SOURCES)
SOURCE_DIRS = $(dir $(SOURCES))

############################################################################
# set global search paths
############################################################################
vpath %.h $(INCLUDES)
vpath %.c $(SOURCE_DIRS)
vpath %.cc $(SOURCE_DIRS)
vpath %.asm $(SOURCE_DIRS)
vpath %.s $(SOURCE_DIRS)
vpath %.S $(SOURCE_DIRS)

############################################################################
# define objects
############################################################################
FIRST_OBJECT = $(notdir $(patsubst %.s,%.o,$(STARTUP_SOURCE:.S=.o)))
ASM_OBJECTS = $(notdir $(patsubst %.s,%.o,$(ASM_SOURCES:.S=.o)))
C_OBJECTS = $(notdir $(C_SOURCES:.c=.o))
CXX_OBJECTS = $(notdir $(CXX_SOURCES:.cc=.o))
OBJECTS = $(ASM_OBJECTS) $(C_OBJECTS) $(CXX_OBJECTS)
PREPROCESSED = $(OBJECTS:.o=.i)

############################################################################
# setting the flags for the tools
############################################################################
CPPFLAGS += -D'SCHEDULER=$(SCHEDSCHEME)' -D'PLATFORM=$(PLATFORM)' -D'CONTROLLER=$(CONTROLLER)' -D'MCU=$(MCU)'
CPPFLAGS += -D'$(SCHEDSCHEME)' $(DEFINES) $(INCLUDES)
ifeq ($(DEBUG),1)
	CFLAGS += -g -DDEBUG
	CXXFLAGS += -g -DDEBUG
endif

#"gcc -print-search-dirs" command gives the searchpath, which is needed for some platforms to 
#find necessary object for the linking process
GCC_SEARCH_DIRS = $(shell $(CC) -print-search-dirs)
#eliminate all words that ends on ':' or starts with '='
GCC_INSTALL_PATH = $(patsubst %:,,$(patsubst =%,,$(GCC_SEARCH_DIRS)))
#do not set LIBPATH, compiler should do this!
#LIBPATH ?= -L$(GCC_INSTALL_PATH)
LDFLAGS += $(LIBPATH)

############################################################################
# other definitions
############################################################################
DEPENDENCIES_FILE = Dependencies.mk
TARGETS += clean all depend check

############################################################################
# Common rules
############################################################################
.PHONY: all clean depend dump preproc check environment targets

all: environment targets $(MAIN_TARGET)
	@echo done

check:
	$(CXX) -c $(CXXFLAGS) $(CHECK_SOURCES)

clean: $(DEPENDENCIES_FILE)_clean
	@rm -f *.o
	@rm -f *.rpo
	@rm -f HandlerTableBuilder
	@rm -f $(HANDLER_TABLE_FILE)
	@rm -f $(MAIN_TARGET)
	@rm -f $(PREPROCESSED)
	@rm -f $(MISSING_FILES)

dump:
	$(OBJDUMP) $(OBJDUMP_FLAGS) $(EXECUTABLE)


depend: $(DEPENDENCIES_FILE)_clean
	$(MAKE) $(DEPENDENCIES_FILE)
	@echo up to date

environment:
	@echo "--------------------------------------------------------------------------------"
	@echo ""
	@echo "Overview of non-standard variables which must be explicitely defined in makefile"
	@echo ""
	@echo "Defined Variables:"
ifdef DEFINED_REFLEX_VARS
	@echo -e "$(DEFINED_REFLEX_VARS)"
	@echo "PATH:"
	@echo "$(PATH)"
	
else
	@echo "None."
endif
	@echo ""
	@echo "Undefined Variables:"
ifdef MISSING_REFLEX_VARS
	@echo	"following variables need to be defined:"
	@echo -e "$(MISSING_REFLEX_VARS)"
	@echo ""
	@echo "Note: Definition of variables may cause further variables need to be defined."
	@echo ""
else
	@echo "None."
endif
	@echo "--------------------------------------------------------------------------------"
	@echo ""

targets:
	@echo "For the chosen environment following make targets exist"
	@echo ""
	@echo $(TARGETS)
	@echo ""
	@echo "--------------------------------------------------------------------------------"


preproc: $(PREPROCESSED)

$(DEPENDENCIES_FILE)_clean:
	@rm -f $(DEPENDENCIES_FILE)

$(DEPENDENCIES_FILE): $(MISSING_FILES)
	touch $(DEPENDENCIES_FILE)
ifneq ($(strip $(C_SOURCES)),)
	$(CC) -MM $(CPPFLAGS) $(CFLAGS) $(C_SOURCES) >> $(DEPENDENCIES_FILE)
endif
ifneq ($(strip $(CXX_SOURCES)),)
	$(CXX) -MM $(CPPFLAGS) $(CXXFLAGS) $(CXX_SOURCES) >> $(DEPENDENCIES_FILE)
endif

%.i : %.s
	$(CC) -E $(CPPFLAGS) $(CFLAGS) -o $@ $<

%.i : %.c
	$(CC) -E $(CPPFLAGS) $(CFLAGS) -o $@ $<

%.i : %.cc
	$(CXX) -E $(CPPFLAGS) $(CXXFLAGS) -o $@ $<

$(EXECUTABLE): $(FIRST_OBJECT) $(OBJECTS) $(HANDLER_TABLE) $(EXECUTABLE_DEPENDENCIES)
	$(LD) $(LDFLAGS) -o $(EXECUTABLE) $(FIRST_OBJECT) $(OBJECTS) $(HANDLER_TABLE) $(LDLIBS)

sinclude $(DEPENDENCIES_FILE)
