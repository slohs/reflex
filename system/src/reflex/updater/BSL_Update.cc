/*
 * BSL_Update.c
 *
 *  Created on: Apr 14, 2011
 *      Author: hannes
 */

#include "reflex/updater/BSL_Update.h"

/*
 * include the definition of the platfrom / controller specific radio
 * the include directive appears here, to ensure that the driver code resides in
 * th update section
 */
//extern "C" {
	#if PLATFORM == EZ430-chronos
		#include "../../../platform/EZ430-chronos/src/reflex/updateDriver/updateRadioDriver.cc"
		#include "../../../platform/EZ430-chronos/src/reflex/updateDriver/updateTimerDriver.cc"
		#include "../../../platform/EZ430-chronos/src/reflex/updateDriver/updateCPUDriver.cc"
		#include "../../../platform/EZ430-chronos/src/reflex/updateDriver/updateFlashDriver.cc"
	#elif PLATFORM == SunSPOT
		#include "../../../platform/SunSPOT/src/reflex/updateDriver/updateRadioDriver.cc"
		...
	#else
		#error "unsupported platform for wireless update"
	#endif

/**
 * symbol to the begin of the text section
 */
extern "C" void* __codeStart__;

/**
 * symbol to the end of the text section
 */
extern "C" void* __codeEnd__;

/**
 * position of initial stack
 */
extern "C" void* __stack;

#define UPDATE_STATE_CPU_OK 			0x1
#define UPDATE_STATE_SPI_OK 			0x2
#define UPDATE_STATE_RADIO_CFG_OK 		0x3
#define UPDATE_STATE_METAPACKET_OK		0x4
#define UPDATE_STATE_IMAGE_OK			0x5
#define UPDATE_STATE_IMAGE_NOTOK		0x6
#define UPDATE_STATE_METAPACKET_NOTOK	0x7

//#define LED_ACTIVE

#ifndef LED_ACTIVE
    #define u_LED_setNumber(X)
    #define u_LED_toggle(X)
    #define u_LED_setLED(X)
#endif

#ifdef DEBUG_UPDATE
	#define SHOW_STATE(X) u_LED_setNumber(X)
#else
	#define SHOW_STATE(X) //no debugging X
#endif

void bslStart()
{
	//initialize StackPointer
	asm("mov #__stack, R1 ;");
	//asm("dec r1;");//reserve some space on stack: compiler bug prevents allocating big things on stack
	asm("add #-4, r1;");

	//disable all interrupts, setting timer, disable wdt, setting frequency etc.
	u_cpu_init();
	SHOW_STATE(UPDATE_STATE_CPU_OK);

	while(!test());

	//@TODO: goto to start of program section -> reset isn't needed
	 __asm__ __volatile__("br %[__codeStart__]"
			: /*"=r"*/
			: [__codeStart__] "i" ((int)&__codeStart__) );
	 __asm__ __volatile__("nop");
	 __asm__ __volatile__("nop");
	 __asm__ __volatile__("nop");
}

bool test()
{
	// checking in persistent "update data memory", if an update is needed:
	if(persistentUpdateMem.receivedCompleteImage == 0)
	{
		// stores the expected packets until transmitting program image is complete
		uint16 lastPacket;

		//stores the epected runs
		/*volatile*/ uint8 expectedRuns;

		//stores packet lengths
		/*volatile*/ uint8 packetLen;

		// the remaining bytes of the last packet
		/*volatile*/ uint8 remainder;

		// the expected program ID and version
		/*volatile*/ uint8 exPID, exVersion;

		// configure radio
		while(!(u_radio_config((persistentUpdateMem.mrfiRadioCfg), 35, persistentUpdateMem.channel)));
		SHOW_STATE(UPDATE_STATE_RADIO_CFG_OK);
		/**
		 * part of handling UpdateMetaInfoPacket
		 */
		{
			UpdateMetaInfoPacket metaPacket;

			//try to receive a UpdateMetaInfo packet
			do {
				packetLen = u_radio_receiveData( (uint8*)((&metaPacket)), sizeof(UpdateMetaInfoPacket) );

				// @TODO: checking the other case of update
				if(	packetLen == sizeof(UpdateMetaInfoPacket) && metaPacket.type == UPDATE_META_INFO &&
					(	(metaPacket.getProgramID() == persistentUpdateMem.programID && metaPacket.version != persistentUpdateMem.version) ||
						(metaPacket.getProgramID() != persistentUpdateMem.programID && metaPacket.getOverride())
					)
				  )
				{
					SHOW_STATE(UPDATE_STATE_METAPACKET_OK);
					lastPacket = (metaPacket.packetCount_run & SEQ_NO_MASK) - 1;
					expectedRuns = (uint8) ((metaPacket.packetCount_run & (~SEQ_NO_MASK)) >> 13);
					remainder = metaPacket.remainder;

					exPID = metaPacket.getProgramID();
					exVersion = metaPacket.version;

					// checking if the expected data are in the area were writing is allowed
					// @TODO: maybe checking lower bounds
					// @TODO: restoring reset vector is only needed if it was erased
					if((lastPacket != MAX_UINT16) &&
						(((uint16)(&__codeStart__) + ((lastPacket + 1) * PAYLOAD_SIZE) - (PAYLOAD_SIZE - remainder)) <= HIGHEST_WRITABLE_ADDRESS))
					{

						// save the reset vector
						uint16 resetVector = *((uint16*)(RESET_VECTOR));


						// erase program memory:
						// 	erase the segment @__codeStart__ until the end of flash is reached
						u_erase_flash((uint8*)(&__codeStart__), lastPacket * PAYLOAD_SIZE + remainder); //including 0xffff


						//restore reset_vector
						u_write_flashBytes((uint8*)&resetVector, (uint8*)RESET_VECTOR, 2);
						SHOW_STATE(UPDATE_STATE_IMAGE_OK);
						break;
					} else {
						SHOW_STATE(UPDATE_STATE_IMAGE_NOTOK);
					}
				} else {
					SHOW_STATE(UPDATE_STATE_METAPACKET_NOTOK);
				}
			} while (true);
		}
		u_LED_setNumber(0x0);

		/**
		 * part of handling UpdateDataPacket
		 */

		// stores the received packets
		UpdateDataPacket packet;

		// copies the data into temporary tmp_helpMem
		PersistentUpdateMemory tmp_helpMem = persistentUpdateMem;


		tmp_helpMem.programID = exPID;
		tmp_helpMem.version = exVersion;

		// little helper
		uint16 seqNo;

		//flag for loop
		uint8 notAllReceived;

//#ifdef USING_RESISTENT_BITARRAY
		//filling up the residual bits of bit array if the number of packets isn't a divisor of 8
		tmp_helpMem.bitArray[lastPacket >> 3] = 0xff >> (7 - (lastPacket % 8));
//#endif

		// try to receive the data packets
		do {
			packetLen = u_radio_receiveData( (uint8*)(&packet), sizeof(UpdateDataPacket) );
			u_LED_toggle(_0);
			
			seqNo = (packet.seqNo_run & SEQ_NO_MASK);

			notAllReceived = 1;

			// @TODO: checking the other cases of update
			if(	packetLen == sizeof(UpdateDataPacket) && packet.type == UPDATE_DATA && seqNo <= lastPacket &&
				packet.getProgramID() == exPID &&
				packet.version == exVersion
				)

			{
				u_LED_toggle(_1);

//#ifdef USING_RESISTENT_BITARRAY
				// checking, if the data have to be written into flash => if the coresponding bit == 0, then writing was already successful
				// and writing the received data into flash isn't needed anymore
				if( tmp_helpMem.bitArray[seqNo >> 3] & (1 << (seqNo % 8)) )
				{
					uint8 size = PAYLOAD_SIZE;
					if(seqNo == lastPacket)
					{
						if(remainder)
							size = remainder;

						if(size)
							size -= 2;// avoid overwritting reset_vector
					}
					u_LED_toggle(_2);
					u_write_flashBytes((uint8*)(&packet.data), (uint8*)((uint16)(&__codeStart__) + ((uint16)seqNo * (uint16)PAYLOAD_SIZE)), size);

					// mark data as successfully written
					tmp_helpMem.bitArray[seqNo >> 3] &= ~(1 << ((seqNo % 8)));

					// mark the corresponding byte in flash as successfully written
					if(tmp_helpMem.bitArray[seqNo >> 3] == 0)
					{
						uint8 null = 0;
						u_LED_toggle(_3);
						u_write_flashBytes( &null, (uint8*)(&(persistentUpdateMem.bitArray[seqNo >> 3])), 1);
					}
				}

				//last packet received
				if( seqNo == lastPacket )
				{
					u_LED_setLED(_4);
					notAllReceived = 0;
					seqNo = (lastPacket >> 3) + 1;
					//checking the bitarray
					do
					{
						//checking bytewise if all data are present
						if(persistentUpdateMem.bitArray[--seqNo] != 0x0)
						{
							notAllReceived = 1;
							break;
						}
					} while (seqNo);
				}
			}// if
		} while (notAllReceived);//how to manage the problem, if last packet is missing????



		// @TODO: the following 2 function calls are critical. If a reset occurs after flash erase, the configuration data
		// of the radio are lost. Currently no feasible idea how to handle
		uint8* helpPtr = (uint8*)&persistentUpdateMem;
		uint8* tmpPtr = (uint8*)&tmp_helpMem;
		uint16 offset = ((uint16)(&(persistentUpdateMem.bitArray))) - ((uint16)(&(persistentUpdateMem.receivedCompleteImage))) - sizeof(persistentUpdateMem.channel);

		// erase persistent memory
		u_erase_flash(helpPtr, sizeof(persistentUpdateMem));


		// update the help memory, but dont touch tmp_helpMem.receivedCompleteImage flag, the channel and the bitarray, because it has to be writable later
		u_write_flashBytes(++tmpPtr, ++helpPtr, offset);

		// @TODO: adding here a specific time for watchdog

		// reset follows here -> writing wrong PWD into Watchdog!
		WDTCTL = 0;

	}//if

	//to prevent compiler warnings
	return true;
}

static void __inline__  __delay_cycles(register unsigned int n)
{
    __asm__ __volatile__ (
                "1: \n"
                " dec      %[n] \n"
                " jne      1b \n"
        : [n] "+r"(n));

}



//}
