/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:		 Karsten Walther
 *
 */

#include "reflex/timer/Clock.h"
#include "conf.h"

using namespace reflex;

Clock::Clock()
{
	time = 0;
	output = 0;
}

void Clock::connectToOutput(Sink0* output)
{
	this->output = output;
}

void Clock::notify()
{
//In case of TimeTriggered scheduling, the wraparound may not be the
//maximum value of Time datatype.
#ifdef MAXTIME
	time = (time + 1) % MAXTIME;
#else
	time++;
#endif

	if (output)
	{
		output->notify();
	}
}


