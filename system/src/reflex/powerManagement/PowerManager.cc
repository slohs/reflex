/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */

#include "reflex/powerManagement/PowerManager.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/interrupts/InterruptLock.h"

using namespace reflex;

PowerManager::PowerManager()
{
	for(int i=0; i< mcu::NrOfPowerModes; i++)
	{
		useCounts[i] = 0;
	}
	currentMode = NOTHING;
}

void PowerManager::enableObject(PowerManageAble* object)
{
	if( !object->enabled ){
		object->enable();
		object->enabled=true;
		useCounts[object->deepestAllowedSleepMode]++;
	}
}

void PowerManager::disableObject(PowerManageAble* object)
{
	if(object->enabled){
		object->disable();
		object->enabled=false;
		useCounts[object->deepestAllowedSleepMode]--;
	}
}

void PowerManager::switchMode(powerManagement::PowerGroups value, powerManagement::PowerGroups mask)
{
	InterruptLock lock;
	currentMode = (value | (currentMode & ~mask));

	//FIXME: an iteratorconcept for that queue is needed;
	PowerManageAble* it = queue.front();

	while(it) 
	{
		if( !it->isSecondary)
		{
			if(it->groups & currentMode)
			{
				enableObject(it);
			}else
			{
				disableObject(it);
			}
		}
		it = static_cast<PowerManageAble*>(it->next);
	}
}

void PowerManager::active() {
	_interruptsEnable();
	return;
}
