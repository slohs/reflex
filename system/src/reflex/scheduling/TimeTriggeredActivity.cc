/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */

#include "reflex/scheduling/TimeTriggeredActivity.h"
#include "reflex/System.h"

using namespace reflex;

TimeTriggeredActivity::TimeTriggeredActivity()
{
    rescheduleCount = 0;
    locked = false;
    next = 0;
}

void TimeTriggeredActivity::unlock()
{
	getSystem().scheduler.unlock(this);
}

