/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Karsten Walther, Stefan Nuernberger
 */
#include "reflex/scheduling/TimeTriggeredScheduler.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/System.h"

using namespace reflex;

TimeTriggeredScheduler::TimeTriggeredScheduler()
	: wake(VirtualTimer::ONESHOT, true) // system priority oneshot timer
{
	frameCount = NrOfSchedulingSlots;
	last = 0;
}

void TimeTriggeredScheduler::start()
{
	while(1){
		_interruptsDisable();
		// compute elapsed time from system time obtained through VirtualTimer
		// this is needed to correctly handle timer wrap around
		Time now = wake.getSystemTime() % MAXTIME;
		Time elapsed = (now - last) % MAXTIME;
		while (elapsed >= ((current->startTime - last) % MAXTIME)) {
			// reached activity slot
			// check if activity has to be invoked
			if(current->act->rescheduleCount >0 && !current->act->locked){
				current->act->rescheduleCount--;
				scheduleCount--;

				_interruptsEnable();
				current->act->run();
				_interruptsDisable();
			}
			if (current->next != 0) {
				current = current->next;
			} else {
				current = list.first();
				break; // startTime wraps around to next round
			}
		}
		// update last scheduling time
		last = now;

		// sleep till next used slot
		Time remaining = (current->startTime - now) % MAXTIME;
		wake->set(remaining); // ensure next interrupt occurs
		getSystem().powerManager.powerDown();
	}
}

void TimeTriggeredScheduler::registerActivity(TimeTriggeredActivity* act, Time startTime)
{
	//as long as there are slotframes for managing
	if(frameCount){
		frameCount--;
		frames[frameCount].act = act;
		frames[frameCount].startTime = startTime;
		list.insert(&(frames[frameCount]));   //sort frame into list
	}
	current = list.first(); // keep current a valid value
}
