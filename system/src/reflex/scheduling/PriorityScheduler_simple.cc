/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/PriorityScheduler.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/System.h"

using namespace reflex;

void PriorityScheduler::start()
{
	_interruptsEnable();
	while(1){
		//getSystem().powerManager.powerDown(!lockCount);
		getSystem().powerManager.powerDown();
	}
}

void PriorityScheduler::dispatch()
{
	//run Activities from head of list until an interrupted one is in
	//front or the list is empty
	PriorityActivity* first = readyList.first();
	while( (first !=0 ) && (first->status != PriorityActivity::INTERRUPTED) ){

	    first->status = PriorityActivity::RUNNING;

	    //run activity
	    _interruptsEnable();
    	first->run();
	    _interruptsDisable();

	    readyList.deque();

	   	first->status = PriorityActivity::IDLE;

		//rescheduling if needed
	    if((first->rescheduleCount > 0) && (!first->locked)){

    		first->rescheduleCount--;
			first->status = PriorityActivity::SCHEDULED;
			readyList.insert(first);
		}

	    first = readyList.first();
	}

	//if there is an interrupted element set it Running again,
	//because this Stack-instance of PriorityScheduler has done its work
	if(first){
		//must not be checked because this the break condition for the
		//while loop
		//if(first->status == PriorityActivity::INTERRUPTED)
		first->status = PriorityActivity::RUNNING;
	}
}

/** This function brings given activity into the scheduling process.
**/
void PriorityScheduler::schedule(PriorityActivity* act)
{
	//Interrupts must be disabled now, at the end they must be enabled
	//when call was softwareinitiated and must stay disabled when schedule
	//was called by an interrupthandler
	InterruptLock lock;

	if(	(act->status != PriorityActivity::IDLE) ||
		act->locked ) {

		act->rescheduleCount++;
	}else{

		enterScheduling();
		readyList.insert(act);
		act->status = PriorityActivity::SCHEDULED;
		leaveScheduling();

	}
}

void PriorityScheduler::unlock(PriorityActivity* act)
{
	InterruptLock lock;

	act->locked = false;

	if(	act->status == PriorityActivity::IDLE ) {

		if(act->rescheduleCount){
			act->rescheduleCount--;
			enterScheduling();
			readyList.insert(act);
			act->status = PriorityActivity::SCHEDULED;
			leaveScheduling();
		}
	}
}

//is always called with interrupts off
void PriorityScheduler::enterScheduling()
{
    stackedSchedules++;

    //If there is a running PriorityActivity mark it INTERRUPTED.
    PriorityActivity* first = (PriorityActivity*)(readyList.first());
    if( (first!=0) ){
    	//because we entered a dispatch monitor, in front of the readyList
    	//is either an interrupted or a running activity, therefore the
    	//check is not needed, Note this is different for the 2-list
    	//FP-scheduler
    	//&& (first->status == PriorityActivity::RUNNING) ){

		first->status = PriorityActivity::INTERRUPTED;
    }
}

//is always called with interrupts off
void PriorityScheduler::leaveScheduling()
{
	stackedSchedules--;
	if(stackedSchedules==0){
		dispatch();
	}
}
