/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/EDFSchedulerNonPreemptive.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/System.h"

using namespace reflex;

// This is the dispatching routine, which empties the readyList.
// It is called with disabled interrupts
void EDFScheduler::start()
{
	for(;;){	//loop endlessly

	    running = 0;	//reset pointer to first activity in the readyList
	    //loop until an activity is in the readyList
		while(!running){
			//getSystem().powerManager.powerDown(!lockCount); //sleep when idle
			getSystem().powerManager.powerDown(); //sleep when idle
			_interruptsDisable();
			running = readyList.deque();
		}

	    _interruptsEnable();	    // interrupts are enabled during execution
    	running->run();				// invocation of the activity
	    _interruptsDisable();		// disable interrupts again

	    running->status = EDFActivity::IDLE;

		//reschedule activity if needed
	    if((running->rescheduleCount > 0) && (!running->locked)){
    		running->rescheduleCount--;
			running->setDeadline();
			readyList.insert(running);
			running->status = EDFActivity::SCHEDULED;
		}
	}
}

// This function brings given activity into the scheduling process.
void EDFScheduler::schedule(EDFActivity* act)
{
	//Interrupts must be disabled now, at the end they must be enabled
	//when call was softwareinitiated and must stay disabled when schedule
	//was called by an interrupthandler
	InterruptLock lock;

	//if activity is already in schedule process, only increment reschedule
	//counter
	if(	(act->status != EDFActivity::IDLE ) ||
		act->locked) {

		act->rescheduleCount++;

	}else{

		act->setDeadline();
		readyList.insert(act);
		act->status = EDFActivity::SCHEDULED;
	}
}

void EDFScheduler::unlock(EDFActivity* act)
{
	InterruptLock lock;

	act->locked = false;

	if(	act->status == EDFActivity::IDLE ) {

		if(act->rescheduleCount){
			act->rescheduleCount--;
			act->setDeadline();
			readyList.insert(act);
			act->status = EDFActivity::SCHEDULED;
		}
	}
}


