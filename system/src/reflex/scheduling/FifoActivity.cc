/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/FifoActivity.h"
#include "reflex/System.h"

using namespace reflex;

FifoActivity::FifoActivity()
{
    status = IDLE;
    next = 0;
    rescheduleCount = 0;
    locked = false;
}

void FifoActivity::lock()
{
	locked = true;
	getSystem().scheduler.lockCount++;
}


void FifoActivity::unlock()
{
	getSystem().scheduler.unlock(this);
	getSystem().scheduler.lockCount--;
}

void FifoActivity::trigger()
{
	getSystem().scheduler.schedule(this);
}

FifoActivity* FifoActivity::dummy()
{
	return this;
}


