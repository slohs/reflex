/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "reflex/scheduling/EDFScheduler_simple.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/powerManagement/PowerManager.h"
#include "reflex/System.h"

using namespace reflex;

//Endless loop which is active whenever the system is idle.
void EDFScheduler::start()
{
	_interruptsEnable();
	while(1){
		//getSystem().powerManager.powerDown(!lockCount);
		getSystem().powerManager.powerDown();
	}
}

// This is the dispatching routine, which empties the readyList.
// Must be called with disabled interrupts
void EDFScheduler::dispatch()
{
	//run Activities from head of list until an interrupted one is in front or the list is empty
	EDFActivity* first = (EDFActivity*)(readyList.first());

	while( (first !=0 ) && (first->status != EDFActivity::INTERRUPTED) ){

	    first->status = EDFActivity::RUNNING;	// mark the activity as running

	    _interruptsEnable();	    // interrupts are enabled during execution
    	first->run();               // invoke the activity
	    _interruptsDisable();		// disable interrupts again

	    readyList.deque();          // remove activity from readyList
	   	first->status = EDFActivity::IDLE;

		//reschedule activity if needed
	    if((first->rescheduleCount > 0) && (!first->locked)){

    		first->rescheduleCount--;
			first->setDeadline();
			readyList.insert(first);
			first->status = EDFActivity::SCHEDULED;

		}

	    first = (EDFActivity*)(readyList.first());
	}

	//if there is an interrupted activity set it Running again,
	//because this Stack-instance of Scheduler has done its work
	if(first){
		if(first->status == EDFActivity::INTERRUPTED)
		first->status = EDFActivity::RUNNING;
	}
}

// This function brings given activity into the scheduling process.
void EDFScheduler::schedule(EDFActivity* act)
{
	//Interrupts must be disabled now, at the end they must be enabled
	//when call was softwareinitiated and must stay disabled when schedule
	//was called by an interrupthandler
	InterruptLock lock;

	if(	(act->status != EDFActivity::IDLE) ||
		act->locked) {

		act->rescheduleCount++;
	}else{

		enterScheduling();      //use scheduling monitor
		act->setDeadline();
		readyList.insert(act);	//sorts activity directly into the readyList
		act->status = EDFActivity::SCHEDULED;
		leaveScheduling();		//use scheduling monitor

	}
}

void EDFScheduler::unlock(EDFActivity* act)
{
	InterruptLock lock;

	act->locked = false;

	if(	act->status == EDFActivity::IDLE ) {

		if(act->rescheduleCount){
			act->rescheduleCount--;
			enterScheduling();
			act->setDeadline();
			readyList.insert(act);
			act->status = EDFActivity::SCHEDULED;
			leaveScheduling();
		}
	}
}

// Signals a starting scheduling process.
//	Must be executed with disabled Interrupts
void EDFScheduler::enterScheduling()
{
	//signalling for scheduling monitor
    stackedSchedules++;

    //If there is a running Activity mark it INTERRUPTED.
    EDFActivity* first = (EDFActivity*)(readyList.first());
    if( (first!=0) && (first->status == EDFActivity::RUNNING) ){
		first->status = EDFActivity::INTERRUPTED;
    }
}

// If no other schedulings are in process, update of readyList is done.
// Must be executed with disabled Interrupts
void EDFScheduler::leaveScheduling()
{
	stackedSchedules--;
	if(stackedSchedules==0){
		dispatch();
	}
}


