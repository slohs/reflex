#/**
# * Makefile to link components
# * Adds some rules and variables to the StandardMake.mk
# */

##########################################################################
# Variables that must be defined by user
##########################################################################

LDFLAGS +=

#COMPONENT_LDFLAGS += -nostdlib -nostartfiles -L/opt/msp430-gcc-4.4.5/lib/gcc/msp430/4.4.5/ -m $(DEVICE) -T$(REFLEXPATH)/platform/$(PLATFORM)/ComponentLinkerScript.ld
COMPONENT_LDFLAGS += -nostdlib -nostartfiles -L$(GCC_INSTALL_PATH) -m $(DEVICE) -T$(REFLEXPATH)/platform/$(PLATFORM)/ComponentLinkerScript.ld



############################################################################
# gather include and src directories
# prefix paths for sources
############################################################################

# Extract all defined variables in the form CC_SOURCES_COMPONENT_xxx from sources.mk
CC_SOURCES_COMPONENTS_APPLICATION = \
	$(shell grep COMPONENT_APPLICATION $(APPLICATIONPATH)/Sources.mk | sed -e 's/=//g' -e 's/ //g' -e 's/\\//g')
CC_SOURCES_COMPONENTS_PLATFORM = \
	$(shell grep COMPONENT_PLATFORM $(APPLICATIONPATH)/Sources.mk | sed -e 's/=//g' -e 's/ //g' -e 's/\\//g')
CC_SOURCES_COMPONENTS_LIB = \
	$(shell grep COMPONENT_LIB $(APPLICATIONPATH)/Sources.mk | sed -e 's/=//g' -e 's/ //g' -e 's/\\//g')

# Remove CC_SOURCES_COMPONENT_ to get the components' plain names (e.g. Blinker)
COMPONENTS =  \
	$(shell echo $(CC_SOURCES_COMPONENTS_APPLICATION) | sed -e 's/CC_SOURCES_COMPONENT_APPLICATION_//g')

# Add the application path to the containing source-files like it is done above
# Blubb is only a placeholder, since make doesn't allow functions to be called from outside of rules
# Since we can have multiple components, we need o do this in a foreach-loop
TRANSFORM = $(addprefix $(APPLICATIONPATH)/,$($(COMPONENT)))
BLUBB := $(foreach COMPONENT,$(CC_SOURCES_COMPONENTS_APPLICATION),$(eval $(COMPONENT) = $(TRANSFORM)))

############################################################################
# define component objects
############################################################################
BLUBB := $(foreach COMPONENT,$(COMPONENTS),$(eval CC_SOURCES_COMPONENT_$(COMPONENT) = $(CC_SOURCES_COMPONENT_APPLICATION_$(COMPONENT)) $(CC_SOURCES_COMPONENT_LIB_$(COMPONENT)) $(CC_SOURCES_COMPONENT_PLATFORM_$(COMPONENT))))
RENAME = $(notdir $(CC_SOURCES_COMPONENT_$(COMPONENT):.cc=.o))

BLUBB := $(foreach COMPONENT,$(COMPONENTS),$(eval $(COMPONENT)_OBJECTS = $(RENAME)))
COMPONENT_OBJECTS = $(foreach COMPONENT,$(COMPONENTS),$($(COMPONENT)_OBJECTS))


############################################################################
# Additional rules to the main Makefile
############################################################################




symbols: $(EXECUTABLE)
	@echo Extracting symbols from $(EXECUTABLE)...
	$(OBJDUMP) -t $(EXECUTABLE) > $(APPLICATION).sym.txt
	@echo Making symbol table compatible to LinkerScript...
#php elftool.php symbols2linkerscriptNoFilter --i=$(APPLICATION).sym.txt --o=$(APPLICATION).sym.filtered.txt --symbol-filter=g	
	perl $(REFLEXPATH)/utils/componentUpdate/extractSymbolsFromELF.pl $(APPLICATION).sym.txt > $(APPLICATION).sym.filtered.txt
	
	
# Generate components. CC_SOURCES_COMPONENT_Xxx becomes ComponentXxx.o
# Incremental Linking of Components
$(COMPONENTS) : $(COMPONENT_OBJECTS)
	@echo #####
	@echo $(CC_SOURCES_COMPONENT_BlueBlinker)
	@echo Linking Component \"$@\"...
# wir wollen ja nicht auf dem knoten linken....
	$(LD) $(COMPONENT_LDFLAGS) -o Component$@.elf  $($@_OBJECTS)  -T$(APPLICATION).sym.filtered.txt -Ttext $(TEXT)
	$(OBJCOPY) -I elf32-$(TOOLPREFIX_UPDATE) -O binary Component$@.elf Component$@.bin
	$(OBJCOPY) -O ihex Component$@.elf Component$@.hex

# Compile and link components
components: symbols $(COMPONENTS)

	

# Reconfigure symbol sections to be programmed to target
kernel_symbols: $(EXECUTABLE) symbols
	@echo Making $(EXECUTABLE) hex for  \"$@\"...	
	$(OBJCOPY) -O ihex $(APPLICATION).elf $(APPLICATION).hex	
