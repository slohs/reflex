############################################################################
#
# REFLEX - Real-time Event FLow EXecutive
#
# A lightweight operating system for deeply embedded systems.
#
# This file states the system source files needed for an Reflex application.
#

# Author:	Karsten Walther
#
############################################################################

ifeq ($(SCHEDSCHEME), FIFO_SCHEDULING)
SCHEDULER = scheduling/FifoScheduler.cc
ACTIVITY = scheduling/FifoActivity.cc
endif
ifeq ($(SCHEDSCHEME), PRIORITY_SCHEDULING)
SCHEDULER = scheduling/PriorityScheduler.cc
ACTIVITY = scheduling/PriorityActivity.cc
endif
ifeq ($(SCHEDSCHEME), PRIORITY_SCHEDULING_SIMPLE)
SCHEDULER = scheduling/PriorityScheduler_simple.cc
ACTIVITY = scheduling/PriorityActivity.cc
endif
ifeq ($(SCHEDSCHEME), PRIORITY_SCHEDULING_NONPREEMPTIVE)
SCHEDULER = scheduling/PrioritySchedulerNonPreemptive.cc
ACTIVITY = scheduling/PriorityActivity.cc
endif
ifeq ($(SCHEDSCHEME), EDF_SCHEDULING)
SCHEDULER = scheduling/EDFScheduler.cc
ACTIVITY = scheduling/EDFActivity.cc
endif
ifeq ($(SCHEDSCHEME), EDF_SCHEDULING_SIMPLE)
SCHEDULER = scheduling/EDFScheduler_simple.cc
ACTIVITY = scheduling/EDFActivity.cc
endif
ifeq ($(SCHEDSCHEME), EDF_SCHEDULING_NONPREEMPTIVE)
SCHEDULER = scheduling/EDFSchedulerNonPreemptive.cc
ACTIVITY = scheduling/EDFActivity.cc
endif
ifeq ($(SCHEDSCHEME), TIME_TRIGGERED_SCHEDULING)
SCHEDULER = scheduling/TimeTriggeredScheduler.cc
ACTIVITY = scheduling/TimeTriggeredActivity.cc
endif

ifdef WIRELESS_UPDATE
CC_SOURCES_SYSTEM += \
	updater/BSL_Update.cc
endif

CC_SOURCES_SYSTEM += \
    interrupts/InterruptGuardian.cc \
    interrupts/InterruptHandler.cc \
    powerManagement/PowerManager.cc \
    powerManagement/PowerManageAble.cc \
    $(SCHEDULER) \
    $(ACTIVITY) \

CC_SOURCES_CHECK_SYSTEM += \
    scheduling/ActivityCheck.cc\
    scheduling/SchedulerCheck.cc

