#ifndef REFLEX_SINGLE_VALUE_H
#define REFLEX_SINGLE_VALUE_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	SingleValue1 - SingleValue3
 *
 *	Author:		Karsten Walther
 *
 *	Description:	This is an active input, which schedules referenced
 *			activity in case of assignment
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/scheduling/Activity.h"
#include "reflex/sinks/Trigger.h"
#include "reflex/sinks/Sink.h"
#include "reflex/interrupts/InterruptLock.h"

#include "reflex/memory/Buffer.h"

namespace reflex {

/** Implements event-flow buffers which store one value, which can
 *  be overwritten anytime. Is therefore an abstraction for an uptodate
 *  value. The corresponding activity is triggered everytime the buffer is
 *  written - no matter if the new value is different or not from current on
 *  it is only checked if the value was read, if not the activity is not
 *  triggered for reducing workload.
 */
template<typename T1>
class SingleValue1 : public Trigger, public Sink1<T1>
{
public:
    SingleValue1(Activity* act = (Activity*)0)
    {
        Trigger::init(act);
        free = true;
    }

    /** implements Sink1
     */
    virtual void assign(T1 value);

    /** retrieve saved values */
    T1 get();
    void get(T1& v1);

private:
    /** buffer
     */
    T1 v1;

    /** for check if activity must be triggered
     */
    bool free;
};

/** Same as SingleValue1 but stores a tuple
 *
 *	@author Karsten Walther
 */
template<typename T1, typename T2>
class SingleValue2 : public Trigger, public Sink2<T1,T2> {
public:
    SingleValue2(Activity* act = (Activity*)0)
    {
        free = true;
        Trigger::init(act);
    }

    /** implements Sink2
     */
    virtual void assign(T1 v1, T2 v2);

    /** retrieve saved values */
    void get(T1& v1, T2& v2);

private:
    /** buffers
     */
    T1 v1;
    T2 v2;

    /** for check if activity must be triggered
     */
    bool free;
};

/** Same as SingleValue1 but stores a triple
 *
 *	@author Karsten Walther
 */
template<typename T1, typename T2, typename T3>
class SingleValue3 : public Trigger, public Sink3<T1,T2,T3> {
public:
    SingleValue3(Activity* act = (Activity*)0)
    {
        free = true;
        Trigger::init(act);
    }

    /** implements Sink3
     */
    virtual void assign(T1 v1, T2 v2, T3 v3);

    /** retrieve saved values */
    void get(T1& v1, T2& v2, T3& v3);

private:
    /** buffers
     */
    T1 v1;
    T2 v2;
    T3 v3;

    /** for check if activity must be triggered
     */
    bool free;
};


/**
 * SingleValue1 specialization for buffer to avoid running out of buffers
 *
 * @author Hannes Menzel
 * @TODO: throw custom error message at compile time
 */
template<>
class SingleValue1<reflex::Buffer*>;

} //namespace reflex


#include "reflex/sinks/SingleValue.cc"


#endif
