/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:		 Karsten Walther
 */

#include "reflex/debug/Assert.h"

template <typename T>
void reflex::Queue<T>::assign(T value)
{
    reflex::InterruptLock lock;
    this->enqueue(value);

    trigger();
}

template <typename T>
T reflex::Queue<T>::get()
{
    reflex::InterruptLock lock;
    return this->dequeue();
}
