/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		Karsten Walther
 */

template <typename T>
void SyncedData<T>::assign(T value)
{
	InterruptLock lock;
	data = value;
	if(subscriber){
		subscriber->notify();
	}
}

template <typename T>
void SyncedData<T>::get(T& target) const
{
	InterruptLock lock;
	target = data;
}
