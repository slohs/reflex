#ifndef InterruptLock_h
#define InterruptLock_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	InterruptLock
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Interruptlocking with restore.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

/**	Declaration of assembler functions for enabling and disabling of interrupts
 */
extern "C" {
	bool _interruptsDisable();
	void _interruptsEnable();
}

namespace reflex {

/**
 *  Interrupt locking object, prevents from forgotten or false re-enabling
 *  of interrupts.
 *
 *  Usage:
 *  void foo()
 *  {
 *		InterruptLock lock;
 *		//critical stuff
 *  }
 *
 */
class InterruptLock {
public:

	/** Constructor stores the current interruptstate and disables the
	 *  interrupts.
	 */
	InterruptLock()
	{
		oldState=_interruptsDisable();
	}

	/** Method for explicit locking of interrupt. Store interrupt state.
	 *
	 *  @return old interrupt state true = enabled, false = disabled
	 */
	bool lock(){
		oldState=_interruptsDisable();
		return oldState;
	}

	/** Method for explicit restoring of interrupt state.
	 */
	void restore(){
		if(oldState){
			_interruptsEnable();
		}
	}

	/**	Destructor implicitely restores old interrupt state.
	 */
	~InterruptLock()
	{
		if(oldState) {
			_interruptsEnable();
		}
	}

private:
	/** Stores the interruptstate before locking.
	 */
	bool oldState;
};

} //namespace reflex

#endif
