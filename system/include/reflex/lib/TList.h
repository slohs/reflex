#ifndef TList_h
#define TList_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	TList
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Generic List implementation used by different schedulers.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

namespace reflex {

/**
 *  This is an List implementation, which can be used for sorted and
 *  unsorted lists.
 *  The template parameter type must have a next element of type T* and
 *  must provide a method bool lessEqual(T& than).
 */
template<typename T>
class TList {
public:
	/** Set start to null.
	 */
	TList()
	{
		start = 0;
	}

	/** The given list element is put in front of the list.
	 *
	 *  @param elem the element to put in front of the list
	 */
	void enque(T* elem);

	/** The first element of the list removed.
     *
     *  @return pointer to the removed element
	 */
	T* deque();

	/** Gives back pointer to the first element from list.
	 *  Element remains in the list.
	 *
	 *  @return pointer to the first element
	 */
	T* first()
	{
		return start;
	}

	/** Sorts the given element into the list in ascending order.
	 *  Using lessEqual(T& than) method of the element.
	 *  The needed time depends on the position there the element is inserted.
	 */
	void insert(T* elem);

	/**
	 * Removes the given element from the list.
	 * @param elem
	 */
	void remove(T* elem);

private:
	/** Pointer to first element of the list.
	*/
	T* start;
};

} //namespace reflex

using namespace reflex;

template<typename T>
void TList<T>::enque(T* elem)
{
	elem->next = start;
	start = elem;
}

template<typename T>
T* TList<T>::deque()
{
	if(start != 0){
		T* first = start;
		start = start->next;
		return first;
	}
	return 0;
}

template <typename T>
void TList<T>::insert(T* elem)
{
    T* pos = start;
    if((pos!=0) && (pos->lessEqual(*elem))){
        while((pos->next != 0) && (pos->next->lessEqual(*elem))){
    	    pos = pos->next;
		}
		elem->next = pos->next;
		pos->next = elem;
    }else{			//if elem is lower than all other
		elem->next = start;
		start = elem;
    }
}

template<typename T>
void TList<T>::remove(T* elem)
{
	if (elem == start)
	{
		start = elem->next;
	}
	else
	{
		T* current = start;
		while (current->next != elem)
		{
			current = current->next;
		}
		current->next = current->next->next;
	}
}


#endif
