#ifndef RADIOCONFIGURATION_H
#define RADIOCONFIGURATION_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Hannes Menzel
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/driverConfiguration/ConfigurationObject.h"
#include "reflex/memory/Pool.h"

class RadioConfiguration : public ConfigurationObject<RadioConfiguration>
{

public:

	/**
	 * stores the configuration within a object
	 */
	struct Configuration
	{
		/**
		 * the signal attenuation from -127dBm till +127dBm
		 */
		int8 signalAttenuation;

		/**
		 * the logical channel
		 */
		uint8 channel;

		/**
		 * every bit represents a special working mode
		 */
		uint16 types;

		/**
		 * for special test purposes -> the radio is forced to
		 */
		uint8 transmissionTime;

	};


	/*
	 * other setup parameters
	 */
	enum miscType
	{
		 CCA = 0x1
		,WHITENING = 0x2
		//,CRC = 0x4
		,SENDING_TOS = 0x8
		,CONT_TRANSMISSION = 0x10
	};

/**
* starting method region
*/


	RadioConfiguration(/*reflex::Pool& pool*/)
	{
		clear();
	}

	void clear()
	{
		data.channel = 0;
		//data.packetSize = 0;
		data.signalAttenuation = 0;
		data.types = 0;
		data.transmissionTime = 0;
	}

	inline void setLogicalChannel(uint8 channel)
	{
		data.channel = channel;
	}

	/**
	 * @param signalAttenuation the gain in dBm
	 */
	inline void setSignalAttenuation(int8 signalAttenuation)
	{

		data.signalAttenuation = signalAttenuation;
	}

	/**
	 * only needed if continuous transmission is enables
	 * @param time time for transmission in seconds
	 */
	inline void setTransmissionTime(uint8 time)
	{
		data.transmissionTime = time;
	}


	/**
	 * enables or disables CCA
	 */
	inline void setCCA(bool val)
	{
		data.types = (data.types & ~CCA) | (val << CCA_SHIFT);
	}

//	/**
//	 * enables or disables CRC
//	 */
//	inline void setCRC(bool val)
//	{
//		data.types = (data.types & ~CRC) | (val << CRC_SHIFT);
//	}

	/**
	 * enables or disables Whitening
	 */
	inline void setWhitening(bool val)
	{
		data.types = (data.types & ~WHITENING) | (val << WHITENING_SHIFT);
	}

	/**
	 * enables or disables transmitting and receiving TOS
	 */
	inline void setSendingTOS(bool val)
	{
		data.types = (data.types & ~SENDING_TOS) | (val << SENDING_TOS_SHIFT);
	}

	/**
	 * enables or disables a continuous transmission. The transmission
	 * starts when the next transmit signal is invoked
	 */
	inline void setContinuousTransmission(bool val)
	{
		data.types = (data.types & ~CONT_TRANSMISSION) | (val << CONT_TRANSMISSION_SHIFT);
	}



	/**
	 * @return the signal attenuation in dBm
	 */
	inline int8 getSignalAttenuation()
	{
		return data.signalAttenuation;
	}

	/**
	 * @return the duration of the data, which have to transmit
	 */
	inline uint8 getTransmissionTime()
	{
		return data.transmissionTime;
	}

	inline uint8 getLogicalChannel()
	{
		return data.channel;
	}

	/**
	 * @return is cca configured
	 */
	inline bool isCCA()
	{
		return (data.types & CCA);
	}
//
//	/**
//	 * @return is CRC configured
//	 */
//	inline bool isCRC()
//	{
//		return (data.types & CRC);
//	}

	/**
	 * @return is whitening configured
	 */
	inline bool isWhitening()
	{
		return (data.types & WHITENING);
	}

	/**
	 * @return is transmitting and receiving TOS is configured
	 */
	inline bool isSendingTOS()
	{
		return (data.types & SENDING_TOS);
	}


	/**
	 * @return if a continous transmission is set and the transmission time is greater than 0
	 */
	inline bool isContinousTransmission()
	{
		return (data.types & CONT_TRANSMISSION) && (data.transmissionTime != 0);
	}



private:

	void* getData()
	{
		return (this);
	}

	uint8 getSize()
	{
		return sizeof(RadioConfiguration);
	}


	enum type_masks
	{
		 CCA_SHIFT = 0
		,WHITENING_SHIFT = 1
		//,CRC_SHIFT = 2
		,SENDING_TOS_SHIFT = 3
		,CONT_TRANSMISSION_SHIFT = 4
	};

	/**
	 * instance of the configuration
	 */
	Configuration data;
};

#endif
