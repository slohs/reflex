/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		 Hannes Menzel
 *	Date:		 12.2011
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#ifndef CONFIGURABLEDRIVER_H_
#define CONFIGURABLEDRIVER_H_

#include "reflex/sinks/SingleValue.h"
#include "reflex/types.h"
#include "reflex/scheduling/ActivityFunctor.h"

#include "reflex/driverConfiguration/ConfigurationObject.h"


namespace reflex {

/**
 * The class represents the base class of all configurable drivers.
 * Configurable drivers change their behavior or parameters by receiving configuration objects.
 * The reconfiguration has been made possible by implementing the pure virtual method "configure()".
 * Configure is called via an ActivityFunctor. The sink of this functor is accessible by
 * get_in_confData.
 * To ensure correct type of configuration object a template parameter ConfObject is used. ConfObject
 * should be a derived class of type ConfigurationObject.
 *
 * @param ConfObject has to be equal to the ConfigurationObject for a certain driver
 */
template<class ConfObject>
class ConfigurableDriver
{
public:


	/**
	 * standard constructor
	 */
	ConfigurableDriver() : confFunctor(*this)
	{
		//connecting functor with data source
		confData.init(&confFunctor);
	}


	/**
	 * @return the input for a configuration object
	 */
	inline SingleValue1<ConfObject>* get_in_confData()
	{
		return &confData;
	}



protected:

	/**
	 * this method performs the reconfiguration of the driver
	 * the driver must ensure the correct implementation of this method
	 */
	virtual void configure() = 0;

	/**
	 * event on confData
	 */
	reflex::ActivityFunctor<ConfigurableDriver, &ConfigurableDriver::configure> confFunctor;

	/**
	 * data sink in the driver -> if notified driver will be informed
	 */
	reflex::SingleValue1<ConfObject> confData;
};

}
#endif /* CONFIGURABLEDRIVER_H_ */
