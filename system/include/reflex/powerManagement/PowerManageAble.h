#ifndef PowerManageAble_h
#define PowerManageAble_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	PowerManageAble
 *
 *	Author:		Karsten Walther
 *
 *	Description:
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
//#include "reflex/powerManagement/PowerManager.h"
#include "reflex/powerManagement/PowerGroups.h"
#include "reflex/data_types/ChainLink.h"
#include "reflex/MachineDefinitions.h"

namespace reflex {
class InterruptHandler;
class PowerManager;

/* FIXME: May we should use two different types instead of Primary Secondary attributes.
	but this only if we release the bound between InterruptHandler and PowerManageAble.
	This affects espezially the InvalidInterruptHandler, which should be stateless and it doesn't.
*/

/** The facility to be managed by the Powermanager.
 *	A suspendable hardware driver has to implement
 *  the suspend/resume code within the enable/disable methods. Further the deepest sleep
 *	mode has to be set to a valid value. @see PowerModes. The default initialization sets the
 *	active power mode, that will not save any power. This will garantee a well defined state for the
 *	abstracted hardware, since deeper powermodes leeds to unpredictable behaviour of some hardwaremodules.
 **/
class PowerManageAble 
	: public data_types::ChainLink ///< ChanLink-ability for powermanagedable objects
{
public:
	enum Priority{
		PRIMARY = 0,
		SECONDARY= 1
	};
protected:
	//also the PowerManager has the permission to change this
	friend class ::reflex::PowerManager;
	friend class ::reflex::InterruptHandler;

	bool enabled:1;	///< enable flag
	bool isSecondary:1; ///< for interrupts that will trigger after software treatment
	bool:6; // remaining bits are reserved
	mcu::PowerModes deepestAllowedSleepMode; ///< the most powersaving sleepmode, which let this entity in an well defined state. Does not break the hardware
	powerManagement::PowerGroups groups; ///< the groups where this entity is part of.

private:
	/** This constructor is called from the invalid interrupthandler only.
	 *  Due to there is no reflex obj while contructing those on some platforms (OMNetPP).
	 */
	explicit PowerManageAble();

public:
	/**Initialize and register this entity with the PowerManager.
	 */
	PowerManageAble(const Priority isSecondary);

	/** set a bunch of groups, which entity is part of. Technically groups is intendet to be 
	 * a bitfield. Each bit represent a group.
	 * @param groups : one or more groups to be part of @see PowerManager::PowerGroups
	 */
	void setGroups(const powerManagement::Groups_t groups) { this->groups=powerManagement::PowerGroups(groups); }
	
	/** set the deepest sleepmode, in which the hardware is still able to work. E. g. the interrupts can fire.
	 * @param  deepestAllowedSleepMode one of hardware specific sleepmodes.
	 */
	void setSleepMode(const mcu::PowerModes deepestAllowedSleepMode) {this->deepestAllowedSleepMode = deepestAllowedSleepMode;}

	/** enables this entity even by the PowerManager. This call is delegated
	 * to the PowerManager::enableObject method.
	 */
	void switchOn();

	/** disables this entity even by the PowerManager. This call is delegated
	 * to the PowerManager::disableObject method.
	 */
	void switchOff();
	
	/** check power state
	 */
	inline
	bool isEnabled() const {return enabled;}

protected:
	/**enable the hardware of a powermanageable object
	 */
	virtual void enable()=0;
	/**disable the hardware of a powermanageable object
	 */
	virtual void disable()=0;

private:
};


} //end namespace reflex

#endif
