#ifndef UPDATE_H
#define UPDATE_H

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Update
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	The Update Main, implements the Updateprotokoll
 *
 *	(c) Carsten Schulze, BTU Cottbus 2006
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "conf.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/update/Command.h"
#include "reflex/update/Communicator.h"
#include "reflex/update/Sections.h"
#include "reflex/update/Updatefunctions.h"
#include "reflex/update/StatusMem.h"


namespace reflex {

/**
 * Initialise and controls the Update
 * 
 * @author Carsten Schulze
  
 */
class Update : public Activity, public Sink0, public Sink1<Command*>
{
public:
	/**
	* Here is the initalization 
	*/
	
	Update(unsigned curVersion, OutputChannel* output);
	
	/**
	 * for updating other Nodes in the neighborhood
	 * after it is called the radio must be initialized again
	 * @param hopCount
	 */
	void distributeSystem(unsigned char hopCount);
	
	/** starts looking for an update, or stops it
	 * @param look true -> searching for an updtae is started, false -> stopped
	 */
	void lookforUpdate(bool look = true);
	
	/**
	 * called from scheduler  
	 */
	virtual void run();
	
	/**
	 * called from the sending instance, to notify if a command was sent
	 */
	virtual void notify();
	
	/**
	 * called from the receiving instance, if a new command was received
	 * @param nCom the new command received
	 */
	virtual void assign(Command* nCom);
	
	/**
	 * member communicator. An Instance for sending and receiving commands
	 */
	Communicator comm;

private :
	
	/**
	 * called from run, to do initialize a distributation of the System
	 */	
	void initdistSysImg();
	
	/**
	 * called after distributing the own image to return to the normal operation
	 */
	void exitdistSysimg();
	
	/**
	 * starts the distrubution of a received image
	 */
	void distributeImg();
	
	/**
	 * called when a command was received and the node is waiting 
	 * for an update or receiving a new version
	 * @param com command received
	 */ 
	void update(Command* com);
	
	/** 
	 * checks if the received command can initialze a new update
	 * if so, a new update is initialized
	 * @param com the command which is checked ( com id == UPDATE)
	 */
	void initUpdate(Command* com);

	/**
	 * stores a command in the datafield for update commands
	 * @param com command to be stored
	 */
	void saveCommand(Command* com);
	
	/**
	 * builds the next Command witch should be send while distributing the system
	 */
	void buildNextCom();
	
	/**
	 * is finishing the update and calls UpdateExec to executes 
	 * the received commands
	 */
	void execImg();
	
	/**
	 * If distributing the system this mehtod is called after a command was sent.
	 * This mthods sends the next command, to distrubute the local system.
	 */
	void sendSys();
	
	/**
	 * sends the next command, while sending a received new version
	 */
	void sendImg();
	
	/**
	 * claculates the checksum of a command, without changing the command
	 * @param com command to evaluate
	 * @return the correct checksum of the command
	 */
	unsigned char calcCheckSum(Command* com);
	
	/**
	 * checks if the received commands are correct.
	 * if they are wrong, it tries to repair them,
	 * if they are correct the commands are distributed in the net 
	 * or executed localy
	 */
	void updateCheck();
	
	/// memory sections of the system
	Sections sections;
	
	/// the current version of the system
	unsigned applVersion;
	
	/// state of the node
	enum State {
		UNDEFINED = 0,
		WAITFORUPDATE = 1,
		UPDATING = 2,
		INITDISTSYSTEM = 3,
		DISTSYSTEM = 4,
		DISTIMG = 5,
		DOUPDATE = 6,
		EXITDISTSYS = 8,
		WAITFORDIST = 9,
		STARTDISTIMG = 10,
		WAIT_FLOOD_DISTSYSTEM = 11
	};
	
	/// waittimes for sending commands
	enum {
		WAITTIMENEXTCOM = SAVEDELAY,
		WAITTIMESTARTDIST = (	WAITTIMENEXTCOM + 
					Communicator::PSEUDOMACSLOTS*RECDELAY + 
					RECDELAY)
	};
	
	/// current state of the update
	volatile State state;
	/// number of commands received (used to check if all correct)
	unsigned nrComReceived;
	/// a temp command, which is currently sending, or build to send next
	Command currSending;
	/// next byte in the current system, which should be packed into a command
	caddr_t nextBytetoSend;
	/// the section of the system which is currently sent
	unsigned sectionToSend;
	/// number of the command, which send next ( from a previously received image)
	unsigned nrImgComSend;
	/// number of hops to distrubute the current image
	unsigned char hopCount;
	/// count how often a version was transmitted by this node
	volatile bool repaired;
	/// pointer to the position where the array of commands is stored
	Command* comList;
	/// count how often a version was transmitted by this node 
	unsigned resendImgCount;// to repeat the update procedure more times
	
	///for access to the machine specific updatefuntions
	Updatefunctions machineupdfunc;
	
};

} //reflex
#endif


