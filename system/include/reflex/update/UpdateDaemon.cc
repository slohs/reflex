/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Andre Sieber
 */

//#include "reflex/update/UpdateDaemon.h"

#ifndef UPDATEDEBUG
#define UPDATEDEBUG 0
#endif
//FIXME: get rid of that Nodeconfiguration dependencies even then they are use in debugmode only
//using namespace reflex;

extern "C" void _reset();

template<class D>
reflex::UpdateDaemon<D>::UpdateDaemon() :

	sendNextImagePartFunctor(*this)
	//,timer(waitEvent)
	,timer(VirtualTimer::PERIODIC)
	,forwardingFinishedFunctor(*this)
	,packet(0)
{
	input.init(this);
	waitEvent.init(&sendNextImagePartFunctor);
	timer.init(waitEvent);
	forwardingFinished.init(&forwardingFinishedFunctor);
	count = 0;

	LOCKED = false;
	startNode = false;
	state = IDLE;
	localRepeats = 0;
	globalRepeats = 0;
	doInit = false;
	red = false;
	mode = 255;
}

template<class D>
void reflex::UpdateDaemon<D>::run()
{

	packet = input.get();
	if (packet) {



		//check if this is a packet for the update of not
		packet->pop(MType);

		if (MType != UPDATE) {
			applicationOutput->assign(packet);
			return;
		}


		packet->read(currentCommand);
#if UPDATEDEBUG==1
		getApplication().debugOut.write("packet ver: ");
						getApplication().debugOut.write(currentCommand.version);
						getApplication().debugOut.writeln();
#endif
		if ((getSystem().status.version >= currentCommand.version)
				&& (currentCommand.version != 0))
		{
#if UPDATEDEBUG==1
			getApplication().debugOut.write("got some older");
			getApplication().debugOut.writeln();
#endif
			packet->downRef();
			return;
		}
		switch (currentCommand.commandID) {
		case Command::START:
			startUpdate( packet);
			break;
		case Command::INIT:
			newVersion = currentCommand.version;
			initUpdate(packet);
			break;
		case Command::INSERT:
			insertPacket(packet);
			break;
		case Command::REBIRTH:
			if (currentCommand.rebirthCommand.doIt)
				rebirth();
			break;
		default:
			break;
		}
		packet->downRef();
	}
}

template<class D>
void reflex::UpdateDaemon<D>::forwardApplicationData()
{
	applicationPacket = applicationInput.get();
	MType = APPLICATION;
	applicationPacket->push(MType);
	output->assign(applicationPacket);
}

template<class D>
void reflex::UpdateDaemon<D>::handleForwardingFinished()
{
	getApplication().led.turnOn(Led::GREEN);
}

template<class D>
void reflex::UpdateDaemon<D>::sendNextImagePart()
{

	timer.set(WaitTime);


	if (ImagePaketNumber <= ImageOverallSize) {

		Buffer* buffer = new (pool) Buffer(pool);
		//be sure to get buffer
		while (!buffer) {
#if UPDATEDEBUG==1
			getApplication().led.blink(Led::ALL);
#endif
		}
		MType = UPDATE;
		buffer->push(MType);
		//buffer->write(MType);

		Command command;
		command.commandID = Command::INSERT;
		command.insertCommand.offset = currentBlock - imageStart;
		command.version = getSystem().status.version;
		command.insertCommand.overallSize = ImageOverallSize;
		command.insertCommand.number = ImagePaketNumber;


		//debug
#if UPDATEDEBUG==1
		getApplication().debugOut.write((command.insertCommand.number + 1));
		getApplication().debugOut.write("/");
		getApplication().debugOut.write(command.insertCommand.overallSize);
		getApplication().debugOut.write("==");
		getApplication().debugOut.flush();
#endif

		Command command1;
		command1.commandID = Command::INSERT;

		//send block
		buffer->write(command);
		char temp[UpdateBlockSize];
		MemoryDevice->read((caddr_t) & temp, currentBlock, UpdateBlockSize);
		//buffer->write(*(Block*)currentBlock);


		buffer->write(temp);
		output->assign(buffer);

		//localy repeat block
		if (++localRepeats == LOCALREPEATS) {
			currentBlock += UpdateBlockSize;
			ImagePaketNumber++;
			localRepeats = 0;
		}

	} else {
#if UPDATEDEBUG==1
		getApplication().debugOut.write("done!!");
		getApplication().debugOut.writeln();
#endif
		if (++globalRepeats == GLOBALREPEATS) {
#if UPDATEDEBUG==1
			getApplication().debugOut.write("reboot??");
			getApplication().debugOut.writeln();
#endif

			timer.set(0);
			if (state == REBIRTH)
			{
				rebirth();
			}
			else // stop
			{

			}

		}
		//or start over
		else {
			currentBlock = imageStart;
			ImagePaketNumber = 0;
		}

	}
}

template<class D>
void reflex::UpdateDaemon<D>::startUpdate(Buffer* packet)
{
#if UPDATEDEBUG==1
	getApplication().led.turnOn(Led::BLUE);
#endif


	//is start node
	if ((currentCommand.startCommand.nodeID == getSystem().status.id) ||
			((mode == Command::PROPAGATE) || (mode == Command::PROPAGATE_WAIT))){



		timer.set(4 * WaitTime);//time to delete flash on the other side;
		startNode = true;

		Command init;
		//re calculate values for the current packet
		init.commandID = Command::INIT;
		init.initCommand.blockSize = UpdateBlockSize;

	if (currentCommand.startCommand.nodeID == getSystem().status.id) {

		ImageOverallSize = ((unsigned) getApplication().sections.UpdateAble[1]
					- (unsigned) getApplication().sections.UpdateAble[0])
					/ UpdateBlockSize;

			if ((((unsigned) getApplication().sections.UpdateAble[1]
					- (unsigned) getApplication().sections.UpdateAble[0]))
					% UpdateBlockSize)
			{
				ImageOverallSize++;
			}

		mode = currentCommand.startCommand.targetMode;
		init.version = getSystem().status.version;

		currentBlock = getApplication().sections.UpdateAble[0];
		imageStart = getApplication().sections.UpdateAble[0];
		imageEnd = getApplication().sections.UpdateAble[1];
		init.initCommand.overallSize = ImageOverallSize;

	} else //propagating received image
	{
		init.version = newVersion;

		currentBlock = getApplication().sections.SpaceforUpdateImg[0];
		imageStart = getApplication().sections.SpaceforUpdateImg[0];
		imageEnd = getApplication().sections.SpaceforUpdateImg[0] + currentCommand.insertCommand.overallSize * UpdateBlockSize;
		init.initCommand.overallSize = ImageOverallSize;

	}


		init.initCommand.targetMode = mode;
		ImagePaketNumber = 0;


		//propagate init-packet into the net
		Buffer* startPacket = new (pool) Buffer(pool);
		if (startPacket != 0) {
			MType = UPDATE;
			startPacket->push(MType);
			startPacket->write(init);
			output->assign(startPacket);
#if UPDATEDEBUG==1
			getApplication().debugOut.write("send init");
			getApplication().debugOut.writeln();
#endif
		}

		while (!startPacket) {
#if UPDATEDEBUG==1
			getApplication().led.blink(Led::ALL);
#endif
		}

	}
}


template<class D>
void reflex::UpdateDaemon<D>::initUpdate(Buffer* paket)
{

	if ((state == IDLE) && (doInit == false)) {
#if UPDATEDEBUG==1
		getApplication().debugOut.write("initUpdate");
		getApplication().debugOut.writeln();
		getApplication().led.blink(Led::RED);
#endif
		doInit = true;
		mode = currentCommand.initCommand.targetMode;

		ImageOverallSize = currentCommand.initCommand.overallSize;

		MemoryDevice->erase(getApplication().sections.SpaceforUpdateImg[0],
				ImageOverallSize * UpdateBlockSize);
	}
	paket->downRef();
}

template<class D>
void reflex::UpdateDaemon<D>::insertPacket(Buffer* paket)
{

#if UPDATEDEBUG==1
	getApplication().led.blink(Led::GREEN);
	getApplication().debugOut.write("r");
	getApplication().debugOut.write(currentCommand.insertCommand.number);
	getApplication().debugOut.write("/");
#endif

	unsigned pos = currentCommand.insertCommand.number / 8;
	char bit = 1 << (currentCommand.insertCommand.number % 8);

	Block current;
	paket->read(current);
	paket->downRef();

	//if not allready got this data
	if (!(bitfield[pos] & bit)) {

		if (MemoryDevice->write(
				(caddr_t) & current,
				getApplication().sections.SpaceforUpdateImg[0]
						+ currentCommand.insertCommand.number * UpdateBlockSize,
				UpdateBlockSize)) {

#if UPDATEDEBUG==1
			getApplication().debugOut.write("w");
#endif
			count++;
			bitfield[pos] |= bit;
		} else {

			return;
		}

		//block already received
	}

#if UPDATEDEBUG==1
	getApplication().debugOut.write("h");
	getApplication().debugOut.write(count);
	getApplication().debugOut.write("==");
	getApplication().debugOut.flush();
#endif

	if (count > currentCommand.insertCommand.overallSize) {

#if UPDATEDEBUG==1
		getApplication().debugOut.write("Got ALL");
		getApplication().debugOut.writeln();

#endif

		state = REBIRTH; //install is one of the next actions

		if (mode == Command::INSTALL)
		{
			rebirth();
		} else if ((mode == Command::PROPAGATE) || (mode == Command::PROPAGATE_WAIT))
		{
			startUpdate(0);
		} else if (mode == Command::WAIT)
		{
			return; //nothing to do here
		}

	}

}

template<class D>
void reflex::UpdateDaemon<D>::rebirth()
{
	if (state != REBIRTH) {
		return;
	}

	getSystem().status.version = newVersion;
	getSystem().status.save();

	getApplication().led.turnOff(Led::ALL);
	MemoryDevice->move(getApplication().sections.SpaceforUpdateImg[0],
				getApplication().sections.UpdateAble[0],
				ImageOverallSize * UpdateBlockSize);
}

//#endif
