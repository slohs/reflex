#ifndef UPDATEEXEC_H
#define UPDATEEXEC_H

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	UpdateExec
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	Executes the update commands locally
 *
 *	(c) Carsten Schulze, BTU Cottbus 2006
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/update/Command.h"

namespace reflex {
/**
 * Initialise and controls the Update
 *
 * @author Carsten Schulze
  
 */
class UpdateExec
{
public:
	UpdateExec();

	/**
	 * Executes an array of coammnds
	 * @param comList pointer to the array of commands executed
	 * @param countComs number of commands in the list
	 * @param sec stuct with information of the memory sections
	 */
	void exec(Command* comList,unsigned countComs, Sections* sec);

	/**
	 * Executes a single update command
	 * @param com command to be executed
	 */
	void executeCommand(Command* com);

private :

	/**
	 * execute a insert-command: inserts data at a given position
	 * It tests if the position is able to be updated.
	 * Data which should be stored in a from update protected area,
	 * will not be stored.
	 * @see Command
	 * @param dest where the bytes are stored
	 * @param data position of the data
	 * @param size size of the datafield to insert
	 */
	void execInsert(caddr_t dest,caddr_t data, uint8 size);

	/**
	 * !Warning: NOT TESTED OR USED SO FAR
	 * Exectutes a copy-command: just copies data from one region to another
	 * To use the function the method UpdateFunctions::copy
	 * has to be save to copy overlapping datafields
	 * @see Command
	 * @param dest position, where the data should be copied to
	 * @param data position to the data to copy
	 * @param size size of the datafield to copy
	 */
	void execCopy(caddr_t dest, caddr_t data, size_t size);

	/**
	 * !WARNING : NOT USED OR TESTED.
	 * To use the function the method UpdateFunctions::copy
	 * has to be save to copy overlapping datafields
	 * see disciption of Command::PackCom
	 * @see Command
	 * @param startpos position, where the command is related to
	 * @param repTrip field of Command::LZTriple, to evaluate
	 * @param countTrip number of triplets
	 */
	void execPack(caddr_t startpos,Command::LZTriple* repTrip, unsigned char countTrip);

	/// pointer to a struct, which stores information about memory locations
	Sections* sections;
};

} //reflex

#endif


