#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):  Communicator
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	Communicator for the Update. It says with
 *                 commands arrived and also sends them
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/io/OutputChannel.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/update/Command.h"
#include "reflex/sinks/Sink.h"
#include "reflex/update/Updatefunctions.h"
#include "reflex/update/StatusMem.h"

namespace reflex {

/**
 *
 * @author Carsten Schulze
  
 */
class Communicator :
public Sink0,
public Sink1<char>,
public Activity
{
public:
	/**
	* Here is the initalization
	*/


	Communicator(Sink0* sender, Sink1<Command*>* receiver, OutputChannel* output);

	void init();

	virtual void assign(char rec);

	virtual void notify();

	virtual void run();
	/**
	 *  with busy waiting
	 */
	void sendCommand(Command* com);


	enum {
		PSEUDOMACSLOTS = 31, // you can only choose (squares -1)
	};

	void pseudoMacWait(uint16 msPSlotSize);

	void insertCheckSum(Command* com,unsigned char checksum);

private :

	//  waits the default time, witch is needed to know, if anything is receiving
	bool somebodySending();

	void insertStartBytes(Command* com);



	Sink0* sender;

	Sink1<Command*>* receiver;

	 /**
	  * PREAMBLESIZE : if 0 no preamble is send. But if bytewise receiving it is necessary to now where to start
	  */



	//receiving

	enum RECEIVESTATUS {
		NUMBER1 = 1,
		NUMBER2 = 2,
		ID = 3,
		PARALEN = 4,
		PARA = 5
	} recstatus;

	unsigned char recCount;
	unsigned char lenofCurrent;

	OutputChannel* output;

	Command comRec;
	Command* comSend;
	unsigned resendCount;
	volatile unsigned listendpos;
	volatile unsigned liststart;
	volatile bool somebodyActive;

	/**
	 * sending some startbytes.. sometimes it is neseccary to find
	 * the start first, because something is received but it is
	 * not clear where it beloangs to.
	 */
	char *startBytes;
};

} //reflex
#endif


