#ifndef Command_h
#define Command_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):  Command
 *
 *	Author:		Carsten Schulze, Karsten Walther
 *
 *	Description:  Base datastructure for update commands.
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"

namespace reflex {

struct Command
{
	enum COMMANDS {
		UNDEFINED = 0,
		START = 1,
		INIT = 2,
		INSERT = 3,
		REBIRTH = 4
	};

	enum MODE {
		INSTALL = 0,
		PROPAGATE = 1,
		WAIT = 2,
		PROPAGATE_WAIT = 3
	};

	struct StartCommand {
		unsigned nodeID;
		uint8 targetMode;
	};

	struct InitCommand {
		uint8 version;
		uint8 blockSize;
		int16 overallSize;
	    uint8 targetMode;
	};

	struct InsertCommand {
		size_t offset;
	    uint16 overallSize;
	    uint16 number;
	    //uint16 checksum;
	};

	struct RebirthCommand {
		uint8 doIt;

	};

	uint8 commandID;
	uint8 version;
	union {

		StartCommand startCommand;
		InitCommand initCommand;
		InsertCommand insertCommand;
		RebirthCommand rebirthCommand;
	};
};

} //reflex
#endif

