/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	EDFActivity
 *
 *	Author:		Karsten Walther
 *
 *	Description:	An EDF-schedulable passive objects
 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as 
 *    published by the Free Software Foundation, either version 3 of the 
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */

#ifndef ProtoThread_h
#define ProtoThread_h

#include "LocalContinuation.h"

namespace reflex {

#define PT_WAITING					0
#define PT_YIELDED					1
#define PT_YIELDED_SCHEDULE				2
#define PT_EXITED					3
#define PT_ENDED					4

/**
 * Initialize a protothread.
 *
 * Initializes a protothread. Initialization must be done prior to
 * starting to execute the protothread.
 *
 * \param pt A pointer to the protothread control structure.
 *
 * \sa PT_SPAWN()
 *
 */
#define PT_INIT(lc)   LC_INIT(lc)

/**
 * Declare the start of a protothread inside the C++ function
 * implementing the protothread.
 *
 * This macro is used to declare the starting point of a
 * protothread. It should be placed at the start of the function in
 * which the protothread runs. All C++ statements above the PT_BEGIN()
 * invokation will be executed each time the protothread is scheduled.
 */
#define PT_BEGIN    { LC_RESUME(lc)

/**
 * Declare the end of a protothread.
 *
 * This macro is used for declaring that a protothread ends. It must
 * always be used together with a matching PT_BEGIN() macro.
 */
#define PT_END	LC_END(lc); \
		PT_INIT(lc); \
		setStatus(PT_ENDED); \
		return; }

/**
 * Exit the protothread.
 *
 * This macro causes the protothread to exit. If the protothread was
 * spawned by another protothread, the parent protothread will become
 * unblocked and can continue to run.
 *
 */
#define PT_EXIT	\
    do { \
		PT_INIT(lc); \
		setStatus(PT_EXITED); \
		return; \
    } while(0)

/**
 * Restart the protothread.
 *
 * This macro will block and cause the running protothread to restart
 * its execution at the place of the PT_BEGIN() call.
 *
 */
#define PT_RESTART  \
    do { \
		PT_INIT(lc); \
		setStatus(PT_WAITING); \
		return; \
    } while(0)

/**
 * Restart the protothread.
 *
 * This macro will block and cause the running protothread to restart
 * its execution at the place of the PT_BEGIN() call.
 *
 */
#define PT_RESTART_SCHEDULE  \
    do { \
		PT_INIT(lc); \
		if(!isSpawned()) trigger(); \
		setStatus(PT_WAITING); \
		return; \
    } while(0)

/**
 * Yield from the current protothread.
 *
 * This function will yield the protothread, thereby allowing other
 * processing to take place in the system.
 *
 */
#define PT_YIELD	\
    do { \
		LC_SET(lc); \
		if(getStatus() != PT_YIELDED) { \
			setStatus(PT_YIELDED); \
			return; \
		} else setStatus(PT_WAITING); \
    } while(0)

/**
 * Yield from the current protothread.
 *
 * This function will yield the protothread, thereby allowing other
 * processing to take place in the system.
 * The protothread schedule itself before it yields.
 *
 */
#define PT_YIELD_SCHEDULE	\
    do { \
		LC_SET(lc); \
		if(getStatus() != PT_YIELDED_SCHEDULE) { \
			if(!isSpawned()) trigger(); \
			setStatus(PT_YIELDED_SCHEDULE); \
			return; \
		} else setStatus(PT_WAITING); \
    } while(0)

/**
 * Block and wait until condition is true.
 *
 * This macro blocks the protothread until the specified condition is
 * true.
 *
 * \param condition The condition.
 *
 */
#define PT_WAIT_UNTIL(condition)    \
	do { \
		LC_SET(lc); \
		if(!(condition)) { \
			setStatus(PT_WAITING); \
			return; \
		} \
	} while(0)

/**
 * Block and wait until condition is false.
 *
 * This macro blocks the protothread until the specified condition is
 * true.
 *
 * \param condition The condition.
 *
 */
#define PT_WAIT_WHILE(condition) PT_WAIT_UNTIL(!condition)

/**
 * Block and wait until condition is true.
 *
 * This macro blocks the protothread until the specified condition is
 * true.
 *
 * \param condition The condition.
 *
 */
#define PT_WAIT_UNTIL_SCHEDULE(condition)    \
	do { \
		LC_SET(lc); \
		if(!(condition)) { \
			if(!isSpawned()) trigger(); \
			setStatus(PT_WAITING); \
			return; \
		} \
	} while(0)

/**
 * Block and wait until condition is false.
 *
 * This macro blocks the protothread until the specified condition is
 * true.
 *
 * \param condition The condition.
 *
 */
#define PT_WAIT_WHILE_SCHEDULE(condition) PT_WAIT_UNTIL_SCHEDULE(!condition)

/**
 * Spawn a child protothread and wait until it exits.
 *
 * This macro spawns a child protothread and waits until it exits. The
 * macro can only be used within a protothread.
 *
 * \param object The child protothread
 * \param method Method of child protothread
 *
 */
#define PT_SPAWN(lc_spawn, method)    \
	do { \
		lc_t lc_temp; \
		PT_INIT(lc_spawn); \
		LC_SET(lc_temp); \
		lc = lc_spawn; \
		(method); \
		if(getStatus() < PT_EXITED) { \
			lc_spawn = lc; \
			lc = lc_temp; \
			return; \
		} \
	} while(0)


/**
 * Spawn a child protothread and wait until it exits.
 *
 * This macro spawns a child protothread and waits until it exits. The
 * macro can only be used within a protothread.
 *
 * \param object The child protothread
 * \param method Method of child protothread
 *
 */
#define PT_SPAWN_OBJECT(object, method)    \
	do { \
		object->setSpawned(1); \
		LC_SET(lc); \
		(method); \
		if(object->getStatus() < PT_EXITED) { \
			if(!isSpawned()) trigger(); \
			return; \
    		} \
		setStatus(PT_WAITING); \
		object->setSpawned(0); \
	} while(0);


class ProtoThread {

    public:
   
		ProtoThread() {
			pt_status = PT_WAITING;
			pt_spawn = false;
			PT_INIT(lc);
		};

		unsigned char getStatus() {
			return pt_status;
		}

		void setStatus(unsigned char status) {
			pt_status = status;
		}
		
		bool isSpawned() {
			return pt_spawn;
		}

		void setSpawned(bool spawn) {
			pt_spawn = spawn;
		}

	protected:
		lc_t lc;

	private:

		unsigned char pt_status;
		bool pt_spawn;
	
};


} //namespace reflex

#endif /* ProtoThread_h */

