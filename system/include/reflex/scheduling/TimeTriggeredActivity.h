#ifndef TimeTriggeredActivity_h
#define TimeTriggeredActivity_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	TimeTriggeredActivity
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Activity implementation for timetriggered action.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/types.h"
#include "reflex/lib/TList.h"

namespace reflex {

/** This class is the base class for activities in time-triggered systems.
 *  After instantiation the activity each slot the activity should use must
 *  be reserved. This is done by calling registerActivity on the scheduler.
 */
class TimeTriggeredActivity {
public:
	TimeTriggeredActivity();

	/** The run method is called by the scheduler and implements the
	 *  functionality of the activity.
	 */
	virtual void run()=0;

	/** This method brings this activity into scheduling
	 */
	void trigger()
	{
		rescheduleCount++;
	}

	/** This prevents activity to be activated, scheduling is still possible
	 *  If the activity is already scheduled but not activated, the effect
	 *  is delayed until activity is completed the next time.
	 */
	void lock()
	{
		locked=true;
	}

	/** Notification that the activity can be activated again. If there were
	 *  scheduling request during lock time, the activity is scheduled now.
	 */
	void unlock();

	bool islocked() const {return locked;}

	/**
	 * @return	the count this activity was triggered
	*/
	unsigned char triggered() {return rescheduleCount;}

private:
	/** The friend declaration allows direct manipulation of activity members
	 *  only at defined points.
	 */
	friend class TimeTriggeredScheduler;
	friend class TList<TimeTriggeredActivity>;
	friend class NodeConfiguration;

	/** next is needed by TList<> (schedulerlist) for chaining.
	 */
	TimeTriggeredActivity* next;

	/** This flag determines if an activity can be putted on the readyList.
	 */
	bool locked;

	/** This count determines how often the activity must be activated
	 *  currently.
	 */
	unsigned char rescheduleCount;

};

} //namespace reflex

#endif
