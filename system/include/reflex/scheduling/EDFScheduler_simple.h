#ifndef EDFSchedulerSimple_h
#define EDFSchedulerSimple_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	EDFScheduler
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Scheduler for a preemptive deadline driven activities.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/scheduling/EDFActivity.h"
#include "reflex/lib/TList.h"

namespace reflex {
/**
 *  The scheduler implements a general preemptive priority based
 *  scheme. Furthermore it is highly reactive, while non-fixed-time
 *  schedulingtasks are done with interrupts enabled.
 *  It has 2 lists in the toSchedule list the activities for scheduling
 *  are inserted in a constant amount of time with disabled interrupts.
 *  During insertion of ordered insertion of activities into the
 *  readyList the interrupts are on, for higher reactivity. Therefore
 *  the monitor is needed for protection of scheduling.
 */
class EDFScheduler
{
public:
	EDFScheduler()
	{
		stackedSchedules=0;
	}

	/** This method starts the system, and will never return.
	 */
	void start();

private:
	/** For preventing invalid use only Activity and InterruptGuardian are
	 *  allowed access of Scheduler.
	 */
	friend class EDFActivity;
	friend class InterruptGuardian;

	/**
	 *  Schedules an activity for execution.
	 *	For getting current priority of activity, the prepareSchedulin
	 *  method of activity is called.
	 *  Note if priority is higher than that of the running activity
	 *	preemption is done.
	 *
	 *  @param act pointer to activity
	 */
	void schedule(EDFActivity* act);

	/** This method allows to set an activity schedulable again. It checks
	 *  if the activity must be scheduled after removing lock.
	 *  Mainly used for interrupt driven output devices.
	 *
	 *  @param act pointer to unlocked activity
	 */
	void unlock(EDFActivity* act);

	/**
	 *  Method which dispatches the cpu to activities, and
	 *  starts rescheduling if needed.
	 */
	void dispatch();

	/**
	 *  This non-blocking function reserves the scheduling monitor.
	 *	While it is called by InterruptGuardian it is public.
	 */
	void enterScheduling();

	/**
	 *  This starts scheduling if possible and frees the monitor.
	 *  (non-blocking)
	 *	While it is called by InterruptGuardian it is public.
	 */
	void leaveScheduling();

	/**
	 *  This is the monitorvariable, if it is 0 the monitor is free.
	 */
	unsigned char stackedSchedules;

	/**
	 *  List of activities ready for dispatching
	 */
	TList<EDFActivity> readyList;

	/** Counts locked components, is needed for powermanagement.
	 **/
	char lockCount;
};

} //namespace reflex

#endif

