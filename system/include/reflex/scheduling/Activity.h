#ifndef Activity_h
#define Activity_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	none
 *
 *	Author:		Karsten Walther
 *
 *	Description:	Provides typedefinition for the MetaType Activity
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

/* This file includes the Activity needed by the choosen scheduling scheme.
 *	All activities must implement following signature.
 *
 *  ActivityType()	//standard constructor
 *	void trigger()	//brings the activity into scheduling
 *  virtual void run()=0 //for the derived objects
 *  void lock()		//preserve activity from activation
 *  void unlock()	//reallow activation
 *
 *	this signature check can be done by compiling with -DCHECKS_ON
 */

#ifdef FIFO_SCHEDULING

#include "reflex/scheduling/FifoActivity.h"
namespace reflex {
	typedef FifoActivity Activity;
} //namespace reflex

#endif



//Activities for the different versions of the priority scheduler do not differ
#if defined PRIORITY_SCHEDULING || defined PRIORITY_SCHEDULING_SIMPLE || defined PRIORITY_SCHEDULING_NONPREEMPTIVE

#include "reflex/scheduling/PriorityActivity.h"
namespace reflex {
	typedef PriorityActivity Activity;
} //namespace reflex

#endif



//Activities for the different versions of the EDF scheduler do not differ
#if defined EDF_SCHEDULING || defined EDF_SCHEDULING_SIMPLE || defined EDF_SCHEDULING_NONPREEMPTIVE

#include "reflex/scheduling/EDFActivity.h"
namespace reflex {
	typedef EDFActivity Activity;
} //namespace reflex

#endif



#ifdef TIME_TRIGGERED_SCHEDULING

#include "reflex/scheduling/TimeTriggeredActivity.h"
namespace reflex {
	typedef TimeTriggeredActivity Activity;
} //namespace reflex

#endif

#endif

