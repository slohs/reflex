#ifndef EDFScheduler_h
#define EDFScheduler_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Scheduler
 *
 *	Author:		Karsten Walther
 *
 *	Description: A preemptive EDF scheduler with bounded interrupt blocking
 *               times.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/scheduling/EDFActivity.h"
#include "reflex/lib/TList.h"

namespace reflex {
/**
 *  The scheduler implements a general preemptive priority based
 *  scheme. Furthermore it is highly reactive, while non-fixed-time
 *  schedulingtasks are done with interrupts enabled.
 *  It has 2 lists in the toSchedule list the activities for scheduling
 *  are inserted in a constant amount of time with disabled interrupts.
 *  During insertion of ordered insertion of activities into the
 *  readyList the interrupts are on, for higher reactivity. Therefore
 *  the monitor is needed for protection of scheduling.
 */
class EDFScheduler
{
public:
	EDFScheduler()
	{
		stackedSchedules=0;
	}

	void start();

	/** This method is for synchronization of concurrent scheduling request.
	 *  application <-> interrupt and interrupt <-> interrupt
	 */
	void enterScheduling();

	/** This method opens a dispatch frame, if there is no concurrent schedule
	 *  request.
	 */
	void leaveScheduling();

private:
	/** For preventing invalid use only Activity and InterruptGuardian are
	 *  allowed access of Scheduler.
	 */
	friend class InterruptGuardian;
	friend class EDFActivity;

	/** This method is for scheduling an activity, it is called in trigger
	 *  of activity.
	 */
	void schedule(EDFActivity* act);

	/** Puts the activity onto readyList if needed.
	 */
	void unlock(EDFActivity* act);

	/** This method activates non-interrupted activities in front of the
	 *  readyList. It is reentrant, and has a stackframe for each preemption
	 */
	void dispatch();

	/** This method inserts the activities from toSchedule list into
	 *  the readyList with interrupts enabled.
	 */
	void updateSchedule();

	/** Variable for determining (counting) concurrent schedules.
	 */
	unsigned char stackedSchedules;

	/** The sorted list of activities which can be activated.
	 */
	TList<EDFActivity> readyList;

	/** The unsorted list, which contains activities which should be activated.
	 * 	Insertion takes a constant amount of time.
	 */
	TList<EDFActivity> toSchedule;

	/** Counts locked components, is needed for powermanagement.
	 **/
	char lockCount;
};

} //namespace reflex

#endif

