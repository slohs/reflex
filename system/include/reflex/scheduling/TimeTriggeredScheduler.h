#ifndef TimeTriggeredScheduler_h
#define TimeTriggeredScheduler_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	TimeTriggeredScheduler
 *
 *	Author:		Karsten Walther, Stefan Nuernberger
 *
 *	Description:	Scheduler for time-triggered systems
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/scheduling/TimeTriggeredActivity.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/lib/TList.h"
#include "conf.h"

namespace reflex {

/** Implements a slot based scheme with one activity per slot, if no activity
 *  is running and slot of activity is reached it is started. A slot is
 *  reserved for an activity by calling registerActivity. Several Activities
 *  in different slots may have the same start time. Those slots will be
 *  executed in the order of registration.
 */
class TimeTriggeredScheduler
{
public:
	TimeTriggeredScheduler();

	void start();

//private:
	/** For preventing invalid use only Activity is
	 *  allowed access of Scheduler.
	 */
	friend class TimeTriggeredActivity;
	friend class NodeConfiguration;

	/** This method is for scheduling an activity, it is called in trigger
	 *  of activity.
	 *
	 *  @param act pointer to the activity
	 */
	void schedule(TimeTriggeredActivity* act)
	{
		act->rescheduleCount++;
		scheduleCount++;
	}

	/** This method allows to set an activity schedulable again. It checks
	 *  if the activity must be scheduled after removing lock.
	 *  Mainly used for interrupt driven output devices.
	 *
	 *  @param act pointer to unlocked activity
	 */
	void unlock(TimeTriggeredActivity* act)
	{
		act->locked = false;
	}

	/** method for assigning a slot to an activity
	 */
	void registerActivity(TimeTriggeredActivity* act, Time startTime);

	/** private struct for realizing non-intrusive readyList
	 */
	struct Frame{
		Frame* next;
		TimeTriggeredActivity* act;
		Time startTime;

		bool lessEqual(Frame& right)
   		{
   			return (startTime <= right.startTime);
		}
	};

	/**
	 *  List of activities ready for dispatching
	 */
	TList<Frame> list;

	/** Buffer for scheduling frames
	 */
	Frame frames[NrOfSchedulingSlots];

	/** occupied frames
	 */
	unsigned int frameCount;

	/** next activity to run
	 */
	Frame* current;

	/** Counts locked components, is needed for powermanagement.
	 */
	unsigned scheduleCount;

	/** time of last scheduler call
	  */
	Time last;

	/** Wake up timer for scheduling notification.
	 *  It simply generates a hardware interrupt when needed to leave powerDown() mode.
	 */
	VirtualTimer wake;
};

} //namespace reflex

#endif
