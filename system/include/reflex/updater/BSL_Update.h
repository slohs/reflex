#ifndef BSL_UPDATE_H_
#define BSL_UPDATE_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Author:		 Hannes Menzel
 *
 *	Description: This header provides the basic function to realize the
 *	the wireless update feature. BSL_Update.h represents only the
 *	platform independent part.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 *
 */


#include "reflex/types.h"
#include "reflex/updater/UpdateTypes.h"
#include "reflex/updateDriver/RadioConfig.h"

#ifndef NODEID
#error: define an unique NODEID macro in Makefile to ensure that your node is configured correctly. You also can remove wireless update to avoid this error!
#endif

#define USING_RESISTENT_BITARRAY
/**
 * gcc designated syntax for array initalisation doesnt work for
 * g++ thats why we use marcos instead of
 */
#define BITARRAY_VALUE_1(x) x
#define BITARRAY_VALUE_2(x) BITARRAY_VALUE_1(x), BITARRAY_VALUE_1(x)
#define BITARRAY_VALUE_4(x) BITARRAY_VALUE_2(x), BITARRAY_VALUE_2(x)
#define BITARRAY_VALUE_8(x) BITARRAY_VALUE_4(x), BITARRAY_VALUE_4(x)
#define BITARRAY_VALUE_16(x) BITARRAY_VALUE_8(x), BITARRAY_VALUE_8(x)
#define BITARRAY_VALUE_32(x) BITARRAY_VALUE_16(x), BITARRAY_VALUE_16(x)
#define BITARRAY_VALUE_64(x) BITARRAY_VALUE_32(x), BITARRAY_VALUE_32(x)
#define BITARRAY_VALUE_128(x) BITARRAY_VALUE_64(x), BITARRAY_VALUE_64(x)
#define BITARRAY_VALUE_256(x) BITARRAY_VALUE_128(x), BITARRAY_VALUE_128(x)

// CC430F6137,
#if MCU == cc430x6137 || MCU == cc430x6127 || MCU == cc430x5137
	#define HIGHEST_WRITABLE_ADDRESS 	0xFFFF
	#define RESET_VECTOR				0xFFFE
//adding all other chips
#endif






/**
 * this procedure marks the point where the update code resides and is not
 * platform or controller specific
 */
void bslStart() __attribute__((section(".firstUpdate")));

/**
 * workaround for strange stack behaviour
 */
bool test() __attribute__((section(".updateCode")));


/**
 * a simple delay function
 */
static void __inline__  __delay_cycles(register unsigned int n) __attribute__((section(".updateCode")));





////////////////////////////////////////////////////////////////////////////////////////////////////////
// radio driver declarations
/////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * reset the radio
 */
void u_reset_radio() __attribute__((section(".updateCode")));

/**
 * configures the radio
 * @param radioCfg pointer to a 2-D array containing pairs of addresses of configuration registers and values, which
 * have to be write into
 * @param pairs number of pairs
 * @param channel the used channel when update packet was received in none-update phase
 *
 * @return 0 -> configuration failed, else configuration was successful
 */
uint8 u_radio_config(uint8 radioCfg[][2], uint8 pairs, uint8 channel) __attribute__((section(".updateCode")));


/**
 * send a commando to the radio
 * @param strobe sends a commando to the radio
 * @return status byte (@see radio driver) to indicate if operation was successful. 0 => operation definitly fails
 */
uint8 u_radio_strobe(uint8 strobe) __attribute__((section(".updateCode")));


/**
 * transmit data via radio
 * @param data pointer to the data
 * @param len size of the data array
 *
 * @return the transmitted data
 */
uint8 u_radio_sendData(uint8* data, uint8 len) __attribute__((section(".updateCode")));


/**
 * reads data form radio core into the buffer buf
 *
 * @param buf the buffer, where the data should be stored
 * @param len the length of the buffer
 *
 *
 * @return size of the transferred data
 */
uint8 u_radio_receiveData(uint8* buf, uint8 len/*, uint16& statusByte_1_2*/)  __attribute__((section(".updateCode")));



/**
 * switch the radio module on
 */
void u_radio_receive_on() __attribute__((section(".updateCode")));




////////////////////////////////////////////////////////////////////////////////////////////////////////
// cpu driver declarations
////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
* disable all interrupts, setting, timer, disable wdt, setting frequency etc.
*/
void u_cpu_init(void) __attribute__((section(".updateCode")));





////////////////////////////////////////////////////////////////////////////////////////////////////////
// flash driver declarations
////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * erases flash memory
 */
void u_erase_flash(uint8* startAddr, uint16 size) __attribute__((section(".updateCode")));

/**
 * writes len bytes into the flash
 */
void u_write_flashBytes(uint8* src, uint8* dst, uint16 len) __attribute__((section(".updateCode")));



/**
 * this structure contains all data needed for the update process and have to be
 * linked into a special section of the memory
 */
struct PersistentUpdateMemory persistentUpdateMem __attribute__((section(".persistent_update_memory_section"))) = {0xFF, 0xFF, NODEID, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
		{
		  /* internal radio configuration */
		  {  IOCFG0,    MRFI_SETTING_IOCFG0       }, /* Configure GDO_0 to output PA_PD signal (low during TX, high otherwise). */
		  {  IOCFG1,    MRFI_SETTING_IOCFG1       }, /* Configure GDO_1 to output RSSI_VALID signal (high when RSSI is valid, low otherwise). */
		  {  MCSM1,     MRFI_SETTING_MCSM1        }, /* CCA mode, RX_OFF_MODE and TX_OFF_MODE */
		  {  MCSM0,     MRFI_SETTING_MCSM0        }, /* AUTO_CAL and XOSC state in sleep */
		  {  PKTLEN,    MRFI_SETTING_PKTLEN       },
		  {  PKTCTRL0,  MRFI_SETTING_PKTCTRL0     },
		  {  PKTCTRL1,  MRFI_SETTING_PKTCTRL1	  },/* added by menzehan*/
		  {  FIFOTHR,   MRFI_SETTING_FIFOTHR      },
		  {  SYNC1,     0xD3                      },/* added by menzehan*/
		  {  SYNC0,     0x91                      },/* added by menzehan*/
		/* imported SmartRF radio configuration */
		  {  FSCTRL1,   SMARTRF_SETTING_FSCTRL1   },
		  {  FSCTRL0,   SMARTRF_SETTING_FSCTRL0   },
		  {  FREQ2,     SMARTRF_SETTING_FREQ2     },
		  {  FREQ1,     SMARTRF_SETTING_FREQ1     },
		  {  FREQ0,     SMARTRF_SETTING_FREQ0     },
		  {  MDMCFG4,   SMARTRF_SETTING_MDMCFG4   },
		  {  MDMCFG3,   SMARTRF_SETTING_MDMCFG3   },
		  {  MDMCFG2,   SMARTRF_SETTING_MDMCFG2   },
		  {  MDMCFG1,   SMARTRF_SETTING_MDMCFG1   },
		  {  MDMCFG0,   SMARTRF_SETTING_MDMCFG0   },
		  {  DEVIATN,   SMARTRF_SETTING_DEVIATN   },
		  {  FOCCFG,    SMARTRF_SETTING_FOCCFG    },
		  {  BSCFG,     SMARTRF_SETTING_BSCFG     },
		  {  AGCCTRL2,  SMARTRF_SETTING_AGCCTRL2  },
		  {  AGCCTRL1,  SMARTRF_SETTING_AGCCTRL1  },
		  {  AGCCTRL0,  SMARTRF_SETTING_AGCCTRL0  },
		  {  FREND1,    SMARTRF_SETTING_FREND1    },
		  {  FREND0,    SMARTRF_SETTING_FREND0    },
		  {  FSCAL3,    SMARTRF_SETTING_FSCAL3    },
		  {  FSCAL2,    SMARTRF_SETTING_FSCAL2    },
		  {  FSCAL1,    SMARTRF_SETTING_FSCAL1    },
		  {  FSCAL0,    SMARTRF_SETTING_FSCAL0    },
		  {  TEST2,     SMARTRF_SETTING_TEST2     },
		  {  TEST1,     SMARTRF_SETTING_TEST1     },
		  {  TEST0,     SMARTRF_SETTING_TEST0     },
		}
//		   BitArray initialization => 205 bytes
		,{ 	  BITARRAY_VALUE_128(0xff)
			, BITARRAY_VALUE_64(0xff)
			, BITARRAY_VALUE_8(0xff)
			, BITARRAY_VALUE_4(0xff)
			, BITARRAY_VALUE_1(0xff)}};


#endif /* BSL_UPDATE_H_ */
