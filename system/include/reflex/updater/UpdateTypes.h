#ifndef UPDATETYPES_H_
#define UPDATETYPES_H_

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Author:		 Hannes Menzel
 *
 *	Description: add here your description
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

/**
 * @TODO: delete this macro
 * expected payload in every data packet
 */
//#define PAYLOAD_SIZE 54
#define PAYLOAD_SIZE 20

// expected size of program image -
// @TODO possible to reduce!!!
#define PROGRAM_IMAGE_SIZE 32768

// size of bit array that stores the information about successful written data
#define BITARRAY_SIZE (PROGRAM_IMAGE_SIZE / PAYLOAD_SIZE / 8)

///////////////////////////////////
// definition of the packet types
///////////////////////////////////
enum packetTypes
{
	 APPLICATION 		=	0x00
	,UPDATE_INIT 		=	0x01
	,UPDATE_META_INFO 	= 	0x02
	,UPDATE_DATA		=	0x03
	,UPDATE_GETIMAGE	=	0x04
};


///////////////////////////////////
// other masks & shifts
///////////////////////////////////
#define SEQ_NO_MASK 	0x1FFF
#define RUN_MASK		0xC000

#define SEQ_NO_SHIFT	0x0
#define RUN_SHIFT		0xE


///////////////////////////////////////////////////
/*
 * @TODO: all structs have to be optimized
 */
//////////////////////////////////////////////////


/**
 * the header is part of every update packet
 */
struct UpdateHeader
{
	/**
	 * packet type
	 */
	uint8 type;

	/**
	 * programm ID
	 */
	uint8 programID;

	/**
	 * version
	 */
	uint8 version;

	/**
	 * is used a padding byte or can be used for additional information
	 */
	uint8 reserved;

	/**
	 * @return the program ID
	 */
	inline uint8 getProgramID()
	{
		return programID >> 2;
	}


	inline uint8 getRes()
	{
		return (programID >> 1) & 0x1;
	}

	inline uint8 getOverride()
	{
		return programID & 0x1;
	}

	/**
	 * @param progID encodes the program id into updateHeader
	 * @param res encodes the ... into updateHeader
	 * @param override encodes the override flag into updateHeader
	 */
	inline void setProgramID(uint8 progID, uint8 res, uint8 override)
	{
		programID = (progID << 2) | (res << 1) | override;
	}

};


/**
 * this kind of packet is being received by the high-level radio driver in reflex and
 * is needed to decide whether an update has to be triggered
 */
struct UpdateInitPacket : public UpdateHeader
{

	/**
	 * stores the needed hops, until the packet is received
	 */
	uint8 hopCount;

	/**
	 * stores the estimated time until the updating is accomplished
	 */
	uint8 updateDelay;

	/**
	 * @TODO: comment
	 */
	uint8 hopDelayOffset;

	/**
	 * represents the diameter of the network
	 */
	uint8 networkDiameter;

};

/**
 * a packet, that contains information about the update process
 */
struct UpdateMetaInfoPacket : public UpdateHeader
{
	/**
	 * @TODO: ... waiting for explanation
	 */
	uint8 probability;

	/**
	 * number of data bytes in the last packet
	 */
	uint8 remainder;

	/**
	 * number of expected packet and the expected transmission cycles
	 */
	uint16 packetCount_run;

};


/**
 * a packet, that contains parts of the program image
 */
struct UpdateDataPacket : public UpdateHeader
{
	/**
	 * sequenzNo and run (transmission cycle no)
	 */
	uint16 seqNo_run;


	/**
	 * size of the payload
	 */
	uint8 data[PAYLOAD_SIZE];

};

/**
 * this structure contains all data needed for the update process and have to be
 * linked into a special section of the memory
 */
struct PersistentUpdateMemory
{

	/**
	 * flag, that indicates whether the memory was deleted or not
	 * per default = 0x0 -> while initial reset update process is not started
	 */
	//uint8 deleted;

	/**
	 * must be the first byte in the image
	 *
	 * shows whether the whole image was received
	 */
	uint8 receivedCompleteImage;

	/**
	 * stores the current used channel for update
	 */
	uint8 channel;

	/**
	 * the ID of the node
	 */
	uint8 nodeID;

	/**
	 * shows whether the image has to be forwarded to other nodes
	 */
	uint8 forwardMe;

	/**
	 * identifies the current running program
	 */
	uint8 programID;

	/**
	 * the current version of the image
	 */
	uint8 version;

	/**
	 * a counter for the received packets
	 */
	uint16 packetCount;

	/**
	 * stores the needed hops, until the packet is received
	 */
	uint8 hopCount;

	/**
	 * stores the estimated time until the updating is accomplished
	 */
	uint8 updateDelay;

	/**
	 * needed to decide if a the received image have to be forwarded
	 */
	uint8 probability;

	/**
	 *
	 */
	uint8 hopDelayOffset;

	/**
	 * represents the diameter of the network
	 */
	uint8 networkDiameter;


	/**
	 * @TODO
	 */
	uint8 wait;

	//uint16

	/**
	 * this array contains the radio configuration consisting of pairs of configuration register + value
	 */
	uint8 mrfiRadioCfg[35][2];

	/**
	 * every bit represents a correct transmitted packet of the whole
	 * image
	 * assumption:
	 * 	32KB program image
	 *  56 Byte data payload in every packet
	 */
	uint8 bitArray[BITARRAY_SIZE + 1];
};


/**
 * somewhere in project the update memory location must be declared
 */
extern PersistentUpdateMemory persistentUpdateMem;


#endif /* UPDATETYPES_H_ */
