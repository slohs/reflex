class NodeConfiguration : public System {
public:
	NodeConfiguration()
	{
		//connect the components
		adc1.connect(converter.input);
		adc2.connect(converter.input);
		input.connect(serial.input);

		//define slots for priority-scheduling
		adc.setPriority(HIGH);
		adc.setPriority(HIGH);
		converter.setPriority(LOW);
		serial.setPriority(LOW);
	}

private:
	//instantiate components
	ADConverter adc1;
	ADConverter adc2;
	Converter converter;
	SerialInterface serial;
};
