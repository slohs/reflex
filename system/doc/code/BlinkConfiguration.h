class NodeConfiguration : public System {
public:
	NodeConfiguration()
	{
		//connecting the components
		timer.connect(a.input);
		a.connect(b.input);

		//setting scheduling metrics
		a.setPriority(0);
		b.setPriority(1);
	}

private:
	//instantiate components
	ComponentA a;
	ComponentB b;
};
