Realtime Event FLow EXecutive
=============================

Reflex is an operating system for deeply embedded control systems and wireless sensor network nodes.
It is based on the event-flow principle, which greatly facilitates implementation of typical applications and reusability of components.

Reflex is being developed at the Brandenburg University of Technology, Distributed Systems / Operating Systems Group.
It is Free Libre Open Source Software available under the 2-clause BSD License. See the LICENSE file for details.

For the current status of all active branches, have a look at our Jenkins [Build Server](https://idun.informatik.tu-cottbus.de/jenkins/).
For more Information, visit the [Reflex Website](https://idun.informatik.tu-cottbus.de/reflex/) at BTU Cottbus-Senftenberg.

