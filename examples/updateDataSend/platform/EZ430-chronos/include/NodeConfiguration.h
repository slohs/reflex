#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 Application
 *
 *	Author:		 Karsten Walther
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "conf.h"
#include "reflex/types.h"

#include "reflex/System.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"

#include "reflex/lcd/LCD_B.h"
#include "reflex/io/Display.h"

#include "reflex/io/Buttons.h"

#include "reflex/data_types.h"
#include "reflex/debug/SizeOf.h"
#include "reflex/sys/SYS.h"
#include "reflex/rf/RADIO.h"

//application
#include "UpdateDataSend.h"

namespace reflex {
/**
 * Define different power groups for an application. To use the available groups provided by the powermanager are highly
 * recommended.
 */
enum PowerGroups {
    AWAKE = reflex::PowerManager::GROUP1,
    SLEEP = reflex::PowerManager::GROUP2,
    NOOPGROUP = reflex::PowerManager::NOTHING
};


class NodeConfiguration : public System
{
public:

	NodeConfiguration() 
		: System()
		, pool(0) //the Buffer is only used as FiFo. So stacksize is 0 @see Buffer
	    , radio(pool)
	{
	    //connect buttonHandler with signalquality
	  //  buttonHandler.init(&quality.setupSensornode);

	    //if data are ready to send, then the radio must notified
	    //quality.init(&radio.input, &radio.confData);
		quality.connect_out_sendToRadio(radio.get_in_input());

		//connect the event sinks from application to the source of the driver
	    //radio.init(&quality.receiveDataFromRadio, &quality.sendReady);
		radio.connect_out_data(quality.get_in_receiveDataFromRadio());
		radio.connect_out_sendingSuccessful(quality.get_in_sendReady());


	    //first initialization of radio
		RadioConfiguration cfg;
		cfg.setCCA(true);
		cfg.setLogicalChannel(1);
		cfg.setSendingTOS(false);
		cfg.setSignalAttenuation(0);
		cfg.setWhitening(false);
		cfg.setTransmissionTime(150);
		cfg.setContinuousTransmission(false);
		cfg.performConfiguration(radio. get_in_confData());



	    //buttonHandler.init(&quality.)
	    timer.setGroups(AWAKE | SLEEP); // put system timer in default group
		display.setGroups(AWAKE | SLEEP);
//		buttonGroup.setGroups(AWAKE | SLEEP);
		radio.setGroups( SLEEP );
        powerManager.enableGroup(SLEEP); //this enables all registered entities (obj of type PowerManageAble) which are part of the group SLEEP
        powerManager.disableGroup(AWAKE); //that disables all entities which are part of the group SLEEP and NOT part of group AWAKE.

        //assign buttons
//        buttonGroup[Buttons::IV_M1] = &(buttonHandler.bm1);
//        buttonGroup[Buttons::IV_M2] = &(buttonHandler.bm2);
//        buttonGroup[Buttons::IV_S1] = &(buttonHandler.bs1);
//        buttonGroup[Buttons::IV_S2] = &(buttonHandler.bs2);
//        buttonGroup[Buttons::IV_BL] = &(buttonHandler.bbl);
	}


	void showUpperLCD(uint16 val)
	{
		display.writeUpperHex(val);
	}


    void showLowerLCD(uint16 val)
    {
        display.writeLowerHex(val);
    }


	PoolManager poolManager;


	/**
	 * a pool of bufferobject with static size
	 */
	SizedPool<IOBufferSize, NrOfStdOutBuffers> pool;

	//display for visualization
	Display display;

	/*
	 * the application
	 */
	UpdateDataSend quality;

	//triggers timer events
	VirtualTimer clock;

	//radio driver
	mcu::RADIO radio;

//	//to set all buttons into a power group
//	Buttons buttonGroup;
//
//	//the single buttons
//	ButtonHandling buttonHandler;

};


inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

}//reflex

#endif
