#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Karsten Walther
# */

CC_SOURCES_APPLICATION += \
	src/main.cc \

CC_SOURCES_LIB += \

sinclude $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk
