#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 NodeConfiguration
 *	Author:		 Stefan Nuernberger
 *
 *	Description: Put together all the components for a node.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/types.h"
#include "reflex/System.h"

#include "reflex/usci/USCI_I2C_Master.h"
#include "reflex/io/I2C_Master.h"
#include "reflex/io/SCP1000_I2C.h"
#include "reflex/io/EEPROM_24xx512.h"
#include "reflex/io/Display.h"
#include "reflex/io/Button.h"
#include "reflex/misc/Distributor.h"

#include "Aggregator.h"
#include "SymbolDisplay.h"
#include "DataDisplay.h"
#include "DataLogCompare.h"

#include "reflex/io/Ports.h"
#include "reflex/pmc/Registers.h"
#include "reflex/usci/Registers.h"

namespace reflex {

using mcu::I2C_Master;
using devices::EEPROM_24xx512;
using mcu::usci::USCI_I2C_Master;
using mcu::usci::RegistersB0;

enum PowerGroups {
	DEFAULT = reflex::PowerManager::GROUP1,
	DISABLED = reflex::PowerManager::NOTHING
};

/* NodeConfiguration
 * This is the complete configuration of all system components.
 */
class NodeConfiguration : public System {
public:

	NodeConfiguration()
		: System() // init base system
		, successIndicator(SymbolDisplay::TOGGLE, SymbolDisplay::HEART)
		, errorIndicator(SymbolDisplay::SET, SymbolDisplay::ALARM)
	{
		/* FIXME: This should use some sort of port mapping configuration object
		 * It is currently aimed at SCL = Button::M1, SDA = Button::M2 on EZChronos */
		/* set up port mapping here... */
		Port2()->map(1, pmc::UCB0SDA); // USCI B0 I2C Data
		Port2()->map(2, pmc::UCB0SCL); // USCI B0 I2C Clock

		// set pins to mapped (USCI-I2C) functionality
		Port2()->DIR |= (0x04 /* Button::M1 */ + 0x02 /* Button::M2 */);
		Port2()->OUT &= ~(0x04 /* Button::M1 */ + 0x02 /* Button::M2 */);
		Port2()->SEL |= (0x04 /* Button::M1 */ + 0x02 /* Button::M2 */);

		// connect button to trigger sensor reading
		button.init(&sensor.measure);
		// connect sensor to distributor
		sensor.init(&distPres, &distTemp);
		// connect distributor to display and aggregator
		distTemp.registerSink(&temperatureDisplay.input);
		distTemp.registerSink(&aggregator.input1);
		distPres.registerSink(&pressureDisplay.input);
		distPres.registerSink(&aggregator.input2);
		// set up data displays, success and error indication
		temperatureDisplay.init(&display.getUpperLine());
		pressureDisplay.init(&display.getLowerLine());
		successIndicator.init(&display.getSymbols());
		errorIndicator.init(&display.getSymbols());
		// connect aggregator with data logger with eeprom
		aggregator.init(&logger.input);
		logger.init(&eeprom.read, &eeprom.write,
			    &successIndicator.input, &errorIndicator.input);
		eeprom.init(&logger.success, &logger.error);

		// connect I2C devices to I2C Master, reconfigures devices
		// NOTE: I2C Master is temporarily activated for this to work!
		sensor.setMaster(&i2cmst_sw);
		eeprom.setMaster(&i2cmst_hw);

		// assign groups for primary powermanageable devices
		timer.setGroups(DEFAULT); // put system timer in default group
		button.setGroups(DEFAULT); // put button in default group
		display.setGroups(DEFAULT); // put LCD display in default group.
		i2cmst_sw.setGroups(DEFAULT); // put I2C Master (internal) in default group
		i2cmst_hw.setGroups(DEFAULT); // put I2C Master (external) in default group
		// the sensor is a secondary (self managed) device
		// the eeprom does not support (or need) power management

		// set initial power group, system timer is started implicitely here
		powerManager.enableGroup(DEFAULT); // enable all entities in default group
		powerManager.disableGroup(~DEFAULT); // disable all others

		// we trigger the button once, so the display doesn't stay black after start
		button.notify();
	}

private:
	/* i2c bus master (pure software implementation) for internal bus */
	I2C_Master<mcu::PortJ, 3 /* PJ.3 = SCL */, 2 /* PJ.2 = SDA */> i2cmst_sw;
	/* i2c bus master for external pins (USCI hardware support) */
	USCI_I2C_Master<RegistersB0> i2cmst_hw;
	/* sensor driver for I2C attached slave */
	SCP1000_I2C sensor; // SCP1000-D11 pressure + temp sensor (on board)
	/* EEPROM on external I2C bus */
	EEPROM_24xx512 eeprom; // 512kBit (64kb) EEPROM

	/* sensor value output */
	Display display; // the EZ430 LCD Display
	DataDisplay<uint32, display::LowerLine> pressureDisplay; // display sensor pressure value (19 bits)
	DataDisplay<uint16, display::UpperLine> temperatureDisplay; // display sensor temperature value (14 bits)
	/* success and error indication */
	SymbolDisplay successIndicator;
	SymbolDisplay errorIndicator;

	/* distributors for delivering signals to different consumers */
	Distributor1<uint16, 2> distTemp; // temperature to display and aggregator
	Distributor1<uint32, 2> distPres; // pressure to display and aggregator

	/* data logging */
	typedef Aggregator<uint16,uint32> Aggregator1;
	Aggregator1 aggregator; // aggregator for temperature and pressure values
	DataLogCompare<Aggregator1::ResultT> logger; // write and read sensor values to/from eeprom (verified log).

	/* others */
	Button<buttons::BL, 250> button; // button that triggers sensor reading (debounced 250msec)
};

inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} // ns reflex

#endif
