/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:		 Karsten Walther
 */
#include "A.h"
#include "NodeConfiguration.h"

using namespace reflex;

A::A() :
	timer(VirtualTimer::PERIODIC), computeFunctor(*this)

{
	ready.init(this);
	ADCinput.init(&computeFunctor);
	timer.init(ready), timer.set(1000);

	counter = 0;
}

void A::init(Sink1<char>* sensorOut) {
	this->sensorOut = sensorOut;
}

void A::run() {
	getApplication().out.write("Measure light..");
	getApplication().out.flush();
	sensorOut->assign('5');

}

void A::compute() {
	getApplication().out.write("..done!");
	getApplication().out.writeln();

	char channel;
	ADCinput.get(channel, values[counter]); //get data from eventbuffer
	counter++;
	if (counter == DATASPACE) {
		counter = 0;
		uint16 mean = 0;
		for (int i = 0; i < DATASPACE; i++) //calculate mean
		{
			getApplication().out.write(values[i]);
			getApplication().out.write(":");
			mean += values[i];
		}
		mean = mean / DATASPACE;
		getApplication().out.write(" MEAN ");
		getApplication().out.write(mean);
		getApplication().out.writeln();
	}
}

