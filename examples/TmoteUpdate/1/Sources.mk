#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Carsten Schulze
# */

include $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk

CC_SOURCES_LIB += \
	io/OutputChannel.cc\
	memory/Buffer.cc\
	memory/FreeList.cc\
	memory/Pool.cc\
	memory/PoolManager.cc\
	misc/Prescaler.cc\
	net/CAMac.cc\
	net/ManchesterCoder.cc\

CC_SOURCES_APPLICATION += \
	src/main.cc\
	src/HelloWorld.cc\
	src/A.cc

CC_SOURCES_SYSTEM += \
	SystemStatusBlock.cc\



