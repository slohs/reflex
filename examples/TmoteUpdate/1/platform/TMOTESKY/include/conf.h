#ifndef conf_h
#define conf_h

enum {
		IOBufferSize = 120,   	//used for OutputChannel
		UpdateBlockSize = 96,  //power of 2
		//	UpdateBlockSize = 2,
		//	UpdateBlockSize = 1,
		UpdateArea = 24*1024, // asuming only half of flash will be used
		WaitTime = 200, //time to wait betwen pakets

		NrOfStdOutBuffers = 10, //amount of buffers in OutputChannel
		LOCALREPEATS = 1,
		GLOBALREPEATS = 1,
		MaxPoolCount=3


};


#define UPDATEDEBUG 1

/* how often clock ticks will be generated (miliseconds) */
#define TICK_PERIOD_MSEC 1

// Defines the duration of a whole cycle in clockticks
#ifdef TIME_TRIGGERED_SCHEDULING
#define MAXTIME 60
#endif

#define SLOT_PER_FRAME 5

/* /\** */
/* * these are the delays needed by Update, */
/* * they depend on the application since middleware could be used. */
/* *\/ */
/* enum UPDATE_DEFINITIONS */
/* { */
/* 	RECDELAY = 3, // ms to recognise that somebody is sending to me */
/* 	SAVEDELAY = 200, // ms to save a rec. Command (!Flash) */
/* 	RESENDCOUNT = 1, //how of an command is repeated? =0 meens it will be send once */
/* 	RESENDIMGCOUNT = 0 //how of an Update will be repeated? 0 means it is send once */

/* }; */

#endif
