#ifndef NodeConfiguration_h
#define NodeConfiguration_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 Application
 *
 *	Author:	 Carsten Schulze, Karsten Walther
 *
 *	Description: Implements the System, UpdateSenario..
 *                 Update chaning the lights with are used
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 */

#include "conf.h"
#include "reflex/System.h"
#include "reflex/sinks/Sink.h"

//Update
//#include "reflex/update/UpdateDaemon.h"
#include "reflex/memory/Flash.h"
#include "reflex/update/Sections.h"
#include "reflex/update/UpdateManager.h"

// I/O
#include "reflex/io/OutputChannel.h"
#include "reflex/io/TMoteRadio.h"
#include "reflex/io/Led.h"
#include "reflex/io/Serial.h"
#include "reflex/io/UserButton.h"



//pool and buffer
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"

#include "HelloWorld.h"

namespace reflex {

	/**
	 *
	 *
	 */
	enum PowerGroup {
		AWAKE = reflex::PowerManager::GROUP1,
		SLEEP = reflex::PowerManager::GROUP2,
		UPDATE = reflex::PowerManager::GROUP3,
		NOOPGROUP = reflex::PowerManager::NOTHING
	};

	class NodeConfiguration : public System {
	public:
		NodeConfiguration() :
		radioPool(3)
		,pool_out(3)
		,pool_debug(3)
		,out(&pool_out)
		,debugOut(&pool_debug)
		,serial(1,SerialRegisters::B9600)
		,radio(radioPool)

		{

			clock.init(hello.timer);
			//Radio Configuration
			out.init(&serial.input);
			debugOut.init(&serial.input);
			serial.init(0,0);

			button.init(&hello.button);

			updateMan.init(&radio.input,0,0,&radioPool);
			radio.init(&updateMan.input,&updateMan.forwardingFinished);


			timer.setGroups( SLEEP | AWAKE | UPDATE); //timer is part of groups SLEEP and group AWAKE
			serial.rxHandler.setGroups( AWAKE );
			//serial TX and ADC are activated on demand,
			//thus put them into a default group
			serial.txHandler.setGroups( NOOPGROUP );
			radio.recvHandler.setGroups( AWAKE | UPDATE );

			button.setGroups(AWAKE | SLEEP);


			//initally (de)-activate groups
			powerManager.disableGroup(SLEEP); //this enables all registered entities (obj of type PowerManageAble) which are part of the group SLEEP
			powerManager.enableGroup(AWAKE); //that disables all entities which are part of the group SLEEP and NOT part of group AWAKE.

		
#ifdef VERSION 
			status.version = VERSION;
			status.save();
#else 
			status.version = 1;
#endif

			out.writeln();
			out.write("Tmote update example.");
			out.writeln();
			out.write("first button press send image,");
			out.writeln();
			out.write("second install it on other node");
			out.writeln();


		}
		PoolManager poolManager;
		SizedPool<127, 5> radioPool;
		SizedPool<127, 10> pool_out;
		SizedPool<127, 10> pool_debug;
		OutputChannel out;
		OutputChannel debugOut;

		UpdateManager updateMan;
		Flash flash;

		Serial serial;
		TMoteRadio radio;
		Led led;
		UserButton button;

		Sections sections;

		HelloWorld hello;


	private:
	};

	inline
	NodeConfiguration& getApplication()
	{
		extern NodeConfiguration system;
		return system;
	}

} //reflex

#endif
