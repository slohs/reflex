/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		Carsten Schulze, Karsten Walther
 *
 *	Description: main.cc, Standardmain
 */

#include "NodeConfiguration.h"

namespace reflex {
    NodeConfiguration system;
}

int main()
{
    reflex::system.scheduler.start();
	return(0);
}
