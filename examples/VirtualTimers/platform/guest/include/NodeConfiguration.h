#ifndef NODECONFIGURATION_H
#define NODECONFIGURATION_H

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 Application
 *
 *	Author:		 Carsten Schulze
 *
 *	Description: Implements the System
 *
 *	(c) Carsten Schulze, BTU Cottbus 2005
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/System.h"
#include "reflex/io/Console.h"
#include "reflex/io/OutputChannel.h"

#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"

#include "BlinkLogic.h"
#include "LedStatusPrinter.h"

enum PowerGroups {
 	DEFAULT = reflex::PowerManager::GROUP1,
 	DISABLED = reflex::PowerManager::NOTHING
};

namespace reflex {

/** The construction plan of the virtual timer application.
 * The blinking of the LEDs is transformed to console output
 */
class NodeConfiguration : public System {
public:

	/** constructor */
	NodeConfiguration() :
        System(),
		pool(0),
		out(&pool),
        led(&out)
	{
		out.init(&console); // initialize console output
		timer.setGroups(DEFAULT); // add system timer to default group

		powerManager.enableGroup(DEFAULT); // start system timer
		powerManager.disableGroup(~DEFAULT); // disable all others

        led.get_in_input()->assign(0); // print initial state
        logic.init(led.get_in_input(), 2000); // initialize logic
	}

    PoolManager poolManager;
    SizedPool<IOBufferSize, NrOfStdOutBuffers> pool;
    OutputChannel out;
private:
    Console console; // console interface
	BlinkLogic logic; // the program logic
	LedStatusPrinter led; // led surrogate
};

inline NodeConfiguration& getApplication()
{
    extern NodeConfiguration system;
	return system;
}

} // ns reflex

#endif
