#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Carsten Schulze, Karsten Walther
# */

CC_SOURCES_CONTROLLER = \
	io/Console.cc

# additional library sources for console output
CC_SOURCES_LIB += \
	io/OutputChannel.cc \
	memory/Buffer.cc \
	memory/FreeList.cc \
	memory/Pool.cc \
	memory/PoolManager.cc \
