#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# *	Author:		 Sören Höckner
# */

CC_SOURCES_CONTROLLER += \
    lcd/LCD_B.cc

CC_SOURCES_PLATFORM += \
    io/Display.cc \

CC_SOURCES_LIB += \
