#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 Application
 *
 *	Author:		 Karsten Walther
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/types.h"
#include "reflex/io/SHT11.h"
#include "reflex/io/Led.h"
#include "reflex/sinks/Sink.h"

#include "reflex/System.h"
#include "TempMonitor.h"

namespace reflex {


enum PowerGroups {
 	DEFAULT = reflex::PowerManager::GROUP1,
 	DISABLED = reflex::PowerManager::NOTHING
};

class NodeConfiguration : public System {
public:

	/** TempSensor
	 *  wrapper for the SHT11 temperature/humidity sensor
	 *  to trigger default temperature measurement by a Sink0
	 **/
	class TempSensor : public Sink0, public SHT11 {
		/** notify
		 *  trigger temperature measurement of the SHT11 sensor
		 */
		void notify() {
			assign(SHT11::TEMPERATURE);
		}
	};

	NodeConfiguration() : System()
	{
		monitor.init(&sensor, &leds);
		sensor.init(&monitor.sensor_result, NULL);

		timer.setGroups(DEFAULT); // put system timer in default group.

		/* the timer is started implicitely through the power manager */
		powerManager.enableGroup(DEFAULT); // enable all entities in default group
		powerManager.disableGroup(~DEFAULT); // disable all others
	}

private:

	TempMonitor monitor; // the temperature monitor component
	TempSensor sensor; // SHT11 temperature sensor
	Led leds; // the LEDs for temperature change indication
};

inline NodeConfiguration& getApplication() 
{
	extern NodeConfiguration system;
	return system;
}

} //reflex

#endif
