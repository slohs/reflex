/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	 TempMonitor
 *
 *	Author:		 Stefan Nuernberger
 */

#include "TempMonitor.h"
#include "NodeConfiguration.h"

using namespace reflex;

/* constructor */
TempMonitor::TempMonitor() : timer(VirtualTimer::PERIODIC) {
	sensor_result.init(this); // result triggers activity (run)
}

/** init
 *  initialize the component
 *  @param sensor sensor action trigger
 *  @param leds led output
 */
void TempMonitor::init(Sink0* sensor, Sink1<char>* leds) {
	this->sensor_trigger = sensor;
	this->leds = leds;

	timer.connect_output(sensor_trigger); // timer triggers sensor
	timer.set(SAMPLING_INTERVAL); // start periodic timer
}

/** run
 *  implements Activity
 *  This reads the received sensor value and sets the LEDs accordingly
 */
void TempMonitor::run() {
	uint16 new_result = sensor_result.get();

	if (new_result > last_result)
		leds->assign(RED); // getting warmer
	else if (new_result < last_result)
		leds->assign(BLUE); // getting colder
	else // no change
		leds->assign(GREEN);

	last_result = new_result;
}
