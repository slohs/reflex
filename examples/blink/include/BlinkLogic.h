#ifndef BlinkLogic_h
#define BlinkLogic_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	 BlinkLogic
 *
 *	Author:		 Karsten Walther
 *
 *	Description: Let the Leds blink in a given interval.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/scheduling/Activity.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/timer/VirtualTimer.h"

/** This class uses a virtual timer for periodic action. It lets the led blink
 *  rotary. This work for 1 to 8 leds.
 */
class BlinkLogic : public reflex::Activity{
public:
	/** Initializes the members timer and event.
	 */
	BlinkLogic();

	/** Initialization of the logic, sets the numbers of leds, the pointer
	 *  to the led driver and starts the timer.
	 *
	 *  @param nrOfLeds : number of Leds
	 *  @param leds : pointer to the led driver component (valid values 1 - 8)
	 *  @param period : change period in logical clock ticks
	 */
	void init(char nrOfLeds, reflex::Sink1<char>* leds, unsigned period);

    /** is called after each period and changes the led state.
     *  Implements the activity interface.
     */
    virtual void run();

	/** The timer for periodic action.
	 */
	reflex::VirtualTimer timer;

private:
	/** The event input, which is notified at each periodic timer tick.
	 */
	reflex::Event event;

	/** The current led state.
	 */
	char currentState;

	/** wrap around mark.
	 */
	char endMarker;

	/** pointer to led driver object
	 */
	reflex::Sink1<char>* leds;

};

#endif

