/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(ses):	 BlinkLogic
 *
 *	Author:		 Karsten Walther
 */

#include "BlinkLogic.h"

BlinkLogic::BlinkLogic() : timer()
{
	currentState = 0;
	event.init(this);
	timer.connect_output(&event);
}

void BlinkLogic::init(char nrOfLeds, reflex::Sink1<char>* leds, unsigned period)
{
	this->leds = leds;
	endMarker = 1 << (nrOfLeds-1);
	timer.set(period);
}

void BlinkLogic::run()
{
	if(currentState == 0){
		currentState = 1;
	}else if(currentState == endMarker){
		currentState = 0;
	}else{
		currentState *= 2;
	}
	leds->assign(currentState);
}

