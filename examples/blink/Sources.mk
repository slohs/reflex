#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Carsten Schulze
# */

include $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk


CC_SOURCES_APPLICATION = \
	src/BlinkLogic.cc \
	src/main.cc


