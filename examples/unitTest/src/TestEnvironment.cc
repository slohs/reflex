#include "TestSuite.h"
#include "TestEnvironment.h"

using namespace reflex;
using namespace unitTest;

TestEnvironment::TestEnvironment() :
	_pool(0), _outputChannel(&_pool), act_start(*this), act_result(*this)
{
	out_finished = 0;
	in_testCase.init(&act_start);
	in_stateMachine.init(&act_result);
	in_result.init(&act_result);
}

void TestEnvironment::run_result()
{
	uint8 result = in_result.get();
	_outputChannel->write("; ");
	switch (result)
	{
	case TestSuite::Ok:
		_outputChannel->write("ok");
		break;
	case TestSuite::Failed:
		_outputChannel->write("fail");
		break;
	case TestSuite::Skipped:
		_outputChannel->write("skip");
		break;
	}
	_outputChannel->write("\n");
	_outputChannel->flush();
	testSuite()->~TestSuite();
	act_start.unlock();
}

void TestEnvironment::run_start()
{
	act_start.lock();
	TestCreator creator = 0;
	uint8 id = 0;
	in_testCase.get(creator, id);
	_state = Running;
	creator(testSuite(), id);
}

