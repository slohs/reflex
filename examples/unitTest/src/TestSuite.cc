#include "TestSuite.h"
using namespace reflex;
using namespace unitTest;

TestSuite::TestSuite(uint16 testCaseId) :
	outputChannel(static_cast<reflex::Pool*>(0)), timer_timeout(VirtualTimer::ONESHOT), act_timeout(*this)
{
	_result = Uninitialized;
	_currentTestCase = testCaseId;
	in_timeout.init(&act_timeout);
	timer_timeout.connect_output(&in_timeout);
	timer_timeout.set(DefaultTimeout);
}

TestSuite::~TestSuite()
{
	timer_timeout.stop();
}

void TestSuite::handleTimeout()
{
	setResultFailed();
	outputChannel->write("timeout");
}

void TestSuite::setResultFailed()
{
	_result = Failed;
	timer_timeout.stop();
	environment->in_result.assign(Failed);
}

void TestSuite::setResultOk()
{
	_result = Ok;
	timer_timeout.stop();
	environment->in_result.assign(Ok);
}

void TestSuite::setResultSkipped()
{
	_result = Skipped;
	timer_timeout.stop();
	environment->in_result.assign(Skipped);
}

