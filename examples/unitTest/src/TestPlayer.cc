#include "TestList.h"
#include "TestPlayer.h"
#include "reflex/memory/Flash.h"


using namespace reflex;
using namespace mcu;
using namespace unitTest;


TestPlayer::TestPlayer(const TestListEntry* testList, uint8 size) :
	outputChannel(static_cast<Pool*>(0)), act_txFinished(*this)
{
	in_txFinished.init(&act_txFinished);

	this->counter = 0;
	this->testList = testList;
	this->size = size;

	in_txFinished.notify();
}

void TestPlayer::run_txFinished()
{
	if (counter < size)
	{
		const TestListEntry* currentEntry = testList + counter;
		outputChannel->write(Flash::read(&currentEntry->name));
		outputChannel->write("; ");
		outputChannel->write(Flash::read(&currentEntry->id));
		outputChannel->write("; ");
		out_testCase->assign(Flash::read(&currentEntry->creator), Flash::read(&currentEntry->id));
		counter++;
	}
	else
	{
		outputChannel->write("# FINISHED");
		outputChannel->writeln();
		act_txFinished.lock();
	}
}
