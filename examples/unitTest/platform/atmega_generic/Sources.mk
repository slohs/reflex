#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Richard Weickelt <richard@weickelt.de>
# *
# */

CC_SOURCES_CONTROLLER += \
	io/ExternalInterrupt.cc \
	io/PinChangeInterrupt.cc \
	memory/Flash.cc 
	
CC_SOURCES_APPLICATION = \
	platform/$(PLATFORM)/src/ExternalInterruptTest.cc \
	platform/$(PLATFORM)/src/NodeConfiguration.cc \
	platform/$(PLATFORM)/src/SpiTest.cc  \
	platform/$(PLATFORM)/src/main.cc
	