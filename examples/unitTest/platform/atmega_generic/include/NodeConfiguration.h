#ifndef NodeConfiguration_h
#define NodeConfiguration_h

#include "reflex/data_types/Singleton.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/io/IOPort.h"
#include "reflex/io/Usart.h"
#include "reflex/System.h"
#include "PortTest.h"
#include "SpiTest.h"
#include "TestEnvironment.h"
#include "TestList.h"
#include "TestPlayer.h"
//#include "EnumeralMismatch.h"

namespace reflex
{
enum PowerGroups
{
	AWAKE = reflex::PowerManager::GROUP1,
	SLEEP = reflex::PowerManager::GROUP2,
	NOOPGROUP = reflex::PowerManager::NOTHING
};

template<mcu::McuType mcuType>
class NodeConfiguration: public System
{
public:
	NodeConfiguration();

	atmega::Usart<mcu::Usart0> serial;
	PoolManager poolManager;
	data_types::Singleton<unitTest::TestEnvironment> testEnvironment;
	unitTest::TestPlayer player;
	static const unitTest::TestListEntry testList[];

};

inline NodeConfiguration<reflex::mcu::CurrentMcu>& getApplication()
{
	extern NodeConfiguration<reflex::mcu::CurrentMcu> system;
	return system;
}



} //reflex

#endif

