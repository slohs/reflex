#ifndef NODECONFIGURATION_H
#define NODECONFIGURATION_H

#include "reflex/debug/PlatformAssert.h"
#include "reflex/io/Console.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/sinks/Sink.h"
#include "reflex/System.h"
#include "reflex/memory/PoolManager.h"

#include "InitialTest.h"
#include "TestEnvironment.h"
#include "TestList.h"
#include "TestPlayer.h"

namespace reflex
{

enum PowerGroups
{
	DEFAULT = reflex::PowerManager::GROUP1, DISABLED = reflex::PowerManager::NOTHING
};

class NodeConfiguration : public System
{
public:
	NodeConfiguration();

	PoolManager poolManager;

private:
	Console console; // console interface
	data_types::Singleton<unitTest::TestEnvironment> testEnvironment;
	unitTest::TestPlayer player;
	static const unitTest::TestListEntry testList[];
};

inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} //reflex

#endif
