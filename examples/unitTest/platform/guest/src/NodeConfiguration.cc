#include "NodeConfiguration.h"

using namespace reflex;
using namespace unitTest;


const TestListEntry NodeConfiguration::testList[] =
{
{ "initialTest", 0, &TestSuite::create<InitialTest> }
};

namespace reflex {

NodeConfiguration::NodeConfiguration() :
	System(), player(&testList[0], sizeof(testList)/sizeof(unitTest::TestListEntry))
{
	testEnvironment.getInstance().setOut_message(&console);
	player.out_testCase = &testEnvironment->in_testCase;
	console.out_txFinished = &player.in_txFinished;

	timer.setGroups(DEFAULT); // add system timer to default group
	powerManager.enableGroup(DEFAULT); // start system timer
	powerManager.disableGroup(~DEFAULT); // disable all others
}

}
