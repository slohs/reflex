#ifndef INITIALTEST_H_
#define INITIALTEST_H_

#include "TestSuite.h"

namespace unitTest
{

/*!
 \brief A test case to test the event flow functionality.

 This test must never fail on any platform and therefore proofs the REFLEX kernel
 running properly.

 */
class InitialTest: public TestSuite
{
public:
	InitialTest(uint16 id) :
		TestSuite(id)
	{
		switch (currentTestCase())
		{
		case 0:
			setResultOk();
			outputChannel->write("Event flow is working");
			break;
		default:
			setResultSkipped();
		}
	}
private:

};

}

#endif /* INITIALTEST_H_ */
