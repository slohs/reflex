#ifndef TESTLIST_H_
#define TESTLIST_H_

#include "TestSuite.h"

namespace unitTest
{

/*!
 \brief List of all test cases.
 */
struct TestListEntry
{
	enum
	{
		MaxLength = 30
	};

	char name[MaxLength];
	uint8 id;
	TestSuite::TestCreator creator;
};
}


#endif /* TESTLIST_H_ */
