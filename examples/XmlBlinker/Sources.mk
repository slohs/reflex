#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Richard Weickelt <richard@weickelt.de>
# */

include $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk

CC_SOURCES_APPLICATION = \
	src/main.cc  \
	src/XmlBlinker.cc

	
CC_SOURCES_LIB += \
	memory/Buffer.cc \
	memory/FreeList.cc \
	memory/Pool.cc \
	memory/PoolManager.cc \
	misc/Prescaler.cc \
	xml/XmlReader.cc \
	xml/XmlTagTable.cc \
	xml/XmlWriter.cc
