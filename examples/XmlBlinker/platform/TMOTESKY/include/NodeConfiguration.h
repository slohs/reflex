#ifndef NodeConfiguration_h
#define NodeConfiguration_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	 NodeConfiguration
 *
 *	Author:		 Richard Weickelt <richard@weickelt.de>
 *
 *	Description: Implements the System
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 */

#include "XmlBlinker.h"
#include "reflex/debug/Assert.h"
#include "reflex/io/Led.h"
#include "reflex/io/Serial.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/System.h"

namespace reflex
{
/**
 * Define different powergroups for an application.
 * To use the available groups provided by the powermanager are highly recommended.
 */
enum PowerGroups
{
	DEFAULT = reflex::PowerManager::GROUP1, DISABLED = reflex::PowerManager::NOTHING
};

/**
 * This is the assembly of the components for the application.
 */
class NodeConfiguration: public System
{
public:
	reflex::Led led;
	PoolManager poolManager; ///< Manages multiple pools
	reflex::Serial serial;
	app::XmlBlinker application;
	reflex::data_types::Singleton<uint16> singleton;
public:
	NodeConfiguration() :
		serial(1, SerialRegisters::B19200)
	{
		timer.setGroups(DEFAULT);

		serial.rxHandler.setGroups(DEFAULT);
		serial.txHandler.setGroups(DISABLED);

		powerManager.enableGroup(DEFAULT); // this enables all registered entities in DEFAULT group (starts system timer)
		powerManager.disableGroup(~DEFAULT); // that disables all other entities

		this->connect();
	}

	void connect()
	{
		application.xml.writer.output_chunk = &serial.input;
		application.xml_led.output = reinterpret_cast<Sink1<uint8>*> (&led);
		serial.init(reinterpret_cast<Sink1<char>* >(&application.xml.reader.input_char), &application.xml.writer.input_chunkHandled);
	}
};

inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

} //reflex

#endif

