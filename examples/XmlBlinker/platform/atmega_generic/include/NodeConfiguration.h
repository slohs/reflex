/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 */
#ifndef NodeConfiguration_h
#define NodeConfiguration_h

#include "XmlBlinker.h"
#include "reflex/debug/Assert.h"
#include "reflex/io/Led.h"
#include "reflex/io/IOPort.h"
#include "reflex/io/Usart.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/System.h"
#include "reflex/timer/CompareMatch.h"
#include "reflex/timer/Overflow.h"
#include "reflex/timer/Timer.h"

namespace reflex
{
/**
 * Define different powergroups for an application.
 * To use the available groups provided by the powermanager are highly recommended.
 */
enum PowerGroups
{
	DEFAULT = reflex::PowerManager::GROUP1, DISABLED = reflex::PowerManager::NOTHING
};

class NodeConfiguration;

inline NodeConfiguration& getApplication()
{
	extern NodeConfiguration system;
	return system;
}

/**
 * This is the assembly of the components for the application.
 */
class NodeConfiguration: public System
{
public:
	PoolManager poolManager; ///< Manages multiple pools
	mcu::Usart<mcu::Usart0> serial; ///< UART
	mcu::Timer<mcu::Timer0> timer;
	mcu::CompareMatch<mcu::Timer0, mcu::ChannelA> compareMatch;
	mcu::Led<mcu::IOPort<mcu::Core::PortC> > led; ///< Synchronous sink to an IO port.
	app::XmlBlinker application; ///< The platform independent application.
public:
	NodeConfiguration() :
		System()
	{
		serial.setBaudRate(57600, 16000000);
		compareMatch.setMatchAt(125);
		compareMatch.switchOn();

		timer.setMode(timer.Mode2);
		timer.start(timer.ClockOsc256);
		serial.rxHandler().switchOn();

		this->connect();
	}

	void connect()
	{
		this->compareMatch.output = &application.input_clock;

		application.xml.writer.output_chunk = serial.get_in_tx();
		serial.set_out_rx(&application.xml.reader.input_char);
		serial.set_out_txFinished(&application.xml.writer.input_chunkHandled);
		/* 1 LED channels is connected to the blinker */
		application.xml_led.output = &led;
	}
};

} //reflex

#endif

