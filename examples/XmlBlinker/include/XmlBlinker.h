#ifndef XMLBLINKER_H_
#define XMLBLINKER_H_
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *	Author:		Richard Weickelt <richard@weickelt.de>
 *
 *	Class(es):	XmlBlinker
 *
 *	Description: The main application component
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2011 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATION SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATION SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/memory/PoolManager.h"
#include "reflex/misc/Distributor.h"
#include "reflex/misc/Prescaler.h"
#include "reflex/xml/XmlEnvironment.h"

namespace app
{
/**
 * Sample application that shows how to use the XML environment.
 * It drives one led and let the user choose the blink frequency (Hz) and
 * the current led state (on/off) when the frequency is 0.0 Hz.
 *
 * <h3>Step 1</h3>
 * Build this application, load it to Your favorite platform. You
 * will see a blinking led with approximately 1Hz.
 *
 * <h3>Step 2</h3>
 * Connect the device to the serial port of Your computer.
 * You will receive a status message every second.
 *
 * <h3>Step 3</h3>
 * Try to change the blink frequency by sending XML messages
 * like <frequency>2.5</frequency>.
 *
 */
class XmlBlinker
{
	friend class NodeConfiguration;

	enum
	{
		SendStatusInterval = 1000, MaxFrequency = 500, DefaultFrequency = 15
	};

public:
	XmlBlinker();

	/* The XML environment in a single object */
	/* Configuration Policy for all XML-related classes */
	reflex::xml::XmlEnvironment xml;

	reflex::Distributor<3> input_clock;

	/**
	 * Lookup table for XML tags. Each entry contains one tag string
	 * and the related XML handler defined somewhere else in the application.
	 * The whole table is linked into flash memory.
	 */
	static const reflex::xml::XmlTagTable::LookupEntry tags[];

	/**
	 * This application sends periodic status messages
	 * containing all tags. XmlWriter needs a message skeleton containing
	 * empty tags to achieve that.
	 */
	void run_sendStatus();
	reflex::ActivityFunctor<XmlBlinker, &XmlBlinker::run_sendStatus> act_sendStatus;
	reflex::Event input_sendStatus;
	reflex::Prescaler timer_sendStatus; ///< Prescaler in PERIODIC mode, should probably use a VirtualTImer
	static const char statusMessage[]; // status message skeleton that is sent periodically

	/**
	 * Reflects the LED state. The led has the states
	 * "on" and "off" which is internally represented by an enum. *
	 * The user can override the LED state by manually writing to
	 * this tag when the blink frequency is 0.
	 */
	enum Led
	{
		Off = 0, On = 1
	};
	typedef reflex::xml::XmlChannel<uint8, reflex::xml::XmlConverterEnum<uint8> > XmlEnumChannel;
	XmlEnumChannel xml_led;
	static const XmlEnumChannel::LookupEntry ledTab[]; // lookup table for LED states

	/**
	 * Toggles a led periodically.
	 */
	void run_blink();
	reflex::ActivityFunctor<XmlBlinker, &XmlBlinker::run_blink> act_blink;
	reflex::Prescaler timer_blink; ///< Prescaler in PERIODIC mode, should probably use a VirtualTimer
	reflex::Event input_blink;

	/**
	 * Sets the blink frequency between 0.0Hz..50.0Hz.
	 * When 0.0Hz, the user can manually override the led state.
	 */
	void run_changeFrequency();
	reflex::ActivityFunctor<XmlBlinker, &XmlBlinker::run_changeFrequency> act_frequency;
	reflex::SingleValue1<uint16> input_frequency;
	reflex::xml::XmlChannel<uint16, reflex::xml::XmlConverterInteger<uint16> > xml_frequency;
};

}

#endif /* XMLBLINKER_H_ */
