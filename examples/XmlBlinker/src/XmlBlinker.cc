#include "NodeConfiguration.h"
#include "XmlBlinker.h"
#include "reflex/memory/Flash.h"

using namespace app;
using namespace reflex;
using namespace xml;

namespace reflex
{
extern reflex::NodeConfiguration system;
}

/*
 * Note: The IN_FLASH attribute is only important for AVR.
 * It is included in conf.h and invisible for other platforms.
 */
const XmlTagTable::LookupEntry XmlBlinker::tags[] IN_FLASH =
{
{ "led", &system.application.xml_led },
{ "frequency", &system.application.xml_frequency },
{ "", 0 } };

const char
		XmlBlinker::statusMessage[] IN_FLASH
				= "\
<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n\
<status>\n\
	<led></led>\n\
	<frequency unit=\"Hz\"></frequency>\n\
</status>\n";

const XmlBlinker::XmlEnumChannel::LookupEntry XmlBlinker::ledTab[] IN_FLASH =
{
{ XmlBlinker::Off, "off" },
{ XmlBlinker::On, "on" },
{ 0, "" } };

XmlBlinker::XmlBlinker() :
	xml(&tags[0]), act_sendStatus(*this), timer_sendStatus(Prescaler::PERIODIC), act_blink(*this),
			timer_blink(Prescaler::PERIODIC), act_frequency(*this)
{
	timer_sendStatus.connect_output(&input_sendStatus);
	timer_blink.connect_output(&input_blink);
	input_sendStatus.init(&act_sendStatus);
	timer_sendStatus.set(SendStatusInterval);
	this->input_clock.registerSink(this->timer_sendStatus);

	input_blink.init(&act_blink);
	timer_blink.set(0);
	this->input_clock.registerSink(this->timer_blink);

	input_frequency.init(&act_frequency);
	xml_frequency.output = &input_frequency;
	xml_frequency.setDecimalPoint(1);
	xml_frequency.assign(DefaultFrequency);

	xml_led.setLookupTable(&ledTab[0]);

}

/*
 * Sending is initiated by by assigning a pointer to the begin of a message
 * to XmlWriter's message input.
 */
void XmlBlinker::run_sendStatus()
{
	this->xml.writer.input_message.assign(&statusMessage[0], 0);
}

void XmlBlinker::run_blink()
{
	if (xml_led.get() == Off)
	{
		xml_led.assign(On);
	}
	else
	{
		xml_led.assign(Off);
	}
}

void XmlBlinker::run_changeFrequency()
{
	uint16 f = input_frequency.get();
	if ((f >= MaxFrequency) || (f == 0))
	{
		this->timer_blink.set(0);
	}
	else
	{
		/* would be 1/f, but we calculate in ms and have dHz fixed point integer (.1) */
		uint16 period = (1000 * 10) / f;
		/* One timer cycle is only a half period */
		this->timer_blink.set(period / 2);
	}
}

