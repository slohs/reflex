#Get acceleration data from Chronos watch.
#Taken from info posted at: http://e2e.ti.com/support/microcontrollers/msp43016-bit_ultra-low_power_mcus/f/166/t/32714.aspx
#
#FIXED: In old version I had the x and z value switched around.
#
#
# Copyright (c) 2010 Sean Brewer 
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
#
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
#
#
#If you want you may contact me at seabre986@gmail.com
#or on reddit: seabre
#


import serial
import array
import datetime
import csv
import sys
import platform
from struct import *
from time import *


#######################################Preparations##################################

#######################################AP Commands##################################



def startAccessPoint():
    return array.array('B', [0xFF, 0x07, 0x03]).tostring()# startMarker, Command, PacketSize

def stopAccessPoint():
    return array.array('B', [0xFF, 0x09, 0x03]).tostring()

def switchChannel(channel):
    return array.array('B', [0xFF, 0x074, 0x04, channel]).tostring()# startMarker, Command, PacketSize, channelNo

def startTX():
    return array.array('B', [0xFF, 0x75, 0x03]).tostring()# startMarker, Command, PacketSize

def stopTX():
    return array.array('B', [0xFF, 0x76, 0x03]).tostring()# startMarker, Command, PacketSize
    
def changeTXPower(power):
    return array.array('B', [0xFF, 0x77, 0x04, power]).tostring()# startMarker, Command, PacketSize, power

def sendData(data):
    x = array.array('B', [0xFF, 0x78, 0x03 + len(data)])# startMarker, Command, PacketSize, data (n bytes)
    for i in range(len(data)):
        x.append(data[i])
#    i = 0
#    y = x.tostring()
#    for i in range(len(y)):
#        print (y[i], ", ")
    return x.tostring()


def startOwnProtocol():
    return array.array('B', [0xFF, 0x7A, 0x03]).tostring()# startMarker, Command, PacketSize, 

def stopOwnProtocol():
    return array.array('B', [0xFF, 0x7B, 0x03]).tostring()# startMarker, Command, PacketSize, 

def getData():
    return array.array('B', [0xFF, 0x79, 0x03]).tostring()# startMarker, Command, PacketSize, 

done = 0

def destPort():
    global done
    ser.write(startAccessPoint())
    tester = ser.read(100) 
    if len(tester)  == 0 :
        done = 0
    else:
        done = 1

if platform.system() == "Darwin":
 #   global done
#while not done:
    try:
        ser = serial.Serial("/dev/tty.usbmodem001",115200,timeout=0.0025)
        #done = 1
        destPort()
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/tty.usbmodem002",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial("/dev/tty.usbmodem003",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/tty.usbmodem004",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
elif platform.system() == "Linux":
 #   global done
    try:
        ser = serial.Serial("/dev/ttyACM0",115200,timeout=0.0025)
        #done = 1
        destPort()
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/ttyACM1",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial("/dev/ttyACM2",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/ttyACM3",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
elif platform.system() == "Windows":
  #  global done
   
    try:
        ser = serial.Serial(0,115200,timeout=0.0025)
        destPort()
        #done = 1
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial(1,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(2,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass
    if done == 0:
        try:
            ser = serial.Serial(3,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(4,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(5,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(6,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(7,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(8,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(9,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(10,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(11,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(12,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(13,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(14,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(15,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(16,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(17,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(18,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(19,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(20,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 

if done == 0:
    print "ERROR NO ACCESS POINT FOUND!"
    sys.exit(0)
else:
    print "connected to access point"
accel = ser.read(100) #clear serial port




#######################################"main"##################################




#Start access point
ser.write(stopAccessPoint()) #wichtig!
ser.write(startAccessPoint())
ser.write(startOwnProtocol())

###read data
while 1:
    #sleep(2.0)

    ser.write(getData())
    data = ser.read(100)
    if len(data) > 6 :
        vcc = unpack_from('<H', data,5) 
        print "answer from node: %d" %(vcc) #auspacken der daten) 
#        for item in data:
#            print "%x" % ord(item),
#        print ":"
        
sys.exit(0)
        

