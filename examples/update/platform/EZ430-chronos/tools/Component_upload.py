#send component to Chronos watch with running component update.
#
#
# Copyright (c) 2011 Andre Sieber 
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
#
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
#


import serial
import array
import sys
import platform
from struct import *
from time import *
from intelhex import IntelHex


#comandlin parameter
from optparse import OptionParser 
parser = OptionParser()
parser.add_option("-i", "--ihex", dest="hexfile", help="hexfile with component code")
parser.add_option("-I", "--ID", type="int",dest="id", help="component id")
parser.add_option("-u", "--startup",type="int", dest="startup", help="component startup function adress (as hex)")
parser.add_option("-d", "--shutdown",type="int", dest="shutdown", help="component shutdown function adress (as hex)")
parser.add_option("-a", "--acknowledgement",type="int", dest="acknowledgement", help="target node sends acknowledgement, 0 for no, 1 for yes")
parser.add_option("-p", "--persistent",type="int", dest="persistent", help="componnet is persistent, 0 for no, 1 for yes")
parser.add_option("-s", "--autostart",type="int", dest="autostart", help="componnet should autostart, 0 for no, 1 for yes")
(options, args) = parser.parse_args()






#######################################Preparations##################################



####################################### constants ##################################

# size of data in msg
HEX_BYTES = 22

##################### hex file stuff###############################

ih = IntelHex()
ih.loadfile(options.hexfile,format='hex')
firstAddress = ih.start_addr['IP']
##get last address with data####
lastAddress = ih.start_addr['IP']
for addr in range(ih.start_addr['IP'],0xffff, 1):
    if ih[addr] != 255:
        lastAddress=addr


##################### hex file stuff###############################

ih = IntelHex()
ih.loadfile(options.hexfile,format='hex')
firstAddress = ih.start_addr['IP']
##get last address with data####
lastAddress = ih.start_addr['IP']
for addr in range(ih.start_addr['IP'],0xffff, 1):
    if ih[addr] != 255:
        lastAddress=addr


##################### commands and constants ###############################


#info data
PERSISTENT = 0x04
AUTOSTART = 0x10


# comands
RECEIVE = 1
INSTALL = 2
UNINSTALL = 3
STARTUP = 4
SHUTDOWN = 5
LIST = 6
STARTUP_ALL = 7
RECEIVE_HEADER = 8
RECEIVE_DATA = 9
ABORT_INSTALL = 10


NO_SPACE_FOR_HEADER = 1
NO_SPACE_FOR_COMPONENT = 2
NO_SLOT_FOR_COMPONENTHEADER = 3
INSTALLATION_FAILED = 4
INSTALLATION_SUCCESSFUL = 5
STARTUP_FAILED = 6
STARTUP_SUCCESSFUL = 7
SHUTDOWN_FAILED = 8
SHUTDOWN_SUCCESSFUL = 9
COMPONENT_LIST = 10
UNINSTALL_FAILED = 11
UNINSTALL_SUCCESSFUL = 12
DATA_RECEIVED = 13
ABORT_SUCCESSFUL = 14
DATA_ALREADY_RECEIVED = 247
ALREADY_INSTALLED = 248
COMMAND_INVALID_RECEIVE = 249
PACKET_INVALID = 250
COMMAND_INVALID = 251
ALREADY_RUNNING = 252
UNKNOWN_COMPONENT = 253
ERROR_STATE = 254


def printResponce(responce):
    if  responce == NO_SPACE_FOR_HEADER :
        print "NO_SPACE_FOR_HEADER"
    elif responce == NO_SPACE_FOR_COMPONENT :
        print "NO_SPACE_FOR_COMPONENT"
    elif responce == NO_SLOT_FOR_COMPONENTHEADER :
        print "NO_SLOT_FOR_COMPONENTHEADER"
    elif  responce == INSTALLATION_FAILED :
        print "INSTALLATION_FAILED"
    elif  responce == INSTALLATION_SUCCESSFUL :
        print "INSTALLATION_SUCCESSFUL"
    elif  responce == STARTUP_FAILED :
        print "STARTUP_FAILED"
    elif  responce == STARTUP_SUCCESSFUL :
        print "STARTUP_SUCCESSFUL"
    elif  responce == SHUTDOWN_FAILED :
        print "SHUTDOWN_FAILED"
    elif  responce == SHUTDOWN_SUCCESSFUL :
        print "SHUTDOWN_SUCCESSFUL"
    elif  responce == COMPONENT_LIST :
        print "COMPONENT_LIST"
    elif  responce == UNINSTALL_FAILED :
        print "UNINSTALL_FAILED"
    elif  responce == UNINSTALL_SUCCESSFUL :
        print "UNINSTALL_SUCCESSFUL"
    elif  responce == ABORT_SUCCESSFUL :
        print "ABORT_SUCCESSFUL"
    elif responce == DATA_RECEIVED :
        print "DATA_RECEIVED"
    elif responce == DATA_ALREADY_RECEIVED :
        print "DATA_ALREADY_RECEIVED"
    elif responce == ALREADY_INSTALLED :
        print "ALREADY_INSTALLED"
    elif responce == COMMAND_INVALID_RECEIVE :
        print "COMMAND_INVALID_RECEIVE"
    elif  responce == ALREADY_RUNNING :
        print "ALREADY_RUNNING"
    elif  responce == PACKET_INVALID :
        print "PACKET_INVALID"
    elif  responce == COMMAND_INVALID :
        print "COMMAND_INVALID"
    elif responce == UNKNOWN_COMPONENT :
        print "UNKNOWN_COMPONENT"
    elif responce == ERROR_STATE :
        print "ERROR_STATE"
    else :
        print "responce unknown - %d" %(responce)



#######################################AP Commands##################################
def startAccessPoint():
    return array.array('B', [0xFF, 0x07, 0x03]).tostring()# startMarker, Command, PacketSize

def stopAccessPoint():
    return array.array('B', [0xFF, 0x09, 0x03]).tostring()

def switchChannel(channel):
    return array.array('B', [0xFF, 0x074, 0x04, channel]).tostring()# startMarker, Command, PacketSize, channelNo

def startTX():
    return array.array('B', [0xFF, 0x75, 0x03]).tostring()# startMarker, Command, PacketSize

def stopTX():
    return array.array('B', [0xFF, 0x76, 0x03]).tostring()# startMarker, Command, PacketSize
    
def changeTXPower(power):
    return array.array('B', [0xFF, 0x77, 0x04, power]).tostring()# startMarker, Command, PacketSize, power

def sendData(data):
    x = array.array('B', [0xFF, 0x78, 0x03 + len(data)])# startMarker, Command, PacketSize, data (n bytes)
    for i in range(len(data)):
        x.append(data[i])
#    i = 0
#    y = x.tostring()
#    for i in range(len(y)):
#        print (y[i], ", ")
    return x.tostring()


def startOwnProtocol():
    return array.array('B', [0xFF, 0x7A, 0x03]).tostring()# startMarker, Command, PacketSize, 

def stopOwnProtocol():
    return array.array('B', [0xFF, 0x7B, 0x03]).tostring()# startMarker, Command, PacketSize, 

def getData():
    return array.array('B', [0xFF, 0x79, 0x03]).tostring()# startMarker, Command, PacketSize, 

#######################################"connect to accesspoint"##################################

done = 0

def destPort():
    global done
    ser.write(startAccessPoint())
    tester = ser.read(100) 
    if len(tester)  == 0 :
        done = 0
    else:
        done = 1

if platform.system() == "Darwin":
 #   global done
#while not done:
    try:
        ser = serial.Serial("/dev/tty.usbmodem001",115200,timeout=0.0025)
        #done = 1
        destPort()
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/tty.usbmodem002",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
elif platform.system() == "Linux":
 #   global done
    try:
        ser = serial.Serial("/dev/ttyACM0",115200,timeout=0.0025)
        #done = 1
        destPort()
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/ttyACM1",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
elif platform.system() == "Windows":
  #  global done
   
    try:
        ser = serial.Serial(0,115200,timeout=0.0025)
        destPort()
        #done = 1
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial(1,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
if done == 0:
    print "ERROR NO ACCESS POINT FOUND!"
    sys.exit(0)
else:
    print "connected to access point"
tmp = ser.read(100) #clear serial port




#######################################"start acces point"##################################
ser.write(stopAccessPoint()) #wichtig!
ser.write(startAccessPoint())
ser.write(startOwnProtocol())


##################### init installation ###############################
acknowledgement_request =  options.acknowledgement

data2sendCommand = [
    RECEIVE, 
    RECEIVE>>8,
    acknowledgement_request,
    acknowledgement_request>>8
     ]

data2sendCommand.insert(0, 0x11)

###send data
ser.write(sendData(data2sendCommand))
sleep(0.1)
data = ser.read(100)   #read pending data
sleep(1.0)
#get data
ser.write(getData())
data = ser.read(100)
if len(data) > 4 :
        print "answer from node:", #auspacken der daten) 
        printResponce(ord(data[5]))


##################### send header ###############################
header_file = firstAddress
if options.autostart == 1:
    if options.persistent == 1 :
        header_info = PERSISTENT | AUTOSTART
    else :
        header_info = AUTOSTART
else:
    if options.persistent == 1 :
        header_info = PERSISTENT
    else :
        header_info = 0

header_id = options.id
header_size = lastAddress - firstAddress + 1
header_startup = options.startup
header_shutdown = options.shutdown


needed_messages = int(round(header_size/HEX_BYTES + 0.5))
print "component size: ", header_size, " bytes "
print "needed messages: ", needed_messages



data2sendHeader = [ 
    RECEIVE_HEADER, 
    RECEIVE_HEADER>>8,
    (header_id & 0x00FF),
    (header_id>>8),
    (header_info & 0x00FF),
    (header_info>>8),
    (header_startup & 0x00FF),
    (header_startup>>8),
    (header_shutdown & 0x00FF),
    (header_shutdown>>8),
    (header_file & 0x00FF),
    (header_file>>8),
    (header_size & 0x00FF),
    (header_size>>8)
    ]

data2sendHeader.insert(0, 0x11)



ser.write(sendData(data2sendHeader))
sleep(0.01)
TempData = ser.read(100)   #read pending data
sleep(1.0)
#get data
ser.write(getData())
data = ser.read(100)
if len(data) > 4 :
        print "answer from node:", #auspacken der daten) 
        printResponce(ord(data[5]))




##################### send data ###############################


msg_number = 0
for addr in range(ih.start_addr['IP'],lastAddress, HEX_BYTES):

    print "sending message %d/%d..." %(msg_number+1,needed_messages),
    msg_number = msg_number + 1

    data2send = ih.tobinarray(start=addr, size=HEX_BYTES)
    data2send.insert(0, addr>>8) #adress for this packet
    data2send.insert(0, addr & 0x00FF)
    data2send.insert(0, RECEIVE_DATA>>8)
    data2send.insert(0, RECEIVE_DATA)


    data2send.insert(0, 0x11)

    ###send data
    ser.write(sendData(data2send))
    sleep(0.01)
    TempData = ser.read(100)   #read pending data
    sleep(0.05)

    ###wait vor answer
    ser.write(getData())
    data = ser.read(100)
    if len(data) > 4 :
        if acknowledgement_request == 1 :
            print "answer from node:", #auspacken der daten) 
            printResponce(ord(data[5]))
    else : 
        print ""

###wait for answer
sleep(1)
ser.write(getData())
data = ser.read(100)
if len(data) > 4 :
    print "answer from node:", #auspacken der daten) 
    printResponce(ord(data[5]))
    print ord(data[5])

sys.exit(0)








