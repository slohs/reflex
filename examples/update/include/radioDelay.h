#ifndef radioDelay_h
#define radioDelay_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	HelloWorld
 *
 *	Author:		Karsten Walther
 *
 *	Description: Famous HelloWorld Application.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/sinks/Event.h"
#include "reflex/sinks/Queue.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/memory/Pool.h"
#include "reflex/memory/Buffer.h"

#include "reflex/timer/VirtualTimer.h"
#include "reflex/sinks/Sink.h"
#include "reflex/types.h"

namespace reflex {


class radioDelay : public reflex::Activity
{
public:

	radioDelay();


	/** Here the famous message is written to standard output
	 */
	virtual void run();

	/**
	* Initialize the sender and the reciever, which use the serial
	*
	* @param receiver the object, which gets all receiving characters
	* @param sender gets notified if something was successfully sent
	*/
	void init(Sink1<Buffer*>* radio, uint16 delayTime);


	void sendData(uint8 *data, uint8 count);
	void sendData(Buffer* buffer);
	void delayDone();
	/** This is the Activity Functor
	 *  Ponting to a class and to a member function of
	 *  this class
	 */
	reflex::ActivityFunctor<radioDelay,&radioDelay::delayDone> delayFunctor;

	/**
	 * All data waiting for transmission is stored in this queue
	 * to get schedueled
	 */
	Queue<Buffer*> input;
	Queue<Buffer*> delayInput;

	Sink1<Buffer*>* radio;


protected:
	reflex::Event delayEvent;
	reflex::VirtualTimer delayTimer;

	uint16 delayTime;

	bool isDelaying;

	Pool& pool;

};

} //ns
#endif
