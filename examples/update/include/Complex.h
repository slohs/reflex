#ifndef Complex_h
#define Complex_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Complex
 *
 *	Author:		Andre Sieber
 *
 *	Description: Send request to installed ADC, process resuld and
 *                   send it using the radio
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/sinks/Event.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/timer/VirtualTimer.h"
#include "reflex/sinks/Sink.h"
#include "reflex/types.h"
#include "reflex/scheduling/ActivityFunctor.h"

#include "reflex/memory/Pool.h"
#include "reflex/memory/Buffer.h"
#include "reflex/adc/ADC12_A.h"

/** This class writes its well known message to a typical output, everytime
 *  it is notified that the next cycle can begin.
 */
class Complex : public reflex::Activity
			
{
public:
  Complex(reflex::Pool& pool);

	virtual void run();

	/**
	 * Connects the componnet outputs to the event flow
	 *
	 * @param ADCout pointer to the ADC input
	 * @param RADIOout pointer to the radio input
	 */
	void init(reflex::Sink1<reflex::mcu::ADC12_A::Request>* ADCout,reflex::Sink1<reflex::Buffer*>* RADIOout);
	
	/**
	 * Process ADC data and send it
	 */
	void send();
	reflex::ActivityFunctor<Complex,&Complex::send> sendFunctor;


	reflex::SingleValue1<uint16> ADCin;
	reflex::VirtualTimer timer;
protected:
	reflex::Event ready;

	reflex::Sink1<reflex::mcu::ADC12_A::Request>* ADCout;
	reflex::Sink1<reflex::Buffer*>* RADIOout;
	reflex::Pool& pool;
};
/**
 * Creates the componnet with the help of MemoryManager,
 * connects it to the system and starts it.
 *
 * @return A pointer to destroy the component later
 */
void* startup();

/**
 * Stops the component, disconnects it from the event flow and
 * frees all used memory.
 *
 * @param pointer to the address
 * @return true when successful
 */
bool shutdown(void*);



#endif
