#include "Receiveid.h"
#include "NodeConfiguration.h"

using namespace reflex;

	Receiveid::Receiveid():timer(event),queue()
	{
	}

	void Receiveid::init()
	{
		secs = 0;
		timer.set(50);
		event.init(this);
		pd.init(&queue);
		queue.init(&pd);
	}

	void Receiveid::run()
	{
		secs++;
		if(secs >= 10)
		{
			getApplication().leds.turnOn(Led::RED);
			getApplication().incSendTimer();
		}
	}
	
	void Receiveid::assign(Buffer* buff)
	{
		queue.assign(buff);
		secs = 0;
		getApplication().leds.turnOff(Led::RED);
	}
