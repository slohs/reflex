#include "Sendid.h"
#include "NodeConfiguration.h"

using namespace reflex;

	Sendid::Sendid():timer(event)
	{
	}

	void Sendid::init(Sink1<Buffer*> *lo)
	{
		lowerOut = lo;
		timer.set(100);//alle 2 sec
		event.init(this);
		getApplication().leds.turnOn(Led::GREEN);
	}

	void Sendid::run()
	{
		getApplication().leds.blink(Led::BLUE);
		getApplication().out.write("Sent:");
		for(int i = 0; i < DS2411::IDSIZE;i++)
		{
			getApplication().out.write(getApplication().ds.id[i]);
			if(i != DS2411::IDSIZE-1)
				getApplication().out.write(':');
			getApplication().out.flush();
		}
		getApplication().out.writeln();
		buff = new(&getApplication().radioPool) Buffer(&getApplication().radioPool);
		buff->write(getApplication().ds.id,DS2411::IDSIZE);
		if (lowerOut) lowerOut->assign(buff);
	}

	void Sendid::notify()
	{
	}
