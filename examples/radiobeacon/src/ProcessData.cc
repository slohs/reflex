#include "ProcessData.h"
#include "NodeConfiguration.h"

using namespace reflex;

	ProcessData::ProcessData()
	{
	}

	void ProcessData::init(Queue<Buffer*> *queue)
	{
		this->queue = queue;
	}

	void ProcessData::run()
	{
		Buffer *buff = queue->get();
		getApplication().out.write("Received:");
		unsigned char id[8];
		buff->read(id,8);
		buff->downRef();
		for(int i = 0;i < 8;i++){
			getApplication().out.write(id[i]);
			if(i < 7)
				getApplication().out.write(':');
			getApplication().out.flush();
		}
		getApplication().out.writeln();
	}
