#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# */

CC_SOURCES_APPLICATION += \
	src/Sendid.cc \
	src/Receiveid.cc \
	src/ProcessData.cc \
	src/main.cc

CC_SOURCES_LIB += \
	io/OutputChannel.cc \
	memory/Buffer.cc \
	memory/FreeList.cc \
	memory/Pool.cc \
	memory/PoolManager.cc \
	timer/PeriodicTimer.cc \
	io/TMoteRadio.cc


CC_SOURCES_CONTROLLER += \
	io/DS2411.o \


sinclude $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk
