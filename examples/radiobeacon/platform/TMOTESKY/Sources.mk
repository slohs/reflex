#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Carsten Schulze
# *
# *	(c) Carsten Schulze, BTU Cottbus 2005
# */

CC_SOURCES_PLATFORM += io/Led.cc \
			io/SPI.cc\
			io/CC2420.cc

CC_SOURCES_CONTROLLER += \
	io/Serial.cc \

ASM_SOURCES_PLATFORM =
