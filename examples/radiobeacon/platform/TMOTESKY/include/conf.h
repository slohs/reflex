#ifndef CONF_H 
#define CONF_H

// the type for the network address
typedef unsigned char NetAddress;

#define BROADCAST 0xFF

// defining of the type for the local time value
typedef long long TimerType;

#define timerMax 0xFFFFFFFF

enum { 
		IOBufferSize = 16,   	//used for OutputChannel
		NrOfStdOutBuffers = 64,
		MaxPoolCount=8
};//amount of buffers in OutputChannel

enum RunMode {
	AWAKE = 0x01,
	SLEEP = 0x02,
	SECONDARY = 0x04
};

/* how often clock ticks will be generated (miliseconds) */
#define TICK_PERIOD_MSEC 20

// Defines the duration of a whole cycle in clockticks
#ifdef TIME_TRIGGERED_SCHEDULING
#define MAXTIME 60
#endif


#define SLOT_PER_FRAME 5

#endif
