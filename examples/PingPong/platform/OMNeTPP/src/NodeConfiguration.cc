/*
 * NodeConfiguration.cpp
 *
 *  Created on: Oct 5, 2010
 *      Author: shoeckne
 */

#include "NodeConfiguration.h"

using namespace reflex;

NodeConfiguration::NodeConfiguration()
	: poolManager()
	, pool(20) //stacksize
	, pingPong(getModule().getParentModule()->getParentModule()->getIndex())
	, radio()
{
	radio.connect_out_upperLayer(pingPong.get_input());
	radio.connect_pool(&pool);

	pingPong.connect_Pool(&pool);
	pingPong.connect_output(radio.get_in_upperLayer());

}
