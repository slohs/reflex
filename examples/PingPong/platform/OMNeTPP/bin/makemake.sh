#!/bin/bash

ROOTPATH="../../../../../"
REFLEXPATH=$ROOTPATH
MIXIMPATH="/opt/mixim/"
DEBUG=1

opp_makemake -f -L${MIXIMPATH}src/ -lmixim -DDEBUG=$DEBUG -o pingpongreflex


echo "#################################################"
echo " GENERATE run.sh skript "

#rm -f ../sim/run.sh
echo "#!/bin/bash" > ../sim/run.sh
echo "ROOTPATH=\"${ROOTPATH}\"" >> ../sim/run.sh
echo "REFLEXPATH=\"${REFLEXPATH}\"" >> ../sim/run.sh
echo "MIXIMPATH=\"${MIXIMPATH}\"" >> ../sim/run.sh
echo " " >> ../sim/run.sh
echo "export NEDPATH=\".:${REFLEXPATH}platform/OMNeTPP/ned:${MIXIMPATH}src/base:${MIXIMPATH}src/inet_stub:${MIXIMPATH}src/modules:pingpongreflex\"" >> ../sim/run.sh
echo " " >> ../sim/run.sh
echo "../bin/pingpongreflex -r 0 -f omnetpp.ini -l ${MIXIMPATH}src/mixim" >> ../sim/run.sh

chmod +x ../sim/run.sh
echo " done "
echo "#################################################"

echo "#################################################"
echo " GENERATE debug.sh skript "

#rm -f ../sim/run.sh
echo "#!/bin/bash" > ../sim/debug.sh
echo "ROOTPATH=\"${ROOTPATH}\"" >> ../sim/debug.sh
echo "REFLEXBRANCH=\"${REFLEXBRANCH}\"" >> ../sim/debug.sh
echo "REFLEXPATH=\"${REFLEXPATH}\"" >> ../sim/debug.sh
echo "MIXIMPATH=\"${MIXIMPATH}\"" >> ../sim/debug.sh
echo " " >> ../sim/debug.sh
echo "export NEDPATH=\".:${REFLEXPATH}platform/OMNeTPP/ned:${MIXIMPATH}src/base:${MIXIMPATH}src/inet_stub:${MIXIMPATH}src/modules:pingpongreflex\"" >> ../sim/debug.sh
echo " " >> ../sim/debug.sh
echo "gdb --args ../bin/pingpongreflex -u Cmdenv -r 0 -f omnetpp.ini -l ${MIXIMPATH}/src/mixim" >> ../sim/debug.sh

chmod +x ../sim/debug.sh
echo " done "
echo "#################################################"
