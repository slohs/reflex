/*
 * NodeConfiguration.h
 *
 *  Created on: Oct 5, 2010
 *      Author: Soeren Hoeckner
 */

#ifndef NODECONFIGURATION_H_
#define NODECONFIGURATION_H_

#include "reflex/types.h"
#include "reflex/System.h"

#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"
#include "reflex/io/OMNeTRadio.h"
#include "PingPong.h"


namespace reflex {
class NodeConfiguration
        : public System
{
public:
    /**	Memory Pool
     *	< BufferType , Number of Buffers >
     */
    PoolManager poolManager;
    SizedPool<IOBufferSize, NrOfStdOutBuffers> pool;

    OMNeTRadio radio;

    PingPong pingPong;

public:
    NodeConfiguration();

    void init() {

        pingPong.initialize();

    };
    void finish() {
        pingPong.finish();
    };

    void callEndSimulation()
    {
        getModule().endSimulation();
    }

    simtime_t getSimTime()
    {
        return simTime();
    }
};

inline
NodeConfiguration& getApplication() {
    return static_cast<NodeConfiguration&>(reflex::getSystem());
}

}
#endif /* NODECONFIGURATION_H_ */
