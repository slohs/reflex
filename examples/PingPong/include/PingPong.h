/*
 * PingPong.h
 *
 *  Created on: Oct 7, 2010
 *      Author: shoeckne
 */

#ifndef PINGPONG_H_
#define PINGPONG_H_

#include "PingPong_Structure.h"
#include "conf.h"


class PingPong : public PingPong_Structure<PingPong>
{
	int nodeId;

public:
	PingPong(int nodeId);

	void timerHandle_Impl();

	void receiveHandle_Impl();

	void initialize();
	void finish();

protected:
	void ping();
	void pong();
};




#endif /* PINGPONG_H_ */
