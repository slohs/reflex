#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# *	Author:		 Soeren Hoeckner
# */

CC_SOURCES_CONTROLLER += \
	lcd/LCD_B.cc

CC_SOURCES_PLATFORM += \
	io/Display.cc \
	io/Buttons.cc \
	power/Battery.cc

CC_SOURCES_LIB += \
	data_types/BCD.cc
