#ifndef SCROLLTEXT_H
#define SCROLLTEXT_H

#include "reflex/scheduling/Activity.h"
#include "reflex/io/Display.h"
#include "reflex/sinks/SingleValue.h"

/*! A Display for characters on EZ430-Chronos screen.
 * Put in single characters and they will appear on
 * the selected display line (template parameter) in
 * a scrolling fashion.
 */
template<typename TDisplayLine>
class ScrollText : public reflex::Activity
{
private:
	TDisplayLine *display; ///< reference to EZ430-Chronos display
	reflex::SingleValue1<uint8> input; ///< single character input
	char screen[TDisplayLine::DigitCount + 1]; ///< screen content

public:
	/*! set up display and activity */
	ScrollText() {
		input.init(this);

		// sanitize initial screen content
		for (uint8 i = 0; i != TDisplayLine::DigitCount + 1; ++i)
		{
			screen[i] = NULL;
		}
	}

	/*! set display line used for output */
	inline void set_out_display(TDisplayLine *display)
	{
		this->display = display;
	}
	/*! get character input event channel */
	inline reflex::SingleValue1<uint8> *get_in_char()
	{
		return &input;
	}

	/*! put next character on screen by
	 *  shifting previously written ones.
	 */
	void run()
	{
		char next = static_cast<char>(input.get());
		if (next == '\r') return;

		// shift first characters
		for (uint8 i = 0; i != TDisplayLine::DigitCount - 1; ++i)
		{
			screen[i] = screen[i + 1];
		}

		// put new character on last valid position
		screen[TDisplayLine::DigitCount - 1] = next;

		// update display
		*display = screen;
	}
};

#endif // SCROLLTEXT_H
