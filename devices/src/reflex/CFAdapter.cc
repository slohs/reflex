/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:		Karsten Walther
 *
 *	Description: Class for transparent use of the Avisaro CF-Modul.
 */

#include "CFAdapter.h"

// The command strings for the CF-Modul.
const char* openmessage = "at+append x";
const char* writemessage = "at+writepacket\r\nx";
const char* closemessage = "at+close\r\nx";

void CFAdapter::init(Sink1<Buffer*>* output)
{

	this->output = output;

	//Build the open command packet "at+append logfileName \r\n"
	for(char* pos=(char*)openmessage; *pos != 'x'; pos++){
		openPacket.write(*pos);
	}
	for(char* pos1=logfileName; *pos1 != 0; pos1++){
		openPacket.write(*pos1);
	}
	openPacket.write('\r');
	openPacket.write('\n');

	//Build the start writing command packet "at+writepacket\r\n"
	for(char* pos2=(char*)writemessage; *pos2 != 'x'; pos2++){
		writePacket.write(*pos2);
	}

	//Build the close command packet "at+close\r\n"
	for(char* pos3=(char*)closemessage; *pos3 != 'x'; pos3++){
		closePacket.write(*pos3);
	}

}

//Lookup-table for CRC generation
static const unsigned int crc_tab[16] =
{
0x0000, 0x1081, 0x2102, 0x3183,
0x4204, 0x5285, 0x6306, 0x7387,
0x8408, 0x9489, 0xA50A, 0xB58B,
0xC60C, 0xD68D, 0xE70E, 0xF78F
};

//Function for generation of the CRC value, expected by the CF-Modul.
unsigned short crc_update (unsigned short crc, unsigned char c)
{
crc = (((crc >> 4) & 0x0FFF) ^ crc_tab[((crc ^ c) & 0x000F)]);
crc = (((crc >> 4) & 0x0FFF) ^ crc_tab[((crc ^ (c>>4)) & 0x000F)]);
return crc;
}

void CFAdapter::run()
{
	SizedFiFoBuffer<IOBufferSize>* buffer = (SizedFiFoBuffer<IOBufferSize>*)input.get();

	char* addr = (char*)buffer->getStart();
	size_t length = buffer->getLength();

	//a data packet is build as follows length+data+crc
	//allocate space for the length 2 byte
	buffer->write('0');
	buffer->write('0');

	//copy data to the packet
	for(char* pos = addr + length - 1; pos >= addr; pos--){
		*(pos+2) = *pos;
	}

	//inserting length in the previously allocated space. First HighByte
	//then LowByte
	*addr = (length & 0xff);
	*(addr+1) = (length >> 8);

	//Calculation of the CRC, refer to CF-Modul documentation
	unsigned crc = 0xffff;
	for(char* pos = addr+2; pos < addr+2+length; pos++){
		crc = crc_update(crc,*pos);
	}
	crc = ~crc;

    //write crc into packet
    //if no checksum is wanted, write 2 zeroes
	buffer->write(crc & 0xff);
	buffer->write(crc >> 8);

    //submitting packets to the output device
	openPacket.upRef();
    output->assign(&openPacket);
    writePacket.upRef();
    output->assign(&writePacket);
	output->assign(buffer);
	closePacket.upRef();
	output->assign(&closePacket);
}

