#ifndef reflex_EasyRadio_h
#define reflex_EasyRadio_h
/**
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Class(es):	EasyRadio<Buffertype>
 *
 *	Author:		S�ren H�ckner
 *
 *	Description:	Driver implementation for the EasyRadio-Module
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/memory/Pool.h"
#include "reflex/sinks/Queue.h"
#include "reflex/sinks/Sink.h"
#include "reflex/io/Serial.h"
#include "reflex/io/Radio.h"
#include "reflex/debug/Assert.h"


namespace reflex
{

/** This driver prefixes packets before transmission and resembles them
 *  on receiver side.
 */
template <typename BufferType>
class EasyRadio :
			public Sink0,
			public Sink1<char>,
			public Radio<BufferType> {
public:
	/**
	*	Constructor
	*/
	EasyRadio(FreeList* pool);

	/**
	 *	initialization.
	 *
	 *	@param lowerOut	sender object, which gets a Buffer (at the moment we got the serial)
	 *	@param upperOut	receiver object, which awaits a Buffer from the air
	 *	@param busyOut	notified then driver busy
	 **/
	void init( Queue<Buffer*> *lowerOut, Sink1<BufferType*> *upperOut, Sink1<bool> *busyOut=0);

	/** upperIn.
	 *	Input from upper Layer.
	 *
	 *	@param buf Argument has to be derived from Buffer
	 **/
	virtual void assign(BufferType *buf);

	/**
	 *	notifacation is called form sender obj.
	 *	is called then message was finally delivered
	 **/
	virtual void notify();

	/**
	 *	.
	 *	async from Hardware we receive a byte-Stream
	 **/
	virtual void assign(char byte);

	/**
	 *	.
	 *	@return false if receiving
	 **/
	virtual bool isBusy();

public:
	Pool* pool;/**< global pool-obj*/

protected:
	Queue<Buffer*> *lowerOut; /**< to the driver */
	Sink1<BufferType*> *upperOut; /**< awaits buffers frome the air*/
	Sink1<bool>		*busyOut;	/**< notify then radio is available again*/

	BufferType* recStack; /**< hold an unfinished received Buffer*/
	bool isHeader;

	uint8* rxpos;	/**<	write position	*/
	uint8 recCount;	/**<	counting bytes going to be received */
//	enum State
//	{
//		WAITING = 	0 ,
//		SENDING = 	0x2,
//		RECEIVING = 0x4
//	}	state;
	uint8 sendCount;	/**<	sended Buffers */
	bool receiving;
};



template
<typename BufferType>
EasyRadio<BufferType>::EasyRadio(FreeList* pool)
{
	this->pool = pool;
	isHeader = 0;
	recStack = 0;
	recCount = 0;
	sendCount= 0;
	receiving= 0;
//	state = WAITING; //initial waiting

}

template
<typename BufferType>
void EasyRadio<BufferType>::init( Queue<Buffer*>* lowerOut, Sink1<BufferType*> *upperOut, Sink1<bool> *busyOut )
{
	Assert(upperOut !=0);
	this->upperOut = upperOut;

	Assert(lowerOut !=0);
	this->lowerOut = lowerOut;

	this->busyOut = busyOut;
}




template
<typename BufferType>
void EasyRadio<BufferType>::assign(BufferType *buf)
{
	Assert(lowerOut!=0);
//	if (this->isBusy())
//	{
//		buf->downRef();
//		return
//	}
	sendCount++;
	uint8 psSize = buf->getLength();//should return size of data
	buf->push(psSize);
	buf->push('@');
	this->lowerOut->assign(buf);
}

template
<typename BufferType>
void EasyRadio<BufferType>::notify()
{
//	this->state=WAITING;
	sendCount--;
	if (busyOut)
	{
		this->busyOut->assign(isBusy());
	}
}

template
<typename BufferType>
void EasyRadio<BufferType>::assign(char byte)
{
	if (receiving) {
		Assert(recStack != 0);
		Assert(rxpos != 0);

		*rxpos = byte;
		rxpos++;
		recCount--;
		// did we receive everything
		if (recCount == 0) {
			//throw packet above
			upperOut->assign(recStack);
			recStack = 0;
			receiving = false;
			rxpos = 0;
		}
		return;
	}

	if (byte == '@') {
		isHeader = true;
		return;
	}

	if (isHeader) {
		isHeader=0;
		recStack = new (pool) BufferType;
		Assert(recStack);

		if (recStack) {
			recCount = byte;
			rxpos=(uint8*)recStack->alloc(recCount);
			if(!rxpos)
			{
				return;
			}
		}
		receiving=true;
		return;
	}
}


template
<typename BufferType>
inline bool EasyRadio<BufferType>::isBusy()
{
	return (bool)receiving || (bool)sendCount;
}

} // end namespace reflex

#endif

