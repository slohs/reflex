#ifndef CFAdapter_h
#define CFAdapter_h

/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	CFAdapter
 *
 *	Author:		Karsten Walther
 *
 *	Description: Class for transparent use of the Avisaro CF-Modul.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/sinks/Queue.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/memory/SizedFiFoBuffer.h"
#include "reflex/io/OutputChannel.h"
#include "conf.h"

using namespace reflex;

/** This adapter forwards a data packet to the CF-Modul. This requires
 *  an open file packet, a begin transmission packet, a data packet and
 *  a close packet. Otherwise data are buffered in the modul and not written
 *  immediately to the CF-Card, which could cause loss of data. Main drawback
 *  is the overhead of such a solution, but no flush operation is implemented
 *  on the external modul.
 */
class CFAdapter : public Activity {
public:
	/**
	 *
	 *  @param logfileName : is the name of the file in which the data are
	 *  written. Note: The file must exist in toplevel of Fat16 formatted
	 *  CF-Card. Despite operation for traversing directories exist, never
	 *  got working writing to files, which are not in toplevel.
	 */
	CFAdapter(char* logfileName) :
		input(this),
		logfileName(logfileName)
	{
	}

	/** Method for connecting the adapter to an output device. Also
	 *  initializes the required packets for open, start and close operation
	 *
	 *  @param output : output device which is connected to the avisaro modul
	 */
	void init(Sink1<Buffer*>* output);

	/** Implements the Activity interface.
	 *
	 *  UNFINISHED - prevent overlapping by use of lock and unlock.
	 */
	virtual void run();

	/** Buffer for packets, which must be propagated to the CF-Modul.
	 */
	Queue<Buffer*> input;

private:
	Sink1<Buffer*>* output;

	/** The packets for open, start and close of a write of data
	 */
	SizedFiFoBuffer<IOBufferSize> openPacket;
	SizedFiFoBuffer<IOBufferSize> writePacket;
	SizedFiFoBuffer<IOBufferSize> closePacket;

	/** Name of file in which data are stored
	 */
	char* logfileName;
};

#endif
