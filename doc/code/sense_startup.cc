void* startup()
{
	Complex* comp = new (getApplication().memoryManager)
						Complex(getApplication().pool);
	comp->init(&getApplication().adc.input, 
			   &getApplication().radio.input);
	getApplication().adc.init(&comp->ADCin);
	return (void*) comp;
}