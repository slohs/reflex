
#include "reflex/scheduling/Activity.h"
#include "reflex/sinks/Event.h"
#include "Mappings.h" //contains definitions of CHANNEL_FRIDGE_TEMP

//let SampleControl be a schedulable entity
class SampleControl : public Activity {
public:
	SampleControl() : input(this) //let the input trigger this object
	{
	}

	//Remember run is only called if a value was assigned to the input
	virtual void run()
	{
		output->assign(CHANNEL_FRIDGE_TEMP);
	}

	Event input;  //Used for notification when it is time to sample
	Sink1<char>* output;   //pointer to input of the next processing component
						   //mostly some kind of an AD-Converter
};

2)
#include "reflex/sinks/Sink.h"
#include "Mappings.h" //contains definitions of CHANNEL_FRIDGE_TEMP

//let SampleControl be an event channel
class SampleControl : public Sink0 {
public:
	//implements the interface for dataless events (Sink0)
	virtual void notify()
	{
		output->assign(CHANNEL_FRIDGE_TEMP);
	}

	Sink1<char>* output;
};
