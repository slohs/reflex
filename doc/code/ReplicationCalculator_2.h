class IReplicationCalculator
{
public:
	virtual bool targetReached() = 0;

	virtual void handleAcknowlegde(reflex::Buffer* ack) = 0;

	virtual void getAdvertiseUpdatePDUBuffer(varid_t varid, nodeid_t owner, 
											 size_t size, timestamp_t timestamp, 
											 SharingType type, 
											 reflex::Buffer* destBuf) = 0;

	virtual void getPlainUpdatePDUBuffer(varid_t varid, nodeid_t owner, 
										 size_t size, timestamp_t timestamp, 
										 SharingType type, 
										 reflex::Buffer* destBuf) = 0;

};