void InterruptTest::removeInterrupt()
{
	this->disable();
	getSystem().guardian.registerInterruptHandler(
										&InterruptHandler::invalidInterruptHandler,
										mcu::interrupts::PORT2);
}