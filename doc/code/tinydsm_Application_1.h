...
#include "tinydsm/SharedVariable.h"
#include "tinydsm/Functionalities.h"

using namespace tinydsm;
using namespace reflex;

class Application {
public:
	Application() : sharedVariable(1, 0), ... {
	... 
	}	

	void init() {
		sharedVariable.start();	
	}

	void processRead() {
		sharedVariable.read(answerSink);
	}
	
	void processReadAnswer() {
		QueryAnswer<uint16> answer = answerSink.get();
		if(answer.isDataValid()) {
			uint16 foo = answer.data;
			...
		}
	}	
	
	void processWrite() {
		sharedVariable.write(testValue++);
	}
	...
private:
	SingleValue<QueryAnswer<uint16> > answerSink;
	SharedVariable<QueryFunctionality<UpdateFunctionality<DataType<uint16> > > >
		sharedVariable;
};