#general Sources for the application
CC_SOURCES_APPLICATION += \
	src/fridgeControl.cc \
	src/Converter.cc \
	src/main.cc

#sources needed from lib directory in Reflex
CC_SOURCES_LIB += \
	timer/PeriodicTimer.cc

#include definition of platform specific sources for this application
sinclude $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk

