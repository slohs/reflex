CC_SOURCES_APPLICATION += \
src/main.cc \
src/msgInterpreter.cc \
src/radioDelay.cc 

CC_SOURCES_COMPONENT_APPLICATION_COMPLEX = \
src/Complex.cc \

CC_SOURCES_COMPONENT_PLATFORM_TEST = \
io/Buttons.cc \

CC_SOURCES_LIB += \
memory/FreeList.cc \
memory/Pool.cc \
memory/Buffer.cc \
memory/PoolManager.cc \

sinclude $(APPLICATIONPATH)/platform/$(PLATFORM)/Sources.mk