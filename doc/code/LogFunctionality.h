template<class functionality>
class LogFunctionality : public functionality {
public:
	typedef typename functionality::Type Type;

	void start()
	{
		functionality::start();
	}
	
	LogFunctionality(varid_t id, nodeid_t owner, SharingType sharingType)
		: functionality(id, owner, sharingType)	{ }

protected:
	virtual void process(reflex::Buffer* buf) {
		TinydDSM_Message * message = (TinydDSM_Message*)buf->getStart();
		
		switch(message->opcode)
		{
		case OPCODE_PLAIN_UPDATE:
			cerr << "An plain update message was received." << endln;
			break;
		
		case OPCODE_ADVERTISE_UPDATE:
			cerr << "An advertise update message was received." << endln;
			break;
		...
		default:
			cerr << "Unknown Message!" << endln;
		}

		functionality::process(buf);
	}
};

//using
SharedVariable< LogFunctionality< DataType< uint16 > > > loggingVariable(...);
