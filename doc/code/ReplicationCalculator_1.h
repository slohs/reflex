class IReplicationCalculator {
public:
	void setReplicationRange(hops_t range) {
		replicationrange = range;
	}
	
	hops_t getReplicationRange() {
		return replicationrange;
	}
	...
private:
	hops_t replicationrange;
};
