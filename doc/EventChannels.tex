%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% REFLEX - Real-time Event FLow EXecutive
%
% A lightweight operating system for deeply embedded systems.
%
% This file is a basic documentation of the system part of Reflex.
%

% Authors: Karsten Walther
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Event Channels}
Since event flow in {\sc Reflex} is asynchronous, buffers are needed
between components. Buffers store data or events and trigger the corresponding
activity. Writing to buffers is interpreted as raising an event. It is possible
that multiple sources write to one buffer, but only the corresponding activity
is allowed to read. All buffers implement the {\tt Sink} interface.
The most important buffer types, {\tt Event}, {\tt SingleValue}, {\tt Fifo} and
{\tt Queue}, are already provided. The inheritance graph for them is shown
in figure \ref{sinks}. The base class {\tt Sink0} represents a dataless sink
and declares the {\tt notify()} method.
In contrast, {\tt Sink1<T>} declares the ({\tt assign(T)})
method which allows assigning a value to derived sinks.
Since buffers implement a sink interface they can be written. For reading them,
they additionally implement the {\tt get()} method.
Note, that the application programmers are free to implement specialized versions
of sinks for their purposes, as shown in \cite{WHN04}.

\begin{figure}[htb]
\centering
\includegraphics[angle=90,width=7cm]{figures/SinkUML}
\caption{Buffer types inheritance graphs}
\label{sinks}
\end{figure}

An {\tt Event} is only a pseudo-buffer since it stores no data. It only triggers the
corresponding activity. This is useful, for example, for clock ticks which
must be propagated. {\tt SingleValue} buffers store exactly one value.
The value can be overwritten, so a new value
replaces the old one. The corresponding activity is triggered when a new value
is written on the buffer and the previous value was already read. This is an optimization
since it makes no sense to read the same data twice. The next buffer type is the {\tt FIFO}, which
stores {\tt m} n-tuples and rejects assignment when it is full. The
{\tt Queue} can store chain-able elements, therefore it is never full.


The buffers are implemented as templates,
and can therefore store any data type, while providing a type safe interface.
Event propagation is done by either writing data to a sink or calling the {\tt
notify()} method of dataless event channels.
The sink is responsible for the scheduling of its corresponding activity. The execution
in the graph is inherently asynchronous, since the scheduled activities are
dispatched sometime in the future and not immediately after scheduling. The components are connected by event channels,
which can propagate pure events and data. When a buffer receives an event it
triggers the corresponding activity.
