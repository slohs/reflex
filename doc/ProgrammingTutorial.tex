\chapter{Programming with Reflex step by step}

This section describes the way from scratch to a {\sc Reflex} application. The
example application is a simple fridge control, which samples the inner temperature
once a second and controls the compressor. The event flow structure is given
in figure \ref{simpleFridge}. Of course it is a very simple application, therefore do not
wonder if some structures seem to be oversized, if you look at more complicated
applications like the heating control, it should become clear why this structures are needed.

\begin{figure}[htb]
\centering
\includegraphics[angle=90,width=8cm]{figures/simpleFridgeFlow}
\caption{Event flow graph for a simple fridge control unit}
\label{simpleFridge}
\end{figure}

\section{Setting up the structure}
For simplicity the shell commands are given with a comment after each command.
Of course you can also copy an application from the {\sc Reflex} directory and adopt
it to your needs. Note, that you can use other structures for your application
directory. Files which are mandatory are marked with (m). Files which are mandatory
at a special place are marked with (m+).

{\tt
>mkdir fridgeControl  //create the application directory wherever you want  \\
>cd fridgeControl \\
>mkdir include //a folder for header files\\
>mkdir src //a folder for src files\\
>mkdir platform //a subfolder which increases portability of the app.\\
>mkdir platform/CardS12 //create a folder for CardS12 platform\\
>mkdir platform/CardS12/include //and a subfolder for headers\\
>mkdir platform/CardS12/bin	//a folder for the binaries and the Makefile\\
>touch Sources.mk //contains list of source files (m+)\\
>touch src/main.cc //standard main (m)\\
>touch include/FridgeControl.h //Header of FridgeControl\\
>touch src/FridgeControl.cc //Implementation of FridgeControl\\
>touch include/Converter.h //Header of Temperature Conversion Component\\
>touch src/Converter.cc //Implementation of Temperature Conversion\\
>touch platform/CardS12/include/NodeConfiguration.h //the master plan file (m)\\
>touch platform/CardS12/include/conf.h //place for static configurations\\
>touch platform/CardS12/include/Mappings.h //defines to which ports sensors and actuators are connected\\
>touch platform/CardS12/Sources.mk //list of platform/controller specific sources\\
>touch platform/CardS12/bin/Makefile //controls the make process (m+)\\
}

\section{Implement the Logic}
The following listing shows the implementation for the fridge control logic.
It starts the compressor if the temperature is above a given temperature and
stops the compressor if it is below a second temperature. Note that the code
is not split in header and source file here for better overview.

\lstinputlisting[caption={Implementation of the fridge control},label=fridgeControlImpl]{code/FridgeControl.h}

The code for the fridge control logic is straight forward and free of any
explicit synchronization or code for adjusting to a special scheduling scheme.
Further it does not have to know about how the temperature sensor or the compressor
are connected to the microcontroller. The intelligent input stores the assigned
temperature until it is evaluated and triggers the fridge control object when
a value is assigned to the input.

Listing \vref{sampleControlImpl} shows the implementation of the sample control component in
two versions. In the first implementation it is a schedulable object, in the second
an event channel. The second version causes less overhead but could lead to long delays
for other components if many of such event channel components are
in a chain.

\lstinputlisting[caption={Implementation of the sample control},label=sampleControlImpl]{code/SampleControl.h}

\section{Build the application}
The master plan of the application is given in the file NodeConfiguration.h.
A NodeConfiguration object contains all the components of the application as
members and makes the initial connections and parametrizations in the constructor.
The advantage of such a scheme is that an initialization order is defined and
in network simulators multiple instances of a NodeConfiguration can be simulated
in one address space. For the fridge control application the NodeConfiguration
is given in listing \ref{nodeConfFridge}.

\lstinputlisting[caption={Masterplan of the fridge control},label=nodeConfFridge]{code/NodeConfigurationFridge.h}

\section{Setting Up The Make Environment}
For compilation a configuring Makefile has to be provided by the application
programmer. This has to define several variables which are used by the
Makefiles of the system, controller and platform. An example is given
in listing \ref{MakefileFridge}. The first part defines mandatory variables
which are needed for compiling a {\sc Reflex} based application. The second
part defines variables which are specific to the used controller and platform.
Note, if the platform requires variables which are not defined this is
indicated when you compile the application, if the are defined the current value is
printed out. In the last part the standard Makefile of {\sc Reflex} is included.
It controls the whole compilation process.

\lstinputlisting[language=make,caption={Makefile for fridge control},label=MakefileFridge]{code/MakefileFridge}

A second file which is needed is the file for the source file definition. It
has to be placed in the toplevel of the application and has to be named {\tt Sources.mk}.
An example for that is file is given in listing \ref{SourcesFridge}.

\lstinputlisting[language=make,caption={Source file definitions for fridge control},label=SourcesFridge]{code/SourcesFridge.mk}

Note that
source from the application path must be specified relative to application root,
sources from Reflex (system, lib, controller, platform and lib) only need to
be specified relative to the next higher src/reflex directory, but must be added
to the corresponding {\tt SOURCES} variable. The following {\tt SOURCE} variables
exist.

\begin{itemize}
\item CC\_SOURCES\_CONTROLLER
\item CC\_SOURCES\_PLATFORM
\item CC\_SOURCES\_SYSTEM
\item CC\_SOURCES\_LIB
\item CC\_SOURCES\_APPLICATION
\item EXTERNAL\_CC\_SOURCES\_APPLICATION
\end{itemize}

\section{Advanced Concepts in Programming with Reflex}
\subsection{Components with multiple inputs}
Suppose now that our simple fridge control should be extended, so that it controls
the fridge and a froster. The system now contains two temperature sensors, a compressor
and two electronic valves. The control logic now has to differentiate between
2 temperature inputs. This can be solved by using {\tt ActivityFunctors}, these
act as activities ans call a specified method on the component class. For the
fridge control this looks as shown in listing \ref{fridgeControlMultImpl}.

\lstinputlisting[caption={Implementation of the fridge control with distinguishable inputs},label=fridgeControlMultImpl]
{code/FridgeControlMultiple.h}

\subsection{Changing the Scheduling Scheme}
Now the fridge control showed their applicability and a fridge producer, wants
to have a control which is able to handle the fridge wall in a supermarket,
consisting of numerous fridges and freezers, furthermore for monitoring all
switching activities should submitted to a host PC. The structure for such a
system could be as shown in figure \ref{fridgeWall}

\begin{figure}[htb]
\centering
\includegraphics[angle=90]{figures/fridgeWall}
\caption{Extended version of the fridgeControl for a fridge wall }
\label{fridgeWall}
\end{figure}

You see that the structure has changed there is a {\tt FridgeControl} component
for each fridge/freezer, so that the system is easily extensible. All these
submit their control data for the corresponding electro-magnetic valves to a mediator, which dresses
the data for the output ports and switches off the compressor if no valve is
open. All components use a standard output object which allows formatted output
({\tt OutputChannel}) and propagates whole message buffers to the connected
serial interface. If the system would be scheduled with in Fifo-style there
would be a pause between transmission of messages, which could lead to loss
of messages in phases of high load due to buffer restrictions. Therefore the
serial interface must get a higher priority than the other components.

This is fairly simple to reach, you only have to assign priorities to the components
in the {\tt NodeConfiguration} and switch to priority based
scheduling in the {\tt Makefile}, as shown in listings \ref{NodeConfigurationFridgePrio} and
\ref{MakefileFridgePrio}

\lstinputlisting[caption={Assignment of priorities in the NodeConfiguration},label=NodeConfigurationFridgePrio]{code/NodeConfigurationFridgePrio.h}

\lstinputlisting[language=make,caption={Selection of another scheduling scheme in the Makefile},label=MakefileFridgePrio]{code/MakefileFridgePrio}

\subsection{Porting the Application to another Platform}
Now suppose the fridge control became popular and it should be used for any cooling
device from air conditioners to cryogenic body conservers. It is hard to believe
that all these work with the same microcontroller but even if you stay with the
fridge only; at sometime there may be reasons to switch the controller platform.
For the fridge control application this is fairly simple, just copy the CardS12
directory under fridgeControl/platform, let's say to Mega128. This board features
an Atmel Atmega128 microcontroller.

Now identify the platform specific sources, which indeed are the drivers.
The {\tt ADConverter} of the Atmel has the same signature
like that of the HCS12. The ports are numbered on the new architecture, but due to the event
flow interface they can be used like the PortAB on the HCS12. The Converter can
be the same since the Atmega as well as the HCS12 have 10bit resolution. The resulting
NodeConfiguration look as follows.

Nodeconf here and now

In the {\tt Sources.mk} file below platform only the entry for the port driver has to be changed.
Also the changes in the Makefile are minor, just set the {\tt PLATFORM} variable
to Mega128 and the platform specific variables.

That's it, now you can download the application to the Mega128 board.

\subsection{Implementing drivers}
There are implemented many drivers for {\sc Reflex} already. Nevertheless
sometimes you need to implement a driver because it does not exist or you need
a specialized version of an existing one. This section reflects how we intend
to write a driver.
Suppose for the fridge control we would like to have a watchdog timer, because
the fridge must run for a lifetime without service and a watchdog is always good
for reliability.

At the beginning we should implement an abstraction for the watchdog hardware.
Since the devices on microcontrollers are mostly memory mapped, we just define such a map. (Usually this
mapping is called ...Registers.h). For the Watchdog of the HCS12 this mapping can
be found in CRGRegisters (CRG = Clock Reset Generator) since the CRG contains
the watchdog functionality. Listing \ref{ClockResetGeneratorRegisters} shows this mapping.

\lstinputlisting[language=make,caption={Register definitions for the HCS12's ClockResetGenerator unit}, label=ClockResetGeneratorRegisters]{code/ClockResetGeneratorRegisters.h}

The driver itself will hold a pointer to this register-set to work on it. Note
that this scheme has several advantages over the often used {\tt \#define} of registers.
First with struct there is no pollution of the global namespace. Second the driver
code for multiple instances of a unit (2 AD-Converters, 2 Serial Interface) is
the same. Also the overhead is not a problem since the pointer can be a constant
if only one device is present on a controller, thus the controller can avoid the
stage of indirection at compile time.

The driver for the watchdog now must have an interface for reset - a notification
interface would be fine. Furthermore the interrupt of the watchdog should be handled
to allow signalling this condition before resetting the microcontroller. The resulting
driver is shown in listing \ref{Watchdog}.

\lstinputlisting[language=make,caption={Register definitions for the HCS12's ClockResetGenerator unit}, label=Watchdog]{code/Watchdog.h}

When the watchdog is instantiated it sets its timeout to the maximum. Before
the timeout occurs the driver must be notified to reset the watchdog. If the
notification is not done a microcontroller reset is done.
