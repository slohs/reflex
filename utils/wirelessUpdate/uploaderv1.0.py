#
# Author: Hannes Menzel, 2013
# uploader.py
# tested on pyhton version 2.6
#
# performs a wireless update based upon a  given hexfile

# usage (all fields needed):
# python2.6 uploader.py yourImage.hex startAddress versionNumber
# yourImage.hex: the hexfile, which has to be copied onto the wireless device
# startAddress: the start address in your hex-file where the __codeStart__ -Label points to
# versionNumber: the version number of the update
#


try:
    import serial  # Python2
except ImportError:
    from serial3 import *  # Python3
import array
import datetime
import csv
import sys
import platform
from struct import *
from time import *
from intelhex import IntelHex

timebase =  time()


#######################################Konstanten##################################

#commandos
COMANND_TYPE_SETTIME = 1
COMANND_TYPE_SEND_DATA = 2
COMANND_TYPE_REQUEST_PACKET = 3
COMANND_TYPE_DELETE = 4
COMANND_TYPE_RADIO_OFF = 5



#states
STATE_UNDEF = 0
STATE_RECEIVE_DATA = 1 
STATE_DATA_REQUESTED = 2
STATE_DELETING = 3
STATE_DELETED = 4
STATE_RADIO_OFF=5



#######################################Variablen initialisieren##################################

state=STATE_UNDEF
packetsReceived = 0
numberOfPackets = 0
Messages=[]
#######################################AP Commands##################################



def startAccessPoint():
    return array.array('B', [0xFF, 0x07, 0x03]).tostring()# startMarker, Command, PacketSize

def stopAccessPoint():
    return array.array('B', [0xFF, 0x09, 0x03]).tostring()

def switchChannel(channel):
    return array.array('B', [0xFF, 0x074, 0x04, channel]).tostring()# startMarker, Command, PacketSize, channelNo

def startTX():
    return array.array('B', [0xFF, 0x75, 0x03]).tostring()# startMarker, Command, PacketSize

def stopTX():
    return array.array('B', [0xFF, 0x76, 0x03]).tostring()# startMarker, Command, PacketSize
    
def changeTXPower(power):
    return array.array('B', [0xFF, 0x77, 0x04, power]).tostring()# startMarker, Command, PacketSize, power

def sendData(data):
    x = array.array('B', [0xFF, 0x78, 0x03 + len(data)])# startMarker, Command, PacketSize, data (n bytes)
    for i in range(len(data)):
        x.append(data[i])
    return x.tostring()


def startOwnProtocol():
    return array.array('B', [0xFF, 0x7A, 0x03]).tostring()# startMarker, Command, PacketSize, 

def stopOwnProtocol():
    return array.array('B', [0xFF, 0x7B, 0x03]).tostring()# startMarker, Command, PacketSize, 

def getData():
    return array.array('B', [0xFF, 0x79, 0x03]).tostring()# startMarker, Command, PacketSize, 

def checkCommand(text):
    #time.sleep(0.01)
    sleep(0.01)
    erg = ser.read(100)
    if len(erg) >= 2:
        if erg[1] == 0x06:
            print (text, " ok: ", len(erg))
        else:
            print (text, " failed", "error code: ", erg[1])
    else:
        print (text, " failed", "length: ", len(erg))


def waitForMsg():
    MSGlength = 0
    while MSGlength < 5:
        ser.write(getData())
        TempData = ser.read(100)   
        MSGlength = len(TempData)
    return TempData

def interpreteMsg(msg):
    global state
    global packetsReceived
    global Messages
    global numberOfPackets
    global timebase
   

def buildUpdateHeader(u_type, progID, res, override, version, reserved):
    progID = (progID << 2) | (res << 1) | override
    return array.array('B', [u_type, progID, version, reserved])
        

done = 0

def destPort():
    global done
    ser.write(startAccessPoint())
    tester = ser.read(100) 
    if len(tester)  == 0 :
        done = 0
    else:
        done = 1

if (len(sys.argv) != 4):
    print "ERROR: expecting 3 arguments: [hexfile] [address of __codeStart__] [programID]"
    sys.exit(0)
else:
    hexFile = sys.argv[1]
    __codeStart__ = int(sys.argv[2], 16)
    __progID = int(sys.argv[3])
    print "\nreading", hexFile, "on address (hex / decimal): ", sys.argv[2],"/", __codeStart__


###############################
# try to connect to accesspoint
###############################

if platform.system() == "Darwin":
 #   global done
#while not done:
    try:
        ser = serial.Serial("/dev/tty.usbmodem001",115200,timeout=0.0025)
        #done = 1
        destPort()
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/tty.usbmodem002",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial("/dev/tty.usbmodem003",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/tty.usbmodem004",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
elif platform.system() == "Linux":
 #   global done
    try:
        ser = serial.Serial("/dev/ttyACM0",115200,timeout=0.0025)
        #done = 1
        destPort()
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/ttyACM1",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial("/dev/ttyACM2",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass
    if done == 0:
        try:
            ser = serial.Serial("/dev/ttyACM3",115200,timeout=0.0025)
            #done = 1
            destPort()
        except serial.serialutil.SerialException:
            pass  
elif platform.system() == "Windows":
  #  global done
   
    try:
        ser = serial.Serial(0,115200,timeout=0.0025)
        destPort()
        #done = 1
    except serial.serialutil.SerialException:
        pass
    if done == 0:
        try:
            ser = serial.Serial(1,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(2,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass
    if done == 0:
        try:
            ser = serial.Serial(3,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(4,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(5,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(6,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(7,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(8,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(9,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(10,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(11,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(12,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(13,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(14,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(15,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(16,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(17,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(18,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass  
    if done == 0:
        try:
            ser = serial.Serial(19,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 
    if done == 0:
        try:
            ser = serial.Serial(20,115200,timeout=0.0025)
            destPort()
            #done = 1
        except serial.serialutil.SerialException:
            pass 

if done == 0:
    print( "ERROR NO ACCESS POINT FOUND!")
    sys.exit(0)
else:
    print ("connected to access point")
accel = ser.read(100) #clear serial port




#######################################"main"##################################




#Start access point
ser.write(startAccessPoint())
ser.write(startOwnProtocol())
#checkCommand("startOwnProtocol")
#ser.write(switchChannel(1))   
#ser.write(switchChannel(0))   
#for item in [0]:

startAddress = __codeStart__
override = 1
version = 1
progID = __progID
res = 0
update_type = 1
reserved = 0
dataSize = 20


##################### hex file stuff###############################


ih = IntelHex()
ih.loadfile(hexFile,format='hex')
ih.start_addr = {'IP': startAddress}
firstAddress = ih.start_addr['IP']

##get last address with data####
lastAddress = 0xffff
#lastAddress = ih.start_addr['IP']
#for addr in range(ih.start_addr['IP'],0xffff, 1):
#    if ih[addr] != 255:
#        lastAddress=addr


##################### build header ###############################
#quint16 id; // fixed length identifier (md4-hash of name)
#quint16 info; // flags how to treat the component
#quint16 format; // msp430-elf32, msp430-elf16
#quint16 startup; // locates startup function
#quint16 shutdown; // locates shutdown function
#quint16 file; // pointer to component file
#quint16 sizeof_file; // size of binary data
#quint16 size; // overall size of component's binary data

#info data
PRELINKED = 0x01
RELOCATABLE = 0x02
PERSISTENT = 0x04
REPLACE = 0x08
AUTOSTART = 0x10

#formate data
ELF32_MSP430 = 5

# comands
RECEIVE = 1
INSTALL = 2
UNINSTALL = 3
STARTUP = 4
SHUTDOWN = 5
LIST = 6

#header = {'name' : 0,
#          'file' : 0,
#          'format' : ELF32_MSP430, 
#          'sizeof_file' : 0, 
#          'size' : 0, 
#          'id' : 0,
#          'info' : 0, 
#          'startup' : 0, 
#          'shutdown' : 0}

header_file = firstAddress
#header_info = PRELINKED | PERSISTENT | AUTOSTART
header_info = PRELINKED | PERSISTENT
header_id = 1
header_name = "TES"
header_sizeof_file = lastAddress - firstAddress
header_size = lastAddress - firstAddress + len(header_name) + 1
header_startup = 0xdda2
header_shutdown = 0xdd0e
header_format = ELF32_MSP430







##################### send header ###############################
print ""
print "########### header of Hex-file ###########"

print (header_info)
print (header_id)
print (header_format)
print (header_startup)
print (header_shutdown)
print (header_sizeof_file)
print (header_file) 
print (header_name)
print (header_size)
print (header_startup)
print (header_format)


data2sendHeader = [ 
    (header_id & 0x00FF),
    (header_id>>8),
    (header_info & 0x00FF),
    (header_info>>8),
    (header_format & 0x00FF),
    (header_format>>8),
    (header_startup & 0x00FF),
    (header_startup>>8),
    (header_shutdown & 0x00FF),
    (header_shutdown>>8),
    (header_file & 0x00FF),
    (header_file>>8),
    (header_sizeof_file & 0x00FF),
    (header_sizeof_file>>8),
    (header_size & 0x00FF),
    (header_size>>8),
    ord(header_name[0]),
    ord(header_name[1]),
    ord(header_name[2]),
    ]



for laufzeitTest in range(1, 100, 1):
    ##############
    # prepare header + other needed variables
    ##############
    update_type = 1
    if((laufzeitTest % 3) == 0):
        progID += 1
    header = ""
    header = buildUpdateHeader(update_type, progID, res, override, version, reserved)
    print ""
    print "################# check header ##################"
    print "Update_init header: ", len(header)
    for i in range(0, len(header), 1):
        print hex(header[i])
    sleep(1)
    print "done"

    
    dataBytes = (lastAddress - startAddress) # because last adress contains data, too
    noOfPackets = (dataBytes / dataSize)
    remainder = (dataBytes % dataSize)
    if remainder != 0:
        noOfPackets += 1
    packetCount_run = (3 << 0xE) | noOfPackets #3 runs  + noOfPackets 

    print "\n################# general infos ##################"
    print ("firstAddress: ( hex / decimal)", hex(firstAddress), firstAddress)
    print ("lastAddress: (hex / decimal)", hex(lastAddress), lastAddress)
    print "data to transmit:", dataBytes
    print "number of packets: ", noOfPackets
    print "remainder: ", remainder
    print "dataSize: ", dataSize
    ##############
    # just for testing purposes
    ##############
    #header = buildUpdateHeader(3, 10, 0, 1, 9, 0)
    #for z in range(0, 1, 1):
    #    i = 662
    #    print "SeqNo ", i
    #    header = buildUpdateHeader(3, 10, 0, 1, 9, 0)
    #    seqNo = array.array('B', [0, 0])    
    #    seqNo[0] = i & 0xff
    #    seqNo[1] = (i >> 8) & 0xff    
    #    payload = ih.tobinarray(startAddress + (dataSize * i), size=dataSize)
    #    print "leng:(h, s, p, sum) (",len(header), len(seqNo), len(payload), len(header) + len(seqNo) + len(payload), ")"
    #    ser.write(sendData(header + seqNo + payload))
    #    text = ""
    #    a = 0
    #    for a in range(0, dataSize, 1):
    #	    text += hex(ih[37888 + (dataSize * i) + a]) + " "
    #	    if a == 5:
    #		    text += '\''
    #    print ""
    #    print ("addr: ", 37888 + (dataSize * i), "; seqNo: ", i, "; data: \n", text)    
    #    sleep(0.5)


    ##############
    # send init-packet to force sensor node into update mode
    ##############

    # hopcount, updateDelay, hopDelayOffset, networkDiameter
    payload = array.array('B', [0x0, 0x0, 0x0, 0x0])
    update_type = 1
    header = buildUpdateHeader(update_type, progID, res, override, version, reserved)
    print ""
    print "########### sending update init packet ###########"
    print "updateInit length: ", len(header) + len(payload)
    print "data: ", header + payload
    for i in range(0, 3, 1):
        print "send no. ", i
        ser.write(sendData(header + payload))
        sleep(1)
        ser.read(100)
    print "done"


    ##############
    # send meta info packet to submit all needed information
    ##############
    # probability, remainder, packetCount_run(lo), packetCount_run(hi)
    payload = array.array('B', [0x0, remainder, packetCount_run & 0xff, (packetCount_run >> 8) & 0xff])
    update_type = 2
    header = buildUpdateHeader(update_type, progID, res, override, version, reserved)
    print ""
    print "########### sending update meta packet ###########"
    print "remainder: ", remainder, "noOfPackets: ", noOfPackets
    print "updateMeta length: ", len(header) + len(payload)
    print "data: ", header + payload
    for i in range(0, 3, 1):
        print "send no. ", i
        ser.write(sendData(header + payload))
        sleep(1)
        ser.read(100)
    print "done"


    ##############
    # sending data packets
    ##############
    update_type = 3
    header = buildUpdateHeader(update_type, progID, res, override, version, reserved)
    seqNo = array.array('B', [0, 0])
    i = 0
    exitNo = 1000000
    print ""
    print "########### sending update data packets ###########"
    for addr in range(ih.start_addr['IP'],lastAddress, dataSize):
        seqNo[0] = i & 0xff
        seqNo[1] = (i >> 8) & 0xff
        i += 1
        text = ""
        a = 0
        for a in range(0, dataSize, 1):
	    text += hex(ih[addr + a]) + " "
	    if a == 5:
		    text += '\''

        print ""
        print ("addr: ", addr, "; seqNo: ", i-1, "; data: \n", text)    
        payload = ih.tobinarray(start=addr, size = dataSize)
        print "leng:(h, s, p, sum) (",len(header), len(seqNo), len(payload), len(header) + len(seqNo) + len(payload), ")"
        ser.write(sendData(header + seqNo + payload))
        sleep(0.05)
        ser.read(100)   #read pending data
        if (i-1) == exitNo:
            print ("exit after seqNo ",exitNo, " -> just a test")
            sys.exit(0)

    #print "############################"
    #print "run no: ", laufzeitTest
    #print "############################"
    ###wait for answer
    sleep(3)
    print ("check StartUp")
    ser.write(getData())
    data = ser.read(100)
    if len(data) > 4 :
        for item in data:
                print ("%x", ord(item))
        print (":")

sys.exit(0)







