%
%
% REFLEX - Real-time Event FLow EXecutive
%
% A lightweight operating system for deeply embedded systems.
%

% Author:	Karsten Walther
%
% Description: Base LaTex file for the Reflex HOWTO
%
%%%%%%%%%%%%%%%%% Formatierungen %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass[10pt,oneside,english]{scrartcl}
\usepackage{multicol}
\setlength{\columnseprule}{0.3pt}
\setlength{\columnsep}{1cm}



%\usepackage[pdftex]{graphics}
\usepackage{graphicx}
\usepackage{listings,color}   %% color für Farbmarkierungen
   \lstset{language=C++}
\usepackage[american,english]{babel}
\usepackage[latin1]{inputenc} %%direkte eingabe von umlauten
%\usepackage{hyperref} %% erstellt PDF-Inhaltsverzeichnis
\usepackage{varioref}
\usepackage{float}
\usepackage{subfigure}
\usepackage{placeins}


\usepackage[pdftex, bookmarks = true]{hyperref} %% PDF- und HTML-Funktionen
\hypersetup{
colorlinks = true,
linkcolor = black,
citecolor = black,
urlcolor = black,
pdfstartview = FitV,
pdfauthor = {},
pdftitle = {},
pdfsubject = {},
pdfkeywords = {},
pdfcreator = {},
pdfproducer = {}
}


%\typearea[current]{last}

\title{The {\sc Reflex} Programming HowTo}
%\author{Andre Sieber}

%\makeindex

%%%%%%%%%%%%%%%%% Das Dokument %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\linespread{1.2}

\lstset{% general command to set parameter(s)
basicstyle=\scriptsize\ttfamily, % print whole listing small
keywordstyle=\color{blue}\bfseries,
identifierstyle=\color{black}, % nothing happens
commentstyle=\color{green}, % white comments
stringstyle=\color{red}, % typewriter type for strings
showstringspaces=false,
frame=topline|bottomline,
backgroundcolor=\color{white},  % no special string spaces
captionpos=b,
tabsize=4,
aboveskip=10pt,
belowskip=10pt,
numbers=left,
float
}



\floatplacement{figure}{htb}
\floatplacement{lstlisting}{htb}



\maketitle

\tableofcontents

%\addtocounter{chapter}{1}

%\part{Reflex - getting into it}

\section{Introduction}

This document is an introduction to programming with the {\sc Reflex} ({\bf R}eal-Time {\bf E}vent {\bf FL}ow {\bf EX}ecutive) operating system for embedded systems.
Starting from a simple example the characteristics of the {\sc Reflex} approach are explained while building a simple sensor network application.
%Consecutively from a simple example the characteristics of the {\sc Reflex} approach are explained while building a simple sensor network like application.

{\sc Reflex} implements as operating system the base abstraction of the event flow model. The executable units in this system are so called activities and interrupt handlers. Both have run-to-completion semantics. Activities are scheduleable and managed through a scheduler. The executable units are grouped in components. These have inputs and outputs, which allow them to be connected via event-channels. Inputs have buffer characteristics and are associated with an activity which is scheduled exactly once for every saved datum. Outputs are delegates for the event-channels and used for decoupling of components.

More about the theory of the event flow model behind {\sc Reflex} and details of the system itself can be found in the  {\sc Reflex Whitepaper}. Hints and information regarding the installation of the toolchains for the supported platforms can be found in the project WIKI located at {\sc http://idun.informatik.tu-cottbus.de/reflex/}

\vspace{\baselineskip}
{\bf This document is based on {\sc reflex} 1.7.}

\section{Where is what? - the directory structure of Reflex}


The directory structure of {\sc Reflex}, shown in listing \ref{reflex_struct_small}, splits the bunch of code into the parts which are described below. The main intention was to build a structure which allows re-use and portability of code.



\lstinputlisting[language=make,caption={Reflex main directory structure},label=reflex_struct_small,frame=single]{code/reflex_structure_small.txt}

\begin{description}
\item[system] This directory contains the code for the base system (Scheduler, InterruptGuardian, Energy Management ...) as well as some fundamental classes for effective event flow programming.
\item[lib] Within this directory code which is not part of the basic system, but often used, like memory management functionality or virtual timers are located.
\item[controller] Here the microcontroller device specific parts, like environment files for compilation and drivers for the on-chip devices of the microcontroller e.g. the timers, I/O can be found.
\item[platform] This directory contains board specific environment files for compilation and download. Additionally driver code for devices on the board e.g. LEDs can be found here. The differentiation from controller is done because numerous boards/platforms exists that use the same controller but with different clock speed and different components on the board or diverse port layouts.
\item[devices] Drivers for external devices which are connected via a standard interface, such as RS232 or I2C, and can occur on various platforms are located in this directory.
\item[applications] This directory contains some sample applications.
\end{description}


Like the whole system an application is divided into a platform independent and a platform dependent part. The independent part, which should be mainly the components of the application itself, without the drivers, is located in the {\tt include} and {\tt src} directories. The dependent application part is located in the subdirectories beneath the {\tt platform} directory. An example is shown in listing \ref{app_struct}

\lstinputlisting[language=make,caption={Sample directory structure},label=app_struct]{code/reflex_structure_application.txt}

\subsection{The MAKE system}

To compile an application the Makefile template located in the corresponding \\{\tt [platform]/bin} directory can be used. In most cases a copy from this template named "Makefile" is sufficient to build an application.

Source code files that are used in an application must be included in the {\tt Source.mk} files located either in the application directory or in the platform directory.

\section{A simple Application}
To show the basics a simple application transmitting strings using a serial connection is used. The application can be found in the {\tt example/HelloWorld} directory.
Figure \ref{fig::HelloWorld} shows its structure. It consists of drivers for the hardware timer, the serial connection and the application logic itself.


\begin{figure}[htb]
\centering
\includegraphics[width=0.8\textwidth]{pics/HelloWorld.pdf}
\caption{Simple Send Application}
\label{fig::HelloWorld}
\end{figure}

\subsection{The NodeConfiguration}
This is the blueprint of the application. Here all used components are instantiated and connected with each other. Additional configurations of the application, like power management, device driver configuration or priority settings are also done here. The NodeConfiguration is platform dependent since it is not guaranteed that hardware driver components are the same on different platforms.

In listing \ref{list::HelloNodeConf} a snippet of the NodeConfiguration.h is shown
%, demonstrating the instantiation and connection of the application parts
. Not shown are the instructions needed for the power management, details about that can be found in section \ref{powermanagement}.


\lstinputlisting[language=c++,
caption={NodeConfiguration.h of Sample Application}
,label=list::HelloNodeConf]
{code/Hello_NodeConfiguration.h}

Lines 14 to 17 show the {\tt instantiation}, which defines all used components. In lines 5 to 7 the components (or objects in the c++ nomenclature) are {\tt initialized}, if necessary with parameters. The {\tt configuration} is shown in lines 10 and 11.

In this example there are three main components: the serial driver, the application hello and the clock. The clock shown in figure \ref{fig::HelloWorld} is utilized using a {\tt VirtualTimer} and not connected in the NodeConfiguration but within the application code.
%Note that the {\tt clock} is a standard interface for a platform hardware timer connected within the {\tt System.h}. The {\tt clock} has a millisecond granularity. Besides this, the components PoolManager and SizedPool are necessary for the memory management, further described in section \ref{memory}.


Most components use an {\tt init(..)} method for connection purposes, information about that can be found in the doxygen documentation available at the WIKI or in the source code of the specific component.

In this example the output of the hello component is connected with the input of the serial driver by using a pointer. Additionally hello gets a pointer to the memory pool.

\subsection{The Application Code}

Since the executable elements in the event flow are the activities, the application code for the component HelloWorld is derived  from the Activity class. Listing \ref{list::hello_h} shows its header file. As an activity the class HelloWorld gets a  {\tt run()}-method which is called by the scheduler and contains the functional instructions. Additionally HelloWorld has an  {\tt init(..)}-method for connection purposes.

\lstinputlisting[language=c++,
caption={Header of Sample Application}
,label=list::hello_h]
{code/Hello.h}

In lines 19 to 22 the class members are instantiated. The output is a pointer to a Sink of type Buffer. The event {\tt ready} triggers the activity an thus the execution of the {\tt run()}-method. The VirtualTimer will be configured in the .cc to generate a periodic signal, which will occur as the ready event. Finally the pointer to the pool is needed to allocate memory.


In listing \ref{list::hello_c} the related source file is shown. In the class constructor (lines 5 to 10) the timer is set to be periodic, connected to the event and the event is connected to the class itself, so that the firing timer calls the event which triggers the activity. The timer is set to 1000 milliseconds and thus firing once in a second. Further information about the timers can be found in section  \ref{timer}.

\lstinputlisting[language=c++,
caption={Source of Sample Application}
,label=list::hello_c]
{code/hello.cc}

The  {\tt init()}-Method in lines 12 to 16 connects the outer world with the inner objects. The  {\tt run()}-Method is the one called by the scheduler and therefore the actual function of the activity HelloWorld. Within  {\tt run()}, and ff a subsequent component exists and thus the output is connected, a chunk of memory is allocated and the string ''HelloWorld!\(\backslash\)n'' written into it. The pointer to the memory chunk is written to this component input. Further information about memory management in {\sc reflex} can be found in section \ref{memory}

As one can see, the output on the terminal includes the string end character {\sc \(\backslash\)0x00}, since it is part of the string written to the buffer. To avoid this, each character can be written separately, or the predefined OutputChannel described in section \ref{output} can be used.




\section{Component In and Output}
\label{C_IO}

Inputs are public objects of a component and bound to an activity. In most cases the activity is identical with the component, so the bounding process is rather simple:

\[input.init(this);\]

Outputs are pointers to inputs of a specific type. As shown in the example they are connected by using a  {\tt init()}-method.

There are various types of inputs. They all have in common, that they buffer an event (with data or not) and trigger the associated activity, which means that a write to an input buffer leads to the execution of the activity via the chosen scheduling scheme.


The implemented input types are:
\begin{itemize}
\item {\bf Event} - simple data less signal
\item {\bf SingleValue1$<$T1$>$} - 1-type buffer
\item {\bf SingleValue2$<$T1,T2$>$} - 2-type buffer
\item {\bf SingleValue3$<$T1,T2,T3$>$}  - 3-type buffer
\item {\bf Fifo1$<$T1,n$>$} - n-element 1-type buffer
\item {\bf Fifo2$<$T1,T2,n$>$} - n-element 2-type buffer
%\item {\bf Fifo3$<$T1,T2,T3,n$>$} - n-element 3-value buffer
\item {\bf Queue$<$T1$>$} - 1-type queue
\end{itemize}

As the name says, a SingleValue holds one value which can consist of maximal 3 elements (with the same or different data types). A FIFO can hold n elements, where n is specified at declaration time. The queue uses chainable elements.

\section{Memory Management}
\label{memory}

Memory management is based on static allocated regions, called pools. They supply Buffer objects - containers for saving data. Through the static allocation, the size and number of elements (Buffers), is predefined within conf.h. Figure \ref{fig::pools} shows the pool organization while figure \ref{fig::buffer} gives a short overview over the buffer concept.



\begin{figure}[htb]
\centering
\includegraphics[width=0.4\textwidth]{pics/pool.pdf}
\caption{Memory management - pool}
\label{fig::pools}
\end{figure}


\begin{figure}[htb]
\centering
\includegraphics[width=0.8\textwidth]{pics/buffer.pdf}
\caption{Memory management - buffer}
\label{fig::buffer}
\end{figure}

As shown in figure \ref{fig::buffer}, a buffer can be used in two ways: as stack and as FIFO. An initial offset divides the buffer into two parts. This offset is specified at the pool's instantiation time and is being used for every new buffer. Anyway, it is possible to change the offset for each single buffer using the {\tt initOffsets(uint8 offset)}-method. If it is 0, all space is used for the FIFO part.

The buffer interface was designed in respect of packet oriented communication. Through the life time of a packet, headers are read and written with stack semantic, while FIFO semantic is suitable for processing the payload. If the buffer size and its offset is adjusted wisely, no data has to be copied or moved while going through the protocol stack.

To use buffers it is necessary to include the PoolManager and one or more SizedPools into the application via the NodeConfiguration.h as shown in example \ref{list::HelloNodeConf}. The placement new() operator allocates a fresh Buffer and delivers a pointer to it as shown in the example application code \ref{list::hello_h} and  \ref{list::hello_c}.

\begin{itemize}
\item {\bf buffer-$>$write()} - writes data in the fifo part
\item {\bf buffer-$>$get()}  - reads data from the fifo part
\item {\bf buffer-$>$push()} - pushes data onto the header part
\item {\bf buffer-$>$pop()} - pops data from the header part
\end{itemize}

Each buffer contains a reference counter to decide if it is free or in use. If the counter is set to zero, the buffer is released. To increase or decrease it, use the member functions {\tt upRef()} and {\tt downRef()}. This functionality enables multiple activities (e.g. data processor, mac, transceiver) to use the content of the same buffer. None of them needs to concern with cleaning up memory when finished.


\section{Formatted Output}
\label{output}


To provide formatted output on the serial connection the OutputChannel component can be used. It takes care of the buffer management for message output. The OutputChannel has no event flow inputs, it provides only a method based interface, since this is better suited for output purposes. The use of the OutputChannel requires the definition of IOBufferSize and NrOfStdOutBuffers in conf.h. Therefore all channels have the same amount of buffers of the same size.

The OutputChannel can easy be used by the {\tt write()}-method. This puts the data which should be send into a buffer. The send process is implicitly initiated in {\tt writeln()} or alternatively {tt flush()}-method. The difference between these two  is that {\tt writeln()} puts a "\textbackslash n" at the end of the output.


\begin{figure}[htb]
\centering
\includegraphics[width=\textwidth]{pics/formattedOutput.pdf}
\caption{Application using OutputChannel}
\label{fig::formattedOutput}
\end{figure}


Figure \ref{fig::formattedOutput} and listings \ref{list::formattedOutputNodeConf} and \ref{list::formattedOutputCC} show the changes resulting from using the a OutputChannel instead writing directly to the serial driver.

\lstinputlisting[language=c++,
caption={NodeConfiguration.h of Application using OutputChannel.h}
,label=list::formattedOutputNodeConf]
{code/formattedOutput_NodeConfiguration.h}

In the NodeConfiguration the OutputChannel object is added (line 18) and bound to the pool (line 7). At line 6 the pool is instantiated with the initial Offset of 0, so the fifo part is absent. The output of the OutputChannel is connected to the input of the serial interface in line 11. The {\tt init()}-method of the HelloWorld application has changed too. It now becomes a reference to the OutputChannel.


\lstinputlisting[language=c++,
caption={Source of Application using OutputChannel.h}
,label=list::formattedOutputCC]
{code/formattedOutput.cc}

The source code of the application logic has changed dramatically. There is no need for buffer management or sanity checks anymore, only using the methods of the OutputChannel (line 20 and 21).

\section{Sensing}
\label{sensing}

Sensing is an important tasks for many embedded systems. This section will show how to utilize sensors.
A common interface is the built in Analog-Digital-Converter available nearly on every platform.
%The interface to a sensor can be rather different. It could use a standard interface like SPI, UART or I2C or implement their own by using the I/O pins of the microcontroller.

%An other and  common interface is the build in Analog-Digital-Converter available on nearly every platform.



Figure \ref{fig::ADC} shows an application utilizing the ADC to obtain sensor values and send them via the serial connection.



\begin{figure}[htb]
\centering
\includegraphics[width=0.8\textwidth]{pics/ADC.pdf}
\caption{Application utilizing the ADConverter}
\label{fig::ADC}
\end{figure}

The Sample component is time triggered and initiates the measurement of one of the ADC channels. When the value is gathered, the interrupt of the ADC fires and is handled. As consequence the value is written to the event buffer of the DataProcessor component and processed. In this example 10 values are stored and sent together with their mean.
%This component then uses a OutputChannel to transmit the values.


Listing \ref{list::ADC_sense} shows the source code of the Sense component. The activity writes the channel number on the output (line 20), which should be connected to the ADC driver input.


\lstinputlisting[language=c++,
caption={Source of Sense component}
,label=list::ADC_sense]
{code/ADC_sense.cc}


The source of the processing component is shown in listing \ref{list::ADC_compute}. The most important line is number 20. The call of {\tt input.get()} returns the data from the event buffer. When only one value is stored (SingleValue1, Fifo1, Queue1) the data can retrieved direct as return value of {\tt get()}. In all other cases the get method has to be supplied with the certain variables to store the data into.


The variables to store the data into are passed as parameter of {\tt get()} or {\bf additionally} when only one value is stored (SingleValue1, Fifo1, Queue1) retrieved direct as return value of {\tt get()}.


\lstinputlisting[language=c++,
caption={Source of Compute component}
,label=list::ADC_compute]
{code/ADC_compute.cc}


\section{Timer}
\label{timer}

Timers are one of the central input sources for event triggered applications. Very often events happen periodically
or we have to wait for a certain amount of time before an action may happen, e.g. after activating an external device. The current
version of {\sc Reflex} uses a concept of virtual timers for this. One hardware timer is virtualized to provide various components
with timer events in millisecond precision.

With the introduction of virtual timers this version of {\sc Reflex} does not use a static tick system anymore but rather dynamic ticks. The system
will only wake on clock ticks when the next timer event is due or the hardware timer overflows. There is no clock. Each component that needs
a timer input simply creates a new {\bf VirtualTimer} object, either as a {\bf VirtualTimer::ONESHOT} or {\bf VirtualTimer::PERIODIC} timer.
The virtual timer does not need to be explicitly connected to any input sources or hardware timers. It will trigger the timer events {\emph automagically}.
In fact a dedicated virtualized hardware timer is used on every controller and the connection is made when the timer is created.

When an application needs higher precision than the milliseconds a virtual timer can provide, another true hardware timer on the
corresponding platform must be used directly. However, milliseconds should suffice for most applications. The {\bf OneShotTimer} and
{\bf PeriodicTimer} known from previous versions of {\sc Reflex} are deprecated. Their functionality is now provided by the {\bf Prescaler}
class that can operate in {\bf Prescaler::ONESHOT} or {\bf Prescaler::PERIODIC} mode. When replacing the deprecated classe, the virtual timers
should be used where possible, since those have much better qualities in terms of energy management.

The old {\tt TICK\_PERIOD\_MSEC} define from the applications {\tt conf.h} is no longer supported. By default there are no periodic wakeups other
than for counting the hardware timer overflows used for system time accounting. The current system time can be obtained through any {\bf VirtualTimer}
instance with the {\bf getSystemTime()} call. The maximum number of {\bf VirtualTimer} instances in the application including all its components and
the system level components is limited to 16 by default. This can be overridden by the {\bf #define VIRTUAL\_TIMERS <any number up to 255>} define in
the applications {\tt conf.h} file.

The {\bf VirtualTimers} example demonstrates the usage of virtual timers on different platforms.

\section{Distributors}

When an event needs to be propagated to more than one input, an event {\tt Distributor} is used. It is connected to one input and triggers
an arbitrary number of outputs. The usage of a {\tt Distributor} is shown in listing {\ref list::distributor}.

\lstinputlisting[language=c++,
caption={Usage of Distributor}
,label=list::distributor]
{code/distributor_NodeConfiguration.h}

The Distributor is instantiated in line 13. The template parameter specifies the maximum number of subscribers. In line 7 the clock is connected to the Distributor. Lines 8 to 10 show how the subscribers are connected using the {\tt registerSink()}-method. The {\tt removeSink()}-method can be used to remove a subscriber.

The Distributor.h features also a Distributor1 which can be used for data afflicted events.

\section{Functors}
\label{functors}
It is very impractical to write a component for each activity and more natural to put all functional code which belongs together in a single component. To do so, one can use the so called Activity Functors. These objects are associated with an input buffer and redirect the scheduler call to the chosen function. Application \ref{fig::functor} shows the same sensing activity from section \ref{sensing} utilizing an Activity Functor to combine the functionality into a single component.

\begin{figure}[htb]
\centering
\includegraphics[width=0.8\textwidth]{pics/functor.pdf}
\caption{Application utilizing a activity functor to group activities in a single component}
\label{fig::functor}
\end{figure}

Listings \ref{list::functors_h} and \ref{list::functors_cc} show the usage of an Activity Functor. The example is based on the previous application consisting of independent components for initiating the sampling and analyzing the results. Now these are merged into one component using an Activity Functor. The component still derives from the activity class and thus has a {\tt run()} method, but it is possible to use two Functors and to do it without derivation.

Line 28 of listing \ref{list::functors_h} shows the instantiation of the Activity Functor computeFunctor. Its template parameters link it to the class and the function which should be executed. Note that it is necessary do declare the function header before the Functor.



\lstinputlisting[language=c++,
caption={Usage of activity functors}
,label=list::functors_h]
{code/functors.h}

In listing \ref{list::functors_cc} the source code of the new combined component is shown. In line 6 the Functor is bound to the class. The input which should trigger the function the Functor points to is initialized in line 9. If a event is written into this input, it schedules the Functor which calls the {\tt compute()}-method.
The run method is identical to the former sense component, while the compute method contains the code from the former compute component.

\lstinputlisting[language=c++,
caption={Usage of activity functors}
,label=list::functors_cc]
{code/functors.cc}


\section{Interrupts}

There are two ways to handle an interrupt,  deriving a component class from InterruptHandler, or  use an Interrupt Functor. Both ways share the need for certain methods. An {\tt enable()} and {\tt disable()} method and one as a {\tt handler()}-method must be provided. Additionally the deepest possible sleep mode must specified (details about that can be found in chapter \ref{powermanagement}).


Listings \ref{list::interruptHandler_h} and \ref{list::interruptHandler_cc} show the first way. In line 4 of the header listing, the class derived from InterruptHandler.


\lstinputlisting[language=c++,
caption={InterruptHandler example header}
,label=list::interruptHandler_h]
{code/interruptHandler.h}

In line 4 of the source the interrupt number which should be handled by this component is defined. The available interrupts are platform dependent and can be found in the  InterruptVector.h in the {\tt [platform]/include/reflex/interrupts/} directory. In line 6 the platform dependent deepest possible sleep mode is defined.

\lstinputlisting[language=c++,
caption={InterruptHandler example source}
,label=list::interruptHandler_cc]
{code/interruptHandler.cc}


Using an Interrupt Functor makes it possible do combine functionality into one component and mix activities with interrupt handlers. This is an advantage for driver programming, since the access from all driver parts to specific variables is eased.

Listings \ref{list::interruptFunctor_h} and \ref{list::interruptFunctor_cc} show parts of the implementation of the driver for the serial connection in the MSP430. The full code can be found at~{\tt controller/MSP430/[include|\\src]/reflex/io}.

\lstinputlisting[language=c++,
caption={Interrupt functor example header}
,label=list::interruptFunctor_h]
{code/interruptFunctor.h}

In the header shown in listing \ref{list::interruptFunctor_h} two Interrupt Functors are created (lines 17 and 18). Additional to the handle() method defined for an Activity Functor they need enable() and disable() methods.

\lstinputlisting[language=c++,
caption={Interrupt functor example source}
,label=list::interruptFunctor_cc]
{code/interruptFunctor.cc}


In source code listing \ref{list::interruptFunctor_cc} the Interrupt Functors are bound to the interrupt (lines 2 and 3). The deepest possible sleep mode for the interrupts are set in lines 6 an 7.


\section{Power Management}
\label{powermanagement}

The key idea of the {\sc Reflex} power management is to send the controller to sleep whenever possible and to efficiently utilize the sleep modes provided by the controller. The power manager implicitly chooses the deepest possible sleep mode to consume as little energy as possible while maintain reactivity.


The power management in {\sc Reflex} is divided into two abstractions,
a system view and a user view.
The system view is responsible for the determination of the deepest possible sleep mode.
The user view provides the programmer with two instruments,
namely groups and modes, to ease the handling of all hard and software components.

\subsection{System View}



Since the initial source for activity are the interrupts,
they define the deepest possible sleep mode.
In general there are two types of interrupts,
 primary and secondary. The first are caused by external (hardware) events,
the second are a result of software events.
E.g. the TX interrupt of a serial connection has only to be
active when a send operation is in progress,
if it is finished, the driver of the serial connection can deactivate
the interrupt and possibly change the deepest possible sleep mode.
Thus the power management approach is implicit for secondary interrupts,
because the drivers know when a interrupt has to be enabled.

Primary interrupts can not be deactivated implicitly.
Their state is determined by the current stage of the application.
Sampling of sensors and sleeping for a given time needs different
interrupts at different times.
The decision which interrupt has to be active must be decided by the application programmer.

The classes {\tt InterruptHandler} and  {\tt InterruptFunctor} derivate from the base class {\tt Energy\-ManageAble} and thus are concerned by the power management.
Manageable objects can also be activities or event channels.

Every instance of a component has a variable which specifies the deepest possible sleep mode that may be used when it is active. A sleep mode deeper and the component can not work anymore and a mode higher wastes energy. The value can be assigned during initialization (e.g. for non hardware specific objects) or hard coded (e.g. for hardware specific objects).

The deepest possible sleep mode can be set by the {\tt setSleepMode(..)}-method where the available sleep modes can be found in {\tt controller/[XYZ]/src/reflex/power\\Management/ControllerPowerManagement.cc}.

To activate/deactivate a component the {\tt switchOn()}/{\tt swichOff()}-methods can be used. But the desired way is to use the mechanisms of the user view to keep consistency.

\subsection{User View}

 The user view provides two mechanisms to ease the control of
hardware and even software components: groups and modes.
At start up, each manageable object is registered with the power management
and assigned to one or more programmer defined groups as shown in figure \ref{g_m_example}.
\begin{figure}[hbt]
\begin{small}
\lstset{language=c++}
\begin{lstlisting}[breaklines=true]
//set group memberships
timer.setGroups( SLEEP | AWAKE );
serial.RX.setGroups( AWAKE );
sensor.setGroups( AWAKE );

//activate all members from group sleep
powerManager.enableGroup(SLEEP)

//switch mode from group SLEEP to AWAKE
powerManager.switchMode(AWAKE,SLEEP);
\end{lstlisting}
\caption{Groups and Modes example}
\label{g_m_example}

\end{small}
\end{figure}



During operation, groups can be independently activated and deactivated.
This allows to easily activate or deactivate any number of objects with only one method call.
If a manageable object is member of multiple groups it is only
deactivated when all of these groups are deactivated.


Modes are defined to switch easily between active groups and thus utilizing different hardware configurations.
This makes it possible to divide the execution of an application into different phases,
while ensuring that the hardware components are active when demanded.
The programmer is responsible for changing the modes.
For example a timer driven module can be used for mode changes.



\subsection{Example}
\label{pm_ex}

Figure \ref{pm_example_efg} shows an example for the power management and the utilization of modes.

 \begin{figure}[hbt]
         \centering
         \includegraphics[width=0.8\textwidth]{pics/powermanagement_example.pdf}
         \caption{Power management example}
         \label{pm_example_efg}
 \end{figure}

The application is an extension of the one in section \ref{functors}. Besides the sampling of the AD Converter, it receives data from the serial interface and sends it back. To utilize the modes of the power management there are two modes/groups, SLEEP and AWAKE. In SLEEP only the timer is active, AWAKE also includes the receiving part of the serial interface. The Sleep Manager component is responsible for changes of these modes and initiates the sampling.

Listing \ref{list::pm_example_sm_cc} shows the Sleep Manager component. Depending on the actual mode the new mode is set and, if it is the AWAKE mode, the application logic is invoked. In lines 26/27 and 32/33 the mode is changed using the {\tt switchMode()}-method.

\lstinputlisting[language=c++,
caption={Sleep Mode handling example source}
,label=list::pm_example_sm_cc]
{code/PowerManagment_example_sleepmanager.cc}

The NodeConfiguration is shown in listing \ref{list::pm_example_nodeconf}. In line 2 to 6 the groups/modes are defined by using the predefined groups from the power manager. The driver components are bound to groups in lines 28 to 33. Initially the mode SLEEP is activated in line 36 and thus AWAKE deactivated in line 37.

Additionally the character output of the serial interface is connected to the input buffer of the senseCompute component in line 21. Since it is only active in the AWAKE mode, characters sent while SLEEP can not be received.

\lstinputlisting[language=c++,
caption={Sleep Mode handling example NodeConfiguration}
,label=list::pm_example_nodeconf]
{code/PowerManagment_example_nodeconf.h}

\section{Radio}

A state-of-the-art research topic in deeply embedded systems are the wireless sensor networks. Within this networks small notes, containing a microcontroller, a radio interface, sensors and a battery, cooperate to achieve a common goal (sensing, alarms, etc.).

 \begin{figure}[hbt]
         \centering
         \includegraphics[width=0.8\textwidth]{pics/radio_example.pdf}
         \caption{Radio example}
         \label{radio_example}
 \end{figure}


In this section the example from section \ref{pm_ex} is extended to send the calculated mean to other nodes. No MAC or routing Protocols are used in this example. Only the TMoteSky platform is supported in this example, but the implementation of other platforms should be nearly equal, depending of the radio hardware. The event flow graph of the example is shown in figure \ref{radio_example}

Mainly the changes took place in the SenseCompute component. Instead of receiving data from the serial interface, it gets data from the radio and writes to the serial output. Additionally the calculated mean value is sent over the radio.

\lstinputlisting[language=c++,
caption={Radio example application}
,label=list::radio_example_application_cc]
{code/radio_application.cc}

The {\tt init()}-method shown in listing \ref{list::radio_example_application_cc} line 15 got a link to the radio output. Since the radio takes data as reference to a buffer, it is from type buffer*.

The calculated mean is sent from the {\tt compute()}-activity. In lines 48 and 49 a new buffer is allocated from the standard pool an the mean is written into it.

The {\tt doRadio()}-activity is invoked when data is received from the radio. The input is from type Buffer* (line 57). The mean is read and the reference count of buffer is decremented. At last the received mean is written using the output component.


\lstinputlisting[language=c++,
caption={NodeConfiguration of the radio example application}
,label=list::readio_example_application_nc]
{code/radio_application_nodeconf.h}

The NodeConfiguration is shown in listing \ref{list::readio_example_application_nc}. The radio component is included into the event flow (line 16 and 17) and included into the power manager (line 28). It is only part of the AWAKE group. The groups are managed by the SleepManager from the previous example.











% \begin{appendix}

% \begin{multicols}{2}
% \lstinputlisting[language=make,caption={Reflex directory structure},label=reflex_struct,frame=single]{code/reflex_structure.txt}
% \end{multicols}

% %\listoffigures
% %\lstlistoflistings
% %\bibliographystyle{abbrv}
% %\bibliography{references}

% \begin{thebibliography}{50}
% \vspace*{0.5mm}

% \bibitem{karsten_diss}
% K.Walter

% \bibitem{B99}
% A. Burns. The ravenscar prole. Ada Lett., XIX(4):4952, 1999.

% \bibitem{RS06}
% G. D. Reis and B. Stroustrup. Specifying c++ concepts. In POPL '06: Con-
% ference record of the 33rd ACM SIGPLAN-SIGACT symposium on Principles of
% programming languages, pages 295308, New York, NY, USA, 2006. ACM Press.

% \end{thebibliography}

% \end{appendix}

\end{document}
