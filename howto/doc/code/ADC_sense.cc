#include "Sense.h"

using namespace reflex;

Sense::Sense() : vtimer(VirtualTimer::PERIODIC)
{
	ready.init(this);
	vtimer.init(ready)
	vtimer.set(1000);
}


void Sense::init(Sink1<char>* out)
{
	this->out = out;
}

void Sense::run()
{
  	out->assign('5');
}
