#include "SenseCompute.h"
using namespace reflex;

SenseCompute::SenseCompute()
  : vtimer(VirtualTimer::PERIODIC)
	  , computeFunctor(*this) 
{
	ready.init(this);
	input.init(&computeFunctor);
	vtimer.init(ready);
	vtimer.set(100);
	counter = 0;
}

void SenseCompute::init(Sink1<char>* sensorOut, OutputChannel* out)
{
	this->sensorOut = sensorOut;
	this->out = out;
}

void SenseCompute::run()
{
  	sensorOut->assign('5');
}

void SenseCompute::compute()
{
	char channel;
    	input.get(channel,values[counter]); //get data from eventbuffer
	counter++;
	if (counter == DATASPACE) 
	{ 
	  	counter = 0;
		uint16 mean = 0;
		for(int i = 0; i < DATASPACE; i++) //calculate mean
		{
			out->write(values[i]);
			out->write(":");
			mean += values[i];
		}
		mean = mean / DATASPACE;
		out->write(" MEAN ");	
       		out->write(mean);
  		out->writeln();
	}
}
