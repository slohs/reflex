class NodeConfiguration : public System {
 public:
 NodeConfiguration() :

  // -------- initialization 

    System(),
    pool(12),           
    out(&pool),
    serial(1,SerialRegisters::B19200),
    pack(&pool)	      
      {

	// -------- configuration/connection
		
	out.init(&serial.input);
	serial.init(&controller.SERIALinput,0);
	controller.init(&pack);
	pack.init(&radio.input,&controller.receiver);
	radio.init(&pack,&controller);
	adc.init(&controller.ADCinput); /*from ADC to controller */
	controller.ADCoutput = &(adc.input); /*from controller to ADC */
	 
	radio.setProperties(InterruptHandler::SECONDARY);
	serial.rxProperties = InterruptHandler::SECONDARY;
	serial.txProperties = InterruptHandler::SECONDARY;
	  	 
	clock.connectToOutput(&sensor.timer);
	timer.setTickSpeed(TICK_PERIOD_MSEC);
	timer.start();
      }

  // -------- instantiation
       
  PoolManager poolManager;
  SizedPool<IOBufferSize,NrOfStdOutBuffers> pool;
  Serial serial;
  OutputChannel out;
  myControl controller;
  Packetizer pack;
  TMoteRadio radio;
};
	
