#include "reflex/scheduling/Activity.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/interrupts/InterruptFunctor.h"

class Serial : public Activity
{

public:
	virtual void enableRX();
	virtual void disableRX();
	virtual void handleRX();

	virtual void enableTX();
	virtual void disableTX();
	virtual void handleTX();

	InterruptFunctor<Serial,&Serial::handleTX,&Serial::enableTX,&Serial::disableTX> txHandler;
	InterruptFunctor<Serial,&Serial::handleRX,&Serial::enableRX,&Serial::disableRX> rxHandler;


};
