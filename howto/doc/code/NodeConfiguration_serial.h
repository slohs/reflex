#include "reflex/types.h"
#include "reflex/io/Serial.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/memory/PoolManager.h"
#include "reflex/memory/SizedPool.h"
#include "HelloWorld.h"
namespace reflex {
  class NodeConfiguration : public System {
  public:
    NodeConfiguration() :
      System(),
      pool(12),           
      out(&pool),
      serial(1,SerialRegisters::B19200),	      
      {
	  out.init(&serial.input);
	  serial.init(0,0);

	  serial.rxProperties = InterruptHandler::SECONDARY;
	  serial.txProperties = InterruptHandler::SECONDARY;
	  	 
	  clock.connectToOutput(&hello.timer);
	  timer.setTickSpeed(TICK_PERIOD_MSEC);
	  timer.start();
      }
      PoolManager poolManager;
      SizedPool<IOBufferSize,NrOfStdOutBuffers> pool;
      Serial serial;
      OutputChannel out;
      Helloworld hello;
  };

  inline NodeConfiguration& getApplication() 
  {
    extern NodeConfiguration system;
    return system;
  }
} //reflex
