void PriorityScheduler::dispatch()
{
	//run Activities from head of list until an interrupted one is in
	//front or the list is empty
	PriorityActivity* first = readyList.first();
	while( (first !=0 ) && (first->status != PriorityActivity::INTERRUPTED) ){

	    first->status = PriorityActivity::RUNNING;

	    //run activity
	    _interruptsEnable();
    	first->run();
	    _interruptsDisable();

	    readyList.deque();

	   	first->status = PriorityActivity::IDLE;

		//rescheduling if needed
	    if((first->rescheduleCount > 0) && (!first->locked)){

    		first->rescheduleCount--;
			first->status = PriorityActivity::SCHEDULED;
			readyList.insert(first);
		}

	    first = readyList.first();
	}

	//if there is an interrupted element set it Running again,
	//because this Stack-instance of PriorityScheduler has done its work
	if(first){
		first->status = PriorityActivity::RUNNING;
	}
}
