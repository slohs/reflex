class NodeConfiguration : public System 
{
public:
	NodeConfiguration() 
		: System()
		, pool(0) 
		, out(&pool)
		, serial(1,SerialRegisters::B19200)
	{
	    //wires components together
	    out.init(&serial.input);
	    hello.init(&out);	
	}

	PoolManager poolManager; ///< manages pools
	SizedPool<IOBufferSize,NrOfStdOutBuffers> pool; ///< a pool of bufferobject
	
	OutputChannel out; ///< an object that provides easy formated output. 

	Serial serial;	///< the driver for the serial interface
	HelloWorld hello; ///< here the famous HelloWorld message is produzed
};
