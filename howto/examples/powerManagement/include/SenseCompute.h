#ifndef SenseCompute_h
#define SenseCompute_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	SenseCompute
 *
 *	Author:		Andre Sieber
 *
 *	Description: Sensor Application.
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */
#include "reflex/sinks/Event.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/sinks/Sink.h"
#include "reflex/io/OutputChannel.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/io/OutputChannel.h"



#ifndef DATASPACE
#define DATASPACE 10
#endif


class SenseCompute : public reflex::Activity{
public:
	SenseCompute();

	/** This is the Activity
	 *  Here the sensor is inwoked
	 */
	virtual void run();


	/** This is the other Activity
	 *  Here the data is written to standard output
	 */
	virtual void compute();

	/** This is the other Activity
	 *  Here the data from the serial connection is processed
	 */
	virtual void doSerial();

	/** This is the Activity Functor
	 *  Ponting to a class and to a member function of
	 *  this class
	 */
	reflex::ActivityFunctor<SenseCompute,&SenseCompute::compute> computeFunctor;

	/** This is the Activity Functor
	 *  Ponting to a class and to a member function of
	 *  this class
	 */
	reflex::ActivityFunctor<SenseCompute,&SenseCompute::doSerial> serialFunctor;

	/** Sets the pointer to a subsequent component, which handle buffers.
	 *
	 *  @param out: pointer to the sensor input
	 *  @param out: pointer to the outputchannel
	 */
	void init(reflex::Sink1<char>* sensorOut, reflex::OutputChannel* out);

	reflex::Sink1<char> *sensorOut; //pointer to the sensor input
	reflex::OutputChannel* out;
	reflex::SingleValue2<char,uint16> adcInput;
	reflex::SingleValue1<char> serialInput; //pointer to the sensor input
	reflex::Event eventInput;

private:  
	uint16 values[DATASPACE];
	int counter;


};

#endif
