/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Karsten Walther
 */
#include "SenseCompute.h"

using namespace reflex;

SenseCompute::SenseCompute()
	: computeFunctor(*this)
	, serialFunctor(*this)
{
	eventInput.init(this);
	adcInput.init(&computeFunctor);
	serialInput.init(&serialFunctor);
	counter = 0;
}


void SenseCompute::init(Sink1<char>* sensorOut, OutputChannel* out)
{
	this->sensorOut = sensorOut;
	this->out = out;
}


void SenseCompute::run()
{
  	sensorOut->assign('5');
}

void SenseCompute::compute()
{
	char channel;
    	adcInput.get(channel,values[counter]); //get data from eventbuffer
	counter++;
	if (counter == DATASPACE) 
	{ 
	  	counter = 0;
		uint16 mean = 0;
		for(int i = 0; i < DATASPACE; i++) //calculate mean
		{
			out->write(values[i]);
			out->write(":");
			mean += values[i];
		}
		mean = mean / DATASPACE;
		out->write(" MEAN "); //write result
       		out->write(mean);
  		out->writeln();
	}
}


void SenseCompute::doSerial()
{
	char in;
	serialInput.get(in); //get data from buffer
	out->write("Got: "); //mirror it back
	out->write(in);
  	out->writeln();
}
