#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# */

EXECUTABLE = $(APPLICATION).elf
HEX = $(APPLICATION).hex
EEP = $(APPLICATION).eep
MAIN_TARGET = $(EXECUTABLE)

ifdef SERIAL_PORT
DEFINED_REFLEX_VARS += "SERIAL_PORT "$(SERIAL_PORT) \r\n
endif
ifndef SERIAL_PORT
MISSING_REFLEX_VARS += "SERIAL_PORT  PC port to which device is connected" \r\n
endif

TARGETS += download

AVRDUDE_DEVICE ?= $(DEVICE)

# Alternative linkerscript is not needed
#LDFLAGS += -T$(REFLEXPATH)/controller/$(CONTROLLER)/$(ARCHITECTURE).x

include $(REFLEXPATH)/platform/$(PLATFORM)/Sources.mk



download : all $(HEX) $(EEP)
	avrdude	-p$(AVRDUDE_DEVICE) -c$(PROGRAMMER) -P$(SERIAL_PORT) $(AVRDUDE_ARGS) -Uflash:w:$(APPLICATION).hex:a -Ueeprom:w:$(APPLICATION).eep:a  \
		-Ulfuse:w:$(LO_FUSE):m \
		-Uhfuse:w:$(HI_FUSE):m \
		-Uefuse:w:$(E_FUSE):m 

$(HEX): $(EXECUTABLE)
	$(OBJCOPY) -R .eeprom -O ihex $(EXECUTABLE) $(HEX)

$(EEP): $(EXECUTABLE)
	$(OBJCOPY) -j .eeprom --no-change-warnings --change-section-lma .eeprom=0 -O ihex $(EXECUTABLE) $(EEP)

###################### includes and base sources for the controller ########

INCLUDES += -I$(REFLEXPATH)/platform/$(PLATFORM)/include

