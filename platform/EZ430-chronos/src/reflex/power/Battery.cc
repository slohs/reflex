/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/

#include "reflex/power/Battery.h"

#include "NodeConfiguration.h"


using namespace reflex;

Battery::Battery()
	: adcOut(0)
{
	input.init(this);
	ADCmem = ADC::CH1;
}

void Battery::run()
{
  ADC::Request request(ADC::Ref_Vref_AVss,ADC::InCh_AVccDIV,ADC::Ref20,(reflex::msp430x::ADC12_A::Channel)ADCmem);
	if(adcOut) adcOut->assign(request);
}


//subclass Calculator

Battery::Calculator::Calculator()
	: SingleValue1<uint16>(this)
	, valueOut(0)
	, valueOutFloat(0)
{

	for (uint8 i= 0; i<HISTORY_SIZE;i++)
	  {
	    rawHistory[i] = 3100; //means 304V
	  }
	writePos = 0;
	raw_mean = 0;
	voltage_float = 304.87;

}

void Battery::Calculator::run()
{
    uint16 value = this->get();

	rawHistory[writePos] = value;
	writePos = (writePos + 1) % HISTORY_SIZE;

	//make mean of history
	raw_mean = 0;
	for (uint8 i = 0; i<HISTORY_SIZE;i++)
	  {
	    raw_mean = raw_mean + rawHistory[i];
	  }
	raw_mean = raw_mean / HISTORY_SIZE;

	if(valueOut) valueOut->assign((raw_mean<<2)/41);
	//vref is at 2V actually 1.93V!
	voltage_float = raw_mean * 2 * 1.93 / 40.96;
	if(valueOutFloat) valueOutFloat->assign(voltage_float); // (adc * 4 / 4095) * 100


}

