/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_EZ430_CHRONOS_BATTERY_H
#define REFLEX_EZ430_CHRONOS_BATTERY_H

#include "reflex/types.h"
#include "reflex/adc/ADC12_A.h"

#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/sinks/SingleValue.h"
#include "reflex/scheduling/Activity.h"


#define HISTORY_SIZE  20

namespace reflex {
class Battery
	: protected Activity
{
	typedef mcu::ADC12_A ADC;
protected:
	//! calculate adc values into voltage
	class Calculator: public SingleValue1<uint16>, public Activity {
		friend class Battery;
	public:
		Calculator();
	protected:
		//! doing conversion
		virtual void run();
	protected:
		Sink1<uint16>* valueOut; //! outport to assign the voltage to
		Sink1<float>* valueOutFloat;
	protected:
		uint8 writePos;
		uint16 rawHistory[HISTORY_SIZE];
	public:
		float voltage_float;
		uint16 raw_mean;

	};

public:
    Battery();
	~Battery() {}

	//! initializes output ports
	/*! \param	valueOut	output of the measured value
		\param	adcOut		output to the ADconverter
	*/
	void init(Sink1<uint16>* valueOut,Sink1<mcu::ADC12_A::Request>* adcOut)	{setOut(valueOut); setADC(adcOut);}
	void init(uint8 ADCmem) {this->ADCmem = ADCmem;}
	void setADC(Sink1<mcu::ADC12_A::Request>* adcOut)	{this->adcOut= adcOut;}
	void setOut(Sink1<uint16>* valueOut)	{adcInput.valueOut=valueOut;}
	void setFloatOut(Sink1<float>* valueOutFloat)	{adcInput.valueOutFloat=valueOutFloat;}
	float getFloatVoltage()	{return adcInput.voltage_float;}
	uint16 getRawVoltage()	{return adcInput.raw_mean;}

protected:
	//! initiate battery voltage measurement with adc
	virtual void run();
protected:

public:
	Event		input;	//! notify input for battery measurement
	Calculator	adcInput; //! consumes adc result and calculates voltage value

	float float_voltage;
protected:
	Sink1<mcu::ADC12_A::Request>* adcOut; //! output to adc which engage a measure.
	uint8 ADCmem;

};
}
#endif // REFLEX_EZ430_CHRONOS_BATTERY_H
