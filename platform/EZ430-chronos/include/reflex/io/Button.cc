/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Stefan Nuernberger
 */

#include "reflex/MachineDefinitions.h"
#include "reflex/io/Button.h"

using namespace reflex;
using namespace mcu;

template <uint8 PIN>
Button<PIN, 0>::Button()
    : PowerManageAble(PowerManageAble::PRIMARY)
    , port2IV(mcu::interrupts::PORT2)
{
    this->setSleepMode(mcu::LPM4);
}

template <uint8 PIN>
void Button<PIN, 0>::init(Sink0 *sink)
{
    (*port2IV)[Port2::Interrupt_traits::localVector(PIN)] = sink;
}

/*! configures the pin on port2 for desired button.
    The direction will be configured as input with enabled internal
    pulldown resistor. The interrupt transition will be configured with
    "low to high".
 */
template <uint8 PIN>
void Button<PIN, 0>::enable()
{
    //select io function for pins
    Port2()->SEL &= ~BUTTON_MASK;
    //configure pins as input
    Port2()->DIR &= ~BUTTON_MASK;
    //enable internal pulldowns
    Port2()->OUT &= ~BUTTON_MASK;
    Port2()->REN |= BUTTON_MASK;
    //set interrupts on low to high transition
    Port2()->IES &= ~BUTTON_MASK;
    //reset interrupt flags
    Port2()->IFG &= ~BUTTON_MASK;
    //enable the interupts for the connected pins
    Port2()->IE |= BUTTON_MASK;
}

/*! disables interrupt for button
*/
template <uint8 PIN>
void Button<PIN, 0>::disable()
{
    Port2()->IE &= ~BUTTON_MASK;
}

/** implementation for debounced button */

template <uint8 PIN, Time DEBOUNCE_TIME>
Button<PIN, DEBOUNCE_TIME>::Button() : Button<PIN,0>(),
        debounce_timer(VirtualTimer::ONESHOT),
        debouncer(*this)
{
    debounce_timer.connect_output(&debouncer);
}

template <uint8 PIN, Time DEBOUNCE_TIME>
void Button<PIN, DEBOUNCE_TIME>::init(Sink0 *sink)
{
    (*(this->Button<PIN, 0>::port2IV))[Port2::Interrupt_traits::localVector(PIN)] = this;
    output = sink;
}

template <uint8 PIN, Time DEBOUNCE_TIME>
void Button<PIN, DEBOUNCE_TIME>::notify()
{
    this->disable(); // disable further interrupts on pin
    debounce_timer.set(DEBOUNCE_TIME); // set debounce time
    // notify output
    if (output)
        output->notify();
}
