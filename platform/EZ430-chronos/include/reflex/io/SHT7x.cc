/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *	Author:	Stefan Nuernberger
 */
#include "SHT7x.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/MachineDefinitions.h"

using namespace reflex;

/**
 * constructor
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::SHT7x(): PowerManageAble(PowerManageAble::SECONDARY),
                                firstRequest(0), nextRequest(0), status(0),
                                startupTimer(VirtualTimer::ONESHOT),
                                timeoutTimer(VirtualTimer::ONESHOT),
                                portIV(TPort::Interrupt_traits::globalVector()),
                                sendCommandFunctor(*this),
                                getResultFunctor(*this),
                                timeoutFunctor(*this)
{
	startupTimer.connect_output(&sensorReady);
	timeoutTimer.connect_output(&sensorTimeout);
        sensorReady.init(&sendCommandFunctor); // register Activity for timer event
        resultAvailable.init(&getResultFunctor); // register Activity for interrupt
        sensorTimeout.init(&timeoutFunctor); // register Activity for sensor timeout

        // initialize Port
        TPort()->SEL &= ~(CLK | DATA | POWER); // set pins to I/O functionality
        TPort()->IES |= DATA; // select falling edge for DATA line interrupt
        TPort()->IE &= ~(CLK | DATA | POWER); // disable interrupts on all pins
        TPort()->DIR |= POWER; // make power an output pin
        TPort()->OUT &= ~POWER; // deactivate power line (switch sensor off)
        TPort()->DS |= POWER; // set high output drive strength operation on power pin

        // register interrupt handler on data pin
        (*portIV)[TPort::Interrupt_traits::localVector(DATAPIN)] = this;

        // external port interrupts still work on deepest sleep mode
        setSleepMode(mcu::LPM4);
}

/**
 * init
 * connect the data outputs of the sensor
 * @param temp Temperature output
 * @param hum Humidity output
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::init(Sink1<uint16>* temp, Sink1<uint16>* hum) {
        temperature = temp;
        humidity = hum;
}

/**
 * assign
 * implements Sink1 interface
 * used to receive measurement requests from application
 * activates the sensor and sets a timer for measurement start
 * @param request bitmask indicating desired operation
 * (see enum Operation)
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::assign(uint8 request)
{
        // critical, update ringbuffer
        InterruptLock lock;

        // enable sensor if deactivated
        if (nextRequest == firstRequest) {
                // activate sensor (power up)
                TPort()->OUT |= POWER;
                // set timer
                startupTimer.set(SHT_POWERUP_MSEC);
        }

        // copy request (will be evaluated by run())
        this->request[nextRequest++] = request;
        // update nextRequest
        if (nextRequest == SHT_MAX_REQUESTS)
                nextRequest = 0;
}

/**
 * delay
 * just a little spinning for clear signals
 */
inline void delay() {
    volatile int i = 0;
    while (i != 1000) ++i;
}

/**
 * sendCommand
 * This method issues a command to the sensor and enables the
 * interrupt for the result
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::sendCommand()
{
        // critical, get request from ringbuffer
        if (true) {
                InterruptLock lock;
                currentRequest = this->request[firstRequest];
        }

        // compute status register from request
        unsigned char newStatus =
                          (((currentRequest & HEATER) && true) << SR_HEATER)
                        | (((currentRequest & LOW_RES) && true) << SR_RESOLUTION)
                        | (((currentRequest & NO_CALIBRATE) && true) << SR_OTP_RELOAD);

        // write status (if changed)
        if (newStatus != status) {
                initTransmit();
                writeByte(CMD_SR_WRITE);
                if (readAck()) goto err_out;
                writeByte(newStatus);
                if (readAck()) goto err_out;

                status = newStatus;
        }

        initTransmit();
        // transmit measure command
        if (currentRequest & HUMIDITY) { // measure humidity
                writeByte(CMD_MEASURE_HUM);
        } else { // measure temperature
                writeByte(CMD_MEASURE_TEMP);
        }
        // read acknowledgment
        if (readAck()) goto err_out;

        // wait for result (until DATA is low) or timeout
        // NOTE: we are notified by interrupt
        TPort()->DIR &= ~DATA; // data is input
        TPort()->REN |= DATA; // data with pullup/down
        TPort()->OUT |= DATA; // pullup

        delay();

        if ((TPort()->IN & DATA) != 0) {
            // set timer for measure timeout
            timeoutTimer.set(SHT_MEASURE_TIMEOUT);
            // enable interrupt
            switchOn();

            return; // no error
        }


err_out:
        TPort()->REN &= ~DATA; // disable pullup/down on data

        // return error value
        if (currentRequest & HUMIDITY) {
                if (humidity)
                        humidity->assign(SENSOR_ERROR);
        } else {
                if (temperature)
                        temperature->assign(SENSOR_ERROR);
        }

        // handle next request or deactivate sensor
        handleNextRequest();
}

/**
 * notify
 * emulates InterruptHandler (from dispatched port interrupt)
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::notify() {
        InterruptLock lock;

        timeoutTimer.stop(); // no timeout
        // disable Interrupt
        switchOff();
        // clear InterruptFlag
        TPort()->IFG &= ~DATA;
        // announce result available
        resultAvailable.notify();
}

/**
 * getResult
 * This method reads the result from the sensor after an interrupt
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::getResult() {
        uint16 result = 0;
        result |= (readByte() << 8); // MSB
        writeAck(); // ack first byte
        result |= readByte(); // LSB
        // skip ack since we don't need CRC

        TPort()->REN &= ~DATA; // disable pullup/down on data

        // return value
        if (currentRequest & HUMIDITY) {
                if (humidity) humidity->assign(result);
        } else {
                if (temperature) temperature->assign(result);
        }

        // handle next request or deactivate sensor
        handleNextRequest();
}

/**
 * timeout
 * This method stops the current sensor reading due to sensor timeout
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
        void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::timeout() {

    // disable Interrupt
    switchOff();

    TPort()->REN &= ~DATA; // disable pullup/down on data

    // return sensor error
    if (currentRequest & HUMIDITY) {
            if (humidity) humidity->assign(SENSOR_TIMEOUT);
    } else {
            if (temperature) temperature->assign(SENSOR_TIMEOUT);
    }

    // handle next request or deactivate sensor
    handleNextRequest();
}

/**
 * handleNextRequest
 * advance request pointer (ringbuffer) and
 * trigger next measurement or deactivate sensor
 * if no request is left
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::handleNextRequest() {
        // critical, update ringbuffer
        InterruptLock lock;

        ++firstRequest; // this request is done
        if (firstRequest == SHT_MAX_REQUESTS)
                firstRequest = 0;
        if (nextRequest == firstRequest) {
                TPort()->OUT &= ~POWER; // deactivate sensor
                status = 0; // reset status
        } else {
                // trigger sendCommandFunctor for next measurement
                sendCommandFunctor.trigger();
        }
}

/**
 * enable
 * PowerManagement enable function
 * enables interrupt on DATA port
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::enable() {
        TPort()->IFG &= ~DATA; // reset all pending interrupts
        TPort()->IE |= DATA;
}

/**
 * disable
 * PowerManagement disable function.
 * disables interrupt on DATA port
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::disable() {
        TPort()->IE &= ~DATA;
}

/**
 * initTransmit
 * sends the "start transmission" sequence
 *       ____         ____
 *  DATA     |_______|
 *          ___     ___
 *  CLK  __|   |___|   |__
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::initTransmit()
{
        TPort()->DIR |= (DATA | CLK); // set data and clock to output

        //initial state
        TPort()->OUT &= ~CLK; delay(); //CLK=0
        TPort()->OUT |= DATA; delay(); //DATA=1
        TPort()->OUT |= CLK; delay(); //CLK=1
        TPort()->OUT &= ~DATA; delay(); //DATA=0
        TPort()->OUT &= ~CLK; delay(); //CLK=0
        TPort()->OUT |= CLK; delay(); //CLK=1
        TPort()->OUT |= DATA; delay(); //DATA=1
        TPort()->OUT &= ~CLK; delay(); //CLK=0
}

/**
 * readAck
 * tries to read an acknowledgment.
 * @return 0 if everything went well
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
int SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::readAck()
{
        TPort()->DIR |= CLK; // set clock to output
        TPort()->DIR &= ~DATA; // set data to input

        delay();

        TPort()->OUT |= CLK; delay(); // CLK = 1;
        // DATA should be low now
        if(TPort()->IN & DATA) return -1;
        TPort()->OUT &= ~CLK; delay(); // CLK = 0;

        return 0;
}

/**
 * writeAck
 * write an Acknowledgment
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::writeAck()
{
        TPort()->DIR |= (CLK | DATA); // set clock and data to output

        TPort()->OUT &= ~DATA; delay(); // DATA = 0;
        TPort()->OUT |= CLK; delay(); // CLK = 1;
        TPort()->OUT &= ~CLK; delay(); // CLK = 0;
        TPort()->OUT |= DATA; delay(); // DATA = 1;
}

/**
 * readByte
 * read a single byte
 * @return the received byte
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
unsigned char SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::readByte()
{
        unsigned char bit, byte = 0;

        TPort()->DIR |= CLK;	// clock is output
        TPort()->DIR &= ~DATA; // data is input

        for (bit = 0x80; bit != 0; bit >>= 1) // shift bit for masking
        {
                TPort()->OUT |= CLK; delay(); // CLK = 1
                if (TPort()->IN & DATA) byte |= bit; // read bit
                TPort()->OUT &= ~CLK; delay(); // CLK = 0;
        }

        return byte;
}

/**
 * writeByte
 * write a single byte
 * @param byte the byte to write
 */
template <typename TPort, uint8 POWERPIN, uint8 CLKPIN, uint8 DATAPIN>
void SHT7x<TPort,POWERPIN,CLKPIN,DATAPIN>::writeByte(char byte)
{
        unsigned char bit;
        TPort()->DIR |= (DATA | CLK); // data and clock as output

        for (bit = 0x80; bit != 0; bit >>= 1) //shift bit for masking
        {
                // set data line
                if (bit & byte)
                        TPort()->OUT |= DATA;	// DATA = 1
                else
                        TPort()->OUT &= ~DATA;	// DATA = 0

                delay();
                // let the clock tick
                TPort()->OUT |= CLK; delay();	// CLK=1
                TPort()->OUT &= ~CLK;	// CLK=0;
    }
}
