/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#ifndef REFLEX_EZ430_CHRONOS_DISPLAY
#define REFLEX_EZ430_CHRONOS_DISPLAY

#include "reflex/lcd/LCD_B.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/MachineDefinitions.h"
#include "reflex/data_types/BCD.h"
#include "reflex/data_types/Encode.h"
namespace reflex {
class Display;
namespace display {
	class Lookup {
	public:
		enum Segments {
			// 7-segment character bit assignments
			SEG_A = (BIT_4)
			,SEG_B = (BIT_5)
			,SEG_C = (BIT_6)
			,SEG_D = (BIT_7)
			,SEG_E = (BIT_2)
			,SEG_F = (BIT_0)
			,SEG_G = (BIT_1)
			,SEG_MASK = SEG_A|SEG_B|SEG_C|SEG_D|SEG_E|SEG_F|SEG_G
		};

		enum SymbolMask {
			 SEG_L1_0_Mask = (BIT_2+BIT_1+BIT_0+BIT_7+BIT_6+BIT_5+BIT_4)
			,SEG_L1_1_Mask = (BIT_2+BIT_1+BIT_0+BIT_7+BIT_6+BIT_5+BIT_4)
			,SEG_L1_2_Mask = (BIT_2+BIT_1+BIT_0+BIT_7+BIT_6+BIT_5+BIT_4)
			,SEG_L1_3_Mask = (BIT_2+BIT_1+BIT_0+BIT_7+BIT_6+BIT_5+BIT_4)
			,SEG_L1_COL_Mask = (BIT_5)
			,SEG_L1_DP1_Mask = (BIT_6)
			,SEG_L1_DP0_Mask = (BIT_2)
			,SEG_L2_0_Mask = (BIT_3+BIT_2+BIT_1+BIT_0+BIT_6+BIT_5+BIT_4)
			,SEG_L2_1_Mask = (BIT_3+BIT_2+BIT_1+BIT_0+BIT_6+BIT_5+BIT_4)
			,SEG_L2_2_Mask = (BIT_3+BIT_2+BIT_1+BIT_0+BIT_6+BIT_5+BIT_4)
			,SEG_L2_3_Mask = (BIT_3+BIT_2+BIT_1+BIT_0+BIT_6+BIT_5+BIT_4)
			,SEG_L2_4_Mask = (BIT_3+BIT_2+BIT_1+BIT_0+BIT_6+BIT_5+BIT_4)
			,SEG_L2_5_Mask = (BIT_7)
			,SEG_L2_COL1_Mask = (BIT_4)
			,SEG_L2_COL0_Mask = (BIT_0)
			,SEG_L2_DP_Mask = (BIT_7)
			,SEG_L2_ONE_Mask = (BIT_7)
			,SYMB_AM_Mask = (BIT_1+BIT_0)
			,SYMB_PM_Mask = (BIT_0)
			,SYMB_ARROW_UP_Mask = (BIT_2)
			,SYMB_ARROW_DOWN_Mask = (BIT_3)
			,SYMB_PERCENT_Mask = (BIT_4)
			,SYMB_TOTAL_Mask = (BIT_7)
			,SYMB_AVERAGE_Mask = (BIT_7)
			,SYMB_MAX_Mask = (BIT_7)
			,SYMB_BATTERY_Mask = (BIT_7)
			,UNIT_L1_FT_Mask = (BIT_5)
			,UNIT_L1_K_Mask = (BIT_6)
			,UNIT_L1_M_Mask = (BIT_1)
			,UNIT_L1_I_Mask = (BIT_0)
			,UNIT_L1_PER_S_Mask = (BIT_3)
			,UNIT_L1_PER_H_Mask = (BIT_2)
			,UNIT_L1_DEGREE_Mask = (BIT_1)
			,UNIT_L2_KCAL_Mask = (BIT_4)
			,UNIT_L2_KM_Mask = (BIT_5)
			,UNIT_L2_MI_Mask = (BIT_6)
			,ICON_HEART_Mask = (BIT_3)
			,ICON_STOPWATCH_Mask = (BIT_3)
			,ICON_RECORD_Mask = (BIT_7)
			,ICON_ALARM_Mask = (BIT_3)
			,ICON_BEEPER1_Mask = (BIT_3)
			,ICON_BEEPER2_Mask = (BIT_3)
			,ICON_BEEPER3_Mask = (BIT_3)
		};
		//! offsets into the lookup table Lookup::offsets
		enum Offsets {
			 LCD_MEM_1 = 9
			,LCD_MEM_2 = 0
			,LCD_MEM_3 = 1
			,LCD_MEM_4 = 2
			,LCD_MEM_5 = 10
			,LCD_MEM_6 = 3
			,LCD_MEM_7 = 11
			,LCD_MEM_8 = 8
			,LCD_MEM_9 = 7
			,LCD_MEM_10 = 6
			,LCD_MEM_11 = 5
			,LCD_MEM_12 = 4
		};
		//! the offsets of each Symbol into the table Lookup::offsets
		enum SymbolOffsets {
			 UpperLine_Offs= LCD_MEM_2 //start of upperline
			,LowerLine_Offs= LCD_MEM_12

			,SEG_L1_COL_Offs = LCD_MEM_1
			,SEG_L1_DP1_Offs = LCD_MEM_1
			,SEG_L1_DP0_Offs = LCD_MEM_5
			,SEG_L2_COL1_Offs = LCD_MEM_1
			,SEG_L2_COL0_Offs = LCD_MEM_5
			,SEG_L2_DP_Offs = LCD_MEM_9
			,SEG_L2_ONE_Offs = LCD_MEM_12

			,SYMB_AM_Offs= (LCD_MEM_1)
			,SYMB_PM_Offs= (LCD_MEM_1)
			,SYMB_ARROW_UP_Offs= (LCD_MEM_1)
			,SYMB_ARROW_DOWN_Offs= (LCD_MEM_1)
			,SYMB_PERCENT_Offs= (LCD_MEM_5)
			,SYMB_TOTAL_Offs= (LCD_MEM_11)
			,SYMB_AVERAGE_Offs= (LCD_MEM_10)
			,SYMB_MAX_Offs= (LCD_MEM_8)
			,SYMB_BATTERY_Offs= (LCD_MEM_7)
			,UNIT_L1_FT_Offs= (LCD_MEM_5)
			,UNIT_L1_K_Offs= (LCD_MEM_5)
			,UNIT_L1_M_Offs= (LCD_MEM_7)
			,UNIT_L1_I_Offs= (LCD_MEM_7)
			,UNIT_L1_PER_S_Offs= (LCD_MEM_5)
			,UNIT_L1_PER_H_Offs= (LCD_MEM_7)
			,UNIT_L1_DEGREE_Offs= (LCD_MEM_5)
			,UNIT_L2_KCAL_Offs= (LCD_MEM_7)
			,UNIT_L2_KM_Offs= (LCD_MEM_7)
			,UNIT_L2_MI_Offs= (LCD_MEM_7)
			,ICON_HEART_Offs= (LCD_MEM_2)
			,ICON_STOPWATCH_Offs= (LCD_MEM_3)
			,ICON_RECORD_Offs= (LCD_MEM_1)
			,ICON_ALARM_Offs= (LCD_MEM_4)
			,ICON_BEEPER1_Offs= (LCD_MEM_5)
			,ICON_BEEPER2_Offs= (LCD_MEM_6)
			,ICON_BEEPER3_Offs= (LCD_MEM_7)
		};

	protected:
		enum {	FONT_SIZE=45
			,OFFSETS_SIZE=12
			,SYMBOLS_SIZE=42
		}; //font+symbol bits

		static const uint8 font[];
		static const uint8 offsets[];
//		static const uint8 symbols[];
//		static const uint8 offsetsUpper[];
//		static const uint8 offsetsLower[];
	};

	enum Symbol {
		SEG_L1_COL = ((Lookup::SEG_L1_COL_Offs)<<8) + (Lookup::SEG_L1_COL_Mask)
	   ,SEG_L1_DP1 = ((Lookup::SEG_L1_DP1_Offs )<<8) + (Lookup::SEG_L1_DP1_Mask )
	   ,SEG_L1_DP0 = ((Lookup::SEG_L1_DP0_Offs )<<8) + (Lookup::SEG_L1_DP0_Mask )
	   ,SEG_L2_COL1 = ((Lookup::SEG_L2_COL1_Offs )<<8) + (Lookup::SEG_L2_COL1_Mask )
	   ,SEG_L2_COL0 = ((Lookup::SEG_L2_COL0_Offs )<<8) + (Lookup::SEG_L2_COL0_Mask )
	   ,SEG_L2_DP = ((Lookup::SEG_L2_DP_Offs )<<8) + (Lookup::SEG_L2_DP_Mask )
	   ,SEG_L2_ONE = ((Lookup::SEG_L2_ONE_Offs )<<8) + (Lookup::SEG_L2_ONE_Mask )
	   ,SYMB_AM = ((Lookup::SYMB_AM_Offs )<<8) + (Lookup::SYMB_AM_Mask )
	   ,SYMB_PM = ((Lookup::SYMB_PM_Offs )<<8) + (Lookup::SYMB_PM_Mask )
	   ,SYMB_ARROW_UP = ((Lookup::SYMB_ARROW_UP_Offs )<<8) + (Lookup::SYMB_ARROW_UP_Mask )
	   ,SYMB_ARROW_DOWN = ((Lookup::SYMB_ARROW_DOWN_Offs )<<8) + (Lookup::SYMB_ARROW_DOWN_Mask )
	   ,SYMB_PERCENT = ((Lookup::SYMB_PERCENT_Offs )<<8) + (Lookup::SYMB_PERCENT_Mask )
	   ,SYMB_TOTAL = ((Lookup::SYMB_TOTAL_Offs )<<8) + (Lookup::SYMB_TOTAL_Mask )
	   ,SYMB_AVERAGE = ((Lookup::SYMB_AVERAGE_Offs )<<8) + (Lookup::SYMB_AVERAGE_Mask )
	   ,SYMB_MAX = ((Lookup::SYMB_MAX_Offs )<<8) + (Lookup::SYMB_MAX_Mask )
	   ,SYMB_BATTERY = ((Lookup::SYMB_BATTERY_Offs )<<8) + (Lookup::SYMB_BATTERY_Mask )
	   ,UNIT_L1_FT = ((Lookup::UNIT_L1_FT_Offs )<<8) + (Lookup::UNIT_L1_FT_Mask )
	   ,UNIT_L1_K = ((Lookup::UNIT_L1_K_Offs )<<8) + (Lookup::UNIT_L1_K_Mask )
	   ,UNIT_L1_M = ((Lookup::UNIT_L1_M_Offs )<<8) + (Lookup::UNIT_L1_M_Mask )
	   ,UNIT_L1_I = ((Lookup::UNIT_L1_I_Offs )<<8) + (Lookup::UNIT_L1_I_Mask )
	   ,UNIT_L1_PER_S = ((Lookup::UNIT_L1_PER_S_Offs )<<8) + (Lookup::UNIT_L1_PER_S_Mask )
	   ,UNIT_L1_PER_H = ((Lookup::UNIT_L1_PER_H_Offs )<<8) + (Lookup::UNIT_L1_PER_H_Mask )
	   ,UNIT_L1_DEGREE = ((Lookup::UNIT_L1_DEGREE_Offs )<<8) + (Lookup::UNIT_L1_DEGREE_Mask )
	   ,UNIT_L2_KCAL = ((Lookup::UNIT_L2_KCAL_Offs )<<8) + (Lookup::UNIT_L2_KCAL_Mask )
	   ,UNIT_L2_KM = ((Lookup::UNIT_L2_KM_Offs )<<8) + (Lookup::UNIT_L2_KM_Mask )
	   ,UNIT_L2_MI = ((Lookup::UNIT_L2_MI_Offs )<<8) + (Lookup::UNIT_L2_MI_Mask )
	   ,ICON_HEART = ((Lookup::ICON_HEART_Offs )<<8) + (Lookup::ICON_HEART_Mask )
	   ,ICON_STOPWATCH = ((Lookup::ICON_STOPWATCH_Offs )<<8) + (Lookup::ICON_STOPWATCH_Mask )
	   ,ICON_RECORD = ((Lookup::ICON_RECORD_Offs )<<8) + (Lookup::ICON_RECORD_Mask )
	   ,ICON_ALARM = ((Lookup::ICON_ALARM_Offs )<<8) + (Lookup::ICON_ALARM_Mask )
	   ,ICON_BEEPER1 = ((Lookup::ICON_BEEPER1_Offs )<<8) + (Lookup::ICON_BEEPER1_Mask )
	   ,ICON_BEEPER2 = ((Lookup::ICON_BEEPER2_Offs )<<8) + (Lookup::ICON_BEEPER2_Mask )
	   ,ICON_BEEPER3 = ((Lookup::ICON_BEEPER3_Offs )<<8) + (Lookup::ICON_BEEPER3_Mask )

	};


	class LCDMemWriter
		: protected Lookup
	{
	private:
		LCDMemWriter(){}
	public:
		void write(const uint8 &memposOffset, const uint8 &valOffset , const bool &swap=false) __attribute__((noinline));
//		void setSymbol(const uint8& memposOffset,const uint8& valOffset);
		void setSymbol(const Symbol&);
//		void clearSymbol(const uint8& memposOffset,const uint8& valOffset);
		void clearSymbol(const Symbol&);
		void toggleSymbol(const Symbol&);
	};


	//! represents one Segment of the display
	class Segment
	{
	public:
		Segment(LCDMemWriter*const mem,const uint8& offset, const bool swap=false): mem(mem), offset(offset), swap(swap) {}
	public:
		//! writes a value on the segment
		/*! \param	val hexvalue to be set on the segment*/
		void operator= (const int& val) { mem->write(offset,val,swap);}
		void operator= (const char& val) { mem->write(offset,val-48,swap);}
	protected:
		LCDMemWriter*const mem;
		const word offset;
		const bool swap;
	};


	class UpperLine
		: protected LCDMemWriter
	{
	public:
		enum { DigitCount=4 };
	private:
		UpperLine(); //nobody is allowed to construct an object of this type
	public:
		static UpperLine& instance() {return *reinterpret_cast<UpperLine*>(&mcu::lcd::LCD_B_Memory() );}
		static UpperLine& instanceBlink() {return *reinterpret_cast<UpperLine*>(&mcu::lcd::LCD_B_BlinkMemory() );}

		//!
		UpperLine& operator=(const char (&string)[DigitCount+1]) __attribute__((noinline));
		//! writes a ptr to the line (hex)
		UpperLine& operator=(const uint16& val)__attribute__((noinline));

		Segment operator[](const word& index) { return Segment(this,index); }

	};


	class LowerLine
		: protected LCDMemWriter
	{
	public:
		enum { DigitCount=5 };
	private:
		LowerLine(); //nobody is allowed to construct an object of this type
	public:
		static LowerLine& instance() {return *reinterpret_cast<LowerLine*>(&mcu::lcd::LCD_B_Memory() );}
		static LowerLine& instanceBlink() {return *reinterpret_cast<LowerLine*>(&mcu::lcd::LCD_B_BlinkMemory() );}

		//! writes a string to the lower line
		LowerLine& operator=(const char (&string)[DigitCount+1]) __attribute__((noinline));
		//! writes value to the whole line
		LowerLine& operator=(const uint16& val)__attribute__((noinline));
		//! writes bigger value for all digits
		LowerLine& operator=(const uint32& val)__attribute__((noinline));

		Segment operator[](const word& index) { return Segment(this,index+UpperLine::DigitCount,true); }

	};

	class Symbols
		: protected LCDMemWriter
	{
		friend class Display;
	private:
		Symbols();
	public:
		static Symbols& instance() {return *reinterpret_cast<Symbols*>(&mcu::lcd::LCD_B_Memory() );}
		static Symbols& instanceBlink() {return *reinterpret_cast<Symbols*>(&mcu::lcd::LCD_B_BlinkMemory() );}
		//! set/clear a Symbol @{
		void set(const Symbol& symb) { setSymbol(symb);}
		void clear(const Symbol& symb){ clearSymbol(symb);}
		void toggle(const Symbol& symb) {toggleSymbol(symb);}
		//! @}
	};
}//ns display


class Display
	: public PowerManageAble
{
public:
	typedef display::UpperLine UpperLine;
	typedef display::LowerLine LowerLine;
	typedef display::Symbols Symbols;
public:
	//! initialize the hardware which is still off
	Display();
	//! disable hardware.
	~Display();
protected:
	//! enable the lcd hardware so its consuming power
	virtual void enable();
	//! disables the  lcd hardware so its not consuming power anymore
	virtual void disable();
public:
	UpperLine& getUpperLine() {return UpperLine::instance();}
	LowerLine& getLowerLine() {return LowerLine::instance();}
	Symbols& getSymbols() {return Symbols::instance();}

public: //deprecated
	//! write string, character or variable to the upper line @{
	void writeUpper(const char (&string)[5]) __attribute__((deprecated)) {getUpperLine()=string;}
	void writeUpper(const data_types::Encode<data_types::BCD<4> >& val) __attribute__((deprecated)) {getUpperLine()=val;}
	void writeUpperHex(const uint16&val) __attribute__((deprecated)) {getUpperLine()=val;}
	//! write string, character or variable to the lower line @{
	void writeLower(const char (&string)[6]) __attribute__((deprecated)) {getLowerLine()=string;}
	void writeLower(const data_types::Encode<data_types::BCD<4> >&val) __attribute__((deprecated))  {getLowerLine()=val;}
	void writeLowerHex(const uint16&val) __attribute__((deprecated)) {getLowerLine()=val;}
	//! @}

protected:
	msp430x::LCD_B lcdC;
};

}//ns reflex



#endif // REFLEX_EZ430-CHRONOS_DISPLAY
