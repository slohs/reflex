#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
# *	Author:		Karsten Walther
# */

CONTROLLER = MSP430X

#DEVICE is deprecated
DEVICE = cc430x6137

MCU = cc430x6137

INCLUDES += -I$(REFLEXPATH)/platform/$(PLATFORM)/include

LDFLAGS += -nostdlib

ifdef COMPONENT_UPDATE
	ifdef WIRELESS_UPDATE
	LDFLAGS += -T$(REFLEXPATH)/platform/$(PLATFORM)/WirelessAndComponentUpdate_LinkerScript
	else
	LDFLAGS += -T$(REFLEXPATH)/platform/$(PLATFORM)/ComponentUpdate_LinkerScript
	endif
else
	ifdef WIRELESS_UPDATE
	LDFLAGS += -T$(REFLEXPATH)/platform/$(PLATFORM)/WirelessUpdate_LinkerScript
        else
	LDFLAGS += -T$(REFLEXPATH)/platform/$(PLATFORM)/LinkerScript
	endif
endif



