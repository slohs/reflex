
#ifndef T123A_H
#define T123A_H

#include "reflex/types.h"
#include "reflex/io/Ports.h"
#include "reflex/usci/Registers.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/data_types/FifoQueue.h"
#include "reflex/memory/Pool.h"
#include "reflex/memory/Buffer.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/interrupts/InterruptHandler.h"

namespace reflex {
class T123A
  : public Activity
//  : public InterruptHandler
{
  typedef msp430x::usci::RegistersB0 Registers;

public:
  enum Address {
    ADDRESS             = 0x3A,   //i²c address of the T123A-I2C, should be 0x74
    LINE1               = 0x00,
    LINE2               = 0x20,
    LINE3               = 0x40,
    CHAR1               = 0x00,
    CHAR2               = 0x01,
    CHAR3               = 0x02,
    CHAR4               = 0x03,
    CHAR5               = 0x04,
    CHAR6               = 0x05,
    CHAR7               = 0x06,
    CHAR8               = 0x07,
    CHAR9               = 0x08,
    CHAR10              = 0x09,
    CHAR11              = 0x0A,
    CHAR12              = 0x0B
  };

  enum PINS {
    PIN_SCL = BIT_2,
    PIN_SDA = BIT_3
  };

  //PCF2116 control byte
  enum MODES {
    MODE_CTRL_FUNCTION  = 0x00, //for sending control data
    MODE_READ_STATUS    = 0x20, //for reading busy flag and address counter
    MODE_READ_DATA      = 0x60, //for reading DDRAM, CGRAM addresses
    MODE_WRITE_DATA     = 0x40  //for writing DDRAM, CGRAM addresses
  };

  enum OPERATIONS {
    NOP                 = 0x00,
    CLEAR_DISPLAY       = 0x01,
    RETURN_HOME         = 0x02,
    ENTRY_MODE_SET      = 0x04,
    DISPLAY_CONTROL     = 0x08,
    CURSOR_DISPLAY_SHIFT= 0x10,
    FUNCTION_SET        = 0x3E, //0x20 + 0x10(8bit data length) + 0x08 + 0x04 + 0x02
    SET_CGRAM_ADDRESS   = 0x40,
    SET_DDRAM_ADDRESS   = 0x80
  };

  enum COMMAND_BITS {
    ENTRY_MODE_INCREMENT        = 0x02,
    ENTRY_MODE_DISPLAY_SHIFT    = 0x01,
    DISPLAY_CONTROL_DISPLAY_ON  = 0x04,
    DISPLAY_CONTROL_CURSOR_ON   = 0x02,
    DISPLAY_CONTROL_CHAR_BLINK  = 0x01,
    CURSOR_DISPLAY_SHIFT_DISPLAY= 0x08,
    CURSOR_DISPLAY_SHIFT_RIGHT  = 0x04,
    CO                          = 0x80
  };

  Event displayOut;

private:
  Pool& pool;
  uint8 cmdQueue[5];

  bool enabled;
  uint8 waitCursor;

public:
  T123A(Pool& pool);

  void init();
  void triggerDisplay();
  void clearDisplay();          //write spaces into all DDRAM addresses, set address counter to 0

private:
  virtual void enable();
  virtual void disable();
  virtual void run();   //Activity

  void handle();        //i²c interrupt routine

  void delay();
  void nop();
  void checkBusyFlag();

  void updateWaitCursor();
  void sendInstructions(uint8[]);

private:
  ActivityFunctor<T123A, &T123A::triggerDisplay> displayFunctor;
};

} //ns


#endif // T123A_H
