#ifndef SHT21_H
#define SHT21_H

#include "reflex/types.h"
#include "reflex/io/Ports.h"
#include "reflex/usci/Registers.h"
#include "reflex/sinks/Sink.h"
#include "reflex/sinks/Event.h"
#include "reflex/scheduling/Activity.h"
#include "reflex/scheduling/ActivityFunctor.h"
#include "reflex/powerManagement/PowerManageAble.h"
#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/timer/VirtualTimer.h"

namespace reflex {

class SHT21
  : public Sink1<uint8>
  , public InterruptHandler
{

  typedef msp430x::usci::RegistersB0 Registers;

private:

  enum Address {
    ADDRESS             = 0x40   //i²c address of the SHT21
  };

  enum PINS {
    PIN_SCL = BIT_2,
    PIN_SDA = BIT_3
  };

  enum Operation {
    TEMPERATURE         = 0xF3,
    HUMIDITY            = 0xF5,
    READREGISTER        = 0xE7,
    WRITEREGISTER       = 0xE6,
    RESET               = 0xFE  //soft reset
  };

  enum Locking {
    ISFREE      = 0,
    TMEASURING  = 1,
    HMEASURING  = 2
  };

  enum Timing {
    SHT_MEASUREMENT_MSEC        = 200,
    SHT_TIMEOUT_MSEC            = 2000
  };

protected:
  VirtualTimer startupTimer;    //timer for result request
  VirtualTimer timeoutTimer;    //timer for chip timeout

  uint8 recFrameCount;
  uint8 lock;
  uint8 measuringResult[3];

  Event resultAvailable;        // triggered by startup timer
  Event sensorTimeout;          // triggered by timeout timer
  Event calculateResult;        // triggered by the handle function

public:
  Sink1<int>* temperature;
  Sink1<int>* humidity;

  Event measureTemperature;     // triggered external
  Event measureHumidity;        // triggered external


public:
  SHT21();

  void init(Sink1<int>*, Sink1<int>*);
  void triggerTempMeasurement();
  void triggerHumidityMeasurement();

  virtual void assign(uint8 request);

private:
  //! enable the module
  virtual void enable();
  //! disable the module
  virtual void disable();

  //interrupt routine
  void handle();

  void getResult();
  void timeout();

  void calculate();

  inline void delay();

private:
  /** activity triggered by resultAvailable event */
  ActivityFunctor<SHT21, &SHT21::getResult> getResultFunctor;
  /** activity triggered by sensorTimeout event */
  ActivityFunctor<SHT21, &SHT21::timeout> timeoutFunctor;

  ActivityFunctor<SHT21, &SHT21::calculate> calculateFunctor;

  ActivityFunctor<SHT21, &SHT21::triggerTempMeasurement> measureTempFunctor;
  ActivityFunctor<SHT21, &SHT21::triggerHumidityMeasurement> measureHumidityFunctor;

};

} //end namespace

#endif // SHT21_H
