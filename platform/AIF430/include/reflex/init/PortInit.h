#ifndef REFLEX_AIF430_PORT_INIT_H
#define REFLEX_AIF430_PORT_INIT_H
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(es):	PortInit
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	set sane defaults for port configuration
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and
 *    Operating Systems. All rights reserved.
 *
 *    Redistribution and use in source and binary forms, with or without
 *    modification, are permitted provided that the following conditions
 *    are met:
 *
 *       1. Redistributions of source code must retain the above copyright
 *          notice, this list of conditions and the following disclaimer.
 *
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the
 *          distribution.
 *
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *    The views and conclusions contained in the software and documentation
 *    are those of the authors and should not be interpreted as representing
 *    official policies, either expressed or implied, of BTU Cottbus,
 *    Department for Distributed Systems and Operating Systems.
 *
 **/

#include "reflex/io/Ports.h"

namespace reflex {

class PortInit
{
public:
	/* reduce voltage drain on ports
	 */
	inline PortInit()
	{
		/* unconnected/unused pins should select I/O port functionality
		 * (which is the default) and output direction (not default)
		 */
		reflex::msp430x::Port1()->DIR = 0xff;
		reflex::msp430x::Port2()->DIR = 0xff;
		reflex::msp430x::Port3()->DIR = 0xff;
		reflex::msp430x::Port4()->DIR = 0xff;
		reflex::msp430x::Port5()->DIR = 0xff;
		reflex::msp430x::PortJ()->DIR = 0xffff;

		/* Pin output is undefined. We have to set those pins to low
		 * that are connected against GND and those to high
		 * that are connected against VCC (to prevent voltage drain)
		 */
		reflex::msp430x::Port1()->OUT = 0x00;
		reflex::msp430x::Port2()->OUT = 0x00;
		reflex::msp430x::Port3()->OUT = 0x00;
		reflex::msp430x::Port4()->OUT = 0x00;
		reflex::msp430x::Port5()->OUT = 0x00;
		reflex::msp430x::PortJ()->OUT = 0x0000;
	}
};

} // reflex

#endif // REFLEX_AIF430_PORT_INIT_H
