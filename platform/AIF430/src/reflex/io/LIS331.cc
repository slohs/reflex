
#include "reflex/io/LIS331.h"
#include "reflex/io/Ports.h"
#include "reflex/pmc/Registers.h"
#include "reflex/MachineDefinitions.h"
#include "NodeConfiguration.h"

using namespace reflex;
using namespace msp430x;
using namespace usci;

LIS331::LIS331()
  : requestDataFunctor(*this)
{
  requestData.init(&requestDataFunctor);

}

void LIS331::init()
{
  Port1()->SEL |= (PIN_CLK | PIN_MISO | PIN_MOSI);

  /* FIXME: please use mnemonics to describe what is mapped here! */
  pmc::Registers()->PMAPPWD = 0x02D52;
  pmc::Port<3>()->MAP5 = 5;
  pmc::Port<3>()->MAP6 = 3;
  pmc::Registers()->PMAPPWD = 0;

  Port3()->DIR |= 0x60;

  //CS pins
  Port3()->DIR |= PIN_CSN;
  Port3()->OUT |= (PIN_CSN | 0x40 | 0x20);

  Registers()->UCCTL1 |= (UCSWRST | UCSSELsmclk);

  Registers()->UCMCTL = 0x00;         //should be cleard
  Registers()->UCBR0 = 0x06;          //Bitrate - 400kHz
  Registers()->UCBR1 = 0x00;

  Registers()->UCCTL0 |= ( UCMST | UCSYNC | UCMODE0 | UCMSB | UCCKPL);

  Port3()->OUT &= ~PIN_CSN;
}

void LIS331::enable()
{
  Port3()->OUT &= ~PIN_CSN;
  Registers()->UCCTL1 &= ~UCSWRST;
}

void LIS331::disable()
{
  Registers()->UCCTL1 |= UCSWRST;
  Port3()->OUT |= PIN_CSN;
}

void LIS331::handle()
{
  if(Registers()->UCIFG & UCTXIFG) {
      getApplication().toggleLed(4);
      delay(); // FIXME: DO NOT DELAY IN INTERRUPT ROUTINE! BAD IDEA!
//      Registers()->UCIFG &= ~UCTXIFG;
      Registers()->UCTXBUF = 0xFF;
//      disable();
  }
  else if(Registers()->UCIFG & UCRXIFG) {
      getApplication().toggleLed(5);
      uint8 t = Registers()->UCRXBUF;
      Registers()->UCIFG &= ~UCRXIFG;
  }
}

/**
 * delay
 * just a little spinning
 */
inline void LIS331::delay()
{
  volatile int i = 0;
  while (i != 1000) ++i;
}

void LIS331::getValues()
{
  uint8 t;
  Port3()->OUT &= ~PIN_CSN;
  Registers()->UCCTL1 &= ~UCSWRST;
  getApplication().toggleLed(4);
  enable();

  delay();

//  while(Registers()->UCSTAT&UCBUSY);
//  Registers()->UCTXBUF = WHO_AM_I | READ;

  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = CTRL_REG1;
  while(Registers()->UCSTAT&UCBUSY);
  //0xDF
  Registers()->UCTXBUF = 0xF;
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = 0xD;
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = CTRL_REG1 | READ;

  while(Registers()->UCSTAT&UCBUSY);
  t = Registers()->UCRXBUF;
  Registers()->UCTXBUF = OUT_X_L | READ;

  while(Registers()->UCSTAT & UCBUSY);
  t = Registers()->UCRXBUF;
  Registers()->UCTXBUF = OUT_Y_L | READ;

  while(Registers()->UCSTAT & UCBUSY);
  t = Registers()->UCRXBUF;

  delay();

  disable();
  Registers()->UCIE |= (UCTXIE | UCRXIE);

  delay();
  Registers()->UCCTL1 |= UCSWRST;

//  uint8 addr = 0x1F << 2; //shift by 2 and ad RW flag
//  spi.enable();
//  addr = spi.send(addr);
//  addr = spi.send(0); //dummy write
//  spi.disable();
}
