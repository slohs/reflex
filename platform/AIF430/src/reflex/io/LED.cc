/*
 *      REFLEX - Real-time Event FLow EXecutive
 *
 *      A lightweight operating system for deeply embedded systems.
 *
 */

#include "reflex/io/LED.h"

using namespace reflex;

LED::LED()
{
  msp430x::Port3()->SEL &= ~(LED_1 | LED_2| LED_3| LED_4| LED_5);
  msp430x::Port3()->DIR |= LED_1 | LED_2| LED_3| LED_4| LED_5;
//  msp430x::Port3()->OUT |= LED_1 | LED_2| LED_3| LED_4| LED_5;

  msp430x::Port3()->OUT |= LED_1;

}

void LED::toggleAll()
{
  msp430x::Port3()->OUT ^= (LED_1 | LED_2| LED_3| LED_4| LED_5);
}

void LED::toggle(char value)
{
  switch(value) {
  case 1:
    msp430x::Port3()->OUT ^= 0x01;
    break;
  case 2:
      msp430x::Port3()->OUT ^= 0x02;
      break;
  case 3:
      msp430x::Port3()->OUT ^= 0x04;
      break;
  case 4:
      msp430x::Port3()->OUT ^= 0x08;
      break;
  case 5:
      msp430x::Port3()->OUT ^= 0x10;
      break;
  default:
    break;
  }

}

void LED::assign(char value)
{
  uint8 v = msp430x::Port3()->OUT;

  v &= ~(LED_1 | LED_2| LED_3| LED_4| LED_5);
  v |= value; //value will be inverted
//  v |= ((~value)<<4); //value will be inverted

  msp430x::Port3()->OUT = v;
}
