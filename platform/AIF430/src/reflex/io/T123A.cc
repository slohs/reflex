
#include "reflex/io/T123A.h"

using namespace reflex;
using namespace msp430x;
using namespace usci;

T123A::T123A(Pool& pool)
  :
//      InterruptHandler(interrupts::USCIB0, PowerManageAble::PRIMARY )
    displayFunctor(*this)
  , pool(pool)
{

  displayOut.init(&displayFunctor);
}

void T123A::init()
{
  waitCursor = 0;

  Port1()->SEL |= (PIN_SCL | PIN_SDA);

  //Baudrate - 100kHz
  Registers()->UCBR0 = 0x18;
  Registers()->UCBR1 = 0x00;

  Registers()->UCCTL1 |= (UCSWRST | UCSSELsmclk);
  Registers()->UCCTL0 = (UCMST | UCMODE3 | UCSYNC);
  Registers()->UCCTL1 &= ~UCSWRST;

  //init display-controller(pcf2116)
  Registers()->UCIE = 0;

//  this->lock();
//
//  checkBusyFlag();
//
//  while(Registers()->UCSTAT&UCBUSY);
//  Registers()->UCCTL1 |= (UCTR | UCTXSTT);
//  Registers()->UCTXBUF = MODE_CTRL_FUNCTION;
//  Registers()->UCI2CSA = ADDRESS;
//
//  delay();
//  while(Registers()->UCSTAT&UCBUSY);
//  Registers()->UCTXBUF = FUNCTION_SET;

  cmdQueue[0] = FUNCTION_SET;
  cmdQueue[1] = DISPLAY_CONTROL | DISPLAY_CONTROL_DISPLAY_ON | DISPLAY_CONTROL_CURSOR_ON;
  cmdQueue[2] = ENTRY_MODE_SET;
  cmdQueue[3] = CLEAR_DISPLAY;
  cmdQueue[4] = NOP;

  sendInstructions(cmdQueue);

//  delay();
//  while(Registers()->UCSTAT&UCBUSY);
//  Registers()->UCTXBUF = DISPLAY_CONTROL | DISPLAY_CONTROL_DISPLAY_ON | DISPLAY_CONTROL_CURSOR_ON;
//
//  delay();
//  while(Registers()->UCSTAT&UCBUSY);
////  Registers()->UCTXBUF = ENTRY_MODE_SET | ENTRY_MODE_INCREMENT | ENTRY_MODE_DISPLAY_SHIFT;
//  Registers()->UCTXBUF = ENTRY_MODE_SET;
//
//  delay();
//  while(Registers()->UCSTAT&UCBUSY);
//  Registers()->UCCTL1 |= UCTXSTP;
//
//  delay();
//  while(Registers()->UCSTAT&UCBUSY);
//  disable();
//
//  this->unlock();

//  this->clearDisplay();
}


void T123A::enable()
{
  Registers()->UCCTL1 &= ~UCSWRST;
  enabled = true;
}

void T123A::disable()
{
  Registers()->UCCTL1 |= UCSWRST;
  enabled = false;
}

void T123A::run()
{

}

void T123A::handle()
{

}

/**
 * delay
 * just a little spinning for clear signals
 */
inline void T123A::delay()
{
  volatile int i = 0;
  while (i != 30) ++i;
}

inline void T123A::nop()
{
  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = NOP;
  while(Registers()->UCSTAT&UCBUSY);
}

void T123A::sendInstructions(uint8 cmd[])
{
  this->lock();
  enable();

  checkBusyFlag();

  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCCTL1 |= (UCTR | UCTXSTT);
  Registers()->UCTXBUF = MODE_CTRL_FUNCTION;
  Registers()->UCI2CSA = ADDRESS;
  delay();
  while(Registers()->UCSTAT&UCBUSY);

  for(int i=0; i<5; i++) {
    if(cmd[i] == NOP) break;

    Registers()->UCTXBUF = cmd[i];

    if(cmd[i] == CLEAR_DISPLAY) {
      volatile int i = 0;
      while (i != 3000) ++i;
    }else delay();

    while(Registers()->UCSTAT&UCBUSY);
  }

  disable();

  this->unlock();
}

inline void T123A::checkBusyFlag()
{
  uint8 c = 0;

  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCCTL1 |= (UCTR | UCTXSTT);
  Registers()->UCTXBUF = MODE_READ_STATUS;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCCTL1 &= ~UCTR;
  Registers()->UCCTL1 |= UCTXSTT;

  delay();
  while(Registers()->UCSTAT&UCBUSY);

  while(Registers()->UCRXBUF & 0x80)
  {
    if(++c = 1000) return;

    delay();
    while(Registers()->UCSTAT&UCBUSY);
  }

  Registers()->UCCTL1 |= UCTXSTP;
  delay();
  while(Registers()->UCSTAT&UCBUSY);
}

/**
 * clear display, write spaces to all ddram addresses
 * set address counter to 0
 */
void T123A::clearDisplay()
{
  enable();
  Registers()->UCIE = 0;

  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCCTL1 |= (UCTR | UCTXSTT);
  Registers()->UCTXBUF = MODE_CTRL_FUNCTION;
  Registers()->UCI2CSA = ADDRESS;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = CLEAR_DISPLAY;

  delay();delay();delay();delay();delay();delay();delay();delay();
  while(Registers()->UCSTAT&UCBUSY);

  Registers()->UCCTL1 |= UCTXSTP;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
}

void T123A::updateWaitCursor()
{
  checkBusyFlag();

  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCCTL1 |= (UCTR | UCTXSTT);
  Registers()->UCI2CSA = ADDRESS;
  Registers()->UCTXBUF = MODE_CTRL_FUNCTION;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = RETURN_HOME;

  //set cursor address
  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = SET_DDRAM_ADDRESS | LINE1 | CHAR12;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCCTL1 |= (UCTR | UCTXSTT);
  Registers()->UCTXBUF = MODE_WRITE_DATA;
  Registers()->UCI2CSA = ADDRESS;

  delay();
  while(Registers()->UCSTAT&UCBUSY);

  if(waitCursor) {
      Registers()->UCTXBUF = 0x20;
      waitCursor = 0;
  } else {
    Registers()->UCTXBUF = 0xAA;
    waitCursor++;
  }

  delay();
  Registers()->UCCTL1 |= UCTXSTP;
}

void T123A::triggerDisplay()
{
  this->lock();

  enable();
  Registers()->UCIE = 0;

  checkBusyFlag();

  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCCTL1 |= (UCTR | UCTXSTT);
  Registers()->UCI2CSA = ADDRESS;
  Registers()->UCTXBUF = MODE_CTRL_FUNCTION;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = RETURN_HOME;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = SET_DDRAM_ADDRESS | 0x20;    //DRAM address 0x20

  delay();
  while(Registers()->UCSTAT&UCBUSY);

  Registers()->UCCTL1 |= (UCTR | UCTXSTT);
  delay();

  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = MODE_WRITE_DATA;
  Registers()->UCI2CSA = ADDRESS;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = 0xB1;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = 0xB2;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = 0xB3;

  delay();
  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCTXBUF = 0x3D;

  nop();

  while(Registers()->UCSTAT&UCBUSY);
  Registers()->UCCTL1 |= UCTXSTP;

//  delay();
  while(Registers()->UCSTAT&UCBUSY);

  updateWaitCursor();

  disable();
  this->unlock();
}
