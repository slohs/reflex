/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 **/

#include "reflex/power/Battery.h"

using namespace reflex;

Battery::Battery()
	: adcOut(0)
{
	input.init(this);
}

void Battery::run()
{
	ADC::Request request(ADC::Ref_Vref_AVss,ADC::InCh_AVccDIV,ADC::CH1);
	if(adcOut) adcOut->assign(request);
}


//subclass Calculator

Battery::Calculator::Calculator()
	: SingleValue1<uint16>(this)
	, valueOut(0)
{}

void Battery::Calculator::run()
{
	// Convert ADC value to "x.xx V"
	// Ideally we have A11=0->AVCC=0V ... A11=4095(2^12-1)->AVCC=4V
	// --> (A11/4095)*4V=AVCC --> AVCC=(A11*4)/4095
	uint16 voltage = (this->get()<<2)/41; // (adc * 4 / 4095) * 100
	if(valueOut) valueOut->assign(voltage);
}

