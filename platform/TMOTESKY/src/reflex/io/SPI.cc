/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
  
 *	Author:		 Carsten Schulze, Olaf Krause
 *
 * design considerations: I think that an interrupt at fifo is NOT nessesary (also sayed in the datasheed, only to determ that somethin is in the RXFIFO)
 *
 *
 */


#include "reflex/io/Port.h"

#include "reflex/io/SerialRegisters.h"
#include "reflex/io/CC2420Registers.h"
#include "reflex/io/SPI.h"
#include "reflex/MachineDefinitions.h"


using namespace reflex;

SPI::SPI() 
// : rxiHandler(INTVEC_USART0_RXI ,*this)

{

	// Configure serial 1 for SPI-mode:
	regs = (volatile SerialRegisters*)(mcu::UART0_BASE);
	regsSFR = (volatile SFRRegisters*)(mcu::USART_SFR_BASE);

	// make it possible to overwrite settings -> reset of UART
	regs->UCTL = SerialRegisters::SWRST;

	// delay UCLK by half clock cycle, falling edge output, use SMCLK, 3 wire SPI
	regs->UTCTL = (SerialRegisters::CKPH | SerialRegisters::SSEL10 | SerialRegisters::STC);

	// set highest clock speed
	regs->UBR0 = (char)2;
	regs->UBR1 = (char)0;
	regs->UMCTL = (char)0;

	// clear interrupt flags
	regsSFR->IFG &= ~(SerialRegisters::UTXIFG0 | SerialRegisters::URXIFG0);

	// clears SWRST so operation starts
	//            8bit chars                    master mode            SPI
	regs->UCTL = SerialRegisters::USART_CHAR | SerialRegisters::MM | SerialRegisters::SYNC;

	receive = false;
	rxBuff = 0;
}


void SPI::enable()
{

	//enable USART interrupts

	//// SPI pins are multiplexed with Port 3 general-purpose IO
	//// Select module funtionality
	Port3()->SEL |= Port::SPI0_BITS;
	Port3()->DIR |= (Port::SIMO0 | Port::RADIO_SCLK);
	Port3()->DIR &= ~Port::SOMI0;


	regsSFR->ME |= SerialRegisters::USPIE0;

	//	regsSFR->IE |= SerialRegisters::URXIE0 | SerialRegisters::UTXIE0 ; // we only use RX

}

void SPI::disable()
{
		// SPI pins are multiplexed with Port 3 general-purpose IO
		// Select IO funtionality
		Port3()->SEL &= ~Port::SPI0_BITS;
		Port3()->DIR &= ~Port::SPI0_BITS;
		regsSFR->ME &= ~SerialRegisters::USPIE0;

		//disable USART interrupts (only RX used)
		regsSFR->IE &= ~SerialRegisters::URXIE0;//
}

void SPI::handleRXI()
{
  receive = true;
  rxBuff = this->regs->URXBUF;
}

bool SPI::rx_interrupt()
{
  return ((regsSFR->IFG = SerialRegisters::URXIFG0) !=0);
}


void SPI::tx_byte(uint8 b)
{
  while (!(regs->UTCTL & SerialRegisters::TXEPT));
  regs->UTXBUF = b;
}

uint8 SPI::rx_byte()
  {
    rxBuff = this->regs->URXBUF;
    return rxBuff;
  }

void SPI::flash_tx_byte(uint8 b)
{
  while (!(regs->UTCTL & SerialRegisters::TXEPT));
  regs->UTXBUF = b;
  while (!(regs->UTCTL & SerialRegisters::TXEPT)); // important for Flash communication!
}

uint8 SPI::flash_rx_byte()
{
	regs->UTXBUF = 0; // seems to be important for flash communication
  rxBuff = this->regs->URXBUF;
  return rxBuff;
}
