#include "reflex/io/CC2420.h"

#include "reflex/interrupts/InterruptLock.h"

using namespace reflex;

CC2420::CC2420(SPI &spi_ref) :
  spi(spi_ref)
  //chip_select_port(Port::PORT4)
  //p1(Port::PORT1)
{
  // set IO functionality
  Port4()->SEL &= ~(Port::RADIO_CS | Port::RADIO_SFD |  Port::RADIO_RESET | Port::RADIO_VREF_EN);

  // SFD pin functionality is input to TimerB (capture/compare input 1)
  // Port4()->SEL |= Port::RADIO_SFD;
  Port4()->DIR &= ~Port::RADIO_SFD;

  Port4()->OUT |= (Port::RADIO_CS | Port::RADIO_RESET | Port::RADIO_VREF_EN );
  //  Port4()->OUT &= ~Port::RADIO_VREF_EN;

  // set chip select, reset, and vref enable pins as outputs
  Port4()->DIR |= (Port::RADIO_CS | Port::RADIO_RESET | Port::RADIO_VREF_EN);

  // configure CCA, FIFO, and FIFOP signals as inputs (with IO functionality)
  Port1()->SEL &= ~(Port::RADIO_GIO0 | Port::RADIO_GIO1 | Port::PKT_INT);
  Port1()->OUT &= ~(Port::PKT_INT | Port::RADIO_GIO0 | Port::RADIO_GIO1);
  Port1()->DIR &= ~(Port::RADIO_GIO0 | Port::RADIO_GIO1 | Port::PKT_INT);
  Port1()->IES &= ~(Port::PKT_INT); //added do false, do not need CCA
    Port1()->IE &= ~(Port::PKT_INT);

    //the upper version works as well
// 	*p1.SEL &= ~(Port::RADIO_GIO0 | Port::RADIO_GIO1 |Port::PKT_INT); //sel for pin function
// 	Port1()->OUT &= ~(Port::PKT_INT | Port::RADIO_GIO0 | Port::RADIO_GIO1); //clear output register
// 	Port1()->DIR &= ~(Port::RADIO_GIO0 | Port::RADIO_GIO1 |Port::PKT_INT); // set pin as input
// 	Port1()->IES &= ~Port::PKT_INT; // set interrupt behavior low to high
// 	//??	Port1()->IFG &= ~Port::PKT_INT; // reset interruos

// Port1()->IE &= ~(Port::PKT_INT);



// 	volatile Port p4(Port::PORT4);
// 	*p4.SEL &= ~(Port::RADIO_VREF_EN | Port::RADIO_RESET | Port::RADIO_CS | Port::RADIO_SFD); //set for pin function
// 	p4.reg->OUT |= Port::RADIO_RESET | Port::RADIO_CS | Port::RADIO_VREF_EN; //pins low active (exapt VREF)
// 	p4.reg->DIR |= Port::RADIO_VREF_EN | Port::RADIO_RESET | Port::RADIO_CS; //set as output
// 	p4.reg->DIR &= ~(Port::RADIO_SFD); //set as input

  this->rx_fifo_bytes_read = 0;
}

void CC2420::voltage_regulator_enable()
{
  Port4()->OUT |= Port::RADIO_VREF_EN;
}

void CC2420::voltage_regulator_disable()
{
  Port4()->OUT &= ~Port::RADIO_VREF_EN;
}

void CC2420::hardware_reset()
{
  Port4()->OUT &= ~Port::RADIO_RESET;
  asm("nop;");
  asm("nop;");
  Port4()->OUT |= Port::RADIO_RESET;
}

void CC2420::software_reset()
{
  this->write_register(REG_MAIN, 0x7800);
  this->write_register(REG_MAIN, 0xF800);
}

void CC2420::enable_rx_interrupt()
{
  //Port1()->IES &= ~Port::PKT_INT;
/*
  if (Port1()->IN & Port::PKT_INT)
  {
    Port1()->IFG |= Port::PKT_INT;
  }
*/
  //Port1()->IN &= ~Port::PKT_INT;
  Port1()->IE |= Port::PKT_INT;
}

void CC2420::set_rf_channel(uint8 channel)
{
  uint16 reg_value;

  // freq[9:0] = 357 + 5 * (k - 11)
  if ((channel < 11) || (channel > 26))
  {
    return;
  }

  channel = channel - 11;
  channel += ((channel << 2) & 0xFC);

  this->read_register(REG_FSCTRL, reg_value);
  reg_value &= 0xFC00;
  reg_value |= (357 + channel);
  this->write_register(REG_FSCTRL, reg_value);
}

/** Power level settings
PA_LEVEL | TXCTRL register | Output Power [dBm] | Current Consumption [mA]
--------------------------------------------------------------------------
31       | 0xA0FF          |      0             |  17.4
27       | 0xA0FB          |      -1            |  16.5
23       | 0xA0F7          |      -3            |  15.2
19       | 0xA0F3          |      -5            |  13.9
15       | 0xA0EF          |      -7            |  12.5
11       | 0xA0EB          |      -10           |  11.2
7        | 0xA0E7          |      -15           |  9.9
3        | 0xA0E3          |      -25           |  8.5
**/

void CC2420::set_output_power_level(uint8 power_level)
{
  uint8 temp[2];
  this->read_register_buffer(REG_TXCTRL, temp);
  temp[1] &= 0xE0;
  temp[1] |= (power_level & 0x1F);
  this->write_register_buffer(REG_TXCTRL, temp);
}

void CC2420::set_short_addr(uint16 short_addr)
{
  char temp[2];
  temp[0] = (char) short_addr;
  temp[1] = (char) (short_addr >> 8);
  this->write_ram(SECURITY_BANK, RAM_SHORTADR, temp, 2);
}

void CC2420::set_pan_id(uint16 value)
{
  char temp[2];
  temp[0] = (char) value;
  temp[1] = (char) (value >> 8);
  this->write_ram(SECURITY_BANK, RAM_PANID, temp, 2);
}

void CC2420::set_extended_addr(char *addr)
{
  this->write_ram(SECURITY_BANK, RAM_IEEEADR, addr, 8);
}

void CC2420::set_pan_coordinator(bool value)
{
  uint8 temp[2];
  this->read_register_buffer(REG_MDMCTRL0, temp);
  if (value)
  {
    temp[0] |= 0x10;
  }
  else
  {
    temp[0] &= 0xEF;
  }
  this->write_register_buffer(REG_MDMCTRL0, temp);
}

void CC2420::set_auto_ack(bool value)
{
  uint8 temp[2];
  this->read_register_buffer(REG_MDMCTRL0, temp);
  if (value)
  {
    temp[1] |= 0x10;
  }
  else
  {
    temp[1] &= 0xEF;
  }
  this->write_register_buffer(REG_MDMCTRL0, temp);
}

void CC2420::set_auto_crc(bool value)
{
  uint8 temp[2];
  this->read_register_buffer(REG_MDMCTRL0, temp);
  if (value)
  {
    temp[1] |= 0x20;
  }
  else
  {
    temp[1] &= 0xDF;
  }
  this->write_register_buffer(REG_MDMCTRL0, temp);
}

void CC2420::set_rxfifo_protection(bool value)
{
  uint8 temp[2];
  this->read_register_buffer(REG_SECCTRL0, temp);
  if (value)
  {
    temp[0] |= 0x02;
  }
  else
  {
    temp[0] &= 0xFD;
  }
  this->write_register_buffer(REG_SECCTRL0, temp);
}

void CC2420::set_address_filter(bool value)
{
  uint8 temp[2];
  this->read_register_buffer(REG_MDMCTRL0, temp);
  if (value)
  {
    temp[0] |= 0x08;
  }
  else
  {
    temp[0] &= 0xF7;
  }
  this->write_register_buffer(REG_MDMCTRL0, temp);
}

void CC2420::set_accept_beacons(bool value)
{
  uint8 temp[2];
  this->read_register_buffer(REG_IOCFG0, temp);
  if (value)
  {
    temp[0] |= 0x08;
  }
  else
  {
    temp[0] &= 0xF7;
  }
  this->write_register_buffer(REG_IOCFG0, temp);
}

void CC2420::set_fifo_threshold(uint8 value)
{
  uint8 temp[2];
  this->read_register_buffer(REG_IOCFG0, temp);
  temp[1] &= 0x80;
  temp[1] |= (value & 0x7F);
  this->write_register_buffer(REG_IOCFG0, temp);
}

uint8 CC2420::command_strobe(uint8 address)
{
  // Address 1 format: 0 0 A5 A4 ... A0
  return this->spi_transfer(address, 0xFF, 0, false, 0);
}

uint8 CC2420::read_register(uint8 address, uint16& value)
{
  char temp[2];
  uint8 status_byte = this->read_register_buffer(address, (uint8*) temp);

  value = (uint8) temp[1];
  value += ((uint16) (temp[0] << 8));

  return status_byte;
}

uint8 CC2420::write_register(uint8 address, uint16 value)
{
  char temp[2];
  temp[1] = (char) value;
  temp[0] = (char) (value >> 8);

  return this->write_register_buffer(address, (uint8*) temp);
}

uint8 CC2420::read_register_buffer(uint8 address, uint8 *value)
{
  return this->spi_transfer(0x40 | address, 0xFF, 2, false, value);
}

uint8 CC2420::write_register_buffer(uint8 address, uint8 *value)
{
  return this->spi_transfer(address, 0xFF, 2, true, value);
}

uint8 CC2420::read_ram(uint8 bank, uint8 address, char* buffer, uint8 length)
{
  // Address 1 format: 1 A6 A5 A4 ... A0
  return this->spi_transfer(0x80 | address, bank | 0x20, length, false, (uint8*) buffer);
}

uint8 CC2420::write_ram(uint8 bank, uint8 address, char* buffer, uint8 length)
{
  // Address 1 format: 1 A6 A5 A4 ... A0
  return this->spi_transfer(0x80 | address, bank, length, true, (uint8*) buffer);
}

//uint8 CC2420::read_rx_fifo(char* buffer, uint8 length, bool consider_fifo_pin)
uint8 CC2420::read_rx_fifo(uint8 *buffer, uint8 length, bool consider_fifo_pin)
{
  return this->spi_transfer(0x40 | REG_RXFIFO, 0xFF, length, false,
    (uint8*) buffer, consider_fifo_pin);
}

uint8 CC2420::write_tx_fifo(uint8* buffer, uint8 length)
{
  return this->spi_transfer(REG_TXFIFO, 0xFF, length, true, (uint8*) buffer);
}

uint8 CC2420::spi_transfer(
                           uint8 address,     // first byte to be written
                           uint8 address2,    // second byte to be written,
                                              // bit0 set to '1' indicates "invalid", i.e. not used
                           uint8 length,     // number of bytes to be written or read
                           bool write_data,   // buffer is to be written
                           uint8 *buffer,
                           bool rxfifo_mode   // read bytes only if FIFO pin is high
                          )
{
  uint8 status_byte = 0;
  uint8 i;
  uint8 rx_byte;

  // make sure SPI access is not interrupted
  // this way, SPI bus can be used by multiple clients without contention
  // System responsiveness is increased!


  //at this point it is seams ok, period is short and an interrupt driven version slows down
  InterruptLock lock;


  // assert chip select signal
  this->start_transfer();

  // clear RX interrupt flag
  rx_byte = this->spi.rx_byte();

  this->spi.tx_byte(address);
  while (!this->spi.rx_interrupt());


  status_byte = this->spi.rx_byte();

  // write second address byte, if valid
  if (!(address2 & 0x01))
  {
    this->spi.tx_byte(address2);
    while (!this->spi.rx_interrupt());
    rx_byte = this->spi.rx_byte();
  }

  i = 0;


  //transmit data
  while ((i < length) && ((!rxfifo_mode) || ((Port1()->IN & Port::RADIO_GIO0) != 0x00)))
    { // read or write number of bytes
      if (write_data)
	{
	  this->spi.tx_byte(*buffer);
	  buffer++;
	}
      else
	{
	  this->spi.tx_byte(0x00);
	  if (i > 0)
	    {
	      *buffer = rx_byte;
	      buffer++;
	    }
	}

      while (!this->spi.rx_interrupt());
      rx_byte = this->spi.rx_byte();
      i++;
    }

  // deassert chip select signal
  this->stop_transfer();

  if (rxfifo_mode)
    {
      this->rx_fifo_bytes_read = i;
    }

  // write last read byte into the buffer
  if ((i > 0) && (!write_data))
    {
      *buffer = rx_byte;
      buffer++;
  }

  return status_byte;
}
