#ifndef CC2420Registers_h
#define CC2420Registers_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	none
 *
 *	Author:		Carsten Schulze
 *
 *	Description:	accessing the cc2420
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

namespace reflex {
	
enum Command_Strobes
{
	CMD_SNOP         = 0x00,
	CMD_SXOSCON      = 0x01,
	CMD_STXCAL       = 0x02,
	CMD_SRXON        = 0x03,
	CMD_STXON        = 0x04,
	CMD_STXONCCA     = 0x05,
	CMD_SRFOFF       = 0x06,
	CMD_SXOSCOFF     = 0x07,
	CMD_SFLUSHRX     = 0x08,
	CMD_SFLUSHTX     = 0x09,
	CMD_SACK         = 0x0A,
	CMD_SACKPEND     = 0x0B,
	CMD_SRXDEC       = 0x0C,
	CMD_STXENC       = 0x0D,
	CMD_SAES         = 0x0E
};

enum CC2420_Registers
{
	REG_MAIN       = 0x10,
	REG_MDMCTRL0   = 0x11,
	REG_MDMCTRL1   = 0x12,
	REG_RSSI       = 0x13,
	REG_SYNCWORD   = 0x14,
	REG_TXCTRL     = 0x15,
	REG_RXCTRL0    = 0x16,
	REG_RXCTRL1    = 0x17,
	REG_FSCTRL     = 0x18,
	REG_SECCTRL0   = 0x19,
	REG_SECCTRL1   = 0x1a,
	REG_BATMON     = 0x1b,
	REG_IOCFG0     = 0x1c,
	REG_IOCFG1     = 0x1d,
	REG_TXFIFO     = 0x3E,
	REG_RXFIFO     = 0x3F
};

enum CC2420_Main_Reg
{
	RESETn = 0x8000, // bit 15
	ENC_RESETn = 0x4000, // bit 14
	DEMOD_RESETn = 0x2000, // bit 13
	MOD_RESETn = 0x1000, // bit 12
	FS_RESETn = 0x0800, // bit 11
	XOSC16M_BYPASS = 0x0001 //bit 0
};

enum CC2420_MDMCTRL0
{
	ADR_DECODE = 0x0800,
	AUTO_ACK   = 0x0010,
	AUTOCRC    = 0x0020,
	CCA_MODE   = 0x00c0,
	CCA_MODE1  = 0x0040,
	CCA_MODE2  = 0x0080,
	CCA_MODE3  = 0x00c0
};

enum CC2420_MDMCTRL1
{
	CORR_THR    = 0x07c0,
	CORR_THR_20 = 0x0500
};

enum CC2420_SECCTRL
{
	RXFIFO_PROTECTION = 0x0200,
	SEC_MODE     = 0x0003,
	SEC_MODE_CBC = 0x0001,
	SEC_MODE_CTR = 0x0002,
	SEC_MODE_CCM = 0x0003
	
};

enum CC2420_IOCFG0
{
	BCN_ACCPET = 0x0800,
	FIFOP_THR  = 0x007f
};

enum CC2420_RXCTRL
{
	RXBPF_LOCUR = 0x2000
};

enum Memory_Bank
{
	TX_FIFO_BANK = 0x00,
	RX_FIFO_BANK = 0x40,
	SECURITY_BANK = 0x80
};

enum CC2420_RAM_Addresses
{
	RAM_SHORTADR = 0x6A,
	RAM_PANID = 0x68,
	RAM_IEEEADR = 0x60
};

enum STATUS_BITS
{
	RSSI_VALID = 0x02,
	LOCKED = 0x04,
	TX_ACTIVE = 0x08,
	ENC_BUSY = 0x10,
	TX_UNDERFLOW = 0x20,
	XOSC16M_STABLE = 0x40
};

enum PINS 
{
	FIFOP    = 0x01,  // P1.0, CC2420_FIFOP
	FIFO = 0x08,  // P1.3, CC2420_FIFO
	CCA = 0x10,  // P1.4, CC2420_CCA
	SFD     = 0x02,  // P4.1, CC2420 start frame delimiter
	CS      = 0x04  // P4.2, CC2420 chip select
};
 

/*
struct CC2420_Main_Reg
{
	char RESETn:1; // bit 15
	char ENC_RESETn:1; // bit 14
	char DEMOD_RESETn:1; // bit 13
	char MOD_RESETn:1; // bit 12
	char FS_RESETn:1; // bit 11
	char dummy:10; // bit 10:1 reserved
	char XOSC16M_BYPASS; //bit 0
};
*/
} //reflex

#endif
