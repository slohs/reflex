#ifndef HWTrigger_h
#define HWTrigger_h
/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	Trigger
 *
 *	Author:		Stefan Nuernberger
 *
 *	Description:	Class for triggering General I/O-pins (GIO) on TmoteSky
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#include "reflex/sinks/Sink.h"

namespace reflex {

class HWTrigger : public Sink1<unsigned char> {
public:

	/**
	* constructor
	* initialises the GIO Pins on Port2
	*/
	HWTrigger();

	enum Pins {
		GIO0 = 0x01, // P2.0 -- needs R14! (10pin Header - pin 10)
		GIO1 = 0x02, // P2.1 -- needs R16! (10pin Header - pin 7)
		GIO2 = 0x08, // P2.3 (6pin Header - pin 3)
		GIO3 = 0x40, // P2.6 (6pin Header - pin 4)
		ALL = 0x4B
	};
	
	/**
	* Turn on voltage on the pin
	* @param pin : no. of pin to activate
	* @param only if true : all other pins will be turned low
	*/
	void turnOn(unsigned char pin, bool only = false);

	/**
	* Turn off voltage on the pin
	* @param pin : no. of pin to deactivate
	*/
	void turnOff(unsigned char pin);

	/**
	* trigger the pin (turn on and off)
	* @param pin : no. of pin to trigger
	*/
	void trigger(unsigned char pin);

	/**
	 *  This method triggers the pin
	 *  Implements the Sink1 interface.
	 *  @param pin : no. of pin to toggle
	 */
	virtual void assign(unsigned char pin);
};

} //reflex
#endif
