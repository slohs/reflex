#ifndef CC2420_h
#define CC2420_h

#include "SPI.h"
#include "reflex/io/Port.h"
#include "reflex/types.h"

namespace reflex
{
  class CC2420
  {
  protected:
    SPI& spi;

    enum Command_Strobes
    {
      CMD_SNOP         = 0x00,
      CMD_SXOSCON      = 0x01,
      CMD_STXCAL       = 0x02,
      CMD_SRXON        = 0x03,
      CMD_STXON        = 0x04,
      CMD_STXONCCA     = 0x05,
      CMD_SRFOFF       = 0x06,
      CMD_SXOSCOFF     = 0x07,
      CMD_SFLUSHRX     = 0x08,
      CMD_SFLUSHTX     = 0x09,
      CMD_SACK         = 0x0A,
	  CMD_SACKPEND     = 0x0B
    };

    enum CC2420_Registers
    {
      REG_MAIN     = 0x10,
      REG_MDMCTRL0 = 0x11,
      REG_MDMCTRL1 = 0x12,
      REG_RSSI     = 0x13,
      REG_TXCTRL   = 0x15,
      REG_RXCTRL   = 0x17,
      REG_FSCTRL   = 0x18,
      REG_SECCTRL0 = 0x19,
      REG_IOCFG0   = 0x1C,
      REG_TXFIFO   = 0x3E,
      REG_RXFIFO   = 0x3F
    };

    enum CC2420_RAM_Addresses
    {
      RAM_SHORTADR = 0x6A,
      RAM_PANID = 0x68,
	  RAM_IEEEADR = 0x60
    };

    // return value: status byte
    uint8 command_strobe(uint8 address);
  public:
    uint8 read_register(uint8 address, uint16& value);
    uint8 write_register(uint8 address, uint16 value);

    uint8 read_register_buffer(uint8 address, uint8 *value);
    uint8 write_register_buffer(uint8 address, uint8 *value);

    // common spi access procedure
    uint8 spi_transfer(  // returns status byte read during writing of first byte
      uint8 address,     // first byte to be written
      uint8 address2,    // second byte to be written,
                         // bit0 set to '1' indicates "invalid", i.e. not used
      uint8 length,      // number of bytes to written or read
      bool write_data,   // buffer is to be written
      uint8 *buffer,     // read or write buffer
      bool rxfifo_mode = false
    );
    
    uint8 rx_fifo_bytes_read;

    // protected:
    inline void start_transfer()
    {
      Port4()->OUT &= ~Port::RADIO_CS;
    }

    inline void stop_transfer()
    {
      Port4()->OUT |= Port::RADIO_CS;
    }

  public:
    enum STATUS_BITS
    {
      RSSI_VALID = 0x02,
      LOCK = 0x04,
      TX_ACTIVE = 0x08,
      ENC_BUSY = 0x10,
      TX_UNDERFLOW = 0x20,
      XOSC16M_STABLE = 0x40
    };

    // CC2420 has got 3 RAM banks, encoded by bits [B1 B0]
    // Encoding is already shifted to bit positions 7 and 6
    enum Memory_Bank
    {
      TX_FIFO_BANK = 0x00,
      RX_FIFO_BANK = 0x40,
      SECURITY_BANK = 0x80
    };

    enum Timing_Parameters
    {
      // all timings are in microseconds
      // information from CC2420 datasheet
      MAX_VREG_START_UP_TIME = 600,
      TYP_VREG_START_UP_TIME = 300,
	  PLL_LOCK_TIME = 192
    };

    CC2420(SPI& spi_ref);

    uint8 read_ram(uint8 bank, uint8 address, char* buffer, uint8 length);
    uint8 write_ram(uint8 bank, uint8 address, char* buffer, uint8 length);

    uint8 read_rx_fifo(uint8* buffer, uint8 length, bool consider_fifo_pin); //as
    uint8 num_rx_fifo_bytes_read();
    uint8 write_tx_fifo(uint8* buffer, uint8 length);

    void hardware_reset();
    void software_reset();

    void set_sfd_gpio();
    void set_sfd_module_input();

    uint8 get_status();
    bool get_cca();
    void clear_cca_ifg();
    void set_cca_ifg();
    bool get_cca_ifg();
    
    void enable_rx_interrupt();
    void disable_rx_interrupt();
    bool get_rx_interrupt_enabled();
        
    bool get_rx_interrupt_flag();
    void set_rx_interrupt_flag();
    void clear_rx_interrupt_flag();
    bool get_rx_interrupt_in();
    void clear_rx_interrupt_in();
    bool get_fifo_in();

    void voltage_regulator_enable(); // brings CC2420 into power down mode
    void voltage_regulator_disable();

    uint8 enable_oscillator(); // brings CC2420 into IDLE mode
    uint8 disable_oscillator();

    void set_rf_channel(uint8 channel);
    void set_output_power_level(uint8 power_level);
    uint8 flush_rx_fifo();
    uint8 flush_tx_fifo();
    uint8 start_rx();
    uint8 start_tx(bool on_cca);
    uint8 send_ack(bool frame_pending);
    uint8 tx_calibrate();
    uint8 stop_rx_tx();
    uint8 get_rssi(uint16 &value);

    void set_short_addr(uint16 short_addr);
    void set_pan_id(uint16 value);
    void set_extended_addr(char *addr);
    void set_pan_coordinator(bool value);
    void set_auto_ack(bool value);
    void set_auto_crc(bool value);
    void set_rxfifo_protection(bool value);
    void set_address_filter(bool value);
    void set_accept_beacons(bool value);
    void set_fifo_threshold(uint8 value);
  };

  //void swap_uint16(uint16 &value);

  inline uint8 CC2420::get_status()
  {
    return this->command_strobe(CMD_SNOP);
  }

  inline uint8 CC2420::enable_oscillator()
  {
    return this->command_strobe(CMD_SXOSCON);
  }

  inline uint8 CC2420::disable_oscillator()
  {
    return this->command_strobe(CMD_SXOSCOFF);
  }

  inline uint8 CC2420::flush_rx_fifo()
  {
    // issue the command twice (according to CC2420 datasheet)
    this->command_strobe(CMD_SFLUSHRX);
    return this->command_strobe(CMD_SFLUSHRX);
  }

  inline uint8 CC2420::flush_tx_fifo()
  {    
    this->command_strobe(CMD_SFLUSHTX);
    return this->command_strobe(CMD_SFLUSHTX);
  }
  
  inline uint8 CC2420::num_rx_fifo_bytes_read()
  {
    return this->rx_fifo_bytes_read;
  }

  inline uint8 CC2420::start_rx()
  {
    return this->command_strobe(CMD_SRXON);
  }

  inline uint8 CC2420::start_tx(bool on_cca)
  {
    if (on_cca)
    {
      return this->command_strobe(CMD_STXONCCA);
    }
    return this->command_strobe(CMD_STXON);
  }

  inline uint8 CC2420::tx_calibrate()
  {
    return this->command_strobe(CMD_STXCAL);
  }

  inline uint8 CC2420::stop_rx_tx()
  {
    return this->command_strobe(CMD_SRFOFF);
  }

  inline uint8 CC2420::get_rssi(uint16 &value)
  {
    return this->read_register(REG_RSSI, value);
  }

  inline uint8 CC2420::send_ack(bool frame_pending)
  {
    if (frame_pending)
    {
      return this->command_strobe(CMD_SACKPEND);
    }
    return this->command_strobe(CMD_SACK);
  }

  inline bool CC2420::get_cca()
  {
    return ((Port1()->IN & Port::RADIO_GIO1) == 0);
  }

  inline void CC2420::clear_cca_ifg()
  {
    Port1()->IFG &= ~Port::RADIO_GIO1;
  }

  inline void CC2420::set_cca_ifg()
  {
    Port1()->IFG |= Port::RADIO_GIO1;
  }

  inline bool CC2420::get_cca_ifg()
  {
    return ((Port1()->IFG & Port::RADIO_GIO1) != 0);
  }

  inline void CC2420::disable_rx_interrupt()
  {
    Port1()->IE &= ~Port::PKT_INT;
  }
    
  inline bool CC2420::get_rx_interrupt_enabled()
  {
    return ((Port1()->IE & Port::PKT_INT) != 0);
  }
  
  inline bool CC2420::get_rx_interrupt_flag()
  {
    return ((Port1()->IFG & Port::PKT_INT) != 0);
  }
  
  inline void CC2420::set_rx_interrupt_flag()
  {
    Port1()->IFG |= Port::PKT_INT;
  }
  
  inline void CC2420::clear_rx_interrupt_flag()
  {
	Port1()->IFG &= ~Port::PKT_INT;
  }
  
  inline bool CC2420::get_rx_interrupt_in()
  {
    return ((Port1()->IN & Port::PKT_INT) != 0);
  }

  inline bool CC2420::get_fifo_in()
  {
    return ((Port1()->IN & Port::RADIO_GIO0) != 0);
  }
  
  inline void CC2420::set_sfd_gpio()
  {
    Port4()->SEL &= ~Port::RADIO_SFD;
  }

  inline void CC2420::set_sfd_module_input()
  {
    Port4()->SEL |= Port::RADIO_SFD;
  }
} // namespace reflex

#endif // CC2420_h
