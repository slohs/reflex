#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		Sören Höckner
#*
 #*    This file is part of REFLEX.
 #*
 #*    REFLEX is free software: you can redistribute it and/or modify
 #*    it under the terms of the GNU Lesser General Public License as 
 #*    published by the Free Software Foundation, either version 3 of the 
 #*    License, or (at your option) any later version.
 #*
 #*    REFLEX is distributed in the hope that it will be useful,
 #*    but WITHOUT ANY WARRANTY; without even the implied warranty of
 #*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #*    GNU Lesser General Public License for more details.
 #*
 #*    You should have received a copy of the GNU General Public License
 #*    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 #* */
##FIXME not ready yet

EXECUTABLE ?= $(APPLICATION).elf
SREC ?= $(APPLICATION).srec
MAIN_TARGET ?= $(EXECUTABLE)

CONTROLLER = linux

HOST_SYSTEM = $(GENSYS)




ifdef UISP_PATH
DEFINED_REFLEX_VARS += "UISP_PATH "$(UISP_PATH) \r\n
endif
ifndef UISP_PATH
MISSING_REFLEX_VARS += "UISP_PATH path to the universal in system programmer" \r\n
endif

ifdef SERIAL_PORT
DEFINED_REFLEX_VARS += "SERIAL_PORT "$(SERIAL_PORT) \r\n
endif
ifndef SERIAL_PORT
MISSING_REFLEX_VARS += "SERIAL_PORT  PC port to which device is connected" \r\n
endif

#may you need your own linkerscript
#LDFLAGS += -T$(REFLEXPATH)/platform/$(PLATFORM)/skeleton.x

TARGETS += download

download : all $(SREC)
	$(SKELETON_DOWNLOADTOOL) --erase --upload if=$(SREC)
	#an distributet downloadtool

$(SREC): $(EXECUTABLE)
	$(OBJCOPY) -O srec $(EXECUTABLE) $(SREC)

###################### includes and base sources for the controller ########

INCLUDES += -I$(REFLEXPATH)/platform/$(PLATFORM)/include

include $(REFLEXPATH)/platform/$(PLATFORM)/Sources.mk
