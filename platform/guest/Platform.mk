#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		Karsten Walther, Sören Höcker
# */

CONTROLLER = linux

INCLUDES += -I$(REFLEXPATH)/platform/$(PLATFORM)/include

#CXXFLAGS += -fno-rtti -fno-exceptions
CXXFLAGS += -fno-exceptions 
#-std=c++0x

#######################################################################

include $(REFLEXPATH)/platform/$(PLATFORM)/Sources.mk

