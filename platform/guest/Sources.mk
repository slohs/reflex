#/*
# *	REFLEX - Real-time Event FLow EXecutive
# *
# *	A lightweight operating system for deeply embedded systems.
# *
#  
# *	Author:		 Carsten Schulze
# */

#Defined here, because SystemTimer cannot be used in OMNetPP which also
#relies on linux as Controller platform
CC_SOURCES_CONTROLLER += \
	timer/SystemTimer.cc

C_SOURCES_PLATFORM += \
	power.c \

# guest system uses virtualized timer facility (but not OMNetPP)
CC_SOURCES_LIB += \
	timer/VirtualTimer.cc \
	timer/VirtualizedTimer.cc \
