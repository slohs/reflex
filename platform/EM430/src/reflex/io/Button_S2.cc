/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *

 *
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 **/
#include "reflex/MachineDefinitions.h"
#include "reflex/io/Button_S2.h"
#include "NodeConfiguration.h"

using namespace reflex;
using namespace mcu;

Button_S2::Button_S2()
    : port1IV(mcu::interrupts::PORT1)
{
  this->setSleepMode(mcu::LPM4);
  this->enable();
}

void Button_S2::init(Sink0 *sink)
{
  (*port1IV)[Port1::Interrupt_traits::PIN7] = sink;
}

void Button_S2::enable()
{
  //select io function for pins
  Port1()->SEL &= ~0x80;
  //configure pins as input
  Port1()->DIR &= ~0x80;

  //enable internal pullup
  Port1()->OUT |= 0x80;
  Port1()->REN |= 0x80;

  //set interrupts on high to low transition
  Port1()->IES |= 0x80;
  //reset interrupt flags
  Port1()->IFG &= ~0x80;
  //enable the interupts for the connected pins
  Port1()->IE |= 0x80;
}

/*! disables interrupts for all buttons
*/
void Button_S2::disable()
{
  Port1()->IE &= ~0x80;
}

void Button_S2::handle()
{

}
