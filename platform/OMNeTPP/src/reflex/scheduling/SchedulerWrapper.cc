/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 
*
 *    This file is part of REFLEX.
 *
 *    REFLEX is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU Lesser General Public License as
 *    published by the Free Software Foundation, either version 3 of the
 *    License, or (at your option) any later version.
 *
 *    REFLEX is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU Lesser General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with REFLEX.  If not, see <http://www.gnu.org/licenses/>.
 * */

#include <omnetpp.h>
#include "reflex/scheduling/SchedulerWrapper.h"
#include "reflex/debug/OmnetDebug.h"
#include "reflex/System.h"
#include "reflex/interrupts/InterruptLock.h"
#include "reflex/interrupts/InterruptVector.h"

using namespace reflex;


SchedulerWrapper<FifoScheduler>::SchedulerWrapper()
	: InterruptHandler(mcu::interrupts::IV_SCHEDULER,PowerManageAble::SECONDARY)
	, FifoScheduler()
	, module(getModule())
{}

SchedulerWrapper<FifoScheduler>::~SchedulerWrapper() {
	//cancel and delete all send messages
	//foreach (it=begin(), it!=end(), module.cancelAndDelete )
}


void SchedulerWrapper<FifoScheduler>::handle() {
	//enable interrupts exec activity and disable them
	FifoActivity* act = queue.front();
	Assert(act);
	module.cancelAndDelete(getSystem().get_currentMsg());
	this->dispatch(); // execute activity
	//resend msg in case of reschedule
	if (act->triggered() && (!act->islocked()) ) {
		sendMsg();
	}
}

void SchedulerWrapper<FifoScheduler>::schedule(FifoActivity* act) {
	if ((!act->islocked()) && (!act->triggered())) {
		this->sendMsg();
	}
	FifoScheduler::schedule(act);
	//prevent
}

void SchedulerWrapper<FifoScheduler>::unlock(FifoActivity* act) {
	if(act->islocked()) {
		FifoScheduler::unlock(act);
		if (act->triggered()) {
			sendMsg();
		}
	}
}

void SchedulerWrapper<FifoScheduler>::enable() {
}

void SchedulerWrapper<FifoScheduler>::disable() {
}

/** send self message for event triggered scheduling
*/
void SchedulerWrapper<FifoScheduler>::sendMsg() {
	cMessage* msg = new cMessage("Scheduler_dispatch",mcu::interrupts::IV_SCHEDULER);
	module.scheduleAt( simTime()+SCHEDULE_DELAY/1000 ,msg);
}

