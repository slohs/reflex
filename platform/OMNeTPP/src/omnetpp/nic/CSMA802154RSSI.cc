/*
 * CSMA802154RSSI.cc
 *
 *  Created on: 01.08.2012
 *      Author: slohs
 */



#include "omnetpp/nic/CSMA802154RSSI.h"

#include <cassert>

#include "DeciderResult802154Narrow.h"
#include "Decider802154Narrow.h"
#include "PhyToMacControlInfo.h"
#include "MacToNetwControlInfo.h"
#include "MacPkt_m.h"


Define_Module(CSMA802154_RSSI);

CSMA802154_RSSI::CSMA802154_RSSI() {
	// TODO Auto-generated constructor stub

}

cPacket *CSMA802154_RSSI::decapsMsg(MacPkt * macPkt) {

	cPacket * msg = csma::decapsMsg(macPkt);

	// get bit error rate
	PhyToMacControlInfo* cinfo = static_cast<PhyToMacControlInfo*> (macPkt->getControlInfo());
	const DeciderResult802154Narrow* result = static_cast<const DeciderResult802154Narrow*> (cinfo->getDeciderResult());
	double ber = result->getBER();
	double rssi = result->getRSSI();

	//get control info attached by base class decapsMsg method
	//and set its rssi and ber
	assert(dynamic_cast<MacToNetwControlInfo*>(msg->getControlInfo()));
	MacToNetwControlInfo* cInfo = static_cast<MacToNetwControlInfo*>(msg->getControlInfo());
	cInfo->setBitErrorRate(ber);
	cInfo->setRSSI(rssi);

	current_signalStrength = rssi;

	return msg;
}

double CSMA802154_RSSI::getSignalStrengthInmW()
{
	return current_signalStrength;
}

double CSMA802154_RSSI::getSignalStrengthIndBm()
{
    return (10 * log10(current_signalStrength));
}
