#include "omnetpp/ReflexBaseApp.h"
#include "reflex/System.h"
#include "reflex/interrupts/InterruptVector.h"

reflex::System* reflex::System::current = NULL; /// pointer to the current system obj
cSimpleModule* reflex::System::currentModule = NULL;


Define_Module(ReflexBaseApp);

ReflexBaseApp::ReflexBaseApp()
{
	setEnv();
	reflex=reinterpret_cast<ReflexAppWrapper*>(reflexObjData);
//	reflex::System::current=reflex;
}

void ReflexBaseApp::initialize()
{
	setEnv();
	reflex=new(reflexObjData) ReflexAppWrapper(*this);
	reflex->init();
    // TODO - Generated method body
}

void ReflexBaseApp::handleMessage(cMessage *msg)
{
	setEnv();
    // TODO - Generated method body
	reflex->setMessage(msg);
	dispatch(msg);
	reflex->setMessage(NULL);
}

void ReflexBaseApp::finish(){
	setEnv();
	reflex->finish();
	reflex->~ReflexAppWrapper();
}

void ReflexBaseApp::setEnv(){
	reflex::System::current=reflex;
	reflex::System::currentModule=this;
}

//protected stuff


void ReflexBaseApp::dispatch(cMessage* msg) {
	reflex::mcu::interrupts::InterruptVector vectorNumber = reflex::mcu::interrupts::INVALID_INTERRUPT;

	if (msg->isSelfMessage())
	{
		vectorNumber = reflex::mcu::interrupts::InterruptVector(msg->getKind());
	}
	else
	{
		vectorNumber = reflex::mcu::interrupts::InterruptVector(msg->getArrivalGate()->getIndex());
	}

	/**
	 * the number of the interruptvector is the  vectornumber of the interrupt. check here if in bounds
	 */
	Assert((vectorNumber >= reflex::mcu::interrupts::INVALID_INTERRUPT) &&  (vectorNumber < reflex::mcu::interrupts::MAX_HANDLERS));
	//delegate to the interrupt guardian
	reflex->guardian.handle(vectorNumber);
}

