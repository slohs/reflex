#ifndef __REFLEX_REFLEXBASEAPP_H_
#define __REFLEX_REFLEXBASEAPP_H_

#include <omnetpp.h>

#include "reflex/System.h"
#include "NodeConfiguration.h"

class ReflexAppWrapper
	: public reflex::NodeConfiguration
{
    cMessage* 	currentMsg;
public:
	ReflexAppWrapper(cSimpleModule& module)
		: reflex::NodeConfiguration()
		, currentMsg(NULL)
	{
	}

	void setMessage(cMessage* msg) {currentMsg=msg;}

protected:
    virtual cMessage* get_currentMsg() { Assert(currentMsg); return currentMsg;}

};

/**
 * TODO - Generated class
 */
class ReflexBaseApp
	: public cSimpleModule
	  // , public INodeFailureInjector
{
	public:
//TODO: we have to verify, whether ReflexAppWrapper has to match with some alignment
		ReflexAppWrapper* reflex;
		uint8 reflexObjData[sizeof(ReflexAppWrapper)];
	public:
	ReflexBaseApp();
    virtual void initialize();
    virtual void finish();
    virtual void handleMessage(cMessage *msg);

  protected:
    void dispatch(cMessage*);
    void setEnv();
  protected:
    int lastRadioState;
};

#endif
