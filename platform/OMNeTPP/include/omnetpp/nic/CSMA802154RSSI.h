/*
 * CSMA802154RSSI.h
 *
 *  Created on: 01.08.2012
 *      Author: slohs
 */

#ifndef CSMA802154RSSI_H_
#define CSMA802154RSSI_H_

#include <omnetpp.h>
#include "CSMA802154.h"

class CSMA802154_RSSI : public CSMA802154  {
private:
	double current_signalStrength;


public:
	CSMA802154_RSSI();
    virtual cPacket *decapsMsg(MacPkt * macPkt);

    double getSignalStrengthInmW();
    double getSignalStrengthIndBm();

};

#endif /* CSMA802154RSSI_H_ */
