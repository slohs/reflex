/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#ifndef SCHEDULERWRAPPER_H
#define SCHEDULERWRAPPER_H

#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/scheduling/Activity.h"
#include <omnetpp.h>

namespace reflex {
template<typename TScheme>
class SchedulerWrapper;
}

#include "reflex/scheduling/FifoScheduler.h"
#include "reflex/scheduling/FifoActivity.h"

namespace reflex {

/** Adapter to bring for(;;) implemented Scheduler into an eventdriven environment
  this wrapper send for every scheduled Activity one selfmessage to omnetpp
*/
template<>
class SchedulerWrapper<FifoScheduler>
	: public InterruptHandler
	, public FifoScheduler //the scheduling implementation
{
	enum {
		SCHEDULE_DELAY = 10 //delay for the selfmessage aka the wcet of the scheduler
	};
public:
    SchedulerWrapper();
	virtual ~SchedulerWrapper();

protected:

	virtual void handle();
	virtual void enable();
	virtual void disable();

private:
	void start();

public:
	void schedule(FifoActivity*);
	void unlock(FifoActivity*);

protected:
	void sendMsg();
public:

protected:
	cSimpleModule& module;
private:

};
}

#include "reflex/debug/StaticAssert.h"
#include "reflex/scheduling/Scheduler.h"

namespace reflex {
/** the other scheduler are not implmented yet**/
template<typename TScheduler>
class SchedulerWrapper {
	// STATIC_ASSERT(false,_NOT_YET_IMPLEMENTED);
};
}








#endif // SCHEDULERWRAPPER_H
