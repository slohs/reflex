/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *	Class(ses):	OmnetDebug
 *
 *	Author:		Soeren Hoeckner
 *
 *	Desciption:	provides output stream to both  (i.d. std::cout and ev - environment output )
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#ifndef OMNETDEBUG_H
#define OMNETDEBUG_H

#include "reflex/System.h"
#include "reflex/data_types/Static.h"

namespace reflex
{


/** Singleton for debug output on omnet platform.
 *	use it like cout
 */
class OmnetDebug
{
public:

	enum Enum{
		Prefix
	};

	enum {
#ifdef DEBUG_PLATFORM
		DEBUGGING=!false
#else
		DEBUGGING=false
#endif
	};
public:

	OmnetDebug(){}

	explicit OmnetDebug(const Enum&) { prefix(); }

	~OmnetDebug()
	{}

	/** present cout like debug usage.
	 *
	 */
	template<typename T>
	OmnetDebug& operator<< (T value)
	{
		if (DEBUGGING) {
			EV<< value;
//			std::cout << value;
		}
		return *this;
	}

	template<typename T>
	OmnetDebug& operator< (T value)
	{
		if (DEBUGGING) {
			this->prefix();
			EV<< value;
		}
		return *this;
	}

	/**	prints Moduleprefix. e.g. "SensorNet.hosts[0].app:"
	 *
	 */
	OmnetDebug& prefix()
	{
		if (DEBUGGING) {
			EV <<getModule().getFullPath().c_str()<<": " ;
		}
		return *this;
	}
};




}//namespace reflex
#endif

