#include "reflex/interrupts/InterruptHandler.h"
#include "reflex/interrupts/InterruptVector.h"
#include "reflex/sinks/Sink.h"

#include "ReflexPacket_m.h"

#include "omnetpp/nic/CSMA802154RSSI.h"

namespace reflex {

template<mcu::interrupts::InterruptVector TGateID>
class InGateWrapper
	: protected InterruptHandler
{
protected:
	Sink1<Buffer*>* output;
	cSimpleModule& module;
private:
	Pool* pool;
	CSMA802154_RSSI* mac;

public:

	/**
	 * constructor
	 */
	InGateWrapper();

	/**
	 *
	 */
	void init(Sink1<Buffer*>* output, Pool* pool)
	{
		this->output=output;
		this->pool = pool;
	}

    double getSignalStrengthInmW();
    double getSignalStrengthIndBm();

protected:
	virtual void handle();
	virtual void enable(){}
	virtual void disable(){}
};

template<mcu::interrupts::InterruptVector TGateID>
InGateWrapper<TGateID>::InGateWrapper()
	: InterruptHandler(TGateID ,PowerManageAble::SECONDARY)
	, output(NULL)
	, module(getModule())
	, mac(NULL)
{
	mac = check_and_cast<CSMA802154_RSSI*>(module.getParentModule()->getParentModule()->getSubmodule("nic")->getSubmodule("mac"));
}

template<mcu::interrupts::InterruptVector TGateID>
double InGateWrapper<TGateID>::getSignalStrengthInmW()
{
	if (mac)
	{
		return mac->getSignalStrengthInmW();
	}
	else
	{
		return 0;
	}
}

template<mcu::interrupts::InterruptVector TGateID>
double InGateWrapper<TGateID>::getSignalStrengthIndBm()
{
    if (mac)
    {
        return mac->getSignalStrengthIndBm();
    }
    else
    {
        return 0;
    }
}

template<mcu::interrupts::InterruptVector TGateID>
void InGateWrapper<TGateID>::handle()
{
    ReflexPacket* reflexPkg = check_and_cast<ReflexPacket*>(getSystem().get_currentMsg() ); //we assume some cPkg here

    if (output) {
        Buffer* recBuffer = new (pool) Buffer(pool);

        if (!recBuffer) {
        	module.cancelAndDelete(reflexPkg);
        	return;
        }

        recBuffer->initOffsets( reflexPkg->getPayLoad(0) );

        for(unsigned i=1;i<reflexPkg->getPayLoadArraySize();++i) {
            recBuffer->write( reflexPkg->getPayLoad(i) );
        }

        std::cerr << "SignalStrength: " << getSignalStrengthIndBm() << "dBm and " << getSignalStrengthInmW() << "mW" << std::endl;

        output->assign( recBuffer );
    }

    module.cancelAndDelete(reflexPkg);
}

}
