/*
 *	REFLEX - Real-time Event FLow EXecutive
 *
 *	A lightweight operating system for deeply embedded systems.
 *
 *
 *
 *
 *
 *    This file is part of REFLEX.
 *
 *    Copyright 2010 BTU Cottbus, Department for Distributed Systems and 
 *    Operating Systems. All rights reserved.
 *    
 *    Redistribution and use in source and binary forms, with or without 
 *    modification, are permitted provided that the following conditions
 *    are met:
 *    
 *       1. Redistributions of source code must retain the above copyright 
 *          notice, this list of conditions and the following disclaimer.
 * 
 *       2. Redistributions in binary form must reproduce the above copyright
 *          notice, this list of conditions and the following disclaimer in
 *          the documentation and/or other materials provided with the 
 *          distribution.
 * 
 *    THIS SOFTWARE IS PROVIDED BY BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED 
 *    SYSTEMS AND OPERATING SYSTEMS ``AS IS'' AND ANY EXPRESS OR IMPLIED 
 *    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF 
 *    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN
 *    NO EVENT SHALL BTU COTTBUS, DEPARTMENT FOR DISTRIBUTED SYSTEMS AND 
 *    OPERATING SYSTEMS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *    DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *    THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 *    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *    The views and conclusions contained in the software and documentation 
 *    are those of the authors and should not be interpreted as representing 
 *    official policies, either expressed or implied, of BTU Cottbus, 
 *    Department for Distributed Systems and Operating Systems.
 *
 * */

#ifndef OMNETPP_HardwareTimerMilli_h
#define OMNETPP_HardwareTimerMilli_h

#include "reflex/timer/HardwareTimer.h"
#include "reflex/timer/SystemTimer.h"
#include "reflex/sinks/Sink.h"
#include "reflex/interrupts/InterruptHandler.h"

namespace reflex {

/**
 * This class represets the hardware layer of the VirtualizedTimer.
 * The calculation value is milliseconds. Note its not an high precision timer
 *
 */
 //FIXME: the whole vtimer implementation, it is wrong to bind it directly to the
 //Moduluscounter there is no konzept
class HardwareTimerMilli
		: public PowerManageAble
		, public HardwareTimer
		, public Sink0
{
private:


	SystemTimer hwTimer; /// the hardware timer implementation
	Time delta; ///< time until notify
	Time zeroTime; ///< time when the timer was set, zeroTime

public:

	/**	registers itself to the hardware timer.
	 */
	HardwareTimerMilli();

	/**	notified if hwtimer fires.
	 * also overflow interrupts are sended here
	 */
	virtual void notify();

	/** calculates the current local time in ms
	 * @return current counter value (local time)
	 */
	Time getNow();

    /** getTimestamp
     * @return 64 bit counter value in hardware precision
     */
    uint64 getTimestamp();

	/** sets a timeout until next interrupt and start the timer
	 * @param ms number of milliseconds until the next timer event should fire
	 */
	void start(Time ms);

	/**
	 * set a new time interrupt to occur ticks after t0
	 * @param t0 time from where to set the timer
	 * @param ticks number of ticks after t0 when the timer should fire
	 */
	void startAt(Time t0, Time ticks);

	/**
	 * stop the hardware timer. The local time counter will still work,
	 * but the alarm counter will be turned off.
	 */
	void stop();

	virtual void enable();
	virtual void disable();

private:

	/** setAlarm
	 * set the alarm counter
	 */
	void setHWTimer();
};

} // reflex

#endif // HARDWARETIMERMILLI_H
